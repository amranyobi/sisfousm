<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ELEKTRO HUB</title>
        <link rel="icon" href="<?php echo $base; ?>img/polines.png">
    <link rel="stylesheet" href="<?php echo $base; ?>themes/css/home.css">
</head>
<body>
    <div id="page">
        <header>
            <a class="logo" title="Electronic Archive System" href="./">EHUB</a>
            <div class="hero">
                <h1>Single Sign On</h1>
                <a class="btn" href="<?php echo $base; ?>login">SIGN IN</a> 
            </div>
        </header>
        <footer>
            <a title="Personal Website" href="#">&copy;2020 Teknik Elektro Polines</a>
            <div class="content">
                <a title="Privacy Policy" href="#">Privacy Policy</a>
                <a title="Term of Service" href="#">Term of Service</a>
            </div>
        </footer>
        <nav>
            <ul>
                <li><a title="Tentang" href="<?php echo $base; ?>about">About</a></li>
                <li><a title="Login" href="<?php echo $base; ?>login">Sign In</a></li>
            </ul>
        </nav>
        <section class="main">
            <aside>
                <div class="content">
                    <h3>
                        <br/><span class="fas fa-calendar"></span><br/>
                        <a href="../opendata/schedule">MY SCHEDULE</a>
                    </h3>
                    <p>Find my class schedule </p>
                </div>
            </aside>
            <aside>
                <div class="content">
                    <h3>
                        <br/><span class="fas fa-calendar"></span><br/>
                        <a href="../opendata/final">FINAL SCHEDULE</a>
                    </h3>
                    <p>Final exam schedule</p>
                </div>
            </aside>
            <aside>
                <div class="content">
                    <h3>
                        <br/><span class="fas fa-calendar"></span><br/>
                        <a href="../opendata/calendar">CALENDAR</a>
                    </h3>
                    <p>Academic Calendar</p>
                </div>
            </aside>
        </section>
    </div>
</body>
</html>

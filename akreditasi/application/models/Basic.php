<?php

class Basic extends CI_Model {

    public function get_status_dosen() {
        $QRY = "SELECT COUNT(dosen_nip) AS jumlah FROM dosen";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_kelas() {
        $QRY = "SELECT COUNT(kelas_id) AS jumlah FROM kelas";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_gedung() {
        $QRY = "SELECT COUNT(gedung_id) AS jumlah FROM gedung";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_ruangan($gedung = "") {
        if (($gedung == "")||($gedung == null)) {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan";
        } else {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan WHERE gedung_id='" . $gedung . "';";
        }
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_aktif() {
        $QRY = "SELECT COUNT(aktif_id) AS jumlah FROM kelas_aktif";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    private function loadview($title = ' | Beranda', $path = '<a href="" title="Beranda"><span class="fa fa-home"></span></a>', $type = 'dashboard', $heading1 = 'Dashboard', $heading2 = 'Dashboard', $content = '', $extra1 = '0', $extra2 = '0', $extra3 = '0', $extra4 = '0') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'extra1' => $extra1,
            'extra2' => $extra2,
            'extra3' => $extra3,
            'extra4' => $extra4
        );
        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);
        $this->load->view('adminlte-content', $data);
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->initialize();
        $title = ' | Beranda';
        $path = $this->template->pathmaker('dashboard');
        $type = 'dashboard';
        $heading1 = 'Dashboard';
        $heading2 = 'Status Penjadwalan';
        $content = '';

        $this->load->model('basic');
        $ex1 = $this->basic->get_status_dosen();
        $ex2 = $this->basic->get_status_kelas();
        $ex3 = $this->basic->get_status_ruangan();
        $ex4 = $this->basic->get_status_aktif();

        $this->loadview($title, $path, $type, $heading1, $heading2, $content, $ex1, $ex2, $ex3, $ex4);
    }
}

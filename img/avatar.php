<?php

/**
 * SMS application bootstrap files
 *
 * Copyright (C) 2016  Muhammad Irwan Yanwari (irwan.yanwari@gmail.com)
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

  /*
 * Created
 * by   : Muhammad Irwan Yanwari
 * year : 2019
 */
$file = "nan";
if (isset($_GET['c'])) {
    $file = $_GET['c'];
    $file = str_replace('"', "", $file);
    $file = str_replace("'", "", $file);
    $file = str_replace('/', "", $file);
}

$img = "./avatar/" . $file . ".png";
header('Content-Type: image/jpeg');
if (!file_exists($img)) {
    session_start();
    if (isset($_SESSION['gender'])) {
        $img = './default/' . $_SESSION['gender'] . '.png';
        if (!file_exists($img)) {
            $img = './default/L.png';
        }
    } else {
        $img = './default/L.png';
    }
}
readfile($img);

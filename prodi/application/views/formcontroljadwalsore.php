<style>
    .tdheading {
        background-color: blue;
        text-align: center;
        vertical-align: middle;
        color: white;
        font-weight: 700;
        border: gray solid 1px;
    }
    .tddata {
        border: gray solid 1px;
        text-align: center;
        vertical-align: middle;
    }
    .tdleftdata {
        border: gray solid 1px;
    }

    .ka-list-item {
        border: gray solid 1px;
        padding: 5px;
        margin-bottom: 5px;
        width: 200px;
    }
    .jenis-ka {
        float: right;
        border: gray solid 1px;
        text-align: center;
        vertical-align: middle;

        font-weight: 700;
        font-size: .7em;

        padding-top: 5px;
        padding-bottom: 0;
        padding-left: 0;
        padding-right: 0;
        width: 30px;
        height: 30px;

        -khtml-border-radius: 20px;
        -moz-border-radius: 20px;
        border-radius: 20px;
    }
    .info-ka {
        width: 150px;
    }
    .nama-mk {
        font-size: .7em;
    }
    .dosen-mk {
        font-size: .7em;
    }
    .kls-mk {
        font-size: .7em;
    }
</style>
<script>

    function start_generate() {

        document.getElementById("tombol-start").style = "display:none;";

        document.getElementById("plog").innerHTML = "PROCESS LOG :";
        document.getElementById("jumlah-ka").innerHTML = "";
        document.getElementById("ka-data").innerHTML = "";

        var target_url = "<?php echo base_url("jadwal_sore"); ?>/restart";
        $.ajax({
            type: 'POST',
            url: target_url,
            success: function (resultData) {
                var txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>" + resultData + "<br/>Pembacaan Kelas Aktif ...";
                load_a();
            },
            error: function (error, error_, text_message) {

            }
        });
    }

    function load_a() {
        var target_url = "<?php echo base_url("jadwal_sore"); ?>/loader_a";
        $.ajax({
            type: 'POST',
            url: target_url,
            success: function (resultData) {
                if (resultData == "1") {
                    load_a();
                } else {
                    var txt = document.getElementById("plog").innerHTML;
                    document.getElementById("plog").innerHTML = txt + "<br/>Akumulasi Kelas Selesai ...";
                    load_b();
                }
            },
            error: function (error, error_, text_message) {

            }
        });
    }

    function load_b() {
        var target_url = "<?php echo base_url("jadwal_sore"); ?>/loader_b";
        $.ajax({
            type: 'POST',
            url: target_url,
            success: function (resultData) {
                var result = jQuery.parseJSON(resultData);
                var jml_x = parseInt(result.jumlah);
                document.getElementById("jumlah-ka").innerHTML = jml_x;
                for (var i = 0; i < jml_x; i++) {
                    var kode = result.data[i].kode;
                    var mk = result.data[i].matakuliah;
                    var dosen = result.data[i].dosen;
                    var kls = result.data[i].kelas;
                    var jenis = result.data[i].jenis;
                    var jam = result.data[i].jam;
                    var j = i + 1;
                    var text = "<div class='ka-list-item' id='ka-l-" + i + "'>";
                    text = text + "<div id='ka-l-" + i + "-ko' style='display: none;'>" + kode + "</div>";
                    text = text + "<div id='ka-l-" + i + "-jm' style='display: none;'>" + jam + "</div>";
                    text = text + "<div id='ka-l-" + i + "-al' style='display: none;'>0</div>";
                    text = text + "<div id='ka-l-" + i + "-jn' style='display: none;'>" + jenis + "</div>";
                    text = text + "<div id='ka-l-" + i + "-no' class='jenis-ka'>" + j + "</div>";
                    text = text + "<div class='info-ka'>";
                    text = text + "<span id='ka-l-" + i + "-mk' class='nama-mk'>" + jenis + ". " + mk + "</span><br/>";
                    text = text + "<span id='ka-l-" + i + "-dn' class='dosen-mk'>" + dosen + "</span><br/>";
                    text = text + "<span id='ka-l-" + i + "-kl' class='kls-mk'>" + kls + "</span>";
                    text = text + "</div>";
                    text = text + "</div>";
                    var older = document.getElementById("ka-data").innerHTML;
                    document.getElementById("ka-data").innerHTML = older + text;
                }
                var txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Dump Daftar Kelas Pada Algoritma ...";
                load_c();
            },
            error: function (error, error_, text_message) {

            }
        });
    }

    function load_c() {
        var stop = false;
        var txt = document.getElementById("plog").innerHTML;
        document.getElementById("plog").innerHTML = txt + "<br/>Memulai Penyusunan Jadwal ...";

        var jml_ka = parseInt(document.getElementById("jumlah-ka").innerHTML);
        var jml_ru = parseInt(document.getElementById("jumlah-ruangan").innerHTML);
        var jml_jm = parseInt(document.getElementById("jumlah-jam").innerHTML);
        var jml_hr = parseInt(document.getElementById("jumlah-hari").innerHTML);

        while (!stop) {
            var upx = 0;
            // SENIN
            var k_hari = 0;
            for (var a = 0; a < jml_ru; a++) {
                var up = 0;
                var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
                var rpos = parseInt(document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML);
                var rsis = jml_jm - rpos;
                document.getElementById("jumlah-hari").style = "background-color: lime;";

                txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Cek : Senin - " + document.getElementById("rd-" + a + "-1").innerHTML;

                for (var b = 0; b < jml_ka; b++) {
                    var b_al = parseInt(document.getElementById("ka-l-" + b + "-al").innerHTML);
                    if (b_al == 0) {
                        var b_ko = parseInt(document.getElementById("ka-l-" + b + "-ko").innerHTML);
                        var b_jm = parseInt(document.getElementById("ka-l-" + b + "-jm").innerHTML);
                        var b_jn = (document.getElementById("ka-l-" + b + "-jn").innerHTML);
                        var b_no = (document.getElementById("ka-l-" + b + "-no").innerHTML);
                        var b_dn = (document.getElementById("ka-l-" + b + "-dn").innerHTML);
                        var b_kl = (document.getElementById("ka-l-" + b + "-kl").innerHTML);

                        if (b_jn == "T") {
                            if (b_jm < rsis) {
                                var xx = parseInt(rpos) + parseInt(b_jm);
                                if (check_bentrok(b_dn, b_kl, k_hari, rpos, xx)) {
                                    for (var c = 0; c < b_jm; c++) {
                                        var c_pos = rpos + c;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-0").innerHTML = b_ko;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-1").innerHTML = b_dn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-2").innerHTML = b_kl;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-3").innerHTML = b_jn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML = b_no;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").style = "background-color: lime;";

                                        c_pos++;
                                        document.getElementById("ka-l-" + b + "-al").innerHTML = 1;
                                        up = 1;
                                        upx = 1;
                                    }
                                    upload_a(k_hari, rpos, b_ko, rkod);

                                    var aloc = parseInt(document.getElementById("teralokasi-ka").innerHTML)
                                    aloc = aloc + 1;
                                    document.getElementById("teralokasi-ka").innerHTML = aloc;

                                    rpos = rpos + b_jm;
                                    document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML = rpos;

                                    document.getElementById("ka-l-" + b + "-no").style = "background-color: lime;";
                                    break;
                                }
                            }
                        }
                    }
                }

                if (up == 0) {
                    document.getElementById("jumlah-hari").style = "";
                }
            }


            // SELASA
            var k_hari = 1;
            for (var a = 0; a < jml_ru; a++) {
                var up = 0;
                var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
                var rpos = parseInt(document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML);
                var rsis = jml_jm - rpos;
                document.getElementById("jumlah-hari").style = "background-color: lime;";

                txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Cek : Selasa - " + document.getElementById("rd-" + a + "-1").innerHTML;

                for (var b = 0; b < jml_ka; b++) {
                    var b_al = parseInt(document.getElementById("ka-l-" + b + "-al").innerHTML);
                    if (b_al == 0) {
                        var b_ko = parseInt(document.getElementById("ka-l-" + b + "-ko").innerHTML);
                        var b_jm = parseInt(document.getElementById("ka-l-" + b + "-jm").innerHTML);
                        var b_jn = (document.getElementById("ka-l-" + b + "-jn").innerHTML);
                        var b_no = (document.getElementById("ka-l-" + b + "-no").innerHTML);
                        var b_dn = (document.getElementById("ka-l-" + b + "-dn").innerHTML);
                        var b_kl = (document.getElementById("ka-l-" + b + "-kl").innerHTML);

                        if (b_jn == "T") {
                            if (b_jm < rsis) {
                                var xx = parseInt(rpos) + parseInt(b_jm);
                                if (check_bentrok(b_dn, b_kl, k_hari, rpos, xx)) {
                                    for (var c = 0; c < b_jm; c++) {
                                        var c_pos = rpos + c;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-0").innerHTML = b_ko;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-1").innerHTML = b_dn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-2").innerHTML = b_kl;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-3").innerHTML = b_jn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML = b_no;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").style = "background-color: lime;";

                                        c_pos++;
                                        document.getElementById("ka-l-" + b + "-al").innerHTML = 1;
                                        up = 1;
                                        upx = 1;
                                    }
                                    upload_a(k_hari, rpos, b_ko, rkod);

                                    var aloc = parseInt(document.getElementById("teralokasi-ka").innerHTML)
                                    aloc = aloc + 1;
                                    document.getElementById("teralokasi-ka").innerHTML = aloc;

                                    rpos = rpos + b_jm;
                                    document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML = rpos;

                                    document.getElementById("ka-l-" + b + "-no").style = "background-color: lime;";
                                    break;
                                }
                            }
                        }
                    }
                }

                if (up == 0) {
                    document.getElementById("jumlah-hari").style = "";
                }
            }


            // RABU
            var k_hari = 2;
            for (var a = 0; a < jml_ru; a++) {
                var up = 0;
                var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
                var rpos = parseInt(document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML);
                var rsis = jml_jm - rpos;
                document.getElementById("jumlah-hari").style = "background-color: lime;";

                txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Cek : Rabu - " + document.getElementById("rd-" + a + "-1").innerHTML;

                for (var b = 0; b < jml_ka; b++) {
                    var b_al = parseInt(document.getElementById("ka-l-" + b + "-al").innerHTML);
                    if (b_al == 0) {
                        var b_ko = parseInt(document.getElementById("ka-l-" + b + "-ko").innerHTML);
                        var b_jm = parseInt(document.getElementById("ka-l-" + b + "-jm").innerHTML);
                        var b_jn = (document.getElementById("ka-l-" + b + "-jn").innerHTML);
                        var b_no = (document.getElementById("ka-l-" + b + "-no").innerHTML);
                        var b_dn = (document.getElementById("ka-l-" + b + "-dn").innerHTML);
                        var b_kl = (document.getElementById("ka-l-" + b + "-kl").innerHTML);

                        if (b_jm < rsis) {
                                var xx = parseInt(rpos) + parseInt(b_jm);
                                if (check_bentrok(b_dn, b_kl, k_hari, rpos, xx)) {
                                for (var c = 0; c < b_jm; c++) {
                                    var c_pos = rpos + c;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-0").innerHTML = b_ko;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-1").innerHTML = b_dn;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-2").innerHTML = b_kl;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-3").innerHTML = b_jn;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML = b_no;
                                    document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").style = "background-color: lime;";

                                    c_pos++;
                                    document.getElementById("ka-l-" + b + "-al").innerHTML = 1;
                                    up = 1;
                                    upx = 1;
                                }
                                upload_a(k_hari, rpos, b_ko, rkod);

                                var aloc = parseInt(document.getElementById("teralokasi-ka").innerHTML)
                                aloc = aloc + 1;
                                document.getElementById("teralokasi-ka").innerHTML = aloc;

                                rpos = rpos + b_jm;
                                document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML = rpos;

                                document.getElementById("ka-l-" + b + "-no").style = "background-color: lime;";
                                break;
                            }
                        }
                    }
                }

                if (up == 0) {
                    document.getElementById("jumlah-hari").style = "";
                }
            }


            // KAMIS
            var k_hari = 3;
            for (var a = 0; a < jml_ru; a++) {
                var up = 0;
                var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
                var rpos = parseInt(document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML);
                var rsis = jml_jm - rpos;
                document.getElementById("jumlah-hari").style = "background-color: lime;";

                txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Cek : Kamis - " + document.getElementById("rd-" + a + "-1").innerHTML;

                for (var b = 0; b < jml_ka; b++) {
                    var b_al = parseInt(document.getElementById("ka-l-" + b + "-al").innerHTML);
                    if (b_al == 0) {
                        var b_ko = parseInt(document.getElementById("ka-l-" + b + "-ko").innerHTML);
                        var b_jm = parseInt(document.getElementById("ka-l-" + b + "-jm").innerHTML);
                        var b_jn = (document.getElementById("ka-l-" + b + "-jn").innerHTML);
                        var b_no = (document.getElementById("ka-l-" + b + "-no").innerHTML);
                        var b_dn = (document.getElementById("ka-l-" + b + "-dn").innerHTML);
                        var b_kl = (document.getElementById("ka-l-" + b + "-kl").innerHTML);

                        if (b_jn == "P") {
                            if (b_jm < rsis) {
                                var xx = parseInt(rpos) + parseInt(b_jm);
                                if (check_bentrok(b_dn, b_kl, k_hari, rpos, xx)) {
                                    for (var c = 0; c < b_jm; c++) {
                                        var c_pos = rpos + c;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-0").innerHTML = b_ko;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-1").innerHTML = b_dn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-2").innerHTML = b_kl;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-3").innerHTML = b_jn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML = b_no;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").style = "background-color: lime;";

                                        c_pos++;
                                        document.getElementById("ka-l-" + b + "-al").innerHTML = 1;
                                        up = 1;
                                        upx = 1;
                                    }
                                    upload_a(k_hari, rpos, b_ko, rkod);

                                    var aloc = parseInt(document.getElementById("teralokasi-ka").innerHTML)
                                    aloc = aloc + 1;
                                    document.getElementById("teralokasi-ka").innerHTML = aloc;

                                    rpos = rpos + b_jm;
                                    document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML = rpos;

                                    document.getElementById("ka-l-" + b + "-no").style = "background-color: lime;";
                                    break;
                                }
                            }
                        }
                    }
                }

                if (up == 0) {
                    document.getElementById("jumlah-hari").style = "";
                }
            }


            // JUMAT
            var k_hari = 4;
            for (var a = 0; a < jml_ru; a++) {
                var up = 0;
                var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
                var rpos = parseInt(document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML);
                var rsis = jml_jm - rpos;
                document.getElementById("jumlah-hari").style = "background-color: lime;";

                txt = document.getElementById("plog").innerHTML;
                document.getElementById("plog").innerHTML = txt + "<br/>Cek : Jumat - " + document.getElementById("rd-" + a + "-1").innerHTML;

                for (var b = 0; b < jml_ka; b++) {
                    var b_al = parseInt(document.getElementById("ka-l-" + b + "-al").innerHTML);
                    if (b_al == 0) {
                        var b_ko = parseInt(document.getElementById("ka-l-" + b + "-ko").innerHTML);
                        var b_jm = parseInt(document.getElementById("ka-l-" + b + "-jm").innerHTML);
                        var b_jn = (document.getElementById("ka-l-" + b + "-jn").innerHTML);
                        var b_no = (document.getElementById("ka-l-" + b + "-no").innerHTML);
                        var b_dn = (document.getElementById("ka-l-" + b + "-dn").innerHTML);
                        var b_kl = (document.getElementById("ka-l-" + b + "-kl").innerHTML);

                        if (b_jn == "P") {
                            if (b_jm < rsis) {
                                var xx = parseInt(rpos) + parseInt(b_jm);
                                if (check_bentrok(b_dn, b_kl, k_hari, rpos, xx)) {
                                    for (var c = 0; c < b_jm; c++) {
                                        var c_pos = rpos + c;

                                        var isi = document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML;
                                        if (isi == "-") {
                                            b_jm++;
                                            break;
                                        }

                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-0").innerHTML = b_ko;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-1").innerHTML = b_dn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-2").innerHTML = b_kl;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-3").innerHTML = b_jn;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").innerHTML = b_no;
                                        document.getElementById("dk-" + k_hari + "-" + rkod + "-" + c_pos + "-4").style = "background-color: lime;";

                                        c_pos++;
                                        document.getElementById("ka-l-" + b + "-al").innerHTML = 1;
                                        up = 1;
                                        upx = 1;
                                    }
                                    upload_a(k_hari, rpos, b_ko, rkod);

                                    var aloc = parseInt(document.getElementById("teralokasi-ka").innerHTML)
                                    aloc = aloc + 1;
                                    document.getElementById("teralokasi-ka").innerHTML = aloc;

                                    rpos = rpos + b_jm;
                                    document.getElementById("dkd-" + k_hari + "-" + rkod).innerHTML = rpos;

                                    document.getElementById("ka-l-" + b + "-no").style = "background-color: lime;";
                                    break;
                                }
                            }
                        }
                    }
                }

                if (up == 0) {
                    document.getElementById("jumlah-hari").style = "";
                }
            }


            var jml_ta = parseInt(document.getElementById("teralokasi-ka").innerHTML);
            if (jml_ka == jml_ta) {
                stop = true;
            }
            document.getElementById("updated-jadwal").innerHTML = upx;
            var updt = parseInt(document.getElementById("updated-jadwal").innerHTML);
            if (updt == 0) {
                stop = true;
            }
        }

        txt = document.getElementById("plog").innerHTML;
        document.getElementById("plog").innerHTML = txt + "<br/>Penyusunan Jadwal Selesai ...";
    }

    function check_bentrok(dosen, siswa, hari, mulai, sampai) {
        console.log(hari + "-" + mulai + "-" + sampai);
        var hasil = true;
        var jml_ru = parseInt(document.getElementById("jumlah-ruangan").innerHTML);
        for (var a = 0; a < jml_ru; a++) {
            var rkod = parseInt(document.getElementById("rd-" + a + "-0").innerHTML);
            for (var c = mulai; c < sampai; c++) {
                var b_dn = document.getElementById("dk-" + hari + "-" + rkod + "-" + c + "-1").innerHTML;
                var b_kl = document.getElementById("dk-" + hari + "-" + rkod + "-" + c + "-2").innerHTML;
                if(b_dn == dosen){
                    hasil = false;
                }
                if(b_kl == siswa){
                    hasil = false;
                }
            }
        }
        return hasil;
    }

    function upload_a(hari, jam, kode, ruangan) {
        var target_url = "<?php echo base_url("jadwal_sore"); ?>/uploader_a/" + kode + "/" + hari + "/" + jam + "/" + ruangan;
        $.ajax({
            type: 'POST',
            url: target_url,
            success: function (resultData) {

            },
            error: function (error, error_, text_message) {

            }
        });
    }
</script>
<div style="display: none;">
    <div id="jumlah-ka">0</div>
    <div id="teralokasi-ka">0</div>
    <div id="updated-jadwal">0</div>
    <div id="jumlah-ruangan"><?php echo $ruangan; ?></div>
    <div id="jumlah-jam"><?php echo $jam; ?></div>
    <div id="jumlah-hari"><?php echo $hari; ?></div>
    <div id="peta-ruangan"><?php echo $peta_ruang; ?></div>
</div>
<div class="input-block-closing">
    <input id="tombol-start" style="font-weight: 700;" onclick="start_generate();" class="form-control btn btn-success" type="submit" value="START GENERATE" placeholder="" />
</div>
<div id="plog" style="height: 100px; overflow-y: scroll;">
    PROCESS LOG :
</div>
<?php
echo $content;

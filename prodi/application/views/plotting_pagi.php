
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Plotting Perkuliahan Pagi</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div align="right" style="margin-right: 10px;">
                        <a style="color:white" href="<?php echo base_url()?>aktif/add/" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Plotting</a>
                        <a style="color:white" href="<?php echo base_url()?>aktif/upload_plotting/" class="btn btn-sm btn-primary"><i class="fa fa-upload"></i> Upload Plotting</a>
                    </div>
                    <br><br>
                    <table id="example1" class="table table-bordered table-striped display nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Kuliah</th>
                                <th>Kelas</th>
                                <th>Pengampu</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            // print_r($data_sp_ls);
                            foreach ($items as $dm => $item) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $item->matakuliah_nama; ?> (<?php echo $item->matakuliah_kode; ?>)</td>
                                    <td><?php echo $item->kelas_nama; ?> (<?php echo $item->kelas_angkatan; ?>)</td>
                                    <td><?php
                                    if(isset($item->dosen3)){
                                        $ci =& get_instance();
                                        $QRY = "SELECT dosen_nama, dosen_nip 
                                        FROM dosen
                                        WHERE dosen_nip = '$item->dosen3'";
                                        $query = $ci->db->query($QRY);
                                        $data = $query->row_array();
                                        ?>
                                         <ol style="margin-left:-25px">
                                          <li><?php echo $item->nama_teori." (".$item->nip_teori.")";?></li>
                                          <li><?php echo $item->nama_praktek." (".$item->nip_praktek.")";?></li>
                                          <li><?php echo $data['dosen_nama']." (".$data['dosen_nip'].")";?></li>
                                        </ol> 
                                        <?php
                                    }else{
                                        if($item->nip_praktek!=$item->nip_teori)
                                        {
                                            ?>
                                             <ol style="margin-left:-25px">
                                              <li><?php echo $item->nama_teori." (".$item->nip_teori.")";?></li>
                                              <li><?php echo $item->nama_praktek." (".$item->nip_praktek.")";?></li>
                                            </ol> 
                                            <?php
                                        }else{
                                            echo $item->nama_teori." (".$item->nip_teori.")";
                                        }
                                    }
                                    ?></td>
                                    <td><a href="<?php echo base_url(); ?>aktif/edit/<?php echo $item->aktif_id ?>" type="button" class="btn-sm btn-primary" title="Ubah"><i style="color:white" class="fa fa-edit"></i></a>&nbsp;<a href="<?php echo base_url(); ?>aktif/delete_konfirmasi/<?php echo $item->aktif_id ?>" type="button" class="btn-sm btn-danger" title="Hapus" onClick="return confirm('Anda yakin akan menghapus data ini?'); if (ok) return true; else return false">Hapus</a></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "autoWidth": false,
      "buttons": ["excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<?php
$halaman = "plotting_pagi";
?>
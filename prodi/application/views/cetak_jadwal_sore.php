<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Rekapitulasi_Jadwal_Sore.xls");
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Jadwal Perkuliahan Sore</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div align="right" style="margin-right: 10px;">
                    </div>
                    <br>
                    <?php
                    for ($f=1; $f<=5; $f++) { 
                        if($f=='1')
                            $hari = "SENIN";
                        elseif($f=='2')
                            $hari = "SELASA";
                        elseif($f=='3')
                            $hari = "RABU";
                        elseif($f=='4')
                            $hari = "KAMIS";
                        elseif($f=='5')
                            $hari = "JUM'AT";
                        ?>
                        <br>
                        <b><?php echo $hari?></b>

                        <table border="1">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <?php
                                    foreach ($ruang as $ru) {
                                        ?>
                                        <th><font size="1"><?php echo $ru->ruangan_nama ?></font></th>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $JAM_MAP[1] = "17.00 - 17.30";
                                $JAM_MAP[2] = "17.30 - 18.00";
                                $JAM_MAP[3] = "18.00 - 18.30";
                                $JAM_MAP[4] = "18.30 - 19.00";
                                $JAM_MAP[5] = "19.00 - 19.30";
                                $JAM_MAP[6] = "19.30 - 20.00";
                                $JAM_MAP[7] = "20.00 - 20.30";
                                $JAM_MAP[8] = "20.30 - 21.00";
                                for ($i=1; $i <=8 ; $i++) { 
                                    ?>
                                    <tr>
                                        <td><font size="1"><?php echo $JAM_MAP[$i]?></font></td>
                                        <?php
                                        foreach ($ruang as $ru) {
                                            $ci =& get_instance();
                                            $QRY = "SELECT * 
                                            FROM bantu_jadwal_sore
                                            WHERE hari = '$f'
                                            AND ruangan='$ru->ruangan_id'
                                            AND kode_jam='$i'";
                                            $query = $ci->db->query($QRY);
                                            $data = $query->row_array();

                                            //kelas
                                            $kelas = "SELECT * 
                                            FROM kelas
                                            WHERE kelas_id = '$data[kelas_id]'";
                                            $kls = $ci->db->query($kelas)->row_array();

                                            //dosen1
                                            $dosen1 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen]'";
                                            $dos1 = $ci->db->query($dosen1)->row_array();

                                            //dosen2
                                            $dosen2 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen2]'";
                                            $dos2 = $ci->db->query($dosen2)->row_array();

                                            //MK
                                            $mk = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.smt
                                            FROM kelas_aktif_sore, matakuliah
                                            WHERE kelas_aktif_sore.matakuliah_kode_utama=matakuliah.matakuliah_kode_utama AND
                                            kelas_aktif_sore.aktif_id = '$data[aktif_id]'";
                                            $m = $ci->db->query($mk)->row_array();
                                            if(isset($m)){
                                                if($m['smt']=='1')
                                                    $wbg = "#B7E5B4";
                                                elseif($m['smt']=='2')
                                                    $wbg = "#FFFC9B";
                                                elseif($m['smt']=='3')
                                                    $wbg = "#96E9C6";
                                                elseif($m['smt']=='4')
                                                    $wbg = "#C7B7A3";
                                                elseif($m['smt']=='5')
                                                    $wbg = "#FDE767";
                                                elseif($m['smt']=='6')
                                                    $wbg = "#99BC85";
                                                elseif($m['smt']=='7')
                                                    $wbg = "#E6A4B4";
                                                elseif($m['smt']=='8')
                                                    $wbg = "#FFCF81";
                                                else
                                                    $wbg = "#FFFFFF";
                                            }else{
                                                $wbg = "#FFFFFF";
                                            }
                                            ?>
                                            <th bgcolor="<?php echo $wbg?>"><font size="2">
                                            <?php
                                            if(isset($kls)){
                                                echo $kls['kelas_nama'];
                                                echo " (".$kls['kelas_angkatan'].")";
                                                echo "<br>";
                                                echo $dos1['dosen_nama'];
                                                if(isset($dos2) && ($dos2['dosen_nama']!=$dos1['dosen_nama']))
                                                {
                                                    echo "/<br>";
                                                    echo $dos2['dosen_nama'];
                                                }
                                                echo "<br>";
                                                echo "(";
                                                echo $m['matakuliah_kode'];
                                                echo ") ";
                                                echo $m['matakuliah_nama'];
                                            }
                                            
                                             ?></font>
                                            </th>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
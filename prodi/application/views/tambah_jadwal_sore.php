<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Tambah Jadwal Perkuliahan Sore</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="row">
                        <div class="data">
                            <div>
                                <form method="POST" action="<?php echo base_url(); ?>aktif_sore/simpan_tambah_jadwal">
                                    <div class="input-block">
                                        <label>Kelas Aktif</label>
                                        <select class="form-control col-8" name="aktif_id">
                                            <?php
                                            foreach ($data_sore as $sore) {
                                                ?>
                                                <option value="<?php echo $sore->aktif_id?>"><?php echo $sore->matakuliah_nama?> - <?php 
                                                if($sore->nip_teori==$sore->nip_praktek)
                                                    echo $sore->nama_teori;
                                                else{
                                                    echo $sore->nama_teori;
                                                    echo " / ";
                                                    echo $sore->nama_praktek;
                                                }?> - <?php echo $sore->kelas_nama?> (<?php echo $sore->kelas_angkatan?>)</option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="input-block">
                                        <label>Hari</label>
                                        <select class="form-control col-3" name="jadwal_hari">
                                            <option value="Senin">Senin</option>
                                            <option value="Selasa">Selasa</option>
                                            <option value="Rabu">Rabu</option>
                                            <option value="Kamis">Kamis</option>
                                            <option value="Jumat">Jumat</option>
                                            <option value="Sabtu">Sabtu</option>
                                            <option value="Minggu">Minggu</option>
                                        </select>
                                    </div>
                                    <div class="input-block">
                                        <label>Jam</label>
                                        <input name="jadwal_jam_mulai" type="time" class="form-control col-3" required>
                                        <input name="jadwal_jam_berakhir" type="time" class="form-control col-3" required>
                                    </div>
                                    <div class="input-block">
                                        <label>Ruangan (Kapasitas)</label>
                                        <select class="form-control col-3" name="ruangan">
                                            <?php
                                            foreach ($ruangan as $ruang) {
                                                ?>
                                                <option value="<?php echo $ruang->ruangan_id?>"><?php echo $ruang->ruangan_nama?> (<?php echo $ruang->ruangan_kapasitas?>)</option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="input-block-closing">
                                        <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<?php
$halaman = "plotting_pagi";
?>

<!-- <div class="row">
    <div class="data">
        <div>
            <form method="POST" action="">
                <div class="input-block">
                    <label>Nama Kelas</label>
                    <input class="form-control" type="text" name="nama" value="" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Angkatan</label>
                    <input class="form-control" type="number" name="angkatan" value="" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Program Studi</label>
                    <select class="form-control" name="prodi">
                        <option value="0">-- Belum Ada --</option>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div> -->
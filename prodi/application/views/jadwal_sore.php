
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Jadwal Perkuliahan Sore</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div align="right" style="margin-right: 10px;">
                        <a style="color:white" href="<?php echo base_url()?>aktif_sore/tambah_jadwal_sore/" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Jadwal</a>
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/autogenerator/" class="btn btn-sm btn-primary"><i class="fa fa-history"></i> Generate Jadwal</a>
                    </div>
                    <br><br>
                    <table id="example1" class="table table-bordered table-striped display">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Kuliah</th>
                                <th>Pengampu</th>
                                <th>Kelas</th>
                                <th>Hari</th>
                                <th>Jam</th>
                                <th>Ruang</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            // print_r($data_sp_ls);
                            foreach ($items as $dm => $item) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $item->matakuliah_nama; ?> (<?php echo $item->matakuliah_kode; ?>)</td>
                                    <td><?php echo $item->dosen_nama; ?> (<?php echo $item->dosen_nip; ?>)</td>
                                    <td><?php echo $item->kelas_nama; ?> (<?php echo $item->kelas_angkatan; ?>)</td>
                                    <td><?php echo $item->jadwal_hari; ?></td>
                                    <td><?php echo $item->jadwal_jam_mulai; ?> - <?php echo $item->jadwal_jam_berakhir; ?></td>
                                    <td><?php echo $item->ruangan_nama; ?></td>
                                    <!-- <td><a href="<?php echo base_url(); ?>/suratjalan/ubah_ls/<?php echo $item->idRinci ?>/<?php echo $item->IdGlobal ?>" type="button" class="btn-sm btn-primary" title="Ubah"><i class="fa fa-edit"></i></a></td> -->

                                   <!--  <td>
                                        <button title="Input Jumlah dan Gudang" onclick="ubah_sj_ls('<?= $item->matakuliah_kode ?>')" class="btn btn-success btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td> -->
                                    <td>
                                        <!-- <a href="<?php echo base_url(); ?>aktif/edit/<?php echo $item->aktif_id ?>" type="button" class="btn-sm btn-primary" title="Ubah"><i style="color:white" class="fa fa-edit"></i></a>&nbsp; -->
                                        <a style="color:white" href="<?php echo base_url(); ?>jadwal_sore/delete_konfirmasi/<?php echo $item->aktif_id ?>" type="button" class="btn-sm btn-danger" title="Hapus" onClick="return confirm('Anda yakin akan menghapus data ini?'); if (ok) return true; else return false">Hapus</a></td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["excel", "pdf", "print"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
<?php
$halaman = "plotting_pagi";
?>
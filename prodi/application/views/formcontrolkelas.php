<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="kode" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nama Kelas</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Angkatan</label>
                    <input class="form-control" type="number" name="angkatan" value="<?php echo $angkatan; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Program Studi</label>
                    <select class="form-control" name="prodi">
                        <option value="0">-- Belum Ada --</option>
                        <?php echo $prodi; ?>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
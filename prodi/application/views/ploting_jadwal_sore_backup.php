<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
<style>
        /* Gaya CSS untuk menghilangkan warna dan garis bawah pada tautan */
        a.no-style {
            color: inherit;
            text-decoration: none;
        }

        a.no-style:hover {
            color: inherit;
            text-decoration: none;
        }
    </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Jadwal Perkuliahan Sore</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div align="right" style="margin-right: 10px;">
                        <!-- <a style="color:white" href="<?php echo base_url()?>aktif/tambah_jadwal_pagi/" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Jadwal</a> -->
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/cek_belum/" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cek Belum Termapping</a>
                        <a onClick="return confirm('Anda yakin akan mereset jadwal?'); if (ok) return true; else return false" style="color:white" href="<?php echo base_url()?>jadwal_sore/reset_jadwal/" class="btn btn-sm btn-danger"><i class="fa fa-retweet"></i> Reset Jadwal</a>
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/mapping_ruang/" class="btn btn-sm btn-success"><i class="fa fa-wrench"></i> Mapping Ruang</a>
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/mapping_jadwal/" class="btn btn-sm btn-primary"><i class="fa fa-history"></i> Generate Jadwal</a>
                        <a target="_blank" style="color:white" href="<?php echo base_url()?>jadwal_sore/cetak_jadwal/" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Cetak Jadwal</a>
                    </div>
                    Terdapat <a href="#" class="btn btn-sm btn-light" data-toggle="modal" data-target="#myModalBelum"><b><?php echo $belum_terjadwal?></b></a> Plotting Jadwal belum terjadwal
                    <br>
                    <?php
                    $hh = $hari_selanjutnya['hari'];
                    if($hh=='1')
                        $hari2 = "SENIN";
                    elseif($hh=='2')
                        $hari2 = "SELASA";
                    elseif($hh=='3')
                        $hari2 = "RABU";
                    elseif($hh=='4')
                        $hari2 = "KAMIS";
                    elseif($hh=='5')
                        $hari2 = "JUM'AT";
                    ?>
                    Penjadwalan berikutnya di hari <b><?php echo $hari2?></b>
                    <br><br>
                    <?php
                    for ($f=1; $f<=5; $f++) { 
                        if($f=='1')
                            $hari = "SENIN";
                        elseif($f=='2')
                            $hari = "SELASA";
                        elseif($f=='3')
                            $hari = "RABU";
                        elseif($f=='4')
                            $hari = "KAMIS";
                        elseif($f=='5')
                            $hari = "JUM'AT";
                        ?>
                        <br>
                        <b><?php echo $hari?></b>

                        <table id="example1" class="table table-bordered table-striped display">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <?php
                                    foreach ($ruang as $ru) {
                                        ?>
                                        <th><font size="1"><?php echo $ru->ruangan_nama ?></font></th>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $JAM_MAP[1] = "17.00 - 17.30";
                                $JAM_MAP[2] = "17.30 - 18.00";
                                $JAM_MAP[3] = "18.00 - 18.30";
                                $JAM_MAP[4] = "18.30 - 19.00";
                                $JAM_MAP[5] = "19.00 - 19.30";
                                $JAM_MAP[6] = "19.30 - 20.00";
                                $JAM_MAP[7] = "20.00 - 20.30";
                                $JAM_MAP[8] = "20.30 - 21.00";
                                // $JAM_MAP[9] = "14.40 - 15.30";
                                for ($i=1; $i <=8 ; $i++) { 
                                    ?>
                                    <tr>
                                        <td><font size="1"><?php echo $JAM_MAP[$i]?></font></td>
                                        <?php
                                        foreach ($ruang as $ru) {
                                            $ci =& get_instance();
                                            $QRY = "SELECT * 
                                            FROM bantu_jadwal_sore
                                            WHERE hari = '$f'
                                            AND ruangan='$ru->ruangan_id'
                                            AND kode_jam='$i'";
                                            $query = $ci->db->query($QRY);
                                            $data = $query->row_array();

                                            //kelas
                                            $kelas = "SELECT * 
                                            FROM kelas
                                            WHERE kelas_id = '$data[kelas_id]'";
                                            $kls = $ci->db->query($kelas)->row_array();

                                            //dosen1
                                            $dosen1 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen]'";
                                            $dos1 = $ci->db->query($dosen1)->row_array();

                                            //dosen2
                                            $dosen2 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen2]'";
                                            $dos2 = $ci->db->query($dosen2)->row_array();

                                            //MK
                                            $mk = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.smt
                                            FROM kelas_aktif_sore, matakuliah
                                            WHERE kelas_aktif_sore.matakuliah_kode_utama=matakuliah.matakuliah_kode_utama AND
                                            kelas_aktif_sore.aktif_id = '$data[aktif_id]'";
                                            $m = $ci->db->query($mk)->row_array();
                                            if($m['smt']=='1')
                                                $wbg = "#B7E5B4";
                                            elseif($m['smt']=='2')
                                                $wbg = "#FFFC9B";
                                            elseif($m['smt']=='3')
                                                $wbg = "#96E9C6";
                                            elseif($m['smt']=='4')
                                                $wbg = "#C7B7A3";
                                            elseif($m['smt']=='5')
                                                $wbg = "#FDE767";
                                            elseif($m['smt']=='6')
                                                $wbg = "#99BC85";
                                            elseif($m['smt']=='7')
                                                $wbg = "#E6A4B4";
                                            elseif($m['smt']=='8')
                                                $wbg = "#FFCF81";
                                            else
                                                $wbg = "#FFFFFF";
                                            ?>
                                            <td bgcolor="<?php echo $wbg?>" data-toggle="<?php
                                            if(isset($kls))
                                                echo "popover";
                                            ?>" title="Kelas <?php
                                            if(isset($kls)){
                                                echo $kls['kelas_nama'];
                                                echo " (".$kls['kelas_angkatan'].")";
                                            }
                                            ?>" data-content="<?php
                                            echo $dos1['dosen_nama'];
                                            if(isset($dos2))
                                            {
                                                echo "/";
                                                echo $dos2['dosen_nama'];
                                            }
                                            ?> - <?php
                                            echo $m['matakuliah_nama'];
                                            ?>">
                                            <?php
                                            if(isset($kls))
                                            {
                                                ?>
                                                <font size="1"><a href="#" data-toggle="modal" data-target="#myModal<?php echo $data['aktif_id']; ?>" class="no-style">
                                                <?php
                                                if(isset($kls)){
                                                    echo $kls['kelas_nama'];
                                                    echo " (".$kls['kelas_angkatan'].")";
                                                }
                                                
                                                 ?></a></font>
                                                <?php
                                            }else{
                                                ?>
                                                <a href="#" class="btn btn-sm btn-light" data-toggle="modal" data-target="#myModalTambah<?php echo $data['id_bantu']; ?>"><i class="fa fa-plus"></i></a>
                                                <?php
                                            }
                                            ?>
                                            
                                            </td>
                                             <!-- Modal -->
                                            <div class="modal fade" id="myModal<?php echo $data['aktif_id']; ?>" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <!-- Konten Modal -->
                                                    
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Ubah Jadwal</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php

                                                            ?>
                                                            <b>Dosen 1 :</b> <?php echo $dos1['dosen_nama'];?><br>
                                                            <b>Dosen 2 :</b> <?php echo $dos2['dosen_nama'];?><br>
                                                            <b>Dosen 3 :</b> -<br>
                                                            <b>Matakuliah :</b> <?php echo $m['matakuliah_nama'];?><br>
                                                            <b>Jadwal :</b><br>
                                                            <?php
                                                            $jd = "SELECT bantu_jadwal_sore.kode_jam, bantu_jadwal_sore.hari, ruangan.ruangan_nama 
                                                            FROM bantu_jadwal_sore, ruangan
                                                            WHERE bantu_jadwal_sore.ruangan=ruangan.ruangan_id AND
                                                            bantu_jadwal_sore.aktif_id = '$data[aktif_id]'";
                                                            $jdw = $ci->db->query($jd)->result();
                                                            foreach ($jdw as $j) {
                                                                if($j->hari=='1')
                                                                    $hari_ = "SENIN";
                                                                elseif($j->hari=='2')
                                                                    $hari_ = "SELASA";
                                                                elseif($j->hari=='3')
                                                                    $hari_ = "RABU";
                                                                elseif($j->hari=='4')
                                                                    $hari_ = "KAMIS";
                                                                elseif($j->hari=='5')
                                                                    $hari_ = "JUM'AT";
                                                                echo $hari_;
                                                                echo " (";
                                                                echo $JAM_MAP[$j->kode_jam];
                                                                echo ") - ";
                                                                echo $j->ruangan_nama;
                                                                echo "<br>";
                                                            }
                                                            ?>
                                                                <form action="ubah_jadwal" method="post">
                                                                <input type="hidden" name="aktif_id" value="<?php echo $data['aktif_id']?>">
                                                                <div class="form-group">
                                                                    <label for="nama">Hari:</label>
                                                                    <select class="form-control" name="hari">
                                                                        <?php
                                                                        for ($h=1; $h<=5; $h++) {
                                                                            if($h=='1')
                                                                                $hari_ = "SENIN";
                                                                            elseif($h=='2')
                                                                                $hari_ = "SELASA";
                                                                            elseif($h=='3')
                                                                                $hari_ = "RABU";
                                                                            elseif($h=='4')
                                                                                $hari_ = "KAMIS";
                                                                            elseif($h=='5')
                                                                                $hari_ = "JUM'AT";
                                                                            ?>
                                                                            <option value="<?php echo $h?>"><?php echo $hari_?></option>
                                                                            <?php
                                                                        }
                                                                        ?>    
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="nama">Ruangan:</label>
                                                                    <select class="form-control" name="ruangan">
                                                                        <?php
                                                                        foreach ($ruang as $ru) {
                                                                            ?>
                                                                            <option value="<?php echo $ru->ruangan_id?>"><?php echo $ru->ruangan_nama?></option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="usia">Waktu Mulai:</label>
                                                                    <select class="form-control" name="jam_mulai">
                                                                    <?php
                                                                    for ($r=1; $r <=8 ; $r++) { 
                                                                        ?>
                                                                        <option value="<?php echo $r?>"><?php echo $JAM_MAP[$r]?></option>
                                                                        <?php
                                                                        $jam_mulai++;
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                                </form>
                                                            
                                                            <!-- Tambahkan detail lain sesuai kebutuhan -->
                                                        </div>
                                                        <div class="modal-footer">
                                                            
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                             <!-- Modal -->
                                            <div class="modal fade" id="myModalTambah<?php echo $data['id_bantu']; ?>" role="dialog">
                                                <div class="modal-dialog modal-lg">
                                                    <!-- Konten Modal -->
                                                    
                                                    <div class="modal-content">

                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Tambah Jadwal</h4>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <?php
                                                            $get_data = "SELECT bantu_jadwal_sore.kode_jam, bantu_jadwal_sore.hari, ruangan.ruangan_nama 
                                                            FROM bantu_jadwal_sore, ruangan
                                                            WHERE bantu_jadwal_sore.ruangan=ruangan.ruangan_id AND
                                                            bantu_jadwal_sore.id_bantu = '$data[id_bantu]'";
                                                            $data_input = $ci->db->query($get_data)->row_array();
                                                            if($data_input['hari']=='1')
                                                                $hari__ = "SENIN";
                                                            elseif($data_input['hari']=='2')
                                                                $hari__ = "SELASA";
                                                            elseif($data_input['hari']=='3')
                                                                $hari__ = "RABU";
                                                            elseif($data_input['hari']=='4')
                                                                $hari__ = "KAMIS";
                                                            elseif($data_input['hari']=='5')
                                                                $hari__ = "JUM'AT";
                                                            ?>
                                                            <b>Hari :</b> <?php echo $hari__;?><br>
                                                            <b>Ruangan :</b> <?php echo $data_input['ruangan_nama'];?><br>
                                                            <b>Waktu Mulai : <?php echo $JAM_MAP[$data_input['kode_jam']] ?></b>
                                                                <form action="simpan_tambah_jadwal" method="post">
                                                                <input type="hidden" name="id_bantu" value="<?php echo $data['id_bantu']?>">
                                                                <div class="form-group">
                                                                    <label for="usia">Kelas Aktif:</label>
                                                                    <?php
                                                                    $semester = $this->session->tahunajaran . '' . $this->session->semester;
                                                                    $get_kelas = "SELECT m.matakuliah_nama, m.matakuliah_jam_teori, d1.dosen_nama AS dosen1, d2.dosen_nama AS dosen2, k.aktif_id, kls.kelas_nama, kls.kelas_angkatan
                                                                    FROM kelas_aktif_sore k
                                                                    JOIN
                                                                        matakuliah m ON k.matakuliah_kode_utama=m.matakuliah_kode_utama
                                                                    JOIN
                                                                        dosen d1 ON k.dosen_nip = d1.dosen_nip
                                                                    JOIN
                                                                        dosen d2 ON k.dosen_nip_p = d2.dosen_nip
                                                                    JOIN
                                                                        kelas kls ON k.kelas_id = kls.kelas_id
                                                                    WHERE 
                                                                        k.proses='0' AND k.semester='$semester'";
                                                                    $gk = $ci->db->query($get_kelas)->result();
                                                                    ?>
                                                                    <select class="form-control" name="aktif_id">
                                                                    <?php
                                                                    foreach ($gk as $g) {
                                                                        ?>
                                                                        <option value="<?php echo $g->aktif_id?>"><?php echo $g->matakuliah_nama?> | <?php echo $g->dosen1?> / <?php echo $g->dosen2?> | <?php echo $g->kelas_nama?> - <?php echo $g->kelas_angkatan?> | <?php echo $g->matakuliah_jam_teori?> jam</option>
                                                                        <?php
                                                                        $jam_mulai++;
                                                                    }
                                                                    ?>
                                                                    </select>
                                                                </div>
                                                                <button type="submit" class="btn btn-primary">Simpan</button>
                                                                </form>
                                                            
                                                            <!-- Tambahkan detail lain sesuai kebutuhan -->
                                                        </div>
                                                        <div class="modal-footer">
                                                            
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="myModalBelum" role="dialog">
    <div class="modal-dialog modal-xl">
        <!-- Konten Modal -->
        
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Kelas Aktif Belum Terjadwal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $semester = $this->session->tahunajaran . '' . $this->session->semester;
                $get_data = "SELECT m.matakuliah_nama, m.matakuliah_jam_teori, d1.dosen_nama AS dosen1, d2.dosen_nama AS dosen2, k.aktif_id, kls.kelas_nama, kls.kelas_angkatan
                FROM kelas_aktif_sore k
                JOIN
                    matakuliah m ON k.matakuliah_kode_utama=m.matakuliah_kode_utama
                JOIN
                    dosen d1 ON k.dosen_nip = d1.dosen_nip
                JOIN
                    dosen d2 ON k.dosen_nip_p = d2.dosen_nip
                JOIN
                    kelas kls ON k.kelas_id = kls.kelas_id
                WHERE 
                    k.proses='0' AND k.semester='$semester'";
                $data_belum = $ci->db->query($get_data)->result();
                ?>
                <table class="table table-bordered table-striped display nowrap">
                    <tr>
                        <th>No</th>
                        <th>Matakuliah</th>
                        <th>Dosen</th>
                        <th>Kelas</th>
                        <th>Angkatan</th>
                    </tr>
                    <?php
                    $no = 1;
                    foreach ($data_belum as $belum) {
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $belum->matakuliah_nama ?></td>
                            <td><?php echo $belum->dosen1 ?> / <?php echo $belum->dosen2 ?></td>
                            <td><?php echo $belum->kelas_nama ?></td>
                            <td><?php echo $belum->kelas_angkatan ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                
                <!-- Tambahkan detail lain sesuai kebutuhan -->
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
            
        </div>
    </div>
</div>

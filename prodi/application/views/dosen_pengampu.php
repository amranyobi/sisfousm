<?php
if($tipe=='1')
    $kata = "Pagi";
else
    $kata = "Sore";
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Rekapitulasi Dosen Pengampu <?php echo $kata?></h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <a style="color:white" href="<?php echo base_url()?>jadwal/dosen_pengampu/1" class="btn btn-sm btn-success"><i class="fa fa-excel"></i> Pagi</a>
                    <a style="color:white" href="<?php echo base_url()?>jadwal/dosen_pengampu/2" class="btn btn-sm btn-success"><i class="fa fa-excel"></i> Sore</a>
                    <div align="right">
                        <a style="color:white" href="<?php echo base_url()?>jadwal/cetak_dosen_pengampu/<?php echo $tipe?>" class="btn btn-sm btn-success" target="_blank"><i class="fa fa-excel"></i> Excel</a>
                        <!-- <button class="btn btn-sm btn-primary" onclick="window.print()">Cetak</button> -->
                    </div><br><br>
                    <table id="example2" class="table table-bordered table-striped display nowrap">
                        <thead>
                            <tr>
                                <th>Semester</th>
                                <th>Kode</th>
                                <th>Matakuliah</th>
                                <th>SKS</th>
                                <th>A</th>
                                <th>B</th>
                                <th>C</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            function ambil_dosen($matakuliah,$nama_kelas){
                                $ci =& get_instance();
                                $smt = "SELECT semester 
                                FROM semester_aktif
                                WHERE status = '1'";
                                $qsmt = $ci->db->query($smt);
                                $s = $qsmt->row_array();

                                $ci =& get_instance();
                                $QRY = "SELECT d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
                                FROM kelas_aktif a 
                                LEFT JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
                                LEFT JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
                                JOIN kelas AS f ON (a.kelas_id = f.kelas_id) 
                                JOIN matakuliah AS g ON (a.matakuliah_kode_utama = g.matakuliah_kode_utama) 
                                WHERE f.kelas_nama = '$nama_kelas' AND g.matakuliah_kode='$matakuliah' AND a.semester='$s[semester]'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }

                            function ambil_dosen_sore($matakuliah,$nama_kelas){
                                $ci =& get_instance();
                                $smt = "SELECT semester 
                                FROM semester_aktif
                                WHERE status = '1'";
                                $qsmt = $ci->db->query($smt);
                                $s = $qsmt->row_array();
                                
                                $ci =& get_instance();
                                $QRY = "SELECT d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
                                FROM kelas_aktif_sore a 
                                LEFT JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
                                LEFT JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
                                JOIN kelas AS f ON (a.kelas_id = f.kelas_id) 
                                JOIN matakuliah AS g ON (a.matakuliah_kode_utama = g.matakuliah_kode_utama) 
                                WHERE f.kelas_nama = '$nama_kelas' AND g.matakuliah_kode='$matakuliah' AND a.semester='$s[semester]'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }
                            // print_r($data_sp_ls);
                            foreach ($items as $dm => $item) {
                                $gap = ambil_dosen($item->matakuliah_kode,'A');
                                $gbp = ambil_dosen($item->matakuliah_kode,'B');
                                $gcp = ambil_dosen($item->matakuliah_kode,'C');
                                $gas = ambil_dosen_sore($item->matakuliah_kode,'A');
                                $gbs = ambil_dosen_sore($item->matakuliah_kode,'B');
                                $gcs = ambil_dosen_sore($item->matakuliah_kode,'C');

                                if(isset($gap) || isset($gbp) || isset($gcp) || isset($gas) || isset($gbs) || isset($gcs))
                                {


                            ?>
                                <tr>
                                    <td><?php echo $item->smt;; ?></td>
                                    <td><?php echo $item->matakuliah_kode; ?></td>
                                    <td><?php echo $item->matakuliah_nama; ?></td>
                                    <td><?php echo $item->matakuliah_sks_teori; ?></td>
                                    <td>
                                        <?php
                                        if($tipe=='1'){
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,'A');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                                
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,'A');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        
                                        ?>

                                    </td>
                                    <td>
                                        <?php

                                        if($tipe=='1')
                                        {
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,'B');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,'B');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                          echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        
                                        ?>
                                    </td>
                                    <td>
                                        <?php

                                        if($tipe=='1')
                                        {
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,'C');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                          echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,'C');
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                       if(isset($get_dosen['nama_teori']))
                                                           echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                       else
                                                           echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>";
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }

                                        ?>
                                    </td>
                                </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
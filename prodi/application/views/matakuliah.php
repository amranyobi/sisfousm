
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Matakuliah</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <table id="example2" class="table table-bordered table-striped display nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Mata Kuliah</th>
                                <th>Nama</th>
                                <th>SKS</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            // print_r($data_sp_ls);
                            foreach ($items as $dm => $item) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $item->matakuliah_kode; ?></td>
                                    <td><?php echo $item->matakuliah_nama; ?></td>
                                    <td><?php echo $item->matakuliah_sks_teori; ?></td>
                                    <!-- <td><a href="<?php echo base_url(); ?>/suratjalan/ubah_ls/<?php echo $item->idRinci ?>/<?php echo $item->IdGlobal ?>" type="button" class="btn-sm btn-primary" title="Ubah"><i class="fa fa-edit"></i></a></td> -->
                                    <td>
                                        <button title="Input Jumlah dan Gudang" onclick="ubah_sj_ls('<?= $item->matakuliah_kode ?>')" class="btn btn-success btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
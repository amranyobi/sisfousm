
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Rekapitulasi Jadwal Mengajar Dosen</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <button class="btn btn-success" onclick="window.print()">Cetak</button><br><br>
                    <table id="example1" class="table table-bordered display nowrap">
                        <thead>
                            <tr>
                                <th width="20" rowspan="2">No</th>
                                <th width="48" rowspan="2">Dosen</th>
                            </tr>
                            <tr>
                              <th width="108"><div align="center">MATA KULIAH </div></th>
                              <th width="43"><div align="center">SKS</div></th>
                              <th width="52">KELAS</th>
                              <th width="68">WAKTU</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            // print_r($data_sp_ls);
                            $jumlah_sks = 0;
                            function ambil_jadwal($aktif_id){
                                $ci =& get_instance();
                                $QRY = "SELECT jadwal.*, ruangan.ruangan_nama  FROM jadwal 
                                JOIN ruangan ON (jadwal.ruangan = ruangan.ruangan_id)
                                WHERE jadwal.aktif_id = '$aktif_id'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }

                            function ambil_jadwal_sore($aktif_id){
                                $ci =& get_instance();
                                $QRY = "SELECT jadwal_sore.*, ruangan.ruangan_nama  FROM jadwal_sore 
                                JOIN ruangan ON (jadwal_sore.ruangan = ruangan.ruangan_id)
                                WHERE jadwal_sore.aktif_id = '$aktif_id'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }

                            foreach ($items as $dm => $item) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $item->dosen_nama; ?> (<?php echo $item->dosen_nip; ?>)</td>
                                    <td colspan="4">
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <?php
                                      $QRY = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.matakuliah_sks_teori, kelas_aktif.dosen_nip, kelas_aktif.dosen_nip_p, kelas.kelas_nama, kelas.kelas_angkatan, kelas_aktif.aktif_id  FROM kelas_aktif 
                                      JOIN matakuliah ON (kelas_aktif.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama)
                                      JOIN kelas ON (kelas_aktif.kelas_id = kelas.kelas_id) 
                                      WHERE kelas_aktif.dosen_nip = '$item->dosen_nip' OR kelas_aktif.dosen_nip_p = '$item->dosen_nip' ORDER BY matakuliah.matakuliah_nama ASC";
                                      $query = $this->db->query($QRY);
                                      $data = $query->result();
                                      $total_sks = 0;
                                      foreach ($data as $mk) {
                                      if($mk->dosen_nip==$mk->dosen_nip_p)
                                        $sks_satuan = $mk->matakuliah_sks_teori;
                                      else
                                        $sks_satuan = $mk->matakuliah_sks_teori/2;
                                      $total_sks = $total_sks + $sks_satuan;
                                        ?>
                                      <tr>
                                        <td width="87"><?php echo $mk->matakuliah_nama ?></td>
                                        <td width="61"><div align="center"><?php echo $sks_satuan ?></div></td>
                                        <td width="94"><?php echo $mk->kelas_nama ?> (<?php echo $mk->kelas_angkatan ?>)</td>
                                        <td width="41"><?php
                                              $get_jadwal = ambil_jadwal($mk->aktif_id);
                                              echo $get_jadwal['jadwal_hari'];
                                              echo "<br>";
                                              echo $get_jadwal['jadwal_jam_mulai'];
                                              echo " - ";
                                              echo $get_jadwal['jadwal_jam_berakhir'];
                                              echo "<br>";
                                              echo $get_jadwal['ruangan_nama'];
                                                ?></td>
                                      </tr>
                                      <?php
                                      }
                                      ?>
                                        <?php
                                      $QRY2 = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.matakuliah_sks_teori, kelas_aktif_sore.dosen_nip, kelas_aktif_sore.dosen_nip_p, kelas.kelas_angkatan, kelas.kelas_nama, kelas_aktif_sore.aktif_id  FROM kelas_aktif_sore 
                                      JOIN matakuliah ON (kelas_aktif_sore.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama)
                                      JOIN kelas ON (kelas_aktif_sore.kelas_id = kelas.kelas_id)  
                                      WHERE kelas_aktif_sore.dosen_nip = '$item->dosen_nip' OR kelas_aktif_sore.dosen_nip_p = '$item->dosen_nip' ORDER BY matakuliah.matakuliah_nama ASC";
                                      $query2 = $this->db->query($QRY2);
                                      $data2 = $query2->result();
                                      $total_sks2 = 0;
                                      foreach ($data2 as $mk2) {
                                      if($mk2->dosen_nip==$mk2->dosen_nip_p)
                                        $sks_satuan2 = $mk2->matakuliah_sks_teori;
                                      else
                                        $sks_satuan2 = $mk2->matakuliah_sks_teori/2;
                                      $total_sks2 = $total_sks2 + $sks_satuan2;
                                        ?>
                                        <tr>
                                          <td width="90"><?php echo $mk2->matakuliah_nama ?></td>
                                          <td width="54"><div align="center"><?php echo $sks_satuan2 ?></div></td>
                                          <td width="81"><?php echo $mk2->kelas_nama ?> (<?php echo $mk2->kelas_angkatan ?>)</td>
                                          <td width="41"><?php
                                              $get_jadwal = ambil_jadwal_sore($mk->aktif_id);
                                              echo $get_jadwal['jadwal_hari'];
                                              echo "<br>";
                                              echo $get_jadwal['jadwal_jam_mulai'];
                                              echo " - ";
                                              echo $get_jadwal['jadwal_jam_berakhir'];
                                              echo "<br>";
                                              echo $get_jadwal['ruangan_nama'];
                                                ?></td>
                                        </tr>
                                        <?php
                                      }
                                      ?>
                                    </table>
                                      </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
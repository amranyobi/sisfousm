<?php
if($tipe=='1')
    $kata = "Pagi";
else
    $kata = "Sore";
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Rekapitulasi_Dosen_Pengampu_$kata.xls");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Rekapitulasi Dosen Pengampu</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <table border="1" id="example2" class="table table-bordered table-striped display nowrap">
                        <thead>
                            <tr>
                                <th>Semester</th>
                                <th>Kode</th>
                                <th>Matakuliah</th>
                                <th>SKS</th>
                                <?php
                                foreach ($kelas as $kls) {
                                    ?>
                                    <th><?php echo $kls->kelas_nama?></th>
                                    <?php
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            function ambil_dosen($matakuliah,$nama_kelas){
                                $ci =& get_instance();
                                $smt = "SELECT semester 
                                FROM semester_aktif
                                WHERE status = '1'";
                                $qsmt = $ci->db->query($smt);
                                $s = $qsmt->row_array();

                                $ci =& get_instance();
                                $QRY = "SELECT d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek 
                                FROM kelas_aktif a 
                                LEFT JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
                                LEFT JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
                                JOIN kelas AS f ON (a.kelas_id = f.kelas_id) 
                                JOIN matakuliah AS g ON (a.matakuliah_kode_utama = g.matakuliah_kode_utama) 
                                WHERE f.kelas_nama = '$nama_kelas' AND g.matakuliah_kode='$matakuliah' AND a.semester='$s[semester]'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }

                            function ambil_dosen_sore($matakuliah,$nama_kelas){
                                $ci =& get_instance();
                                $smt = "SELECT semester 
                                FROM semester_aktif
                                WHERE status = '1'";
                                $qsmt = $ci->db->query($smt);
                                $s = $qsmt->row_array();
                                
                                $ci =& get_instance();
                                $QRY = "SELECT d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek 
                                FROM kelas_aktif_sore a 
                                LEFT JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
                                LEFT JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
                                JOIN kelas AS f ON (a.kelas_id = f.kelas_id) 
                                JOIN matakuliah AS g ON (a.matakuliah_kode_utama = g.matakuliah_kode_utama) 
                                WHERE f.kelas_nama = '$nama_kelas' AND g.matakuliah_kode='$matakuliah' AND a.semester='$s[semester]'";
                                $query = $ci->db->query($QRY);
                                $data = $query->row_array();
                                return $data;
                            }
                            // print_r($data_sp_ls);
                            foreach ($items as $dm => $item) {
                                $gap = ambil_dosen($item->matakuliah_kode,'A');
                                $gbp = ambil_dosen($item->matakuliah_kode,'B');
                                $gcp = ambil_dosen($item->matakuliah_kode,'C');
                                $gas = ambil_dosen_sore($item->matakuliah_kode,'A');
                                $gbs = ambil_dosen_sore($item->matakuliah_kode,'B');
                                $gcs = ambil_dosen_sore($item->matakuliah_kode,'C');

                                if(isset($gap) || isset($gbp) || isset($gcp) || isset($gas) || isset($gbs) || isset($gcs))
                                {
                            ?>
                                <tr>
                                    <td><?php echo $item->smt;; ?></td>
                                    <td><?php echo $item->matakuliah_kode; ?></td>
                                    <td><?php echo $item->matakuliah_nama; ?></td>
                                    <td><?php echo $item->matakuliah_sks_teori; ?></td>
                                    <?php
                                    foreach ($kelas as $kls) {
                                        ?>
                                    <td>
                                        <?php
                                        if($tipe=='1'){
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,$kls->kelas_nama);
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                        <li><?php
                                                        if(isset($data['dosen_nama']))
                                                          echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                        else
                                                          echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                      ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                                
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,$kls->kelas_nama);
                                            if(isset($get_dosen))
                                            {
                                                if(isset($get_dosen['dosen3']))
                                                {
                                                    $ci =& get_instance();
                                                    $QRY = "SELECT dosen_nama, dosen_nip 
                                                    FROM dosen
                                                    WHERE dosen_nip = '$get_dosen[dosen3]'";
                                                    $query = $ci->db->query($QRY);
                                                    $data = $query->row_array();
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php
                                                      if(isset($get_dosen['nama_teori']))
                                                        echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($get_dosen['nama_praktek']))
                                                        echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                        ?></li>
                                                      <li><?php
                                                      if(isset($data['dosen_nama']))
                                                        echo $data['dosen_nama']." (".$data['dosen_nip'].")";
                                                      else
                                                        echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    ?></li>
                                                    </ol>
                                                    <?php
                                                }else{
                                                    if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                        if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                        else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                    else{
                                                        ?>
                                                         <ol style="margin-left:-25px">
                                                          <li><?php
                                                          if(isset($get_dosen['nama_teori']))
                                                            echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                          <li><?php
                                                          if(isset($get_dosen['nama_praktek']))
                                                            echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";
                                                          else
                                                            echo "<font color=red>NIP Dosen Tidak Valid</font>"; 
                                                            ?></li>
                                                        </ol>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        
                                        ?>

                                    </td>
                                    <?php
                                    }
                                    ?>
                                    <!-- <td>
                                        <?php
                                        if($tipe=='1')
                                        {
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,'B');
                                            if(isset($get_dosen))
                                            {
                                                if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                    echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                else{
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";?></li>
                                                      <li><?php echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";?></li>
                                                    </ol>
                                                    <?php
                                                }
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,'B');
                                            if(isset($get_dosen))
                                            {
                                                if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                    echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                else{
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";?></li>
                                                      <li><?php echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";?></li>
                                                    </ol>
                                                    <?php
                                                }
                                            }
                                        }
                                        
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if($tipe=='1')
                                        {
                                            $get_dosen = ambil_dosen($item->matakuliah_kode,'C');
                                            if(isset($get_dosen))
                                            {
                                                if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                    echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                else{
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";?></li>
                                                      <li><?php echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";?></li>
                                                    </ol>
                                                    <?php
                                                }
                                            }
                                        }elseif($tipe=='2')
                                        {
                                            $get_dosen = ambil_dosen_sore($item->matakuliah_kode,'C');
                                            if(isset($get_dosen))
                                            {
                                                if($get_dosen['nip_teori']==$get_dosen['nip_praktek'])
                                                    echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";
                                                else{
                                                    ?>
                                                     <ol style="margin-left:-25px">
                                                      <li><?php echo $get_dosen['nama_teori']." (".$get_dosen['nip_teori'].")";?></li>
                                                      <li><?php echo $get_dosen['nama_praktek']." (".$get_dosen['nip_praktek'].")";?></li>
                                                    </ol>
                                                    <?php
                                                }
                                            }
                                        }
                                        
                                        ?>
                                    </td> -->
                                </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
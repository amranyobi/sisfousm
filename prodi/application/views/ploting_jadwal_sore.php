<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover({
        placement : 'top',
        trigger : 'hover'
    });
});
</script>
<style>
        /* Gaya CSS untuk menghilangkan warna dan garis bawah pada tautan */
        a.no-style {
            color: inherit;
            text-decoration: none;
        }

        a.no-style:hover {
            color: inherit;
            text-decoration: none;
        }
    </style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Jadwal Perkuliahan Sore</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div align="right" style="margin-right: 10px;">
                        <!-- <a style="color:white" href="<?php echo base_url()?>aktif/tambah_jadwal_pagi/" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Tambah Jadwal</a> -->
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/cek_belum/" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cek Belum Termapping</a>
                        <a onClick="return confirm('Anda yakin akan mereset jadwal?'); if (ok) return true; else return false" style="color:white" href="<?php echo base_url()?>jadwal_sore/reset_jadwal/" class="btn btn-sm btn-danger"><i class="fa fa-retweet"></i> Reset Jadwal</a>
                        <!-- <a style="color:white" href="<?php echo base_url()?>jadwal_sore/mapping_ruang/" class="btn btn-sm btn-success"><i class="fa fa-wrench"></i> Mapping Ruang</a> -->
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/mapping_jadwal/" class="btn btn-sm btn-primary"><i class="fa fa-history"></i> Generate Jadwal</a>
                        <a style="color:white" href="<?php echo base_url()?>jadwal_sore/jadwal_dosen/" class="btn btn-sm btn-warning"><i class="fa fa-user"></i> Jadwal Per Dosen</a>
                        <a target="_blank" style="color:white" href="<?php echo base_url()?>jadwal_sore/cetak_jadwal/" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Cetak Jadwal</a>
                    </div>
                    Terdapat <a href="#" class="btn btn-sm btn-light" data-toggle="modal" data-target="#myModalBelum"><b><?php echo $belum_terjadwal?></b></a> Plotting Jadwal belum terjadwal
                    <br>
                    <?php
                    $hh = $hari_selanjutnya['hari'];
                    if($hh=='1')
                        $hari2 = "SENIN";
                    elseif($hh=='2')
                        $hari2 = "SELASA";
                    elseif($hh=='3')
                        $hari2 = "RABU";
                    elseif($hh=='4')
                        $hari2 = "KAMIS";
                    elseif($hh=='5')
                        $hari2 = "JUM'AT";
                    ?>
                    Penjadwalan berikutnya di hari <b><?php echo $hari2?></b>
                    <br><br>
                    <?php
                    $susun_semester = $this->session->tahunajaran.$this->session->semester;
                    for ($f=1; $f<=5; $f++) { 
                        if($f=='1')
                            $hari = "SENIN";
                        elseif($f=='2')
                            $hari = "SELASA";
                        elseif($f=='3')
                            $hari = "RABU";
                        elseif($f=='4')
                            $hari = "KAMIS";
                        elseif($f=='5')
                            $hari = "JUM'AT";
                        ?>
                        <br>
                        <b><?php echo $hari?></b>

                        <table id="example1" class="table table-bordered table-striped display">
                            <thead>
                                <tr>
                                    <th>Waktu</th>
                                    <?php
                                    foreach ($ruang as $ru) {
                                        ?>
                                        <th><font size="1"><?php echo $ru->ruangan_nama ?></font></th>
                                        <?php
                                    }
                                    ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $JAM_MAP[1] = "17.00 - 17.30";
                                $JAM_MAP[2] = "17.30 - 18.00";
                                $JAM_MAP[3] = "18.00 - 18.30";
                                $JAM_MAP[4] = "18.30 - 19.00";
                                $JAM_MAP[5] = "19.00 - 19.30";
                                $JAM_MAP[6] = "19.30 - 20.00";
                                $JAM_MAP[7] = "20.00 - 20.30";
                                $JAM_MAP[8] = "20.30 - 21.00";
                                // $JAM_MAP[9] = "14.40 - 15.30";
                                for ($i=1; $i <=8 ; $i++) { 
                                    ?>
                                    <tr>
                                        <td><font size="1"><?php echo $JAM_MAP[$i]?></font></td>
                                        <?php
                                        foreach ($ruang as $ru) {
                                            $ci =& get_instance();
                                            $QRY = "SELECT * 
                                            FROM bantu_jadwal_sore
                                            WHERE hari = '$f'
                                            AND ruangan='$ru->ruangan_id'
                                            AND kode_jam='$i'";
                                            $query = $ci->db->query($QRY);
                                            $data = $query->row_array();

                                            //kelas
                                            $kelas = "SELECT * 
                                            FROM kelas
                                            WHERE kelas_id = '$data[kelas_id]'";
                                            $kls = $ci->db->query($kelas)->row_array();

                                            //dosen1
                                            $dosen1 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen]'";
                                            $dos1 = $ci->db->query($dosen1)->row_array();

                                            //dosen2
                                            $dosen2 = "SELECT * 
                                            FROM dosen
                                            WHERE dosen_nip = '$data[dosen2]'";
                                            $dos2 = $ci->db->query($dosen2)->row_array();

                                            //MK
                                            $mk = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.smt
                                            FROM kelas_aktif_sore, matakuliah
                                            WHERE kelas_aktif_sore.matakuliah_kode_utama=matakuliah.matakuliah_kode_utama AND
                                            kelas_aktif_sore.aktif_id = '$data[aktif_id]'";
                                            $m = $ci->db->query($mk)->row_array();

                                            $tooltip = "";
                                            if(isset($dos1)){
                                                $tooltip .= "Dosen 1 : " . $dos1['dosen_nama'] . "<br>";
                                            }
                                            if(isset($dos2)){
                                                $tooltip .= "Dosen 2 : " . $dos2['dosen_nama'] . "<br>";
                                            }
                                            if(isset($m['matakuliah_nama'])){
                                                $tooltip .= "Mata Kuliah : " . $m['matakuliah_nama'] . "<br>";
                                            }
                                            if(isset($kls)){
                                                $tooltip .= "Kelas : " . $kls['kelas_nama'] . " (" . $kls['kelas_angkatan'] . ")<br>";
                                            }
                                            if(isset($ru->ruangan_nama)){
                                                $tooltip .= "Ruangan : " . $ru->ruangan_nama . "<br>";
                                            }
                                            $tooltip .= "Hari : " . $hari;

                                            if($data['aktif_id']==NULL){
                                                $wbg = "#FFFFFF";
                                            }else{
                                                if($m['smt']=='1')
                                                    $wbg = "#B7E5B4";
                                                elseif($m['smt']=='2')
                                                    $wbg = "#FFFC9B";
                                                elseif($m['smt']=='3')
                                                    $wbg = "#96E9C6";
                                                elseif($m['smt']=='4')
                                                    $wbg = "#C7B7A3";
                                                elseif($m['smt']=='5')
                                                    $wbg = "#FDE767";
                                                elseif($m['smt']=='6')
                                                    $wbg = "#99BC85";
                                                elseif($m['smt']=='7')
                                                    $wbg = "#E6A4B4";
                                                elseif($m['smt']=='8')
                                                    $wbg = "#FFCF81";
                                                else
                                                    $wbg = "#FFFFFF";
                                            }
                                            ?>
                                            <td bgcolor="<?php echo $wbg?>" data-toggle="tooltip" data-placement="top" title="<?php echo $tooltip; ?>" data-html="true">
                                            <?php
                                            if(isset($kls))
                                            {
                                                ?>
                                                <font size="1"><a href="#" data-toggle="modal" data-target="#myModalUbah"
                                               data-ruangan-id="<?php echo $ru->ruangan_id; ?>"
                                               data-kode-jam="<?php echo $i; ?>"
                                               data-hari="<?php echo $f; ?>"
                                               data-hari-text="<?php echo $hari; ?>"
                                               data-ruangan-nama="<?php echo $ru->ruangan_nama; ?>"
                                               data-waktu-mulai="<?php echo $JAM_MAP[$i]; ?>"
                                               data-aktif-id="<?php echo $data['aktif_id']; ?>"
                                               data-dosen1="<?php echo $dos1['dosen_nama']; ?>"
                                               data-dosen2="<?php echo $dos2['dosen_nama']; ?>"
                                               data-matkul="<?php echo $m['matakuliah_nama']; ?>"
                                               data-kelas-nama="<?php echo $kls['kelas_nama']; ?>"
                                               data-kelas-angkatan="<?php echo $kls['kelas_angkatan']; ?>"
                                               class="ubah-modal no-style">
                                                <?php
                                                if(isset($kls)){
                                                    echo $kls['kelas_nama'];
                                                    echo " (".$kls['kelas_angkatan'].")";
                                                }
                                                
                                                 ?></a></font>
                                                <?php
                                            }else{
                                                ?>
                                                <a href="#" class="btn btn-sm btn-light open-modal" data-toggle="modal" data-target="#myModalTambah" data-ruangan-id="<?php echo $ru->ruangan_id; ?>" data-kode-jam="<?php echo $i; ?>" data-hari="<?php echo $f; ?>" data-hari-text="<?php echo $hari; ?>" data-ruangan-nama="<?php echo $ru->ruangan_nama; ?>" data-waktu-mulai="<?php echo $JAM_MAP[$i]; ?>"><i class="fa fa-plus"></i></a>
                                                <?php
                                            }
                                            ?>
                                            
                                            </td>
                                            <?php
                                        }
                                        ?>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<!-- Modal Tambah Jadwal -->
<div class="modal fade" id="myModalTambah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Tambah Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="simpan_tambah_jadwal" method="post">
                <div class="modal-body">
                    <b>Hari :</b> <span id="modalHari"></span><br>
                    <b>Ruangan :</b> <span id="modalRuangan"></span><br>
                    <b>Waktu Mulai :</b> <span id="modalWaktu"></span>
                    <input type="hidden" name="ruangan_id" id="modalRuanganId">
                    <input type="hidden" name="kode_jam" id="modalKodeJam">
                    <input type="hidden" name="hari" id="modalHariValue">
                    <input type="hidden" name="tahunajaran" value="<?php echo $susun_semester; ?>">
                    <div class="form-group">
                        <label for="usia">Kelas Aktif:</label>
                                    <?php
                                    $semester = $this->session->tahunajaran . '' . $this->session->semester;
                                    $get_kelas = "SELECT m.matakuliah_nama, m.matakuliah_jam_teori, d1.dosen_nama AS dosen1, d2.dosen_nama AS dosen2, k.aktif_id, kls.kelas_nama, kls.kelas_angkatan
                                    FROM kelas_aktif_sore k
                                    JOIN
                                        matakuliah m ON k.matakuliah_kode_utama=m.matakuliah_kode_utama
                                    JOIN
                                        dosen d1 ON k.dosen_nip = d1.dosen_nip
                                    JOIN
                                        dosen d2 ON k.dosen_nip_p = d2.dosen_nip
                                    JOIN
                                        kelas kls ON k.kelas_id = kls.kelas_id
                                    WHERE 
                                        k.proses='0' AND k.semester='$semester'
                                    ORDER BY m.matakuliah_nama ASC, d1.dosen_nama ASC, d2.dosen_nama ASC";
                                    $gk = $ci->db->query($get_kelas)->result();
                                    ?>
                        <select class="form-control select2" name="aktif_id" style="width: 100%;">
                                    <?php
                                    foreach ($gk as $g) {
                                        ?>
                                        <option value="<?php echo $g->aktif_id?>"><?php echo $g->matakuliah_nama?> | <?php echo $g->dosen1?> / <?php echo $g->dosen2?> | <?php echo $g->kelas_nama?> - <?php echo $g->kelas_angkatan?> | <?php echo $g->matakuliah_jam_teori?> jam</option>
                                        <?php
                                        $jam_mulai++;
                                    }
                                    ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalUbah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Ubah Jadwal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="ubah_jadwal" method="post">
                <div class="modal-body">
                    <b>Dosen1 :</b> <span id="modalDosen1Ubah"></span><br>
                    <b>Dosen2 :</b> <span id="modalDosen2Ubah"></span><br>
                    <b>Mata Kuliah :</b> <span id="modalMatkulUbah"></span><br>
                    <b>Kelas :</b> <span id="modalKelasNamaUbah"></span><br><br>

                    <b>Jadwal Saat Ini:</b><br>
                    <b>Hari :</b> <span id="modalHariLamaUbah"></span><br>
                    <b>Ruangan :</b> <span id="modalRuanganLamaUbah"></span><br>
                    <b>Waktu Mulai :</b> <span id="modalWaktuLamaUbah"></span><br><br>

                    <b>Jadwal Baru:</b><br>
                    <input type="hidden" name="ruangan_id" id="modalRuanganIdUbah">
                    <input type="hidden" name="kode_jam" id="modalKodeJamUbah">
                    <input type="hidden" name="hari" id="modalHariValueUbah">
                    <input type="hidden" name="tahunajaran" value="<?php echo $susun_semester; ?>">
                    <input type="hidden" name="aktif_id" id="modalAktifIdUbah">
                    <div class="form-group">
                        <label for="hari_baru">Pilih Hari Baru:</label>
                        <select class="form-control" name="hari_baru" id="modalHariBaruUbah">
                            <option value="1">Senin</option>
                            <option value="2">Selasa</option>
                            <option value="3">Rabu</option>
                            <option value="4">Kamis</option>
                            <option value="5">Jumat</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="ruangan_baru">Pilih Ruangan Baru:</label>
                        <select class="form-control" name="ruangan_baru" id="modalRuanganBaruUbah">
                            <?php
                            foreach ($ruang as $ru) {
                                echo '<option value="' . $ru->ruangan_id . '">' . $ru->ruangan_nama . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="jam_baru">Pilih Waktu Mulai Baru:</label>
                        <select class="form-control" name="jam_baru" id="modalJamBaruUbah">
                            <?php
                            foreach ($JAM_MAP as $kode => $waktu) {
                                echo '<option value="' . $kode . '">' . $waktu . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalBelum" role="dialog">
    <div class="modal-dialog modal-xl">
        <!-- Konten Modal -->
        
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Kelas Aktif Belum Terjadwal</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $semester = $this->session->tahunajaran . '' . $this->session->semester;
                $get_data = "SELECT m.matakuliah_nama, m.matakuliah_jam_teori, d1.dosen_nama AS dosen1, d2.dosen_nama AS dosen2, k.aktif_id, kls.kelas_nama, kls.kelas_angkatan
                FROM kelas_aktif_sore k
                JOIN
                    matakuliah m ON k.matakuliah_kode_utama=m.matakuliah_kode_utama
                JOIN
                    dosen d1 ON k.dosen_nip = d1.dosen_nip
                JOIN
                    dosen d2 ON k.dosen_nip_p = d2.dosen_nip
                JOIN
                    kelas kls ON k.kelas_id = kls.kelas_id
                WHERE 
                    k.proses='0' AND k.semester='$semester'";
                $data_belum = $ci->db->query($get_data)->result();
                ?>
                <table class="table table-bordered table-striped display nowrap">
                    <tr>
                        <th>No</th>
                        <th>Matakuliah</th>
                        <th>Dosen</th>
                        <th>Kelas</th>
                        <th>Angkatan</th>
                    </tr>
                    <?php
                    $no = 1;
                    foreach ($data_belum as $belum) {
                        ?>
                        <tr>
                            <td><?php echo $no++ ?></td>
                            <td><?php echo $belum->matakuliah_nama ?></td>
                            <td><?php echo $belum->dosen1 ?> / <?php echo $belum->dosen2 ?></td>
                            <td><?php echo $belum->kelas_nama ?></td>
                            <td><?php echo $belum->kelas_angkatan ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
                
                <!-- Tambahkan detail lain sesuai kebutuhan -->
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
            
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        // Inisialisasi Tooltip dengan HTML
        $('[data-toggle="tooltip"]').tooltip({
            html: true
        });

        // Fungsi untuk menutup semua tooltip
        function closeAllTooltips() {
            $('[data-toggle="tooltip"]').tooltip('dispose');
        }

        // Event listener untuk membuka tooltip dan menutup tooltip lainnya
        $('[data-toggle="tooltip"]').on('mouseenter', function() {
            closeAllTooltips();
            $(this).tooltip('show');
        });

        // Event listener untuk membuka modal
        $('.open-modal').on('click', function() {
            var ruanganId = $(this).data('ruangan-id');
            var kodeJam = $(this).data('kode-jam');
            var hari = $(this).data('hari');
            var hariText = $(this).data('hari-text');
            var ruanganNama = $(this).data('ruangan-nama');
            var waktuMulai = $(this).data('waktu-mulai');

            // Set nilai pada modal
            $('#modalRuanganId').val(ruanganId);
            $('#modalKodeJam').val(kodeJam);
            $('#modalHariValue').val(hari);
            $('#modalHari').text(hariText);
            $('#modalRuangan').text(ruanganNama);
            $('#modalWaktu').text(waktuMulai);

            // Optionally, you can fetch the options for the select2 dynamically
            var semester = '<?php echo $this->session->tahunajaran . $this->session->semester; ?>';
            $.ajax({
                url: '<?php echo base_url(); ?>jadwal/get_kelas/' + semester,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var options = '<option value="">Pilih Kelas</option>';
                    $.each(data, function(index, item) {
                        options += '<option value="' + item.aktif_id + '">' + item.matakuliah_nama + ' | ' + item.dosen1 + ' / ' + item.dosen2 + ' | ' + item.kelas_nama + ' - ' + item.kelas_angkatan + ' | ' + item.matakuliah_jam_teori + ' jam</option>';
                    });
                    $('#modalKelas').html(options);
                    $('#modalKelas').trigger('change');
                }
            });
        });

        // Event listener untuk membuka modal ubah jadwal
        $('[data-target="#myModalUbah"]').on('click', function() {
            var ruanganId = $(this).data('ruangan-id');
            var kodeJam = $(this).data('kode-jam');
            var hari = $(this).data('hari');
            var hariText = $(this).data('hari-text');
            var ruanganNama = $(this).data('ruangan-nama');
            var waktuMulai = $(this).data('waktu-mulai');
            var aktifId = $(this).data('aktif-id');
            var dosen1 = $(this).data('dosen1');
            var dosen2 = $(this).data('dosen2');
            var matkul = $(this).data('matkul');
            var kelasNama = $(this).data('kelas-nama');
            var kelasAngkatan = $(this).data('kelas-angkatan');

            // Set nilai pada modal ubah
            $('#modalRuanganIdUbah').val(ruanganId);
            $('#modalKodeJamUbah').val(kodeJam);
            $('#modalHariValueUbah').val(hari);
            $('#modalHariUbah').text(hariText);
            $('#modalRuanganUbah').text(ruanganNama);
            $('#modalWaktuUbah').text(waktuMulai);
            $('#modalAktifIdUbah').val(aktifId);

            // Set nilai jadwal saat ini
            $('#modalDosen1Ubah').text(dosen1);
            $('#modalDosen2Ubah').text(dosen2);
            $('#modalMatkulUbah').text(matkul);
            $('#modalKelasNamaUbah').text(kelasNama + ' (' + kelasAngkatan + ')');
            $('#modalHariLamaUbah').text(hariText);
            $('#modalRuanganLamaUbah').text(ruanganNama);
            $('#modalWaktuLamaUbah').text(waktuMulai);

            // Fetch options for the select2 dynamically
            var semester = '<?php echo $this->session->tahunajaran . $this->session->semester; ?>';
            $.ajax({
                url: '<?php echo base_url(); ?>jadwal/get_kelas/' + semester,
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    var options = '<option value="">Pilih Kelas</option>';
                    $.each(data, function(index, item) {
                        var selected = item.aktif_id == aktifId ? ' selected' : '';
                        options += '<option value="' + item.aktif_id + '"' + selected + '>' + item.matakuliah_nama + ' | ' + item.dosen1 + ' / ' + item.dosen2 + ' | ' + item.kelas_nama + ' - ' + item.kelas_angkatan + ' | ' + item.matakuliah_jam_teori + ' jam</option>';
                    });
                    $('#modalKelasUbah').html(options);
                    $('#modalKelasUbah').trigger('change');
                }
            });
        });
    });
</script>
<style>
    .tooltip-inner {
        background-color: rgba(0, 0, 0, 0.8); /* Warna hitam dengan transparansi 80% */
        color: #fff;
    }
    .tooltip.bs-tooltip-top .arrow::before,
    .tooltip.bs-tooltip-right .arrow::before,
    .tooltip.bs-tooltip-bottom .arrow::before,
    .tooltip.bs-tooltip-left .arrow::before {
        background-color: rgba(0, 0, 0, 0.8); /* Warna hitam dengan transparansi 80% */
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Jadwal Dosen Sore</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('jadwal_sore/ploting_jadwal'); ?>">Kembali ke Ploting Jadwal</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <form id="jadwalDosenForm" method="post" action="<?php echo base_url('jadwal_sore/jadwal_dosen'); ?>">
                        <div class="form-group">
                            <label for="dosen_nip">Pilih Dosen:</label>
                            <select id="dosen_nip" name="dosen_nip" class="form-control select2" style="width: 100%;" onchange="this.form.submit()">
                                <option value="">--Pilih Dosen--</option>
                                <?php foreach ($dosen_list as $dosen): ?>
                                    <option value="<?php echo $dosen->dosen_nip; ?>" <?php echo isset($selected_dosen) && $selected_dosen == $dosen->dosen_nip ? 'selected' : ''; ?>><?php echo $dosen->dosen_nama; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </form>

                    <?php if (!empty($jadwal)): ?>
                        <h2 class="mt-4">Jadwal untuk Dosen: <?php echo $selected_dosen_nama; ?></h2>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Matakuliah (Kode)</th>
                                    <th>Nama Kelas (Angkatan)</th>
                                    <th>Ruang</th>
                                    <th>Hari</th>
                                    <th>Jam</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                <?php foreach ($jadwal as $row): ?>
                                    <tr>
                                        <td><?php echo $no++; ?></td>
                                        <td><?php echo $row->matakuliah_nama . ' (' . $row->matakuliah_kode . ')'; ?></td>
                                        <td><?php echo $row->kelas_nama . ' (' . $row->kelas_angkatan . ')'; ?></td>
                                        <td><?php echo $row->ruangan_nama; ?></td>
                                        <td><?php echo $row->jadwal_hari; ?></td>
                                        <td><?php echo $row->jadwal_waktu; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();

        // Submit form on dropdown change
        $('#dosen_nip').change(function() {
            $('#jadwalDosenForm').submit();
        });
    });
</script>

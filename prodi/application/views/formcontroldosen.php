<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <input type="text" name="jarak" value="<?php echo $jarak; ?>" style="display: none;">
            <div class="input-block">
                <label>Nomor Induk Pegawai (NIP)</label><br/>
                <?php echo $nip; ?>
            </div><br/>
            <div class="input-block">
                <label>Nomor Induk Dosen Negeri (NIDN)</label><br/>
                <?php echo $nidn; ?>
            </div><br/>
            <div class="input-block">
                <label>Nama</label><br/>
                <?php echo $nama; ?>
            </div><br/>
            <div class="input-block">
                <label>Status Homebase</label><br/>
                <?php echo $homebase; ?>
            </div><br/>
            <div class="input-block">
                <label>Status Kepegawaian</label><br/>
                <?php echo $status; ?>
            </div>
        </div>
    </div>
</div>
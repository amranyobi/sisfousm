  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 1.0.0
    </div>
    <strong>&copy; 2023 Fakultas Psikologi USM</strong>, All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<?php
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/',$link);
    $page = end($link_array);
    echo $page;

    if($page!='plotting_pagi' && $page!='jadwal_pagi' && $page!='plotting_sore' && $page!='add')
    {
      ?>
      <script src="<?php echo CORE_FOLDER; ?>/AdminLTE/plugins/jquery/jquery.min.js"></script>
      <?php
    }
?>
<!-- Bootstrap 4 -->
<script src="<?php echo CORE_FOLDER; ?>/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo CORE_FOLDER; ?>/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo CORE_FOLDER; ?>/AdminLTE/dist/js/demo.js"></script>
</body>
</html>
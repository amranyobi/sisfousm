<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");

// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=Rekapitulasi_Beban_Mengajar.xls");
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Beban Mengajar Dosen</h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <table border="1" id="example1" class="table table-bordered table-striped display nowrap">
                        <thead>
                            <tr>
                                <th width="20" rowspan="2">No</th>
                                <th width="43" rowspan="2">Dosen</th>
                                <th colspan="2"><div align="center">Pagi</div></th>
                                <th colspan="2"><div align="center">Sore</div></th>
                                <th width="69" rowspan="2">Total SKS</th>
                            </tr>
                            <tr>
                              <th width="116"><div align="center">MATA KULIAH </div></th>
                              <th width="26"><div align="center">SKS</div></th>
                              <th width="116"><div align="center">MATA KULIAH</div></th>
                              <th width="30"><div align="center">SKS</div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            // print_r($data_sp_ls);
                            $jumlah_sks = 0;
                            foreach ($items as $dm => $item) {
                            ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $item->dosen_nama; ?> (<?php echo $item->dosen_nip; ?>)</td>
                                    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <?php
                                      $ci =& get_instance();
                                      $smt = "SELECT semester 
                                      FROM semester_aktif
                                      WHERE status = '1'";
                                      $qsmt = $ci->db->query($smt);
                                      $s = $qsmt->row_array();

                                      $QRY = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.matakuliah_sks_teori, kelas_aktif.dosen_nip, kelas_aktif.dosen_nip_p, kelas_aktif.dosen3, kelas.kelas_nama  FROM kelas_aktif 
                                      JOIN matakuliah ON (kelas_aktif.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama)
                                      JOIN kelas ON (kelas_aktif.kelas_id = kelas.kelas_id) 
                                      WHERE kelas_aktif.semester='$s[semester]' AND (kelas_aktif.dosen_nip = '$item->dosen_nip' OR kelas_aktif.dosen_nip_p = '$item->dosen_nip'  OR kelas_aktif.dosen3 = '$item->dosen_nip') ORDER BY matakuliah.matakuliah_nama ASC";
                                      $query = $this->db->query($QRY);
                                      $data = $query->result();
                                      $total_sks = 0;
                                      foreach ($data as $mk) {
                                      if($mk->dosen3=='')
                                      {
                                          if($mk->dosen_nip==$mk->dosen_nip_p)
                                            $sks_satuan = $mk->matakuliah_sks_teori;
                                          else
                                            $sks_satuan = $mk->matakuliah_sks_teori/2;
                                      }else{
                                          $sks_satuan = $mk->matakuliah_sks_teori/3;
                                      }
                                      $total_sks = $total_sks + $sks_satuan;
                                        ?>
                                            <tr>
                                              <td><?php echo $mk->matakuliah_nama ?> (<?php echo $mk->kelas_nama ?>)</td>
                                              <td><div align="center"><?php echo number_format($sks_satuan,2) ?></div></td>
                                            </tr>
                                            <?php
                                      }
                                      ?>
                                      <?php
                                      if($total_sks!=0)
                                      {
                                      ?>
                                          <tr>
                                            <td><b>Total</b></td>
                                            <td><div align="center"><?php echo number_format($total_sks,2)?></div></td>
                                          </tr>
                                      <?php
                                      } 
                                      ?>
                                    </table></td>
                                    <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                      <?php
                                      $QRY2 = "SELECT matakuliah.matakuliah_kode, matakuliah.matakuliah_nama, matakuliah.matakuliah_sks_teori, kelas_aktif_sore.dosen_nip, kelas_aktif_sore.dosen_nip_p, kelas_aktif_sore.dosen3, kelas.kelas_nama FROM kelas_aktif_sore 
                                      JOIN matakuliah ON (kelas_aktif_sore.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama)
                                      JOIN kelas ON (kelas_aktif_sore.kelas_id = kelas.kelas_id)  
                                      WHERE kelas_aktif_sore.semester='$s[semester]' AND (kelas_aktif_sore.dosen_nip = '$item->dosen_nip' OR kelas_aktif_sore.dosen_nip_p = '$item->dosen_nip' OR kelas_aktif_sore.dosen3 = '$item->dosen_nip') ORDER BY matakuliah.matakuliah_nama ASC";
                                      $query2 = $this->db->query($QRY2);
                                      $data2 = $query2->result();
                                      $total_sks2 = 0;
                                      foreach ($data2 as $mk2) {
                                      if($mk2->dosen3=='')
                                      {
                                          if($mk2->dosen_nip==$mk2->dosen_nip_p)
                                            $sks_satuan2 = $mk2->matakuliah_sks_teori;
                                          else
                                            $sks_satuan2 = $mk2->matakuliah_sks_teori/2;
                                      }else{
                                          $sks_satuan2 = $mk2->matakuliah_sks_teori/3;
                                      }
                                      $total_sks2 = $total_sks2 + $sks_satuan2;
                                        ?>
                                            <tr>
                                              <td><?php echo $mk2->matakuliah_nama ?> (<?php echo $mk2->kelas_nama ?>)</td>
                                              <td><div align="center"><?php echo number_format($sks_satuan2,2) ?></div></td>
                                            </tr>
                                        <?php
                                      }

                                      if($total_sks2!=0)
                                      {
                                        ?>
                                        <tr>
                                          <td><b>Total</b></td>
                                          <td><div align="center"><?php echo number_format($total_sks2,2)?></div></td>
                                        </tr>
                                        <?php
                                      }
                                      ?>
                                    </table></td>
                                    <td><div align="center">
                                        <?php
                                        $jumlah_sks = $total_sks + $total_sks2;
                                        if($jumlah_sks!=0)
                                            echo number_format($jumlah_sks,2);
                                        ?>
                                        </div>                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div><!-- /.card-body -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
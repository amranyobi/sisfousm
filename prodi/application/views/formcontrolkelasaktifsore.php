<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <h3><?php echo $heading2; ?></h3>
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon) { ?>
                    <input class="form-control" type="text" name="ids" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Matakuliah</label>
                    <select name="matakuliah" class="form-control select2">
                        <option value="0"> -- Matakuliah -- </option>
                        <?php echo $matakuliah; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control">
                        <option value="0"> -- Kelas -- </option>
                        <?php echo $kelas; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Dosen 1</label>
                    <select name="dosen" class="form-control">
                        <option value="0"> -- Dosen -- </option>
                        <?php echo $dosen; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Dosen 2</label>
                    <select name="dosenp" class="form-control">
                        <option value="0"> -- Dosen -- </option>
                        <?php echo $dosenp; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Dosen 3</label>
                    <select name="dosen3" class="form-control">
                        <option value="0"> -- Dosen -- </option>
                        <?php echo $dosen3; ?>
                    </select>
                </div>
                <!-- <div class="input-block">
                    <label>Permintaan Khusus Ruangan Teori</label>
                    <select name="ruangant" class="form-control">
                        <option value="0"> -- Tidak Ada Permintaan Khusus -- </option>
                        <?php echo $ruangt; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Permintaan Khusus Ruangan Praktek</label>
                    <select name="ruanganp" class="form-control">
                        <option value="0"> -- Tidak Ada Permintaan Khusus -- </option>
                        <?php echo $ruangp; ?>
                    </select>
                </div> -->
                <div class="input-block">
                    <label>Semester</label><br/>
                    <?php $thn = ((int)$this->session->tahunajaran) + 1; echo $this->session->tahunajaran . " / " . $thn; ?>
                    <?php
                        if($this->session->semester == 1){
                            echo " &nbsp; Gasal";
                        } else {
                            echo " &nbsp; Genap";
                        }
                    ?>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
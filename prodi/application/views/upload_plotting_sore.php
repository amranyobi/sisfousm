
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Upload Data Plotting Dosen Sore</h1>
                </div>
                <div class="col-sm-6" style="text-align: right;">
                    <?php echo $path; ?>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
               <form id="quickForm" method="post" action="<?php echo base_url(); ?>aktif/form_sore" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Download Format</label>&nbsp;&nbsp;&nbsp;
                        <a style="color:white" href="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js" class="btn btn-sm btn-primary">Unduh</a>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Upload File</label>
                        <input type="file" name="file" class="form-control col-sm-4">
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" name="preview" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Preview</button>
                </div>
            </form>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    <?php
    if(isset($_POST['preview'])){
    ?>
    <section class="content">
        <div class="container-fluid">
            <div class="card card card-primary">
                <div class="card-body">
                    <?php
                     // Jika user menekan tombol Preview pada form
                      if(isset($upload_error)){ // Jika proses upload gagal
                        echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
                        die; // stop skrip
                      }

                      // Buat sebuah tag form untuk proses import data ke database
                      echo "<form method='post' action='".base_url("aktif/import_sore")."'>";
                      ?>
                      <?php
                      // Buat sebuah div untuk alert validasi kosong
                      // echo "<div style='color: red;' id='kosong'>
                      // Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                      // </div>";
                      // echo "<br>";
                      echo "<strong>Preview Data</strong><br><br>";
                      echo "<table class='table table-bordered table-striped display nowrap'>
                      <tr>
                        <th>KDMK</th>
                        <th>Nama MK</th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>NIP2</th>
                        <th>Nama2</th>
                        <th>SKS</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                        <th>Angkatan</th>
                      </tr>";

                      $numrow = 1;
                      $kosong = 0;

                      // Lakukan perulangan dari data yang ada di excel
                      // $sheet adalah variabel yang dikirim dari controller
                      foreach($sheet as $row){
                        // Ambil data pada excel sesuai Kolom
                        $kode_matakuliah = $row['A'];
                        $matakuliah = $row['B'];
                        $nip = $row['C'];
                        $nama = $row['D'];
                        $nip2 = $row['E'];
                        $nama2 = $row['F'];
                        $sks = $row['G'];
                        $semester = $row['H'];
                        $kelas = $row['I'];
                        $angkatan = $row['J'];

                        // Cek jika semua data tidak diisi
                        if($nip == "" && $nama == ""  && $nip2 == "" && $nama2 == "" && $kode_matakuliah == "" && $matakuliah == "" && $sks == "" && $semester == "" && $kelas == "" && $angkatan == "")
                          continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                        // Cek $numrow apakah lebih dari 1
                        // Artinya karena baris pertama adalah nama-nama kolom
                        // Jadi dilewat saja, tidak usah diimport
                        if($numrow > 1){
                          // Validasi apakah semua data telah diisi
                          $kode_matakuliah_td = ( ! empty($kode_matakuliah))? "" : " style='background: #E07171;'";
                          $matakuliah_td = ( ! empty($matakuliah))? "" : " style='background: #E07171;'";
                          $nip_td = ( ! empty($nip))? "" : " style='background: #E07171;'";
                          $nama_td = ( ! empty($nama))? "" : " style='background: #E07171;'";
                          $nip_td2 = ( ! empty($nip2))? "" : " style='background: #E07171;'";
                          $nama_td2 = ( ! empty($nama2))? "" : " style='background: #E07171;'";
                          $sks_td = ( ! empty($sks))? "" : " style='background: #E07171;'";
                          $semester_td = ( ! empty($semester))? "" : " style='background: #E07171;'";
                          $kelas_td = ( ! empty($kelas))? "" : " style='background: #E07171;'";
                          $angkatan_td = ( ! empty($angkatan))? "" : " style='background: #E07171;'";

                          // Jika salah satu data ada yang kosong
                          if($nip == "" or $nama == "" or $kode_matakuliah == "" or $matakuliah == "" or $semester == "" or $kelas == "" or $angkatan == ""){
                            $kosong++; // Tambah 1 variabel $kosong
                          }

                          echo "<tr>";
                          echo "<td".$kode_matakuliah_td.">".$kode_matakuliah."</td>";
                          echo "<td".$matakuliah_td.">".$matakuliah."</td>";
                          echo "<td".$nip_td.">".$nip."</td>";
                          echo "<td".$nama_td.">".$nama."</td>";
                          echo "<td".$nip_td2.">".$nip2."</td>";
                          echo "<td".$nama_td2.">".$nama2."</td>";
                          echo "<td".$sks_td.">".$sks."</td>";
                          echo "<td".$semester_td.">".$semester."</td>";
                          echo "<td".$kelas_td.">".$kelas."</td>";
                          echo "<td".$angkatan_td.">".$angkatan."</td>";
                          echo "</tr>";
                        }

                        $numrow++; // Tambah 1 setiap kali looping
                      }

                      echo "</table>";

                      // Cek apakah variabel kosong lebih dari 0
                      // Jika lebih dari 0, berarti ada data yang masih kosong
                      if($kosong > 0){
                      ?>
                        <script>
                        $(document).ready(function(){
                          // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                          $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                          $("#kosong").show(); // Munculkan alert validasi kosong
                        });
                        </script>
                      <?php
                      }else{ // Jika semua data sudah diisi
                        echo "<hr>";

                        // Buat sebuah tombol untuk mengimport data ke database
                        echo "<button type='submit' name='import' class='btn btn-sm btn-primary'>Import</button>";
                        echo "  <a href='".base_url("aktif/upload_plotting_sore")."'>Batal</a>";
                      }

                      echo "</form>";
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    }
    ?>

</div>

<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/jszip/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>../AdminLTE//plugins/datatables-buttons/js/buttons.colVis.min.js"></script>


<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>
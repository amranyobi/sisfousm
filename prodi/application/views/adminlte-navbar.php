<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("../hub/login/close"); ?>" class="nav-link">Keluar</a>
            </li><!-- 
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("dashboard"); ?>" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("../hub/profile"); ?>" class="nav-link">Profile</a>
            </li> -->
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
            <!-- EDIT HERE -->
                <!-- <select onchange="">
                    <option value="">Nama Prodi</option>
                    <option value="">Nama Prodi</option>
                </select> -->
            </li>
            <li class="nav-item dropdown">
                <a title="logout" class="nav-link" href="<?php echo base_url("../hub/login/close"); ?>">
                    <i class="far fa-times-circle"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?php echo base_url(""); ?>" class="brand-link">
            <img src="<?php echo CORE_FOLDER; ?>/img/usm-psikologi.png"
                 alt="PSS Logo"
                 class="brand-image"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">PROGRAM STUDI</span>
        </a>

        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?php echo CORE_FOLDER; ?>/img/avatar.php?c=<?php echo $this->session->user; ?>" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <!-- <a href="<?php echo base_url("../hub/profile"); ?>" class="d-block"> -->
                    <a href="#" class="d-block">
                    <?php
                    //echo $this->session->name;
                    echo "Admin Psikologi"
                    ?></a>
                </div>
            </div>

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="<?php echo base_url("dashboard"); ?>" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-database"></i>
                            <p>
                                Master Data
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left:15px">
                            <li class="nav-item">
                                <a href="<?php echo base_url("kelas"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kelas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("matakuliah"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Matakuliah</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("mahasiswa"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Mahasiswa</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("dosen"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Dosen</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-sitemap"></i>
                            <p>
                                Penjadwalan
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview" style="margin-left: 15px;">
                            <li class="nav-item">
                                <a href="<?php echo base_url("aktif/plotting_pagi"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Plotting Jadwal Pagi</p>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="<?php echo base_url("aktif/jadwal_pagi"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jadwal Kuliah Pagi</p>
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a href="<?php echo base_url("aktif/plotting_sore"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Plotting Jadwal Sore</p>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="<?php echo base_url("aktif_sore/jadwal_sore"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jadwal Kuliah Sore</p>
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a href="<?php echo base_url("jadwal/dosen_pengampu/1"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Rekap Dosen Pengampu</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("jadwal/beban_mengajar"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Rekap Beban Mengajar</p>
                                </a>
                            </li>
                            <!-- <li class="nav-item">
                                <a href="<?php echo base_url("jadwal/rekap_jadwal"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Rekap Jadwal Mengajar</p>
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a href="<?php echo base_url("jadwal/ploting_jadwal"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jadwal Mengajar Pagi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("jadwal_sore/ploting_jadwal"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Jadwal Mengajar Sore</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="nav-item">
                        <a href="<?php echo base_url("aktif"); ?>" class="nav-link">
                            <i class="nav-icon fas fa-file"></i>
                            <p>Perkuliahan Aktif</p>
                        </a>
                    </li>
                    <!-- 
                    <li class="nav-item">
                        <a href="<?php echo base_url("jadwal"); ?>" class="nav-link">
                            <i class="nav-icon fas fa-database"></i>
                            <p>Jadwal Perkuliahan</p>
                        </a>
                    </li> -->
                    <!-- <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            <p>
                                Pelaporan
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url("pbm"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Laporan PBM</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("penilaian"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penilaian</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("survei"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Survei</p>
                                </a>
                            </li>
                        </ul>
                    </li> -->
                </ul>
            </nav>
        </div>
    </aside>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Dosen', $heading2 = 'Dosen', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function page($page) {
        $this->initialize();
        $this->load->model('dosen_mdl');
        $page = (int) $page;
        $title = ' | Dosen';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('dosen');
        $type = 'page';
        $heading1 = 'Dosen';
        $heading2 = 'Daftar Dosen';
        $content = '';

        $ex = $this->dosen_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $dosens = $this->dosen_mdl->get_list($maxdata, $page);
        $i = 0;
        $datadosen;
        $dataheading;
        $datalink;

        $dataheading[0] = "NIDN";
        $dataheading[1] = "DOSEN";
        $dataheading[2] = "VIEW";

        $datadosen[$i][0] = "";
        $datadosen[$i][1] = "";
        $datadosen[$i][2] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";

        foreach ($dosens as $dosen) {
            $datadosen[$i][0] = $dosen->dosen_nidn;
            $datadosen[$i][1] = $dosen->dosen_nama . "<br/><span class='subdata-table subdata'>NIP. " . $dosen->dosen_nip . "</span>";
            $datadosen[$i][2] = "<span class='fa fa-search'></span>";

            $datalink[$i][0] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $datalink[$i][1] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $datalink[$i][2] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datadosen, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("dosen", false);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "dosen/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function index() {
        $this->page(1);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('dosen_mdl');
        $title = ' | Search - Dosen';
        $type = 'page';
        $heading1 = 'Dosen';
        $heading2 = 'Pencarian Dosen';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('dosen');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $dosens = $this->dosen_mdl->search($keyword, $maxdata);
        $i = 0;
        $datadosen;
        $dataheading;
        $datalink;

        $dataheading[0] = "NIDN";
        $dataheading[1] = "DOSEN";
        $dataheading[2] = "VIEW";

        $datadosen[$i][0] = "";
        $datadosen[$i][1] = "";
        $datadosen[$i][2] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";

        foreach ($dosens as $dosen) {
            $datadosen[$i][0] = $dosen->dosen_nidn;
            $datadosen[$i][1] = $dosen->dosen_nama . "<br/><span class='subdata-table subdata'>NIP. " . $dosen->dosen_nip . "</span>";
            $datadosen[$i][2] = "<span class='fa fa-search'></span>";

            $datalink[$i][0] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $datalink[$i][1] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $datalink[$i][2] = base_url() . "dosen/edit/" . $dosen->dosen_nip;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datadosen, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function edit($nip) {
        $this->initialize();
        $extra = "formcontroldosen";
        $data = null;
        $editon = true;
        $title = "| Dosen";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('dosen') . " / Perbaharuan Data";
        $form = "" . base_url() . "dosen/edit_submit";
        $h1 = "Formulir Dosen";
        $h2 = "Formulir Perbaharuan Dosen";

        $this->load->model('dosen_mdl');
        $dosen = $this->dosen_mdl->get_item($nip);

        $data[0][0] = "nip";
        $data[0][1] = "" . $dosen->dosen_nip;
        $data[1][0] = "nidn";
        $data[1][1] = "" . $dosen->dosen_nidn;
        $data[2][0] = "nama";
        $data[2][1] = "" . $dosen->dosen_nama;
        $data[3][0] = "status";
        $data[3][1] = "" . $dosen->dosen_status;
        $data[4][0] = "jarak";
        $data[4][1] = "" . $dosen->dosen_jarak;
        $data[5][0] = "homebase";
        $data[5][1] = "";
        $data[6][0] = "lang";
        $data[6][1] = "";
        $data[7][0] = "lat";
        $data[7][1] = "";

        $this->load->model('prodi_mdl');
        $datas = $this->prodi_mdl->get_item($dosen->homebase);
        $data[5][1] = "" . $datas->nama_prodi . " (" . $datas->jenjang_prodi . ")";

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nip = $this->input->post('nip') . '';
        $nidn = $this->input->post('nidn') . '';
        $nama = $this->input->post('nama') . '';
        $status = $this->input->post('status') . '';
        $homebase = $this->input->post('homebase') . '';
        $lang = 0;
        $lat = 0;
        $jarak = 0;

        $nip = str_replace(" ", "", $nip);
        $nip = str_replace("-", "", $nip);

        $this->load->model('dosen_mdl');
        $dosen = $this->dosen_mdl->add($nip, $nidn, $nama, $status, $homebase);

        redirect(base_url('dosen/edit/' . $nip));
    }

    public function edit_submit() {
        $this->initialize();
        $nip = $this->input->post('kode') . '';
        $nama = $this->input->post('nama') . '';
        $status = $this->input->post('status') . '';
        $homebase = $this->input->post('homebase') . '';
        $lang = 0;
        $lat = 0;
        $jarak = 0;

        $this->load->model('dosen_mdl');
        $dosen = $this->dosen_mdl->edit($nip, $nidn, $nama, $status, $homebase);

        redirect(base_url('dosen'));
    }

}

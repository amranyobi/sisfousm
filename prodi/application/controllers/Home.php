<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private function initialized() {
    }

    public function index() {
        $this->initialized();
        $this->load->view('welcome', $data);
    }

}

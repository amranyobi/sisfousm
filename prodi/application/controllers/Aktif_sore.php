<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aktif_sore extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kelas', $heading2 = 'Kelas', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    public function page($page) {
        $this->initialize();
        $this->load->model('aktif_sore_mdl');
        $page = (int) $page;
        $title = ' | Plotting Perkuliahan Sore';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif_sore');
        $type = 'page';
        $heading1 = 'Plotting Perkuliahan Sore';
        $heading2 = 'Data Plotting Perkuliahan Sore';
        $content = '';

        $ex = $this->aktif_sore_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->aktif_sore_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "MATAKULIAH";
        $dataheading[2] = "KELAS";
        $dataheading[3] = "PENGAMPU";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->matakuliah_nama . " (" . $item->matakuliah_kode . ")";
            $datag[$i][2] = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
            $datag[$i][3] = $item->dosen_nama . " (" . $item->dosen_nip . ")";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "aktif_sore/edit/" . $item->aktif_id;
            $datalink[$i][1] = base_url() . "aktif_sore/edit/" . $item->aktif_id;
            $datalink[$i][2] = base_url() . "aktif_sore/edit/" . $item->aktif_id;
            $datalink[$i][3] = base_url() . "aktif_sore/edit/" . $item->aktif_id;
            $datalink[$i][4] = base_url() . "aktif_sore/edit/" . $item->aktif_id;
            $datalink[$i][5] = base_url() . "aktif_sore/delete/" . $item->aktif_id;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("aktif_sore", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "aktif_sore/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('aktif_mdl');
        $title = ' | Search - Kelas';
        $type = 'page';
        $heading1 = 'Kelas';
        $heading2 = 'Pencarian Kelas';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('aktif');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $items = $this->aktif_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "MATAKULIAH";
        $dataheading[2] = "KELAS";
        $dataheading[3] = "PENGAMPU";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->matakuliah_nama . " (" . $item->matakuliah_kode . ")";
            $datag[$i][2] = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
            $datag[$i][3] = $item->dosen_nama . " (" . $item->dosen_nip . ")";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "aktif/edit/" . $item->aktif_id;
            $datalink[$i][1] = base_url() . "aktif/edit/" . $item->aktif_id;
            $datalink[$i][2] = base_url() . "aktif/edit/" . $item->aktif_id;
            $datalink[$i][3] = base_url() . "aktif/edit/" . $item->aktif_id;
            $datalink[$i][4] = base_url() . "aktif/edit/" . $item->aktif_id;
            $datalink[$i][5] = base_url() . "aktif/delete/" . $item->aktif_id;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kode) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data kelas aktif, lanjutkan?")){
                    window.location = "' . base_url() . 'aktif/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'aktif";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('aktif_sore_mdl');
        $this->aktif_sore_mdl->delete($kode);
        redirect(base_url('aktif/plotting_sore'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolkelasaktifsore";
        $editon = false;
        $title = " | Plotting Perkuliahan Sore";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif_sore') . " / Tambah Data";
        $form = "" . base_url() . "aktif_sore/add_submit";
        $h1 = "Tambah Plotting Perkuliahan Sore";
        $h2 = "";

        $data[0][0] = "prodi";
        $data[0][1] = "";
        $data[1][0] = "dosen";
        $data[1][1] = "";
        $data[2][0] = "kelas";
        $data[2][1] = "";
        $data[3][0] = "matakuliah";
        $data[3][1] = "";
        $data[4][0] = "semester";
        $data[4][1] = "";
        $data[5][0] = "dosenp";
        $data[5][1] = "";
        
        $data[6][0] = "ruangt";
        $data[6][1] = "";
        $data[7][0] = "ruangp";
        $data[7][1] = "";
        $data[8][0] = "dosen3";
        $data[8][1] = "";

        $this->load->model('kelas_mdl');
        $datas = $this->kelas_mdl->get_list(2000, 1);
        foreach ($datas as $datax) {
            $data[2][1] = $data[2][1] . "<option value='" . $datax->kelas_id . "'>" . $datax->kelas_nama . " (" . $datax->kelas_angkatan . ")</option>";
        }

        $this->load->model('matakuliah_mdl');
        $datas = $this->matakuliah_mdl->list_mk_order_nama();
        foreach ($datas as $datax) {
            $data[3][1] = $data[3][1] . "<option value='" . $datax->matakuliah_kode_utama . "'>" . $datax->matakuliah_nama . " (" . $datax->matakuliah_kode . ")</option>";
        }

        $this->load->model('dosen_mdl');
        $datas = $this->dosen_mdl->get_list_by_name();
        foreach ($datas as $datax) {
            $data[1][1] = $data[1][1] . "<option value='" . $datax->dosen_nip . "'>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
        }
        $data[5][1] = $data[1][1];
        $data[8][1] = $data[1][1];

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolkelasaktifsore";
        $data = null;
        $editon = true;
        $title = "| Plotting Perkuliahan Sore";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif_sore') . " / Update Data";
        $form = "" . base_url() . "aktif_sore/edit_submit";
        $h1 = "Update Plotting Perkuliahan Sore";
        $h2 = "";

        $this->load->model('aktif_sore_mdl');
        $item = $this->aktif_sore_mdl->get_item($id);

        $data[0][0] = "prodi";
        $data[0][1] = "";
        $data[1][0] = "dosen";
        $data[1][1] = "";
        $data[2][0] = "kelas";
        $data[2][1] = "";
        $data[3][0] = "matakuliah";
        $data[3][1] = "";
        $data[4][0] = "semester";
        $data[4][1] = "";
        $data[5][0] = "ids";
        $data[5][1] = "" . $id;
        $data[6][0] = "dosenp";
        $data[6][1] = "";
        
        $data[7][0] = "ruangt";
        $data[7][1] = "";
        $data[8][0] = "ruangp";
        $data[8][1] = "";
        $data[9][0] = "dosen3";
        $data[9][1] = "";

        $this->load->model('kelas_mdl');
        $datas = $this->kelas_mdl->get_list(2000, 1);
        foreach ($datas as $datax) {
            if ($item->kelas_id == $datax->kelas_id) {
                $data[2][1] = $data[2][1] . "<option value='" . $datax->kelas_id . "' selected>" . $datax->kelas_nama . " (" . $datax->kelas_angkatan . ")</option>";
            } else {
                $data[2][1] = $data[2][1] . "<option value='" . $datax->kelas_id . "'>" . $datax->kelas_nama . " (" . $datax->kelas_angkatan . ")</option>";
            }
        }

        $this->load->model('matakuliah_mdl');
        $datas = $this->matakuliah_mdl->get_list("", 2000, 1);
        foreach ($datas as $datax) {
            if ($item->matakuliah_kode_utama == $datax->matakuliah_kode_utama) {
                $data[3][1] = $data[3][1] . "<option value='" . $datax->matakuliah_kode_utama . "' selected>" . $datax->matakuliah_nama . " (" . $datax->matakuliah_kode . ")</option>";
            } else {
                $data[3][1] = $data[3][1] . "<option value='" . $datax->matakuliah_kode_utama . "'>" . $datax->matakuliah_nama . " (" . $datax->matakuliah_kode . ")</option>";
            }
        }

        $this->load->model('dosen_mdl');
        $datas = $this->dosen_mdl->get_list_by_name(2000, 1);
        foreach ($datas as $datax) {
            if ($item->dosen_nip == $datax->dosen_nip) {
                $data[1][1] = $data[1][1] . "<option value='" . $datax->dosen_nip . "' selected>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            } else {
                $data[1][1] = $data[1][1] . "<option value='" . $datax->dosen_nip . "'>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            }
            if ($item->dosen_nip_p == $datax->dosen_nip) {
                $data[6][1] = $data[6][1] . "<option value='" . $datax->dosen_nip . "' selected>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            } else {
                $data[6][1] = $data[6][1] . "<option value='" . $datax->dosen_nip . "'>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            }

            if ($item->dosen3 == $datax->dosen_nip) {
                $data[9][1] = $data[9][1] . "<option value='" . $datax->dosen_nip . "' selected>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            } else {
                $data[9][1] = $data[9][1] . "<option value='" . $datax->dosen_nip . "'>" . $datax->dosen_nama . " (" . $datax->dosen_nip . ")</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $matakuliah = $this->input->post('matakuliah') . '';
        $kelas = $this->input->post('kelas') . '';
        $dosen = $this->input->post('dosen') . '';
        $dosenp = $this->input->post('dosenp') . '';
        // $dosenp = $this->input->post('dosen') . '';
        $dosen3 = $this->input->post('dosen3') . '';
        $prodi = $this->session->kodeaktif . '';
        $semester = $this->session->tahunajaran . '' . $this->session->semester;

        $this->load->model('aktif_sore_mdl');
        $this->aktif_sore_mdl->add($prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3);

        redirect(base_url('aktif/plotting_sore'));
    }

    public function edit_submit() {
        $this->initialize();
        $ids = $this->input->post('ids') . '';
        $matakuliah = $this->input->post('matakuliah') . '';
        $kelas = $this->input->post('kelas') . '';
        $dosen = $this->input->post('dosen') . '';
        $dosenp = $this->input->post('dosenp') . '';
        $dosen3 = $this->input->post('dosen3') . '';
        $prodi = $this->session->kodeaktif . '';
        $semester = $this->session->tahunajaran . '' . $this->session->semester;

        $this->load->model('aktif_sore_mdl');
        $this->aktif_sore_mdl->edit($ids, $prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3);

        redirect(base_url('aktif/plotting_sore'));
    }

    public function jadwal_sore() {
        $this->initialize();
        $this->load->model('Aktif_sore_mdl');
        // $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Beban Mengajar Dosen';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $data['path'] = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $data['title'] = ' | Jadwal Perkuliahan Sore';

        $data['items'] = $this->Aktif_sore_mdl->jadwal_sore();

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar');
        $this->load->view('jadwal_sore', $data);
        $this->load->view('adminlte-footer');
    }

    public function tambah_jadwal_sore() {
        $this->initialize();
        $this->load->model('Aktif_sore_mdl');
        $this->load->model('Aktif_mdl');
        // $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Beban Mengajar Dosen';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $data['path'] = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $data['title'] = ' | Tambah Jadwal Perkuliahan Sore';
        $data['data_sore'] = $this->Aktif_mdl->plotting_sore_belum();
        $data['ruangan'] = $this->Aktif_mdl->ruangan();

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar');
        $this->load->view('tambah_jadwal_sore', $data);
        $this->load->view('adminlte-footer');
    }

    public function simpan_tambah_jadwal(){
        $this->initialize();
        $this->load->model('Aktif_sore_mdl');
        $i      = $this->input;
        $data = array('jadwal_hari'=> $i->post('jadwal_hari'),
            'jadwal_jam_mulai'      => $i->post('jadwal_jam_mulai'),
            'jadwal_jam_berakhir'   => $i->post('jadwal_jam_berakhir'),
            'aktif_id'         => $i->post('aktif_id'),
            'jenis'       => 1,
            'ruangan'            => $i->post('ruangan')
        );
        $this->Aktif_sore_mdl->simpan_tambah_jadwal($data);
        $this->Aktif_sore_mdl->ubah_status_aktif($i->post('aktif_id'));
        $this->session->set_flashdata('sukses', 'Jadwal sukses ditambah');
        redirect(base_url('aktif_sore/jadwal_sore'),'refresh');    
    }

}

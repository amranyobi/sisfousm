<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_sore extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kelas', $heading2 = 'Kelas', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    public function page($page = 1) {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $page = (int) $page;
        $title = ' | Jadwal Perkuliahan Sore';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('jadwal_sore');
        $type = 'page';
        $heading1 = 'Jadwal Perkuliahan Sore';
        $heading2 = '';
        $content = '';

        $content = $content . "<div style='float: left;'>";
        $content = $content . "&nbsp;&nbsp;<a href='" . base_url("jadwal_sore") . "/autogenerator' style='color: #ffffff;' class='btn btn-success add-btn'><span class='fa fa-database'></span> Generate Jadwal</a>";
        $content = $content . "</div>";

        $ex = $this->jadwal_sore_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->jadwal_sore_mdl->get_list_d($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "MATAKULIAH";
        $dataheading[2] = "PENGAMPU";
        $dataheading[3] = "KELAS";
        $dataheading[4] = "HARI";
        $dataheading[5] = "JAM";
        $dataheading[6] = "RUANG";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";
        $datag[$i][6] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";
        $headingstyles[6] = "";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->jenis . "." . $item->matakuliah_nama . " (" . $item->matakuliah_kode . ")";
            $datag[$i][2] = "";
            if ($item->jenis == "T") {
                $datag[$i][2] = $item->d_nama_teori . " (" . $item->d_nip_teori . ")";
            } else {
                $datag[$i][2] = $item->d_nama_praktek . " (" . $item->d_nip_praktek . ")";
            }
            $datag[$i][3] = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
            $datag[$i][4] = $item->jadwal_hari . "";
            $datag[$i][5] = $item->jadwal_jam_mulai . "-" . $item->jadwal_jam_berakhir;
            $datag[$i][6] = $item->ruangan_nama;

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = "";
            $datalink[$i][4] = "";
            $datalink[$i][5] = "";
            $datalink[$i][6] = "";
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("jadwal_sore", false);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "jadwal_sore/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $title = ' | Search - Kelas';
        $type = 'page';
        $heading1 = 'Kelas';
        $heading2 = 'Pencarian Kelas';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('jadwal');
        $q1 = str_replace("_", " ", $keyword);
        $content = '';


        $content = $content . "<br/><div style='text-align: left; width: 100%;padding-bottom: 5px;'>";
        $content = $content . '<div style="float: right; padding-top: 5px;" class="search-keyword">Keyword Pencarian : ' . $q1 . '</div>';
        $content = $content . "&nbsp;&nbsp;<a href='" . base_url("jadwal") . "/autogenerator' style='color: #ffffff;' class='btn btn-success add-btn'><span class='fa fa-database'></span> Generate Jadwal</a>";
        $content = $content . "</div>";

        $items = $this->jadwal_sore_mdl->search_d($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "MATAKULIAH";
        $dataheading[2] = "PENGAMPU";
        $dataheading[3] = "KELAS";
        $dataheading[4] = "HARI";
        $dataheading[5] = "JAM";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->jenis . "." . $item->matakuliah_nama . " (" . $item->matakuliah_kode . ")";
            $datag[$i][2] = "";
            if ($item->jenis == "T") {
                $datag[$i][2] = $item->d_nama_teori . " (" . $item->d_nip_teori . ")";
            } else {
                $datag[$i][2] = $item->d_nama_praktek . " (" . $item->d_nip_praktek . ")";
            }
            $datag[$i][3] = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
            $datag[$i][4] = $item->jadwal_hari . "";
            $datag[$i][5] = $item->jadwal_jam_mulai . "-" . $item->jadwal_jam_berakhir;

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = "";
            $datalink[$i][4] = "";
            $datalink[$i][5] = "";
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function autogenerator() {
        $this->initialize();
        $extra = "formcontroljadwalsore";
        $editon = false;
        $title = " | Jadwal Perkuliahan Sore";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('jadwal_sore') . " / Generate Jadwal";
        $form = "";
        $h1 = "Jadwal Perkuliahan Sore";
        $h2 = "Generate Jadwal Perkuliahan Sore";
        $this->load->model('jadwal_sore_mdl');

        $content = '';
        $JAM_PERHARI = 16;
        $RUANGAN_JUMLAH = 0;
        // $JAM_MAP[0] = "07.00 - 07.45";
        // $JAM_MAP[1] = "07.45 - 08.30";
        // $JAM_MAP[2] = "08.30 - 09.15";
        // $JAM_MAP[3] = "09.35 - 10.20";
        // $JAM_MAP[4] = "10.20 - 11.05";
        // $JAM_MAP[5] = "11.05 - 11.50";
        // $JAM_MAP[6] = "12.30 - 13.15";
        // $JAM_MAP[7] = "13.15 - 14.00";
        // $JAM_MAP[8] = "14.00 - 14.45";
        // $JAM_MAP[9] = "14.45 - 15.30";
        // $JAM_MAP[10] = "16.00 - 16.45";
        // $JAM_MAP[11] = "16.45 - 17.30";
        // $JAM_MAP[12] = "17.30 - 18.15";
        // $JAM_MAP[13] = "18.45 - 19.30";
        // $JAM_MAP[14] = "19.30 - 20.15";
        // $JAM_MAP[15] = "20.15 - 21.00";
        // $JAM_MAP[16] = "21.00 - 21.45";

        $JAM_MAP[0] = "17.00 - 17.30";
        $JAM_MAP[1] = "17.30 - 18.00";
        $JAM_MAP[2] = "18.00 - 18.30";
        $JAM_MAP[3] = "18.30 - 19.00";
        $JAM_MAP[4] = "19.00 - 19.30";
        $JAM_MAP[5] = "19.30 - 20.00";
        $JAM_MAP[6] = "20.00 - 20.30";
        $JAM_MAP[7] = "20.30 - 21.00";

        $HARI_JUMLAH = 5;
        $HARI_MAP[0] = "Senin";
        $HARI_MAP[1] = "Selasa";
        $HARI_MAP[2] = "Rabu";
        $HARI_MAP[3] = "Kamis";
        $HARI_MAP[4] = "Jumat";
        $KODE_JUMAT = 4;
        $EKSEPSI_JUMAT = 6;

        $items = $this->jadwal_sore_mdl->ruangan_list();
        $RUANGAN_MAP;
        foreach ($items as $item) {
            $RUANGAN_MAP[$RUANGAN_JUMLAH][0] = $item->ruangan_id;
            $RUANGAN_MAP[$RUANGAN_JUMLAH][1] = $item->ruangan_nama;
            $RUANGAN_JUMLAH ++;
        }

        $content = $content . '<table style="vertical-align: top;">';
        $content = $content . '<tr style="vertical-align: top;">';
        $content = $content . '<td style="width: 860px;vertical-align: top;"><h4>PENJADWALAN</h4>';

        for ($k = 0; $k < $HARI_JUMLAH; $k++) {
            $content = $content . '<div style="padding: 5px;"><br/>';
            $content = $content . $HARI_MAP[$k];
            $content = $content . '</div>';

            $content = $content . '<table>';
            $content = $content . '<tr class="tdheading">';
            $content = $content . '<td class="tdheading" style="width: 150px;" rowspan="2">RUANGAN</td>';
            $content = $content . '<td class="tdheading" style="width: 700px" colspan="' . $JAM_PERHARI . '">JAM KE-</td>';
            $content = $content . '</tr>';
            $content = $content . '<tr class="tdheading">';
            for ($j = 1; $j <= $JAM_PERHARI; $j++) {
                $content = $content . '<td class="tdheading">' . $j . '</td>';
            }
            $content = $content . '</tr>';
            for ($i = 0; $i < $RUANGAN_JUMLAH; $i++) {
                $content = $content . '<tr>';
                $content = $content . '<td class="tdleftdata">';
                $content = $content . '<div id="dkd-' . $k . '-' . $RUANGAN_MAP[$i][0] . '" style="display: none;">0</div>';
                $content = $content . '<div>&nbsp;&nbsp;' . $RUANGAN_MAP[$i][1] . '</div>';
                $content = $content . '</td>';
                for ($j = 0; $j < $JAM_PERHARI; $j++) {
                    if ($k == $KODE_JUMAT) {
                        if ($j == $EKSEPSI_JUMAT) {
                            $content = $content . '<td style="background-color: red;" id="d-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '" class="tddata">';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-0" style="display: none;">-</div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-1" style="display: none;">-</div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-2" style="display: none;">-</div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-3" style="display: none;">-</div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-4">-</div>';
                            $content = $content . '</td>';
                        } else {
                            $content = $content . '<td id="d-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '" class="tddata">';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-0" style="display: none;"></div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-1" style="display: none;"></div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-2" style="display: none;"></div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-3" style="display: none;"></div>';
                            $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-4"></div>';
                            $content = $content . '</td>';
                        }
                    } else {
                        $content = $content . '<td id="d-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '" class="tddata">';
                        $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-0" style="display: none;"></div>';
                        $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-1" style="display: none;"></div>';
                        $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-2" style="display: none;"></div>';
                        $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-3" style="display: none;"></div>';
                        $content = $content . '<div id="dk-' . $k . '-' . $RUANGAN_MAP[$i][0] . '-' . $j . '-4"></div>';
                        $content = $content . '</td>';
                    }
                }
                $content = $content . '</tr>';
            }
            $content = $content . '</table>';
        }

        $content = $content . '</td>';
        $content = $content . '<td style="width: 150px;vertical-align: top;">';
        $content = $content . '<h4 style="text-align: center;">KELAS AKTIF</h4>';
        $content = $content . '<div id="ka-data"></div>';
        $content = $content . '</td>';
        $content = $content . '</tr>';
        $content = $content . '</table>';

        $data[0][0] = "ruangan";
        $data[0][1] = "" . $RUANGAN_JUMLAH;
        $data[1][0] = "jam";
        $data[1][1] = "" . $JAM_PERHARI;
        $data[2][0] = "hari";
        $data[2][1] = "" . $HARI_JUMLAH;
        $data[3][0] = "peta_ruang";
        $data[3][1] = "";

        for ($i = 0; $i < $RUANGAN_JUMLAH; $i++) {
            $data[3][1] = $data[3][1] . "<div id='rd-" . $i . "-0'>" . $RUANGAN_MAP[$i][0] . "</div>";
            $data[3][1] = $data[3][1] . "<div id='rd-" . $i . "-1'>" . $RUANGAN_MAP[$i][1] . "</div>";
            $data[3][1] = $data[3][1] . "<div id='rd-" . $i . "-2'>0</div>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2, $content);
    }

    public function restart() {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->clean();
        echo "Inisialisasi Berhasil ...";
    }

    public function loader_a() {
        $x = 0;
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $items = $this->jadwal_sore_mdl->get_list_a(5, 1);
        $IDS = "";
        foreach ($items as $item) {
            $x = 1;
            $KA_ID = $item->aktif_id;
            $KA_JT = $item->matakuliah_jam_teori;
            $KA_JP = $item->matakuliah_jam_praktek;

            if ($KA_JT > 0) {
                $this->jadwal_sore_mdl->add_ka($KA_ID, "T", $KA_JT);
            }

            if ($KA_JP > 0) {
                $this->jadwal_sore_mdl->add_ka($KA_ID, "P", $KA_JP);
            }

            $this->jadwal_sore_mdl->update_list_a($KA_ID);
        }
        echo $x;
    }

    public function loader_b() {
        $JSON = '{"data": [';
        $JML = 0;
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $items = $this->jadwal_sore_mdl->get_list_b(2000, 1);
        foreach ($items as $item) {
            $JSON = str_replace("[#KOMA#]", ", ", $JSON);

            $MKNM = str_replace("'", "", $item->matakuliah_nama);
            $DNNM = str_replace("'", "", $item->dosen_nama);
            $KLNM = str_replace("'", "", $item->kelas_nama);

            $MKNM = str_replace('"', "", $MKNM);
            $DNNM = str_replace('"', "", $DNNM);
            $KLNM = str_replace('"', "", $KLNM);

            $JSON = $JSON . '{';
            $JSON = $JSON . '"kode": "' . $item->algoritma_id . '", ';
            $JSON = $JSON . '"matakuliah": "' . $MKNM . '", ';
            $JSON = $JSON . '"dosen": "' . $DNNM . '", ';
            $JSON = $JSON . '"kelas": "' . $KLNM . '", ';
            $JSON = $JSON . '"jenis": "' . $item->algoritma_jenis . '", ';
            $JSON = $JSON . '"jam": "' . $item->algoritma_jam . '"';
            $JSON = $JSON . '}[#KOMA#]';
            $JML++;
        }
        $JSON = str_replace("[#KOMA#]", "", $JSON);
        $JSON = $JSON . '], "jumlah": "' . $JML . '"';
        $JSON = $JSON . '}';
        echo $JSON;
    }

    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->delete_jadwal($kode);
        redirect(base_url('aktif_sore/jadwal_sore'));
    }

    public function uploader_a($kode, $hari, $jam, $ruangan) {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $item = $this->jadwal_sore_mdl->get_jadwal_a($kode);

        $durasi = $item->algoritma_jam + $jam - 1;

        // $JAM_MAP[0][0] = "07:00:00";
        // $JAM_MAP[1][0] = "07:45:00";
        // $JAM_MAP[2][0] = "08:30:00";
        // $JAM_MAP[3][0] = "09:35:00";
        // $JAM_MAP[4][0] = "10:20:00";
        // $JAM_MAP[5][0] = "11:05:00";
        // $JAM_MAP[6][0] = "12:30:00";
        // $JAM_MAP[7][0] = "13:15:00";
        // $JAM_MAP[8][0] = "14:00:00";
        // $JAM_MAP[9][0] = "14:45:00";
        // $JAM_MAP[10][0] = "16:00:00";
        // $JAM_MAP[11][0] = "16:45:00";
        // $JAM_MAP[12][0] = "17:30:00";
        // $JAM_MAP[13][0] = "18:45:00";
        // $JAM_MAP[14][0] = "19:30:00";
        // $JAM_MAP[15][0] = "20:15:00";
        // $JAM_MAP[16][0] = "21:00:00";

        // $JAM_MAP[0][1] = "07:45:00";
        // $JAM_MAP[1][1] = "08:30:00";
        // $JAM_MAP[2][1] = "09:15:00";
        // $JAM_MAP[3][1] = "10:20:00";
        // $JAM_MAP[4][1] = "11:05:00";
        // $JAM_MAP[5][1] = "11:50:00";
        // $JAM_MAP[6][1] = "13:15:00";
        // $JAM_MAP[7][1] = "14:00:00";
        // $JAM_MAP[8][1] = "14:45:00";
        // $JAM_MAP[9][1] = "15:30:00";
        // $JAM_MAP[10][1] = "16:45:00";
        // $JAM_MAP[11][1] = "17:30:00";
        // $JAM_MAP[12][1] = "18:15:00";
        // $JAM_MAP[13][1] = "19:30:00";
        // $JAM_MAP[14][1] = "20:15:00";
        // $JAM_MAP[15][1] = "21:00:00";
        // $JAM_MAP[16][1] = "21:45:00";

        $JAM_MAP[0][0] = "17:00:00";
        $JAM_MAP[1][0] = "17:30:00";
        $JAM_MAP[2][0] = "18:00:00";
        $JAM_MAP[3][0] = "18:30:00";
        $JAM_MAP[4][0] = "19:00:00";
        $JAM_MAP[5][0] = "19:30:00";
        $JAM_MAP[6][0] = "20:00:00";
        $JAM_MAP[7][0] = "20:30:00";

        $JAM_MAP[0][1] = "17:30:00";
        $JAM_MAP[1][1] = "18:00:00";
        $JAM_MAP[2][1] = "18:30:00";
        $JAM_MAP[3][1] = "19:00:00";
        $JAM_MAP[4][1] = "19:30:00";
        $JAM_MAP[5][1] = "20:00:00";
        $JAM_MAP[6][1] = "20:30:00";
        $JAM_MAP[7][1] = "21:00:00";


        $HARI_MAP[0] = "Senin";
        $HARI_MAP[1] = "Selasa";
        $HARI_MAP[2] = "Rabu";
        $HARI_MAP[3] = "Kamis";
        $HARI_MAP[4] = "Jumat";
        $HARI_MAP[5] = "Sabtu";

        if ($hari == 4) {
            if (($jam <= 6) && ($durasi >= 6)) {
                if ($jam == 6) {
                    $durasi++;
                    $jam++;
                } else {
                    $durasi++;
                }
            }
        }

        $this->jadwal_sore_mdl->update_jadwal_a($hari, $jam, $ruangan, $item->aktif_id, $item->algoritma_id);
        // $this->jadwal_mdl->update_jadwal_b($HARI_MAP[$hari], $JAM_MAP[$jam][0], $JAM_MAP[$durasi][1], $item->aktif_id, $item->algoritma_jenis);
        $this->jadwal_sore_mdl->update_jadwal_b($HARI_MAP[$hari], $JAM_MAP[$jam][0], $JAM_MAP[$durasi][1], $item->aktif_id, $item->algoritma_jenis,$ruangan);
    }

    public function ploting_jadwal() {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        // $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Jadwal Mengajar';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $data['path'] = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $data['title'] = ' | Jadwal Perkuliahan Pagi';

        $data['ruang'] = $this->jadwal_sore_mdl->ruangan_list();
        $data['belum_terjadwal'] = $this->jadwal_sore_mdl->belum_terjadwal();
        $data['hari_selanjutnya'] = $this->jadwal_sore_mdl->hari_selanjutnya();

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar');
        $this->load->view('ploting_jadwal_sore', $data);
        $this->load->view('adminlte-footer');
    }

    public function cetak_jadwal() {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        // $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Jadwal Mengajar';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $data['path'] = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $data['title'] = ' | Jadwal Perkuliahan Sore';

        $data['ruang'] = $this->jadwal_sore_mdl->ruangan_list();
        $data['belum_terjadwal'] = $this->jadwal_sore_mdl->belum_terjadwal();
        $data['hari_selanjutnya'] = $this->jadwal_sore_mdl->hari_selanjutnya();

        // $this->load->view('adminlte-head', $data);
        // $this->load->view('adminlte-navbar');
        $this->load->view('cetak_jadwal_sore', $data);
        // $this->load->view('adminlte-footer');
    }

    public function isikan_jadwal($dosen,$dosen2="",$dosen3="",$kelas_id,$kelas_aktif,$id_bantu){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->isikan_jadwal($dosen,$dosen2,$dosen3,$kelas_id,$kelas_aktif,$id_bantu);
    }

    public function simpan_tambah_jadwal(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $i      = $this->input;

        // $id_bantu = $i->post('id_bantu');
        $aktif_id = $i->post('aktif_id');

        // $ambil = $this->jadwal_mdl->cek_jadwal_bantu($id_bantu);
        // $hari = $ambil['hari'];
        // $ruangan = $ambil['ruangan'];
        // $kode_jam = $ambil['kode_jam'];

        // $ambil = $this->jadwal_mdl->cek_jadwal_bantu($id_bantu);
        $hari = $i->post('hari');
        $ruangan = $i->post('ruangan_id');
        $kode_jam = $i->post('kode_jam');

        $ambil_jadwal = $this->jadwal_sore_mdl->data_jadwal_lengkap($aktif_id);
        $dosen1 = $ambil_jadwal['dosen_nip'];
        $dosen2 = $ambil_jadwal['dosen_nip_p'];
        $dosen3 = NULL;
        $kelas_id = $ambil_jadwal['kelas_id'];
        $lama = $ambil_jadwal['matakuliah_jam_teori'];
        for ($i=0; $i < $lama ; $i++) { 
            $jam_aktif = $kode_jam + $i;
            $cek_kosong = $this->cek_kosong_jadwal($ruangan,$hari,$jam_aktif);
            if(isset($cek_kosong)){
                echo "<script type='text/javascript'>
                    alert('Jadwal sudah terisi kelas lain');
                    window.location.href = '".base_url("jadwal/ploting_jadwal")."';
                  </script>";
                exit;
            }else{
                $cek_bentrok = $this->cek_bentrok($dosen1,$dosen2,$kelas_id,$jam_aktif,$hari);
                if($cek_bentrok!='0')
                {
                    echo "<script type='text/javascript'>
                        alert('Dosen / Kelas sudah terisi pada jam dan hari yang sama');
                        window.location.href = '".base_url("jadwal_sore/ploting_jadwal")."';
                      </script>";
                    exit;
                }else{
                    $aman = 1;
                }
            }
        }

        if($aman==1){
            for ($j=0; $j < $lama ; $j++) {
                $masuk_jam = $kode_jam + $j;
                $this->jadwal_sore_mdl->ubah_jadwal_baru($dosen1,$dosen2,$dosen3,$kelas_id,$aktif_id,$masuk_jam,$ruangan,$hari);
            }
        }
        $this->jadwal_sore_mdl->update_list_a($aktif_id);
        redirect(base_url('jadwal_sore/ploting_jadwal'));      
    }

    public function buang_sampah($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->buang_sampah($aktif_id);
    }

    public function cek_kosong_jadwal($ruangan,$hari,$jam_aktif){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $hasil = $this->jadwal_sore_mdl->cek_kosong_jadwal($ruangan,$hari,$jam_aktif);
        return $hasil['aktif_id'];
    }

    public function reset_jadwal(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->reset_jadwal();
        redirect(base_url('jadwal_sore/ploting_jadwal'));
    }

    public function hapus_setengah($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->hapus_setengah($aktif_id);
    }

    public function kembalikan($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->kembalikan($aktif_id);
    }

    public function habiskan_sampah(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->habiskan_sampah();
    }

    public function data_sampah(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        return $this->jadwal_sore_mdl->data_sampah();
    }

    public function input_mapping_ruang($kode_jam,$ruangan,$hari){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->input_mapping_ruang($kode_jam,$ruangan,$hari);
    }

    public function cek_batas($jam,$aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $ambil_jam = $this->jadwal_sore_mdl->ambil_jam($aktif_id);
        $jam_max = $jam + $ambil_jam['matakuliah_jam_teori'];
        return $jam_max;
    }

    public function ambil_jam($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $ambil_jam = $this->jadwal_sore_mdl->ambil_jam($aktif_id);
        return $ambil_jam['matakuliah_jam_teori'];
    }

    public function cek_tersedia($kode_jam,$ruangan,$hari){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $cek_tersedia = $this->jadwal_sore_mdl->cek_tersedia($kode_jam,$ruangan,$hari);
        return $cek_tersedia;
    }

    public function cek_ruang_lain($aktif_id,$ruangan){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $cek_ruang_lain = $this->jadwal_sore_mdl->cek_ruang_lain($aktif_id,$ruangan);
        return $cek_ruang_lain;
    }

    public function cek_kosong($kode_jam,$ruangan){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $bentrok = $this->jadwal_sore_mdl->cek_kosong($kode_jam,$ruangan);
        return $bentrok;
    }

    public function ubah_proses($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $ubah_proses = $this->jadwal_sore_mdl->ubah_proses($aktif_id);
    }

    public function cek_bentrok($dosen,$dosen2,$kelas,$kode_jam,$hari){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $bentrok = $this->jadwal_sore_mdl->hitung_bentrok($dosen,$dosen2,$kelas,$kode_jam,$hari);
        return $bentrok;
    }

    public function cek_bentrok2($dosen,$dosen2,$kelas,$kode_jam,$hari,$aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $bentrok = $this->jadwal_sore_mdl->hitung_bentrok2($dosen,$dosen2,$kelas,$kode_jam,$hari,$aktif_id);
        return $bentrok;
    }

    public function data_jadwal($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $data_jadwal = $this->jadwal_sore_mdl->data_jadwal($aktif_id);
        return $data_jadwal;
    }

    public function input_jadwal($dosen,$dosen2,$dosen3,$kelas, $kode_jam,$aktif_id,$ruangan){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $bentrok = $this->jadwal_sore_mdl->input_jadwal($dosen,$dosen2,$dosen3,$kelas,$kode_jam,$aktif_id,$ruangan);
    }

    public function mapping_ruang(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $ruangan = $this->jadwal_sore_mdl->ruangan_list();
        // $kelas_aktif = $this->jadwal_mdl->kelas_aktif();
            for ($i=1; $i <=5 ; $i++) {
                foreach ($ruangan as $ru) {
                    for ($x = 1; $x <= 8; $x++) {
                        // echo $ru->ruangan_nama."-".$x." : ";
                        $this->input_mapping_ruang($x,$ru->ruangan_id,$i);
                    }
                    // echo "<br>";
                }
                // echo "<br>";
            }
        redirect(base_url('jadwal_sore/ploting_jadwal'));
    }

    public function mapping_jadwal(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        // $items = $this->jadwal_mdl->ruangan_list();
        $ruangan = $this->jadwal_sore_mdl->ruangan_list();
        $kelas_aktif = $this->jadwal_sore_mdl->kelas_aktif();
        $mapping_ruang = $this->jadwal_sore_mdl->mapping_ruang();

        foreach ($kelas_aktif as $ka) {
          $jumlah_jam = $ka->matakuliah_jam_teori;
            for($y = 1;$y<=$jumlah_jam;$y++){
               $hasil[] = $ka->aktif_id;
            }
        }

        $nilai_sebelum = null;
        $batas_sebelum = null;
        foreach ($mapping_ruang as $mr) {
            $ambil = $hasil[0];

            $batas = $this->cek_batas($mr->kode_jam,$hasil[0]);
            if($ambil==$nilai_sebelum)
                $batas = $batas_sebelum;
            else
                $batas = $batas;

            //ubah jadwal
            $ambil_data = $this->data_jadwal($ambil);
            $cek_bentrok = $this->cek_bentrok($ambil_data['dosen_nip'],$ambil_data['dosen_nip_p'],$ambil_data['kelas_id'],$mr->kode_jam,$mr->hari);

            //cek kosong di ruangan itu
            $ambil_jam = $this->ambil_jam($hasil[0]);
            $i = 0;
            $tersedia = 0;
            for ($i=0; $i <= $ambil_jam ; $i++) { 
                $jam_cek = $i+$mr->kode_jam+1;
                $cek_tersedia = $this->cek_tersedia($mr->kode_jam,$mr->ruangan,$mr->hari);
                if($cek_tersedia=='1'){
                    $tersedia++;
                }
            }

            if($tersedia!='0')
            {
                $this->buang_sampah($hasil[0]);
                array_shift($hasil);
            }

            //cek terdapat di ruangan lain
            $cek_tersedia = $this->cek_ruang_lain($hasil[0],$mr->ruangan);
            if($cek_tersedia!='0'){
                $this->buang_sampah($hasil[0]);
                array_shift($hasil);
            }

            if($cek_bentrok=='0' && $batas<='8')
            {
                if($ambil!='0')
                    $this->isikan_jadwal($ambil_data['dosen_nip'],$ambil_data['dosen_nip_p'],$ambil_data['dosen3'],$ambil_data['kelas_id'],$ambil,$mr->id_bantu);
                $this->update_list_a($ambil);
                array_shift($hasil);
            }else{
                //buang sampah
                $this->buang_sampah($hasil[0]);
                array_shift($hasil);
            }

            $nilai_sebelum = $ambil;
            $batas_sebelum = $batas;
            if (count($hasil) == 0) {
                // echo 'Array sudah kosong';
                break; // Keluar dari loop foreach
            }
        }

        //hapus yang cuman setengah2 atau gagal
        $data_sampah = $this->data_sampah();
        foreach ($data_sampah as $ds) {
            $this->hapus_setengah($ds->aktif_id);
            //kembalikan
            $this->kembalikan($ds->aktif_id);
        }

        $this->habiskan_sampah();

        redirect(base_url('jadwal_sore/ploting_jadwal'));
    }

    public function update_list_a($aktif_id){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $this->jadwal_sore_mdl->update_list_a($aktif_id);
        return;
    }

    public function ubah_jadwal(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $i      = $this->input;

        $aktif_id = $i->post('aktif_id');
        $ruangan = $i->post('ruangan_baru');
        $jam_mulai = $i->post('jam_baru');
        $hari = $i->post('hari_baru');

        //cek lama jam
        $lama = $this->jadwal_sore_mdl->ambil_jam($aktif_id);
        $jam = $lama['matakuliah_jam_teori'];

        

        //ambil jadwal
        $ambil = $this->jadwal_sore_mdl->cek_jadwal($aktif_id);
        $dosen = $ambil['dosen'];
        $dosen2 = $ambil['dosen2'];
        $dosen3 = $ambil['dosen3'];
        $kelas_id = $ambil['kelas_id'];

        for ($i=0; $i < $jam ; $i++) { 
            $jam_aktif = $jam_mulai + $i;
            $cek_kosong = $this->cek_kosong_jadwal($ruangan,$hari,$jam_aktif);

            if(isset($cek_kosong)){
                echo "<script type='text/javascript'>
                    alert('Jadwal sudah terisi kelas lain');
                    window.location.href = '".base_url("jadwal_sore/ploting_jadwal")."';
                  </script>";
                exit;
            }else{
                // $cek_bentrok = $this->cek_bentrok($dosen,$dosen2,$kelas_id,$jam_aktif,$hari);
                $cek_bentrok = $this->cek_bentrok2($dosen,$dosen2,$kelas_id,$jam_aktif,$hari,$aktif_id);

                if($cek_bentrok!='0')
                {
                    echo "<script type='text/javascript'>
                        alert('Dosen / Kelas sudah terisi pada jam dan hari yang sama');
                        window.location.href = '".base_url("jadwal_sore/ploting_jadwal")."';
                      </script>";
                    exit;
                }else{
                    $aman = 1;
                }
            }
        }

        if($aman==1){
            $this->jadwal_sore_mdl->hapus_setengah($aktif_id);
            for ($i=0; $i <$jam ; $i++) {
                // echo $dosen;
                $kode_jam = $i + $jam_mulai; 
                $this->jadwal_sore_mdl->ubah_jadwal_baru($dosen,$dosen2,$dosen3,$kelas_id,$aktif_id,$kode_jam,$ruangan,$hari);
            }
        }

        redirect(base_url('jadwal_sore/ploting_jadwal'));
        // $this->Aktif_mdl->simpan_tambah_jadwal($data);
        // $this->Aktif_mdl->ubah_status_aktif($i->post('aktif_id'));
        // $this->session->set_flashdata('sukses', 'Jadwal sukses ditambah');
        // redirect(base_url('aktif/jadwal_pagi'),'refresh');    
    }

    public function cek_belum(){
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        $kelas_aktif = $this->jadwal_sore_mdl->kelas_aktif_semua();
        foreach ($kelas_aktif as $kls) {
            $cek_sudah = $this->jadwal_sore_mdl->cek_sudah($kls->aktif_id);
            if($cek_sudah=='0'){
                $this->jadwal_sore_mdl->kembalikan($kls->aktif_id);
            }
        }

        redirect(base_url('jadwal_sore/ploting_jadwal'));
    }

    public function jadwal_dosen() {
        $this->initialize();
        $this->load->model('jadwal_sore_mdl');
        // $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Jadwal Mengajar';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $data['path'] = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah');
        $data['title'] = ' | Jadwal Perkuliahan Pagi';

        $data['ruang'] = $this->jadwal_sore_mdl->ruangan_list();
        $data['belum_terjadwal'] = $this->jadwal_sore_mdl->belum_terjadwal();
        $data['hari_selanjutnya'] = $this->jadwal_sore_mdl->hari_selanjutnya();
        $data['dosen_list'] = $this->jadwal_sore_mdl->get_dosen_list();
        $data['jadwal'] = [];
        $data['selected_dosen'] = '';
        $data['selected_dosen_nama'] = '';
        $dosen_nip = $this->input->post('dosen_nip');
        if ($dosen_nip) {
            $data['jadwal'] = $this->jadwal_sore_mdl->get_jadwal_dosen($dosen_nip);
            $data['selected_dosen'] = $dosen_nip;
            $data['selected_dosen_nama'] = $this->jadwal_sore_mdl->get_dosen_name($dosen_nip);
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar');
        $this->load->view('jadwal_dosen_sore', $data);
        $this->load->view('adminlte-footer');
    }

    public function convert_hari($hari) {
        $hari_list = [
            '1' => 'Senin',
            '2' => 'Selasa',
            '3' => 'Rabu',
            '4' => 'Kamis',
            '5' => 'Jumat'
        ];
        return $hari_list[$hari];
    }

}

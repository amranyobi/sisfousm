<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Peserta', $heading2 = 'Peserta', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->pages(1, "");
    }

    public function pages($page, $param) {
        $this->initialize();
        $this->load->model('peserta_mdl');
        $page = (int) $page;
        $title = ' | Peserta';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kelas') . " / peserta";
        $type = 'page';
        $heading1 = 'Peserta';
        $heading2 = 'Daftar Peserta';
        $content = '';

        $ex = $this->peserta_mdl->get_status($param);

        $num = 0;
        $maxdata = 30;
        $items = $this->peserta_mdl->get_list($param, $maxdata, $page);

        $i = 0;
        $datals;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "NIM";
        $dataheading[2] = "NAMA";
        $dataheading[3] = "KELUARKAN";

        $datals[$i][0] = "";
        $datals[$i][1] = "";
        $datals[$i][2] = "";
        $datals[$i][3] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "text-align: center;";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datals[$i][0] = $in;
            $datals[$i][1] = $item->mahasiswa_nim . "";
            $datals[$i][2] = $item->mahasiswa_nama . "";
            $datals[$i][3] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $short = str_replace(" ", "_", $item->mahasiswa_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][3] = base_url() . "peserta/remove/" . $param . "/" . $item->mahasiswa_nim;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datals, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . "<div style='text-align: right; padding: 10px;'><a href='" . base_url() . "peserta/add/" . $param . "' style='color: #ffffff;' class='btn btn-success add-btn'><span class='fa fa-database'></span> Kelola Kelas</a></div>";
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "peserta/pages/[PGN]/" . $param, "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kelas, $nim) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan mengeluarkan mahasiswa dengan nim ' . $nim . ' dari kelas, lanjutkan?")){
                    window.location = "' . base_url() . 'peserta/delete_konfirmasi/' . $kelas . '/' . $nim . '";
                } else {
                    window.location = "' . base_url() . 'peserta/pages/1/' . $kelas . '";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kelas, $nim) {
        $this->remove($kelas, $nim);
    }
    
    public function enroll($kode, $nim) {
        $this->initialize();
        $this->load->model('peserta_mdl');
        $this->peserta_mdl->enroll_peserta($kode, $nim);
        redirect(base_url('peserta/add/' . $kode));
    }
    
    public function remove($kode, $nim) {
        $this->initialize();
        $this->load->model('peserta_mdl');
        $this->peserta_mdl->remove_peserta($kode, $nim);
        redirect(base_url('peserta/add/' . $kode));
    }
    

    public function add($kode) {
        $this->initialize();
        $extra = "formcontrolpeserta";
        $editon = false;
        $title = "| Peserta Kelas";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kelas') . " / Pengaturan Kelas";
        $form = "" . base_url() . "peserta/add_submit";
        $h1 = "Daftar Peserta Kelas";
        $h2 = "";

        $data[0][0] = "peserta";
        $data[0][1] = "";
        $data[1][0] = "mahasiswa";
        $data[1][1] = "";

        $this->load->model('peserta_mdl');
        
        $kelass = $this->peserta_mdl->get_kelas_peserta($kode, 2000, 1);
        foreach ($kelass as $kelas) {
            $data[0][1] = $data[0][1] . "<div class='peserta-list'>";
            $data[0][1] = $data[0][1] . "<div class='peserta-list-button'>";
            $data[0][1] = $data[0][1] . "<a href='" . base_url() . "peserta/remove/" . $kelas->kelas_id . "/" . $kelas->mahasiswa_nim . "' title='Keluarkan dari daftar peserta kelas'><span class='fa fa-trash'></span></a>";
            $data[0][1] = $data[0][1] . "</div>";
            $data[0][1] = $data[0][1] . "" . $kelas->mahasiswa_nim . " : " . $kelas->mahasiswa_nama . "";
            $data[0][1] = $data[0][1] . "</div>";
        }
        
        $kelass = $this->peserta_mdl->get_free_peserta("", 50, 1);
        foreach ($kelass as $kelas) {
            $data[1][1] = $data[1][1] . "<div class='peserta-list'>";
            $data[1][1] = $data[1][1] . "<div class='peserta-list-button'>";
            $data[1][1] = $data[1][1] . "<a href='" . base_url() . "peserta/enroll/" . $kode . "/" . $kelas->mahasiswa_nim . "' title='Masukkan kedalam daftar peserta kelas'><span class='fa fa-user'></span></a>";
            $data[1][1] = $data[1][1] . "</div>";
            $data[1][1] = $data[1][1] . "" . $kelas->mahasiswa_nim . " : " . $kelas->mahasiswa_nama . "";
            $data[1][1] = $data[1][1] . "</div>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolruangan";
        $data = null;
        $editon = true;
        $title = "| Ruangan";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('ruangan') . " / Perbaharuan Data";
        $form = "" . base_url() . "ruangan/edit_submit";
        $h1 = "Formulir Ruangan";
        $h2 = "Formulir Perbaharuan Ruangan";

        $this->load->model('peserta_mdl');
        $ruangan = $this->peserta_mdl->get_item($id);

        $gdg = $ruangan->gedung_id;
        $data[0][0] = "kode";
        $data[0][1] = "" . $ruangan->ruangan_id;
        $data[1][0] = "ruangan";
        $data[1][1] = "" . $ruangan->ruangan_nama;
        $data[2][0] = "gedung";
        $data[2][1] = "";
        $data[3][0] = "lantai";
        $data[3][1] = "" . $ruangan->ruangan_lantai;
        $data[4][0] = "kapasitas";
        $data[4][1] = "" . $ruangan->ruangan_kapasitas;

        $this->load->model('kelas_mdl');
        $kelass = $this->kelas_mdl->get_list(2000, 1);
        foreach ($kelass as $kelas) {
            if ($gdg == $kelas->gedung_id) {
                $data[2][1] = $data[2][1] . "<option value='" . $kelas->kelas_id . "' selected>" . $kelas->kelas_nama . " (" . $kelas->kelas_angkatan . ")</option>";
            } else {
                $data[2][1] = $data[2][1] . "<option value='" . $kelas->kelas_id . "'>" . $kelas->kelas_nama . " (" . $kelas->kelas_angkatan . ")</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('ruangan') . '';
        $gedung = $this->input->post('gedung') . '';
        $lantai = $this->input->post('lantai') . '';
        $kapasitas = $this->input->post('kapasitas') . '';

        $this->load->model('peserta_mdl');
        $this->peserta_mdl->add($nama, $gedung, $lantai, $kapasitas);

        redirect(base_url('ruangan/pages/1/' . $gedung));
    }

    public function edit_submit() {
        $this->initialize();
        $kode = $this->input->post('kode') . '';
        $nama = $this->input->post('ruangan') . '';
        $gedung = $this->input->post('gedung') . '';
        $lantai = $this->input->post('lantai') . '';
        $kapasitas = $this->input->post('kapasitas') . '';

        $this->load->model('peserta_mdl');
        $this->peserta_mdl->edit($kode, $nama, $gedung, $lantai, $kapasitas);

        redirect(base_url('ruangan/pages/1/' . $gedung));
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kelas', $heading2 = 'Kelas', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    public function page($page) {
        $this->initialize();
        $this->load->model('kelas_mdl');
        $page = (int) $page;
        $title = ' | Kelas';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kelas');
        $type = 'page';
        $heading1 = 'Kelas';
        $heading2 = 'Daftar Kelas';
        $content = '';

        $ex = $this->kelas_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->kelas_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KELAS";
        $dataheading[2] = "ANGKATAN";
        $dataheading[3] = "PESERTA";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->kelas_nama . "";
            $datag[$i][2] = $item->kelas_angkatan . "";
            $datag[$i][3] = "<span class='fa fa-user'></span>";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][1] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][2] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][3] = base_url() . "peserta/pages/1/" . $item->kelas_id;
            $datalink[$i][4] = base_url() . "kelas/edit/" . $item->kelas_id;
            $short = str_replace(" ", "_", $item->kelas_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][5] = base_url() . "kelas/delete/" . $item->kelas_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("kelas", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "kelas/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('kelas_mdl');
        $title = ' | Search - Kelas';
        $type = 'page';
        $heading1 = 'Kelas';
        $heading2 = 'Pencarian Kelas';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('kelas');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';
        
        $items = $this->kelas_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KELAS";
        $dataheading[2] = "ANGKATAN";
        $dataheading[3] = "PESERTA";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->kelas_nama . "";
            $datag[$i][2] = $item->kelas_angkatan . "";
            $datag[$i][3] = "<span class='fa fa-user'></span>";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][1] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][2] = base_url() . "kelas/edit/" . $item->kelas_id;
            $datalink[$i][3] = base_url() . "peserta/pages/1/" . $item->kelas_id;
            $datalink[$i][4] = base_url() . "kelas/edit/" . $item->kelas_id;
            $short = str_replace(" ", "_", $item->kelas_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][5] = base_url() . "kelas/delete/" . $item->kelas_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;
        
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function delete($kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data kelas ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'kelas/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'kelas";
                }
            </script>
            ';
        
        echo $text;
    }
    
    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('kelas_mdl');
        $this->kelas_mdl->delete($kode);
        redirect(base_url('kelas'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolkelas";
        $editon = false;
        $title = "| Kelas";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kelas') . " / Tambah Data";
        $form = "" . base_url() . "kelas/add_submit";
        $h1 = "Formulir Kelas";
        $h2 = "Formulir Penambahan Kelas";

        $data[0][0] = "ids";
        $data[0][1] = "";
        $data[1][0] = "nama";
        $data[1][1] = "";
        $data[2][0] = "angkatan";
        $data[2][1] = "";
        $data[3][0] = "prodi";
        $data[3][1] = "";

        $this->load->model('prodi_mdl');
        $datas = $this->prodi_mdl->get_list(2000, 1);
        foreach ($datas as $datax) {
            $data[3][1] = $data[3][1] . "<option value='" . $datax->no_prodi . "'>" . $datax->nama_prodi . " (" . $datax->jenjang_prodi . ")</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolkelas";
        $data = null;
        $editon = true;
        $title = "| Kelas";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kelas') . " / Perbaharuan Data";
        $form = "" . base_url() . "kelas/edit_submit";
        $h1 = "Formulir Kelas";
        $h2 = "Formulir Perbaharuan Kelas";

        $this->load->model('kelas_mdl');
        $item = $this->kelas_mdl->get_item($id);

        $data[0][0] = "ids";
        $data[0][1] = "" . $item->kelas_id;
        $data[1][0] = "nama";
        $data[1][1] = "" . $item->kelas_nama;
        $data[2][0] = "angkatan";
        $data[2][1] = "" . $item->kelas_angkatan;
        $data[3][0] = "prodi";
        $data[3][1] = "";

        $this->load->model('prodi_mdl');
        $datas = $this->prodi_mdl->get_list(2000, 1);
        foreach ($datas as $datax) {
            if ($item->no_prodi == $datax->no_prodi) {
                $data[3][1] = $data[3][1] . "<option value='" . $datax->no_prodi . "' selected>" . $datax->nama_prodi . " (" . $datax->jenjang_prodi . ")</option>";
            } else {
                $data[3][1] = $data[3][1] . "<option value='" . $datax->no_prodi . "'>" . $datax->nama_prodi . " (" . $datax->jenjang_prodi . ")</option>";
            }
        }
        
        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('nama') . '';
        $alamat = $this->input->post('angkatan') . '';
        $prodi = $this->input->post('prodi') . '';

        $this->load->model('kelas_mdl');
        $gedung = $this->kelas_mdl->add($nama, $alamat, $prodi);
        
        redirect(base_url('kelas'));
    }

    public function edit_submit() {
        $this->initialize();
        $id = $this->input->post('kode') . '';
        $nama = $this->input->post('nama') . '';
        $alamat = $this->input->post('angkatan') . '';
        $prodi = $this->input->post('prodi') . '';

        $this->load->model('kelas_mdl');
        $gedung = $this->kelas_mdl->edit($id, $nama, $alamat, $prodi);
        
        redirect(base_url('kelas'));
    }
}

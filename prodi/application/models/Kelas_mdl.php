<?php

class Kelas_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(kelas_id) AS jumlah FROM kelas WHERE no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas ORDER BY kelas_angkatan, kelas_nama LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_list_prodi($prodi, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas WHERE no_prodi = '" . $prodi . "' ORDER BY kelas_angkatan, kelas_nama LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_item($kelas_id) {
        $QRY = "SELECT * FROM kelas WHERE kelas_id = '" . $kelas_id . "' AND no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM kelas WHERE (kelas_nama LIKE '%" . $keyword . "%' OR kelas_angkatan LIKE '%" . $keyword . "%') AND no_prodi = '" . $this->session->kodeaktif . "' ORDER BY kelas_angkatan, kelas_nama LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }

    public function add($nama, $angkatan, $prodi) {
        $nama = $this->doclean($nama);
        $angkatan = $this->doclean($angkatan);

        $QRY = "INSERT INTO kelas(kelas_nama, kelas_angkatan, no_prodi) VALUES (";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $angkatan . "', ";
        $QRY = $QRY . "'" . $prodi . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function edit($id, $nama, $angkatan, $prodi) {
        $id = $this->doclean($id);
        $nama = $this->doclean($nama);
        $angkatan = $this->doclean($angkatan);

        $QRY = "UPDATE kelas SET ";
        $QRY = $QRY . "kelas_nama='" . $nama . "', ";
        $QRY = $QRY . "kelas_angkatan='" . $angkatan . "', ";
        $QRY = $QRY . "no_prodi='" . $prodi . "' ";
        $QRY = $QRY . "WHERE kelas_id='" . $id . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }

    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE kelas_id = '" . $id . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM kelas_peserta WHERE kelas_id = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM kelas WHERE kelas_id = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }

    public function get_list_peserta($num = 10, $page = 1, $kelas = "-", $siswa = "-") {
        $start = ($page - 1) * $num;
        if (($kelas == "-") && ($siswa == "-")) {
            $QRY = "SELECT * FROM kelas_peserta LIMIT " . $start . ", " . $num;
        }
        else if(($kelas == "-") && ($siswa != "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE mahasiswa_nim = '" . $siswa . "' LIMIT " . $start . ", " . $num;
        }
        else if(($kelas != "-") && ($siswa == "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE kelas_id = '" . $kelas . "' LIMIT " . $start . ", " . $num;
        }
        else if(($kelas != "-") && ($siswa != "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE mahasiswa_nim = '" . $siswa . "' AND kelas_id = '" . $kelas . "' LIMIT " . $start . ", " . $num;
        }
        else {
            $QRY = "SELECT * FROM kelas_peserta LIMIT " . $start . ", " . $num;
        }
        $query = $this->db->query($QRY);
        return $query->result();
    }

}

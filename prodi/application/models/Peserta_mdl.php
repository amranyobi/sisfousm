<?php

class Peserta_mdl extends CI_Model {

    public function get_status($param = "") {
        if (($param == "") || ($param == null)) {
            $QRY = "SELECT COUNT(mahasiswa_nim) AS jumlah FROM kelas_peserta";
        } else {
            $QRY = "SELECT COUNT(mahasiswa_nim) AS jumlah FROM kelas_peserta WHERE kelas_id='" . $param . "';";
        }
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_kelas_peserta($param, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas_peserta a JOIN mahasiswa b ON (a.mahasiswa_nim = b.mahasiswa_nim) WHERE a.kelas_id = '" . $param . "' ORDER BY a.mahasiswa_nim LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_free_peserta($param, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM mahasiswa WHERE mahasiswa_nim NOT IN (SELECT DISTINCT mahasiswa_nim FROM kelas_peserta) AND (mahasiswa_nim LIKE '%" . $param . "%' OR mahasiswa_nama LIKE '%" . $param . "%') LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function enroll_peserta($param, $nim) {
        $nim = $this->doclean($nim);
        $kelas = $this->doclean($param);

        $QRY = "INSERT INTO kelas_peserta(kelas_id, mahasiswa_nim) VALUES (";
        $QRY = $QRY . "'" . $kelas . "', ";
        $QRY = $QRY . "'" . $nim . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function remove_peserta($param, $nim) {
        $nim = $this->doclean($nim);
        $kelas = $this->doclean($param);
        $QRY = "DELETE FROM kelas_peserta WHERE mahasiswa_nim ='" . $nim . "'";
        $this->db->query($QRY);
        return;
    }

    public function get_list($param = "", $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        if (($param == "") || ($param == null)) {
            $QRY = "SELECT * FROM kelas_peserta a JOIN mahasiswa b ON (a.mahasiswa_nim = b.mahasiswa_nim) LIMIT " . $start . ", " . $num;
        } else {
            $QRY = "SELECT * FROM kelas_peserta a JOIN mahasiswa b ON (a.mahasiswa_nim = b.mahasiswa_nim) WHERE a.kelas_id = '" . $param . "' LIMIT " . $start . ", " . $num;
        }
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM mahasiswa WHERE (a.mahasiswa_nama LIKE '%" . $keyword . "%' OR a.mahasiswa_nim LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }

    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM kelas_kelas_peserta WHERE mahasiswa_nim = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }

}

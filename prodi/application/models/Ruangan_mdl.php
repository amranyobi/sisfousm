<?php

class Ruangan_mdl extends CI_Model {

    public function get_status($gedung = "") {
        if (($gedung == "")||($gedung == null)) {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan";
        } else {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan WHERE gedung_id='" . $gedung . "';";
        }
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($gedung = "", $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        if (($gedung == "") || ($gedung == null)) {
            $QRY = "SELECT * FROM ruangan a JOIN gedung b ON (a.gedung_id = b.gedung_id) LIMIT " . $start . ", " . $num;
        } else {
            $QRY = "SELECT * FROM ruangan a JOIN gedung b ON (a.gedung_id = b.gedung_id) WHERE a.gedung_id = '" . $gedung . "' LIMIT " . $start . ", " . $num;
        }
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_item($ruangan_id) {
        $QRY = "SELECT * FROM ruangan a JOIN gedung b ON (a.gedung_id = b.gedung_id) WHERE a.ruangan_id = '" . $ruangan_id . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM ruangan a JOIN gedung b ON (a.gedung_id = b.gedung_id) WHERE (a.ruangan_nama LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }

    public function add($nama, $gedung, $lantai, $kapasitas) {
        $nama = $this->doclean($nama);
        $gedung = $this->doclean($gedung);
        $lantai = $this->doclean($lantai);
        $kapasitas = $this->doclean($kapasitas);

        $QRY = "INSERT INTO ruangan(gedung_id, ruangan_nama, ruangan_kapasitas, ruangan_lantai) VALUES (";
        $QRY = $QRY . "'" . $gedung . "', ";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $kapasitas . "', ";
        $QRY = $QRY . "'" . $lantai . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function edit($kode, $nama, $gedung, $lantai, $kapasitas) {
        $kode = $this->doclean($kode);
        $nama = $this->doclean($nama);

        $QRY = "UPDATE ruangan SET ";
        $QRY = $QRY . "ruangan_nama='" . $nama . "', ";
        $QRY = $QRY . "gedung_id='" . $gedung . "', ";
        $QRY = $QRY . "ruangan_kapasitas='" . $kapasitas . "', ";
        $QRY = $QRY . "ruangan_lantai='" . $lantai . "' ";
        $QRY = $QRY . "WHERE ruangan_id='" . $kode . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }

    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE ruangan_id = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM ruangan WHERE ruangan_id = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }

    public function pengguna($ruangan) {
        $ruangan = $this->doclean($ruangan);
        $QRY = "SELECT * FROM ruangan_prodi a JOIN program_studi b ON (a.no_prodi = b.no_prodi) WHERE a.ruangan_id='" . $ruangan . "' LIMIT 0, 2000";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function nonpengguna($ruangan) {
        $ruangan = $this->doclean($ruangan);
        $QRY = "SELECT * FROM program_studi WHERE no_prodi NOT IN (SELECT no_prodi FROM ruangan_prodi WHERE ruangan_id='" . $ruangan . "')";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function tambahpengguna($ruangan, $prodi) {
        $ruangan = $this->doclean($ruangan);
        $prodi = $this->doclean($prodi);
        $QRY = "INSERT INTO ruangan_prodi(no_prodi, ruangan_id) VALUES (";
        $QRY = $QRY . "'" . $prodi . "', ";
        $QRY = $QRY . "'" . $ruangan . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function hapuspengguna($ruangan, $prodi) {
        $ruangan = $this->doclean($ruangan);
        $prodi = $this->doclean($prodi);
        $QRY = "DELETE FROM ruangan_prodi WHERE no_prodi = '" . $prodi . "' AND ruangan_id='" . $ruangan . "'";
        $this->db->query($QRY);
        return;
    }

}

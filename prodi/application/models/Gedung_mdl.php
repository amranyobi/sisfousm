<?php

class Gedung_mdl extends CI_Model {
    
    public function get_status() {
        $QRY = "SELECT COUNT(gedung_id) AS jumlah FROM gedung";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM gedung LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($gedung_id) {
        $QRY = "SELECT * FROM gedung WHERE gedung_id = '" . $gedung_id . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM gedung WHERE (gedung_nama LIKE '%" . $keyword . "%' OR gedung_alamat LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($nama, $alamat) {
        $nama = $this->doclean($nama);
        $alamat = $this->doclean($alamat);
        
        $QRY = "INSERT INTO gedung(gedung_nama, gedung_alamat) VALUES (";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $alamat . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($id, $nama, $alamat) {
        $id = $this->doclean($id);
        $nama = $this->doclean($nama);
        $alamat = $this->doclean($alamat);
        
        $QRY = "UPDATE gedung SET ";
        $QRY = $QRY . "gedung_nama='" . $nama . "', ";
        $QRY = $QRY . "gedung_alamat='" . $alamat . "' ";
        $QRY = $QRY . "WHERE gedung_id='" . $id . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }
    
    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE ruangan_id IN (SELECT ruangan_id FROM ruangan WHERE gedung_id = '" . $id . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM ruangan WHERE gedung_id = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM gedung WHERE gedung_id = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }
    
}

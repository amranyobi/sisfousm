<?php

class Prodi_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(no_prodi) AS jumlah FROM program_studi";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM program_studi LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($keyword) {
        $QRY = "SELECT * FROM program_studi WHERE no_prodi = '" . $keyword . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM program_studi WHERE (kode_prodi LIKE '%" . $keyword . "%' OR nama_prodi LIKE '%" . $keyword . "%' OR jenjang_prodi LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($nama, $kode, $jenjang, $kurikulum) {
        $kode = $this->doclean($kode);
        $nama = $this->doclean($nama);
        $jenjang = $this->doclean($jenjang);
        $kurikulum = $this->doclean($kurikulum);
        
        $QRY = "INSERT INTO program_studi(kode_prodi, nama_prodi, jenjang_prodi, kurikulum_id) VALUES (";
        $QRY = $QRY . "'" . $kode . "', ";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $jenjang . "', ";
        $QRY = $QRY . "'" . $kurikulum . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($id, $nama, $kode, $jenjang, $kurikulum) {
        $id = $this->doclean($id);
        $kode = $this->doclean($kode);
        $nama = $this->doclean($nama);
        $jenjang = $this->doclean($jenjang);
        $kurikulum = $this->doclean($kurikulum);
        
        $QRY = "UPDATE program_studi SET ";
        $QRY = $QRY . "kode_prodi='" . $kode . "', ";
        $QRY = $QRY . "nama_prodi='" . $nama . "', ";
        $QRY = $QRY . "jenjang_prodi='" . $jenjang . "', ";
        $QRY = $QRY . "kurikulum_id='" . $kurikulum . "' ";
        $QRY = $QRY . "WHERE no_prodi='" . $id . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }
    
    public function delete($kode) {
        $kode = $this->doclean($kode);
        $QRY = "DELETE FROM ruangan_prodi WHERE no_prodi = '" . $kode . "';";
        $this->db->query($QRY);
        $QRY = "UPDATE dosen SET homebase = '0' WHERE homebase = '" . $kode . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM program_studi WHERE no_prodi = '" . $kode . "';";
        $this->db->query($QRY);
        return;
    }
    
}

<?php

class Matakuliah_mdl extends CI_Model {

    public function get_status($parameter = "") {
        if (($parameter == "") || ($parameter == null)) {
            $QRY = "SELECT COUNT(matakuliah_kode) AS jumlah FROM matakuliah WHERE kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "')";
        } else {
            $QRY = "SELECT COUNT(matakuliah_kode) AS jumlah FROM matakuliah WHERE kurikulum_id = '" . $parameter . "' AND kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "')";
        }
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_list($parameter = "", $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        if (($parameter == "") || ($parameter == null)) {
            $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) LIMIT " . $start . ", " . $num;
        } else {
            //$QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) WHERE a.kurikulum_id = '" . $parameter . "' AND a.kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "') LIMIT " . $start . ", " . $num;
            $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) LIMIT " . $start . ", " . $num;
        }
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function list_mk() {
        $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) ORDER BY matakuliah_kode ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function list_mk_order_nama() {
        $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) ORDER BY matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_item($parameter) {
        $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) WHERE a.matakuliah_kode = '" . $parameter . "' AND a.kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "')";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function get_list_kurkulum($parameter, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) WHERE a.kurikulum_id = '" . $parameter . "' AND a.kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "') LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM matakuliah a JOIN kurikulum b ON (a.kurikulum_id = b.kurikulum_id) WHERE (a.matakuliah_nama LIKE '%" . $keyword . "%' OR a.matakuliah_kode LIKE '%" . $keyword . "%') AND a.kurikulum_id IN (SELECT kurikulum_id FROM program_studi WHERE no_prodi = '" . $this->session->kodeaktif . "') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }

    public function add_item($matakuliah_kode, $kurikulum, $matakuliah_nama, $teori, $praktek) {
        $matakuliah_kode = $this->doclean($matakuliah_kode);
        $matakuliah_nama = $this->doclean($matakuliah_nama);
        $teori = $this->doclean($teori);
        $praktek = $this->doclean($praktek);
        $kurikulum = $this->doclean($kurikulum);

        $QRY = "INSERT INTO matakuliah(matakuliah_kode, kurikulum_id, matakuliah_nama, matakuliah_sks_teori, matakuliah_sks_praktek) VALUES (";
        $QRY = $QRY . "'" . $matakuliah_kode . "', ";
        $QRY = $QRY . "'" . $kurikulum . "', ";
        $QRY = $QRY . "'" . $matakuliah_nama . "', ";
        $QRY = $QRY . "'" . $teori . "', ";
        $QRY = $QRY . "'" . $praktek . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function edit_item($matakuliah_kode, $matakuliah_nama, $teori, $praktek) {
        $matakuliah_kode = $this->doclean($matakuliah_kode);
        $matakuliah_nama = $this->doclean($matakuliah_nama);
        $teori = $this->doclean($teori);
        $praktek = $this->doclean($praktek);

        $QRY = "UPDATE matakuliah SET ";
        $QRY = $QRY . "matakuliah_nama='" . $matakuliah_nama . "', ";
        $QRY = $QRY . "matakuliah_sks_teori='" . $teori . "', ";
        $QRY = $QRY . "matakuliah_sks_praktek='" . $praktek . "'";
        $QRY = $QRY . "WHERE matakuliah_kode='" . $matakuliah_kode . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }

    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE matakuliah_kode = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM matakuliah WHERE matakuliah_kode = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }

}

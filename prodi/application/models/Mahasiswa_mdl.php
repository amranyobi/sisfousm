<?php

class Mahasiswa_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(mahasiswa_nim) AS jumlah FROM mahasiswa";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM mahasiswa LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($dosen_nip) {
        $QRY = "SELECT * FROM mahasiswa WHERE mahasiswa_nim = '" . $dosen_nip . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM mahasiswa WHERE (mahasiswa_nama LIKE '%" . $keyword . "%' OR mahasiswa_nim LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($nim, $nama, $thnmasuk, $thnlulus, $lulus, $status) {
        $nim = $this->doclean($nim);
        $nama = $this->doclean($nama);
        $thnmasuk = $this->doclean($thnmasuk);
        $thnlulus = $this->doclean($thnlulus);
        $lulus = $this->doclean($lulus);
        $status = $this->doclean($status);
        
        $QRY = "INSERT INTO mahasiswa(mahasiswa_nim, mahasiswa_nama, mahasiswa_tahun_masuk, mahasiswa_tahun_lulus, mahasiswa_lulus, mahasiswa_status) VALUES (";
        $QRY = $QRY . "'" . $nim . "', ";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $thnmasuk . "', ";
        $QRY = $QRY . "'" . $thnlulus . "', ";
        $QRY = $QRY . "'" . $lulus . "', ";
        $QRY = $QRY . "'" . $status . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($nim, $nama, $thnmasuk, $thnlulus, $lulus, $status, $angkatan) {
        $nim = $this->doclean($nim);
        $nama = $this->doclean($nama);
        $thnmasuk = $this->doclean($thnmasuk);
        $thnlulus = $this->doclean($thnlulus);
        $lulus = $this->doclean($lulus);
        $status = $this->doclean($status);
        
        $QRY = "UPDATE mahasiswa SET ";
        $QRY = $QRY . "mahasiswa_nama='" . $nama . "', ";
        $QRY = $QRY . "mahasiswa_tahun_masuk='" . $thnmasuk . "', ";
        $QRY = $QRY . "mahasiswa_tahun_lulus='" . $thnlulus . "', ";
        $QRY = $QRY . "mahasiswa_lulus='" . $lulus . "', ";
        $QRY = $QRY . "mahasiswa_status='" . $status . "' ";
        $QRY = $QRY . "WHERE mahasiswa_nim='" . $nim . "';";
        $this->db->query($QRY);
        
        $QRY = "DELETE FROM kelas_peserta WHERE mahasiswa_nim = '" . $nim . "';";
        $this->db->query($QRY);
        
        $QRY = "INSERT INTO kelas_peserta(mahasiswa_nim, kelas_id) VALUES (";
        $QRY = $QRY . "'" . $nim . "', ";
        $QRY = $QRY . "'" . $angkatan . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        
        return;
    }
    
    public function delete($kode) {
        $kode = $this->doclean($kode);
        $QRY = "DELETE FROM kelas_peserta WHERE mahasiswa_nim = '" . $kode . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM mahasiswa WHERE mahasiswa_nim = '" . $kode . "';";
        $this->db->query($QRY);
        return;
    }
    
}

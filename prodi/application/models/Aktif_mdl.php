<?php

class Aktif_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(aktif_id) AS jumlah FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE a.no_prodi = '" . $this->session->kodeaktif . "' LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($ids) {
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE a.aktif_id = '" . $ids . "' AND a.no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function ruangan() {
        $QRY = "SELECT * FROM ruangan ORDER BY ruangan_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function simpan_tambah_jadwal($data) {
        $this->db->insert('jadwal',$data);
    }

    public function ubah_status_aktif($aktif_id) {
        $QRY = "UPDATE kelas_aktif SET proses='1' WHERE aktif_id='$aktif_id'";
        $query = $this->db->query($QRY);
    }

    public function upload_file($filename){
        $this->load->library('upload'); // Load librari upload
        
        $config['upload_path'] = '../img/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size'] = '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $filename;
    
        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload('file')){ // Lakukan upload dan Cek jika proses upload berhasil
            // Jika berhasil :
            $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            return $return;
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            return $return;
        }
    }

    public function plotting_pagi() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT a.aktif_id, b.kelas_nama, b.kelas_angkatan, c.matakuliah_nama, c.matakuliah_kode, d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
        FROM kelas_aktif a
        JOIN kelas AS b ON (a.kelas_id = b.kelas_id) 
        JOIN matakuliah AS c ON (a.matakuliah_kode_utama = c.matakuliah_kode_utama) 
        JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
        JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
        WHERE a.semester='$row2[semester]' 
        ORDER BY c.matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function plotting_sore() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT a.aktif_id, b.kelas_nama, b.kelas_angkatan, c.matakuliah_nama, c.matakuliah_kode, d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
        FROM kelas_aktif_sore a
        JOIN kelas AS b ON (a.kelas_id = b.kelas_id) 
        JOIN matakuliah AS c ON (a.matakuliah_kode_utama = c.matakuliah_kode_utama) 
        JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
        JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
        WHERE a.semester='$row2[semester]' 
        ORDER BY c.matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function plotting_pagi_belum() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT a.aktif_id, b.kelas_nama, b.kelas_angkatan, c.matakuliah_nama, c.matakuliah_kode, d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
        FROM kelas_aktif a
        JOIN kelas AS b ON (a.kelas_id = b.kelas_id) 
        JOIN matakuliah AS c ON (a.matakuliah_kode_utama = c.matakuliah_kode_utama) 
        JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
        JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
        -- WHERE a.semester='$row2[semester]' 
        WHERE a.proses='0'
        ORDER BY c.matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function plotting_sore_belum() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT a.aktif_id, b.kelas_nama, b.kelas_angkatan, c.matakuliah_nama, c.matakuliah_kode, d.dosen_nip AS nip_teori, d.dosen_nama AS nama_teori, e.dosen_nip AS nip_praktek, e.dosen_nama AS nama_praktek, a.dosen3
        FROM kelas_aktif_sore a
        JOIN kelas AS b ON (a.kelas_id = b.kelas_id) 
        JOIN matakuliah AS c ON (a.matakuliah_kode_utama = c.matakuliah_kode_utama) 
        JOIN dosen AS d ON (a.dosen_nip = d.dosen_nip)
        JOIN dosen AS e ON (a.dosen_nip_p = e.dosen_nip) 
        -- WHERE a.semester='$row2[semester]'
        WHERE a.proses='0' 
        ORDER BY c.matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function jadwal_pagi() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT jadwal.aktif_id, jadwal.jadwal_hari, jadwal.jadwal_jam_mulai, jadwal.jadwal_jam_berakhir, ruangan.ruangan_nama, kelas.kelas_nama, kelas.kelas_angkatan, matakuliah.matakuliah_nama, matakuliah.matakuliah_kode, dosen.dosen_nip, dosen.dosen_nama FROM jadwal
        JOIN master_hari ON (jadwal.jadwal_hari = master_hari.hari)
        JOIN kelas_aktif ON (jadwal.aktif_id = kelas_aktif.aktif_id) 
        JOIN kelas ON (kelas_aktif.kelas_id = kelas.kelas_id) 
        JOIN ruangan ON (jadwal.ruangan = ruangan.ruangan_id) 
        JOIN matakuliah ON (kelas_aktif.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama) 
        JOIN dosen ON (kelas_aktif.dosen_nip = dosen.dosen_nip) 
        WHERE kelas_aktif.semester='$row2[semester]'
        ORDER BY master_hari.id ASC, jadwal.jadwal_jam_mulai ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function insert_multiple($data){
            $this->db->insert_batch('kelas_aktif', $data);
    }

    public function insert_multiple_sore($data){
            $this->db->insert_batch('kelas_aktif_sore', $data);
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE (a.dosen_nip LIKE '%" . $keyword . "%' OR c.dosen_nama LIKE '%" . $keyword . "%' OR b.matakuliah_nama LIKE '%" . $keyword . "%' OR d.kelas_nama LIKE '%" . $keyword . "%') AND a.no_prodi = '" . $this->session->kodeaktif . "' LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3) {
        $matakuliah = $this->doclean($matakuliah);
        $kelas = $this->doclean($kelas);
        $dosen = $this->doclean($dosen);
        $dosenp = $this->doclean($dosenp);
        $dosen3 = $this->doclean($dosen3);

        $prodi = $this->doclean($prodi);
        $semester = $this->doclean($semester);
        
        if($dosen3=='' OR $dosen3==0){
            $QRY = "INSERT INTO kelas_aktif (no_prodi, dosen_nip, kelas_id, matakuliah_kode_utama, ruangan_khusus, semester, dosen_nip_p) VALUES (";
            $QRY = $QRY . "'" . $prodi . "', ";
            $QRY = $QRY . "'" . $dosen . "', ";
            $QRY = $QRY . "'" . $kelas . "', ";
            $QRY = $QRY . "'" . $matakuliah . "', ";
            $QRY = $QRY . "'0', ";
            $QRY = $QRY . "'" . $semester . "', ";
            $QRY = $QRY . "'" . $dosenp . "'";
            $QRY = $QRY . ");";
        }else{
            $QRY = "INSERT INTO kelas_aktif (no_prodi, dosen_nip, kelas_id, matakuliah_kode_utama, ruangan_khusus, semester, dosen_nip_p, dosen3) VALUES (";
            $QRY = $QRY . "'" . $prodi . "', ";
            $QRY = $QRY . "'" . $dosen . "', ";
            $QRY = $QRY . "'" . $kelas . "', ";
            $QRY = $QRY . "'" . $matakuliah . "', ";
            $QRY = $QRY . "'0', ";
            $QRY = $QRY . "'" . $semester . "', ";
            $QRY = $QRY . "'" . $dosenp . "', ";
            $QRY = $QRY . "'" . $dosen3 . "'";
            $QRY = $QRY . ");";
        }
        
        $this->db->query($QRY);
        return;
    }
    
    public function edit($ids, $prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3) {
        $ids = $this->doclean($ids);
        $matakuliah = $this->doclean($matakuliah);
        $kelas = $this->doclean($kelas);
        $dosen = $this->doclean($dosen);
        $dosenp = $this->doclean($dosenp);
        $prodi = $this->doclean($prodi);
        $semester = $this->doclean($semester);
        $dosen3 = $this->doclean($dosen3);
        
        if($dosen3=='' OR $dosen3==0){
            $QRY = "UPDATE kelas_aktif SET ";
            $QRY = $QRY . "matakuliah_kode_utama='" . $matakuliah . "', ";
            $QRY = $QRY . "kelas_id='" . $kelas . "', ";
            $QRY = $QRY . "dosen_nip_p='" . $dosenp . "', ";
            $QRY = $QRY . "dosen_nip='" . $dosen . "', ";
            $QRY = $QRY . "dosen3=NULL ";
            $QRY = $QRY . "WHERE aktif_id ='" . $ids . "';";
        }else{
            $QRY = "UPDATE kelas_aktif SET ";
            $QRY = $QRY . "matakuliah_kode_utama='" . $matakuliah . "', ";
            $QRY = $QRY . "kelas_id='" . $kelas . "', ";
            $QRY = $QRY . "dosen_nip_p='" . $dosenp . "', ";
            $QRY = $QRY . "dosen_nip='" . $dosen . "', ";
            $QRY = $QRY . "dosen3='" . $dosen3 . "' ";
            $QRY = $QRY . "WHERE aktif_id ='" . $ids . "';";
        }
        
        
        $this->db->query($QRY);
        return;
    }
    
    public function delete($ids) {
        $ids = $this->doclean($ids);
        $QRY = "DELETE FROM kelas_aktif WHERE aktif_id = '" . $ids . "' AND no_prodi = '" . $this->session->kodeaktif . "';";
        $this->db->query($QRY);
        return;
    }
    
}

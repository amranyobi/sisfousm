<?php

class Jadwal_mdl extends CI_Model {

    public function ruangan_status() {
        $QRY = "SELECT COUNT(DISTINCT ruangan_id) AS jumlah FROM ruangan_prodi WHERE no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function ruangan_list() {
        $QRY = "SELECT * FROM ruangan_prodi a JOIN ruangan b ON (a.ruangan_id = b.ruangan_id) WHERE a.no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function cek_tersedia($kode_jam,$ruangan,$hari) {
        $QRY = "SELECT aktif_id FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND ruangan='$ruangan' AND hari='$hari'";
        $query = $this->db->query($QRY);
        $aktif_id = $query->row_array();
        if($aktif_id['aktif_id']=='' OR $aktif_id['aktif_id']==NULL)
        {
            $hitung = 0;
        }else{
            $hitung = 1;
        }

        return $hitung;
    }

    public function ambil_jam($aktif_id) {
        $QRY = "SELECT b.matakuliah_jam_teori FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) WHERE a.aktif_id='$aktif_id'";
        $query = $this->db->query($QRY);
        return $query->row_array();
    }

    public function hari_selanjutnya() {
        $QRY = "SELECT hari FROM hari_bantu";
        $query = $this->db->query($QRY);
        return $query->row_array();
    }

    public function data_jadwal($aktif_id) {
        $QRY = "SELECT * FROM kelas_aktif WHERE aktif_id='$aktif_id'";
        $query = $this->db->query($QRY);
        return $query->row_array();
    }

    public function cek_kosong_jadwal($ruangan,$hari,$jam_aktif) {
        $QRY = "SELECT aktif_id FROM bantu_jadwal WHERE kode_jam='$jam_aktif' AND ruangan='$ruangan' AND hari='$hari'";
        $query = $this->db->query($QRY);
        return $query->row_array();
    }

    public function data_jadwal_lengkap($aktif_id) {
        $QRY = "SELECT kelas_aktif.*, matakuliah.matakuliah_jam_teori FROM kelas_aktif, matakuliah WHERE kelas_aktif.aktif_id='$aktif_id' AND kelas_aktif.matakuliah_kode_utama=matakuliah.matakuliah_kode_utama";
        $query = $this->db->query($QRY);
        return $query->row_array();
    }

    public function kelas_aktif() {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "SELECT * FROM kelas_aktif a 
        JOIN semester_aktif b ON (a.semester = b.semester)
        JOIN matakuliah c ON (a.matakuliah_kode_utama=c.matakuliah_kode_utama)
        WHERE a.no_prodi = '" . $this->session->kodeaktif . "'
        AND a.kelas_id!='0'
        AND a.matakuliah_kode_utama!=''
        AND a.proses='0'
        AND a.semester='$semester'
        AND b.status='1' ORDER BY RAND()";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_status() {
        $QRY = "SELECT COUNT(jadwal_id) AS jumlah FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) WHERE b.no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function semester_aktif() {
        $QRY = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query = $this->db->query($QRY);
        $row = $query->row_array();
        return $row['semester'];
    }

    public function dosen_aktif($semester) {
        $QRY = "SELECT * FROM dosen ORDER BY dosen_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function jenis_kelas() {
        $QRY = "SELECT kelas_nama FROM kelas GROUP BY kelas_nama ORDER BY kelas_nama";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function matakuliah() {
        $QRY = "SELECT * FROM matakuliah ORDER BY smt ASC, matakuliah_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function hitung_bentrok($dosen,$dosen2,$kelas,$kode_jam,$hari) {
        $QRY = "SELECT id_bantu FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND hari='$hari' AND 
        (dosen='$dosen' OR dosen='$dosen2' OR dosen2='$dosen' OR dosen2='$dosen2')";
        $query = $this->db->query($QRY);
        $hitung_dosen = $query->num_rows();

        $QRY2 = "SELECT id_bantu FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND kelas_id='$kelas' AND hari='$hari'";
        $query2 = $this->db->query($QRY2);
        $hitung_kelas = $query2->num_rows();

        $total_hitung = $hitung_dosen+$hitung_kelas;

        return $total_hitung;
    }

    public function hitung_bentrok2($dosen,$dosen2,$kelas,$kode_jam,$hari,$aktif_id) {
        $QRY = "SELECT id_bantu FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND hari='$hari' AND 
        (dosen='$dosen' OR dosen='$dosen2' OR dosen2='$dosen' OR dosen2='$dosen2') AND aktif_id!='$aktif_id'";
        $query = $this->db->query($QRY);
        $hitung_dosen = $query->num_rows();

        $QRY2 = "SELECT id_bantu FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND kelas_id='$kelas' AND hari='$hari' AND aktif_id!='$aktif_id'";
        $query2 = $this->db->query($QRY2);
        $hitung_kelas = $query2->num_rows();

        $total_hitung = $hitung_dosen+$hitung_kelas;

        return $total_hitung;
    }

    public function cek_kosong($kode_jam,$ruangan) {
        $QRY = "SELECT id_bantu FROM bantu_jadwal WHERE kode_jam='$kode_jam' AND $ruangan='$ruangan'";
        $query = $this->db->query($QRY);
        $hitung_kosong = $query->num_rows();
        return $hitung_kosong;
    }

    public function belum_terjadwal() {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "SELECT aktif_id FROM kelas_aktif WHERE proses='0' AND semester='$semester'";
        $query = $this->db->query($QRY);
        $hitung_kosong = $query->num_rows();
        return $hitung_kosong;
    }

    public function cek_ruang_lain($aktif_id,$ruangan) {
        $QRY = "SELECT id_bantu FROM bantu_jadwal WHERE ruangan!='$ruangan' AND aktif_id='$aktif_id'";
        $query = $this->db->query($QRY);
        $hitung_ruang_lain = $query->num_rows();
        return $hitung_ruang_lain;
    }

    public function input_jadwal($dosen,$dosen2,$dosen3,$kelas,$kode_jam,$aktif_id,$ruangan) {
        $QRY = "INSERT INTO bantu_jadwal (dosen,dosen2,dosen3,kelas_id,kode_jam,aktif_id,ruangan) VALUES ('$dosen','$dosen2','$dosen3','$kelas','$kode_jam','$aktif_id','$ruangan')";
        $query = $this->db->query($QRY);
        // return $query->num_rows();
    }

    public function input_mapping_ruang($kode_jam,$ruangan,$hari) {
        $QRY = "INSERT INTO bantu_jadwal (kode_jam,ruangan,hari) VALUES ('$kode_jam','$ruangan','$hari')";
        $query = $this->db->query($QRY);
        // return $query->num_rows();
    }

    public function buang_sampah($aktif_id) {
        $ht = "SELECT aktif_id FROM sampah WHERE aktif_id='$aktif_id'";
        $hitung = $this->db->query($ht);
        $hitung_kosong = $hitung->num_rows();

        if($hitung_kosong=='0'){
            $QRY = "INSERT INTO sampah (aktif_id) VALUES ('$aktif_id')";
            $query = $this->db->query($QRY);
        }
    }

    public function mapping_ruang() {
        $hari = "SELECT hari FROM hari_bantu ORDER BY id ASC";
        $dino = $this->db->query($hari);
        $day = $dino->row_array();
        $now = $day['hari'];

        $ht = "SELECT id_bantu FROM bantu_jadwal WHERE aktif_id IS NOT NULL";
        $hitung = $this->db->query($ht);
        $htt = $hitung->num_rows();
        if($htt=='0')
            $sekarang = 1;
        else
            $sekarang = $now;

        $QRY = "SELECT * FROM bantu_jadwal WHERE aktif_id IS NULL AND hari='$sekarang' ORDER BY id_bantu ASC";
        $query = $this->db->query($QRY);

        if($sekarang=='5')
            $next = 1;
        else
            $next = $sekarang + 1;

        $upd = "UPDATE hari_bantu SET hari = '$next'";
        $this->db->query($upd);

        return $query->result();
    }

    public function cek_jadwal($aktif_id){
        $ht = "SELECT * FROM bantu_jadwal WHERE aktif_id='$aktif_id'";
        $hasil = $this->db->query($ht);
        return $hasil->row_array();
    }

    public function cek_jadwal_bantu($id_bantu){
        $ht = "SELECT * FROM bantu_jadwal WHERE id_bantu='$id_bantu'";
        $hasil = $this->db->query($ht);
        return $hasil->row_array();
    }

    public function data_sampah() {
        $QRY = "SELECT * FROM sampah ORDER BY id ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function isikan_jadwal($dosen,$dosen2="",$dosen3="",$kelas_id,$kelas_aktif,$id_bantu){
        $QRY = "UPDATE bantu_jadwal SET dosen = '$dosen', dosen2='$dosen2', dosen3='$dosen3', kelas_id='$kelas_id', aktif_id='$kelas_aktif' WHERE id_bantu = '$id_bantu'";
        $this->db->query($QRY);
    }

    public function ubah_jadwal_baru($dosen,$dosen2,$dosen3,$kelas_id,$aktif_id,$kode_jam,$ruangan,$hari){
        $QRY = "UPDATE bantu_jadwal SET dosen = '$dosen', dosen2='$dosen2', dosen3='$dosen3', kelas_id='$kelas_id', aktif_id='$aktif_id' WHERE kode_jam = '$kode_jam' AND ruangan='$ruangan' AND hari='$hari'";
        $this->db->query($QRY);
    }

    public function hapus_setengah($aktif_id){
        $QRY = "UPDATE bantu_jadwal SET dosen = NULL, dosen2=NULL, dosen3=NULL, kelas_id=NULL, aktif_id=NULL WHERE aktif_id = '$aktif_id'";
        $this->db->query($QRY);
    }

    public function reset_jadwal(){
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "UPDATE bantu_jadwal SET dosen = NULL, dosen2=NULL, dosen3=NULL, kelas_id=NULL, aktif_id=NULL";
        $this->db->query($QRY);
        $proses = "UPDATE kelas_aktif SET proses = 0 WHERE semester='$semester'";
        $this->db->query($proses);
    }

    public function kembalikan($aktif_id){
        $QRY = "UPDATE kelas_aktif SET proses = '0' WHERE aktif_id = '$aktif_id'";
        $this->db->query($QRY);
    }

    public function habiskan_sampah(){
        $this->db->truncate('sampah');
    }

    public function get_list_d($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT a.jenis, e.matakuliah_nama, e.matakuliah_kode, c.dosen_nip as d_nip_teori, c.dosen_nama as d_nama_teori, f.dosen_nip as d_nip_praktek, f.dosen_nama as d_nama_praktek, d.kelas_nama, d.kelas_angkatan, a.jadwal_hari, a.jadwal_jam_mulai, a.jadwal_jam_berakhir, r.ruangan_nama FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) JOIN dosen f ON (b.dosen_nip_p = f.dosen_nip) JOIN ruangan r ON (a.ruangan = r.ruangan_id) WHERE b.no_prodi = '" . $this->session->kodeaktif . "' ORDER BY d.kelas_angkatan, d.kelas_nama LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE b.no_prodi = '" . $this->session->kodeaktif . "' ORDER BY d.kelas_angkatan, d.kelas_nama LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_item($kelas_id) {
        $QRY = "SELECT * FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE a.jadwal_id = '" . $kelas_id . "' AND b.no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function search_d($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT a.jenis, e.matakuliah_nama, e.matakuliah_kode, c.dosen_nip as d_nip_teori, c.dosen_nama as d_nama_teori, f.dosen_nip as d_nip_praktek, f.dosen_nama as d_nama_praktek, d.kelas_nama, d.kelas_angkatan, a.jadwal_hari, a.jadwal_jam_mulai, a.jadwal_jam_berakhir FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) JOIN dosen f ON (b.dosen_nip_p = f.dosen_nip) WHERE (d.kelas_nama LIKE '%" . $keyword . "%' OR d.kelas_angkatan LIKE '%" . $keyword . "%' OR e.matakuliah_nama LIKE '%" . $keyword . "%' OR c.dosen_nama LIKE '%" . $keyword . "%' OR c.dosen_nip LIKE '%" . $keyword . "%' OR f.dosen_nama LIKE '%" . $keyword . "%' OR f.dosen_nip LIKE '%" . $keyword . "%') AND b.no_prodi = '" . $this->session->kodeaktif . "' ORDER BY d.kelas_angkatan, d.kelas_nama LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE (d.kelas_nama LIKE '%" . $keyword . "%' OR d.kelas_angkatan LIKE '%" . $keyword . "%' OR e.matakuliah_nama LIKE '%" . $keyword . "%' OR c.dosen_nama LIKE '%" . $keyword . "%' OR c.dosen_nip LIKE '%" . $keyword . "%') AND b.no_prodi = '" . $this->session->kodeaktif . "' ORDER BY d.kelas_angkatan, d.kelas_nama LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }

    public function add($nama, $angkatan, $prodi) {
        $nama = $this->doclean($nama);
        $angkatan = $this->doclean($angkatan);

        $QRY = "INSERT INTO kelas(kelas_nama, kelas_angkatan, no_prodi) VALUES (";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $angkatan . "', ";
        $QRY = $QRY . "'" . $prodi . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function edit($id, $nama, $angkatan, $prodi) {
        $id = $this->doclean($id);
        $nama = $this->doclean($nama);
        $angkatan = $this->doclean($angkatan);

        $QRY = "UPDATE kelas SET ";
        $QRY = $QRY . "kelas_nama='" . $nama . "', ";
        $QRY = $QRY . "kelas_angkatan='" . $angkatan . "', ";
        $QRY = $QRY . "no_prodi='" . $prodi . "' ";
        $QRY = $QRY . "WHERE kelas_id='" . $id . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }

    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE kelas_id = '" . $id . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM kelas_peserta WHERE kelas_id = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM kelas WHERE kelas_id = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }

    public function get_list_peserta($num = 10, $page = 1, $kelas = "-", $siswa = "-") {
        $start = ($page - 1) * $num;
        if (($kelas == "-") && ($siswa == "-")) {
            $QRY = "SELECT * FROM kelas_peserta LIMIT " . $start . ", " . $num;
        }
        else if(($kelas == "-") && ($siswa != "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE mahasiswa_nim = '" . $siswa . "' LIMIT " . $start . ", " . $num;
        }
        else if(($kelas != "-") && ($siswa == "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE kelas_id = '" . $kelas . "' LIMIT " . $start . ", " . $num;
        }
        else if(($kelas != "-") && ($siswa != "-")) {
            $QRY = "SELECT * FROM kelas_peserta WHERE mahasiswa_nim = '" . $siswa . "' AND kelas_id = '" . $kelas . "' LIMIT " . $start . ", " . $num;
        }
        else {
            $QRY = "SELECT * FROM kelas_peserta LIMIT " . $start . ", " . $num;
        }
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function clean() {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "UPDATE kelas_aktif SET proses = '0' WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "'";
        $this->db->query($QRY);
        $QRY = "DELETE FROM jadwal WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM algoritma_daftar_dijadwalkan WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM algoritma_temp_jadwal WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "');";
        $this->db->query($QRY);
        return;
    }

    public function clean2() {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "DELETE FROM algoritma_daftar_dijadwalkan WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM algoritma_temp_jadwal WHERE aktif_id IN (SELECT aktif_id FROM kelas_aktif WHERE no_prodi = '" . $this->session->kodeaktif . "' AND semester = '" . $semester . "');";
        $this->db->query($QRY);
        return;
    }

    public function get_list_a($num = 10, $page = 1) {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas_aktif b JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE b.no_prodi = '" . $this->session->kodeaktif . "' AND b.semester = '" . $semester . "' AND proses = '0' LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function add_ka($ka, $jenis, $jam) {
        $QRY = "INSERT INTO algoritma_daftar_dijadwalkan (aktif_id, algoritma_jenis, algoritma_jam) VALUES (";
        $QRY = $QRY . "'" . $ka . "', ";
        $QRY = $QRY . "'" . $jenis . "', ";
        $QRY = $QRY . "'" . $jam . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function update_list_a($id) {
        $QRY = "UPDATE kelas_aktif SET proses = '1' WHERE aktif_id = '" . $id . "'";
        $this->db->query($QRY);
        return;
    }

    public function get_list_b($num = 10, $page = 1) {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM algoritma_daftar_dijadwalkan a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE b.no_prodi = '" . $this->session->kodeaktif . "' AND b.semester = '" . $semester . "' ORDER BY a.algoritma_jenis DESC LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function update_jadwal_a($algoritma_hari, $algoritma_jam_ke, $ruangan_id, $aktif_id, $daftar_id) {
        $algoritma_hari = $this->doclean($algoritma_hari);
        $algoritma_jam_ke = $this->doclean($algoritma_jam_ke);
        $ruangan_id = $this->doclean($ruangan_id);
        $aktif_id = $this->doclean($aktif_id);
        $daftar_id = $this->doclean($daftar_id);
        $QRY = "INSERT INTO algoritma_temp_jadwal (algoritma_hari, algoritma_jam_ke, ruangan_id, aktif_id, daftar_id) VALUES (";
        $QRY = $QRY . "'" . $algoritma_hari . "', ";
        $QRY = $QRY . "'" . $algoritma_jam_ke . "', ";
        $QRY = $QRY . "'" . $ruangan_id . "', ";
        $QRY = $QRY . "'" . $aktif_id . "', ";
        $QRY = $QRY . "'" . $daftar_id . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function update_jadwal_b($jadwal_hari, $jadwal_jam_mulai, $jadwal_jam_berakhir, $aktif_id, $jenis, $ruangan_id) {
        $jadwal_hari = $this->doclean($jadwal_hari);
        $jadwal_jam_mulai = $this->doclean($jadwal_jam_mulai);
        $jadwal_jam_berakhir = $this->doclean($jadwal_jam_berakhir);
        $aktif_id = $this->doclean($aktif_id);
        $jenis = $this->doclean($jenis);
        $ruangan_id = $this->doclean($ruangan_id);
        $QRY = "INSERT INTO jadwal (jadwal_hari, jadwal_jam_mulai, jadwal_jam_berakhir, aktif_id, jenis, ruangan) VALUES (";
        $QRY = $QRY . "'" . $jadwal_hari . "', ";
        $QRY = $QRY . "'" . $jadwal_jam_mulai . "', ";
        $QRY = $QRY . "'" . $jadwal_jam_berakhir . "', ";
        $QRY = $QRY . "'" . $aktif_id . "', ";
        $QRY = $QRY . "'" . $jenis . "', ";
        $QRY = $QRY . "'" . $ruangan_id . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }

    public function get_jadwal_a($algoritma_id) {
        $QRY = "SELECT * FROM algoritma_daftar_dijadwalkan a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE a.algoritma_id = '" . $algoritma_id . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }

    public function delete_jadwal($ids) {
        $ids = $this->doclean($ids);
        $QRY = "DELETE FROM jadwal WHERE aktif_id = '" . $ids . "'";
        $this->db->query($QRY);
        $QRY2 = "UPDATE kelas_aktif SET proses = '0' WHERE aktif_id = '" . $ids . "'";
        $this->db->query($QRY2);
        return;
    }

    public function kelas_aktif_semua() {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "SELECT * FROM kelas_aktif a 
        JOIN semester_aktif b ON (a.semester = b.semester)
        JOIN matakuliah c ON (a.matakuliah_kode_utama=c.matakuliah_kode_utama)
        WHERE a.no_prodi = '" . $this->session->kodeaktif . "'
        AND a.kelas_id!='0'
        AND a.matakuliah_kode_utama!=''
        AND a.semester='$semester'
        AND b.status='1' ORDER BY RAND()";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function cek_sudah($aktif_id){
        $ht = "SELECT id_bantu FROM bantu_jadwal WHERE aktif_id='$aktif_id'";
        $hasil = $this->db->query($ht);
        return $hasil->num_rows();
    }

    public function get_dosen_list() {
        $QRY = "SELECT dosen_nip, dosen_nama FROM dosen ORDER BY dosen_nama ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_jadwal_dosen($dosen_nip) {
        $semester = $this->session->tahunajaran . '' . $this->session->semester;
        $QRY = "SELECT b.id_bantu, b.dosen, b.dosen2, b.dosen3, b.kelas_id, b.kode_jam, b.aktif_id, b.ruangan, b.hari, 
                       e.matakuliah_nama, e.matakuliah_kode, d.kelas_nama, d.kelas_angkatan, r.ruangan_nama
                FROM bantu_jadwal b 
                JOIN kelas_aktif a ON b.aktif_id = a.aktif_id 
                JOIN dosen c ON a.dosen_nip = c.dosen_nip 
                JOIN kelas d ON a.kelas_id = d.kelas_id 
                JOIN matakuliah e ON a.matakuliah_kode_utama = e.matakuliah_kode_utama 
                JOIN ruangan r ON b.ruangan = r.ruangan_id 
                WHERE (b.dosen = ? OR b.dosen2 = ?) AND b.tahunajaran = ?
                ORDER BY FIELD(b.hari, '1', '2', '3', '4', '5'), b.kode_jam";
        $query = $this->db->query($QRY, array($dosen_nip, $dosen_nip, $semester));
        $results = $query->result();

        $JAM_MAP = [
            1 => "08.00 - 08.50",
            2 => "08.50 - 09.40",
            3 => "09.40 - 10.30",
            4 => "10.30 - 11.20",
            5 => "11.20 - 12.10",
            6 => "12.10 - 13.00",
            7 => "13.00 - 13.50",
            8 => "13.50 - 14.40",
            9 => "14.40 - 15.30"
        ];

        foreach ($results as &$result) {
            $result->jadwal_waktu = isset($JAM_MAP[$result->kode_jam]) ? $JAM_MAP[$result->kode_jam] : 'Unknown';
            $result->jadwal_hari = $this->convert_hari($result->hari);
        }

        return $results;
    }

    public function convert_hari($hari) {
        $hari_list = [
            '1' => 'Senin',
            '2' => 'Selasa',
            '3' => 'Rabu',
            '4' => 'Kamis',
            '5' => 'Jumat'
        ];
        return isset($hari_list[$hari]) ? $hari_list[$hari] : 'Unknown';
    }

    public function get_dosen_name($dosen_nip) {
        $QRY = "SELECT dosen_nama FROM dosen WHERE dosen_nip = ?";
        $query = $this->db->query($QRY, array($dosen_nip));
        $result = $query->row();
        return $result ? $result->dosen_nama : '';
    }
}

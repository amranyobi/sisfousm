<?php

class Kurikulum_mdl extends CI_Model {
    public function get_status() {
        $QRY = "SELECT COUNT(kurikulum_id) AS jumlah FROM kurikulum";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kurikulum LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($id) {
        $QRY = "SELECT * FROM kurikulum WHERE kurikulum_id = '" . $id . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM kurikulum WHERE (kurikulum_nama LIKE '%" . $keyword . "%' OR kurikulum_tahun LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($nama, $tahun) {
        $nama = $this->doclean($nama);
        $tahun = $this->doclean($tahun);
        
        $QRY = "INSERT INTO kurikulum(kurikulum_nama, kurikulum_tahun) VALUES (";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $tahun . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($id, $nama, $tahun) {
        $id = $this->doclean($id);
        $nama = $this->doclean($nama);
        $tahun = $this->doclean($tahun);
        
        $QRY = "UPDATE kurikulum SET ";
        $QRY = $QRY . "kurikulum_nama='" . $nama . "', ";
        $QRY = $QRY . "kurikulum_tahun='" . $tahun . "' ";
        $QRY = $QRY . "WHERE kurikulum_id='" . $id . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }
    
    public function delete($id) {
        $id = $this->doclean($id);
        $QRY = "DELETE FROM jadwal WHERE matakuliah_kode IN (SELECT matakuliah_kode FROM matakuliah WHERE kurikulum_id = '" . $id . "');";
        $this->db->query($QRY);
        $QRY = "DELETE FROM matakuliah WHERE kurikulum_id = '" . $id . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM kurikulum WHERE kurikulum_id = '" . $id . "';";
        $this->db->query($QRY);
        return;
    }
    
}

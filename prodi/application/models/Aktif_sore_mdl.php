<?php

class Aktif_sore_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(aktif_id) AS jumlah FROM kelas_aktif_sore WHERE no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas_aktif_sore a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE a.no_prodi = '" . $this->session->kodeaktif . "' LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($ids) {
        $QRY = "SELECT * FROM kelas_aktif_sore a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE a.aktif_id = '" . $ids . "' AND a.no_prodi = '" . $this->session->kodeaktif . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM kelas_aktif_sore a JOIN matakuliah b ON (a.matakuliah_kode_utama = b.matakuliah_kode_utama) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE (a.dosen_nip LIKE '%" . $keyword . "%' OR c.dosen_nama LIKE '%" . $keyword . "%' OR b.matakuliah_nama LIKE '%" . $keyword . "%' OR d.kelas_nama LIKE '%" . $keyword . "%') AND a.no_prodi = '" . $this->session->kodeaktif . "' LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3) {
        $matakuliah = $this->doclean($matakuliah);
        $kelas = $this->doclean($kelas);
        $dosen = $this->doclean($dosen);
        $dosenp = $this->doclean($dosenp);
        $prodi = $this->doclean($prodi);
        $semester = $this->doclean($semester);
        $dosen3 = $this->doclean($dosen3);
        
        if($dosen3=='' OR $dosen3==0){
            $QRY = "INSERT INTO kelas_aktif_sore (no_prodi, dosen_nip, kelas_id, matakuliah_kode_utama, ruangan_khusus, semester, dosen_nip_p) VALUES (";
            $QRY = $QRY . "'" . $prodi . "', ";
            $QRY = $QRY . "'" . $dosen . "', ";
            $QRY = $QRY . "'" . $kelas . "', ";
            $QRY = $QRY . "'" . $matakuliah . "', ";
            $QRY = $QRY . "'0', ";
            $QRY = $QRY . "'" . $semester . "', ";
            $QRY = $QRY . "'" . $dosenp . "'";
            $QRY = $QRY . ");";
        }else{
            $QRY = "INSERT INTO kelas_aktif_sore (no_prodi, dosen_nip, kelas_id, matakuliah_kode_utama, ruangan_khusus, semester, dosen_nip_p, dosen3) VALUES (";
            $QRY = $QRY . "'" . $prodi . "', ";
            $QRY = $QRY . "'" . $dosen . "', ";
            $QRY = $QRY . "'" . $kelas . "', ";
            $QRY = $QRY . "'" . $matakuliah . "', ";
            $QRY = $QRY . "'0', ";
            $QRY = $QRY . "'" . $semester . "', ";
            $QRY = $QRY . "'" . $dosenp . "', ";
            $QRY = $QRY . "'" . $dosen3 . "'";
            $QRY = $QRY . ");";
        }

        

        $this->db->query($QRY);
        return;
    }
    
    public function edit($ids, $prodi, $matakuliah, $kelas, $dosen, $semester, $dosenp, $dosen3) {
        $ids = $this->doclean($ids);
        $matakuliah = $this->doclean($matakuliah);
        $kelas = $this->doclean($kelas);
        $dosen = $this->doclean($dosen);
        $dosenp = $this->doclean($dosenp);
        $prodi = $this->doclean($prodi);
        $semester = $this->doclean($semester);
        $dosen3 = $this->doclean($dosen3);
        if($dosen3=='' OR $dosen3==0){
            $QRY = "UPDATE kelas_aktif_sore SET ";
            $QRY = $QRY . "matakuliah_kode_utama='" . $matakuliah . "', ";
            $QRY = $QRY . "kelas_id='" . $kelas . "', ";
            $QRY = $QRY . "dosen_nip_p='" . $dosenp . "', ";
            $QRY = $QRY . "dosen_nip='" . $dosen . "', ";
            $QRY = $QRY . "dosen3=NULL ";
            $QRY = $QRY . "WHERE aktif_id ='" . $ids . "';";
        }else{
            $QRY = "UPDATE kelas_aktif_sore SET ";
            $QRY = $QRY . "matakuliah_kode_utama='" . $matakuliah . "', ";
            $QRY = $QRY . "kelas_id='" . $kelas . "', ";
            $QRY = $QRY . "dosen_nip_p='" . $dosenp . "', ";
            $QRY = $QRY . "dosen_nip='" . $dosen . "', ";
            $QRY = $QRY . "dosen3='" . $dosen3 . "' ";
            $QRY = $QRY . "WHERE aktif_id ='" . $ids . "';";
        }
        
        $this->db->query($QRY);
        return;
    }
    
    public function delete($ids) {
        $ids = $this->doclean($ids);
        $QRY = "DELETE FROM kelas_aktif_sore WHERE aktif_id = '" . $ids . "' AND no_prodi = '" . $this->session->kodeaktif . "';";
        $this->db->query($QRY);
        return;
    }

    public function jadwal_sore() {
        //aktif
        $QRY2 = "SELECT semester FROM semester_aktif WHERE status='1'";
        $query2 = $this->db->query($QRY2);
        $row2 = $query2->row_array();

        $QRY = "SELECT jadwal_sore.aktif_id, jadwal_sore.jadwal_hari, jadwal_sore.jadwal_jam_mulai, jadwal_sore.jadwal_jam_berakhir, ruangan.ruangan_nama, kelas.kelas_nama, kelas.kelas_angkatan, matakuliah.matakuliah_nama, matakuliah.matakuliah_kode, dosen.dosen_nip, dosen.dosen_nama FROM jadwal_sore
        JOIN master_hari ON (jadwal_sore.jadwal_hari = master_hari.hari)
        JOIN kelas_aktif_sore ON (jadwal_sore.aktif_id = kelas_aktif_sore.aktif_id) 
        JOIN kelas ON (kelas_aktif_sore.kelas_id = kelas.kelas_id) 
        JOIN ruangan ON (jadwal_sore.ruangan = ruangan.ruangan_id) 
        JOIN matakuliah ON (kelas_aktif_sore.matakuliah_kode_utama = matakuliah.matakuliah_kode_utama) 
        JOIN dosen ON (kelas_aktif_sore.dosen_nip = dosen.dosen_nip) 
        WHERE kelas_aktif_sore.semester='$row2[semester]'
        ORDER BY master_hari.id ASC, jadwal_sore.jadwal_jam_mulai ASC";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function simpan_tambah_jadwal($data) {
        $this->db->insert('jadwal_sore',$data);
    }

    public function ubah_status_aktif($aktif_id) {
        $QRY = "UPDATE kelas_aktif_sore SET proses='1' WHERE aktif_id='$aktif_id'";
        $query = $this->db->query($QRY);
    }
    
}

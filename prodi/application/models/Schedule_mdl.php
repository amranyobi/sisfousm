<?php

class Schedule_mdl extends CI_Model {
    public function get_schedule_meta_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $this->db->select('*');
        $this->db->from('penjadwalan');
        $this->db->limit($start, $num);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_schedule_meta($penjadwalan) {
        $this->db->select('*');
        $this->db->from('penjadwalan');
        $this->db->where('penjadwalan_id', $penjadwalan);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_schedule($penjadwalan, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $this->db->select('*');
        $this->db->from('jadwal a');
        $this->db->join('matakuliah b', 'a.matakuliah_kode = b.matakuliah_kode');
        $this->db->join('ruangan c', 'a.ruangan_id = c.ruangan_id');
        $this->db->join('jam_kuliah d', 'a.jam_id = d.jam_id');
        $this->db->where('a.penjadwalan_id', $penjadwalan);
        $this->db->limit($start, $num);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function get_lecturer_schedule($id, $penjadwalan, $num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $this->db->select('*');
        $this->db->from('jadwal a');
        $this->db->join('matakuliah b', 'a.matakuliah_kode = b.matakuliah_kode');
        $this->db->join('ruangan c', 'a.ruangan_id = c.ruangan_id');
        $this->db->join('jam_kuliah d', 'a.jam_id = d.jam_id');
        $this->db->where('a.dosen_nip', $id);
        $this->db->where('a.penjadwalan_id', $penjadwalan);
        $this->db->limit($start, $num);
        $query = $this->db->get();
        return $query->result();
    }

    public function new_scheduling($penjadwalan_nama, $penjadwalan_tahun, $penjadwalan_semester, $penjadwalan_status) {
        $data = array(
            'penjadwalan_nama' => $penjadwalan_nama,
            'penjadwalan_tahun' => $penjadwalan_tahun,
            'penjadwalan_semester' => $penjadwalan_semester,
            'penjadwalan_status' => $penjadwalan_status
        );
        $this->db->insert('penjadwalan', $data);
    } 

    public function update_schedule_meta($penjadwalan_id, $penjadwalan_nama, $penjadwalan_tahun, $penjadwalan_semester, $penjadwalan_status) {
        $data = array(
            'penjadwalan_nama' => $penjadwalan_nama,
            'penjadwalan_tahun' => $penjadwalan_tahun,
            'penjadwalan_semester' => $penjadwalan_semester,
            'penjadwalan_status' => $penjadwalan_status
        );
        $this->db->update('penjadwalan', $data,  array('penjadwalan_id' => $penjadwalan_id));
    }
    
    public function add_schedule($penjadwalan_id, $ruangan_id, $matakuliah_kode, $dosen_nip, $kelas_id, $jam_id, $jadwal_status) {
        $data = array(
            'penjadwalan_id' => $penjadwalan_id,
            'ruangan_id' => $ruangan_id,
            'matakuliah_kode' => $matakuliah_kode,
            'dosen_nip' => $dosen_nip,
            'kelas_id' => $kelas_id,
            'jam_id' => $jam_id,
            'jadwal_status' => $jadwal_status
        );
        $this->db->insert('jadwal', $data);
    }

    public function update_schedule($jadwal_id, $penjadwalan_id, $ruangan_id, $matakuliah_kode, $dosen_nip, $kelas_id, $jam_id, $jadwal_status) {
        $data = array(
            'penjadwalan_id' => $penjadwalan_id,
            'ruangan_id' => $ruangan_id,
            'matakuliah_kode' => $matakuliah_kode,
            'dosen_nip' => $dosen_nip,
            'kelas_id' => $kelas_id,
            'jam_id' => $jam_id,
            'jadwal_status' => $jadwal_status
        );
        $this->db->update('jadwal', $data,  array('jadwal_id' => $jadwal_id));
    }
    
    public function delete_schedule($jadwal_id) {
        $this->db->delete('jadwal', array('jadwal_id' => $jadwal_id));
    }
    
    public function clean_schedule($penjadwalan_id) {
        $this->db->delete('jadwal', array('penjadwalan_id' => $penjadwalan_id));
    }
    
    public function purge_schedule($penjadwalan_id) {
        $this->db->delete('jadwal', array('penjadwalan_id' => $penjadwalan_id));
        $this->db->delete('penjadwalan', array('penjadwalan_id' => $penjadwalan_id));
    }
    
    public function reset_kelas_aktif() {
        $this->db->delete('kelas_aktif');
    }
}

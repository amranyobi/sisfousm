<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <input class="form-control" type="text" name="kode" value="<?php echo $nip; ?>" style="display: none;" />
                <div class="input-block">
                    <label>Nomor Induk Pegawai (NIP)</label>
                    <?php echo "<br/>" . $nip; ?>
                </div>
                <div class="input-block">
                    <label>Nama</label>
                    <?php echo "<br/>" . $nama; ?>
                </div>
                <div class="input-block">
                    <label>Password Lama</label>
                    <input class="form-control" type="password" name="p1" value="" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Password Baru</label>
                    <input class="form-control" type="password" name="p2" value="" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Password Baru (Konfirmasi)</label>
                    <input class="form-control" type="password" name="p3" value="" placeholder="" />
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
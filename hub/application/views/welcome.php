<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sistem Informasi Penjadwalan Fakultas Psikologi USM | Halaman Depan</title>
    <link rel="icon" href="<?php echo CORE_FOLDER; ?>/img/usm-psikologi-pink.png">
    <link rel="stylesheet" href="<?php echo CORE_FOLDER; ?>/AdminLTE/themes/css/home.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/fontawesome-free/css/all.min.css">
    <style>
        .main-icons {
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div id="page">
        <header>
            <div><a class="logo" title="Electronic Archive System" href="./"> <img width="80%" src="<?php echo CORE_FOLDER; ?>/img/usm-psikologi.png"></a></div>
            <!-- <div class="hero">
                <h1>Masukkan Akun Anda</h1>
                <?php if ($log == 1) { ?>
                    <a class="btn" href="#">Welcome, <?php echo $this->session->name; ?></a>    
                <?php } else { ?>
                    <a class="btn" href="<?php echo $base; ?>login">Masuk</a>
                <?php } ?>
            </div> -->
        </header>
        <footer>
            <a title="Personal Website" href="#">&copy;2023 Fakultas Psikologi USM</a>
            <div class="content">
                <!-- <a title="Privacy Policy" href="#">Privacy Policy</a>
                <a title="Term of Service" href="#">Term of Service</a> -->
            </div>
        </footer>
        <nav>
            <ul>
                <li><a title="Tentang" href="">Tentang</a></li>
                <?php if ($log == 1) { ?>
                    <li><a title="Login" href="<?php echo $base; ?>login/close">Keluar</a></li>
                <?php } else { ?>
                    <li><a title="Login" href="<?php echo $base; ?>login">Masuk</a></li>
                <?php } ?>
            </ul>
        </nav>
        <section class="main">
            <?php if (isset($_SESSION['adminer'])) {
                  if ($_SESSION['adminer'] == 1) { ?>
                    <aside>
                        <div class="content">
                            <h3>
                                <br/><span class="main-icons fas fa-users"></span><br/><br/>
                                <a href="../adminer/dashboard">ADMIN DASHBOARD</a>
                            </h3>
                            <p>Admin Dashboard</p>
                        </div>
                    </aside>
            <?php } } ?>
            <?php if (isset($_SESSION['prodi'])) {
                  if ($_SESSION['prodi'] == 1) { ?>
                    <aside>
                        <div class="content">
                            <h3>
                                <br/><span class="main-icons fas fa-tachometer-alt"></span><br/><br/>
                                <a href="../prodi/dashboard">DASHBOARD PRODI</a>
                            </h3>
                            <p>Data Utama Program Studi</p>
                        </div>
                    </aside>
                    <!-- <aside>
                        <div class="content">
                            <h3>
                                <br/><span class="main-icons fas fa-tachometer-alt"></span><br/><br/>
                                <a href="../akreditasi/dashboard">PRODI AKREDITASI</a>
                            </h3>
                            <p>Data dan Kelengkapan Akreditasi Program Studi</p>
                        </div>
                    </aside> -->
            <?php } } ?>
            <!-- <aside>
                <div class="content">
                    <h3>
                        <br/><span class="main-icons fas fa-calendar"></span><br/><br/>
                        <a href="../opendata/schedule">MY SCHEDULE</a>
                    </h3>
                    <p>Find my class schedule </p>
                </div>
            </aside>
            <aside>
                <div class="content">
                    <h3>
                        <br/><span class="main-icons fas fa-calendar"></span><br/><br/>
                        <a href="../opendata/final">FINAL SCHEDULE</a>
                    </h3>
                    <p>Final exam schedule</p>
                </div>
            </aside>
            <aside>
                <div class="content">
                    <h3>
                        <br/><span class="main-icons fas fa-calendar"></span><br/><br/>
                        <a href="../opendata/calendar">CALENDAR</a>
                    </h3>
                    <p>Academic Calendar</p>
                </div>
            </aside> -->
        </section>
        <br/><br/><br/>
    </div>
</body>
</html>

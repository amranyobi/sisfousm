<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("../"); ?>" class="nav-link">Hub</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("dashboard"); ?>" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo base_url("../hub/profile"); ?>" class="nav-link">Profile</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown">
                <a title="logout" class="nav-link" href="<?php echo base_url("../hub/login/close"); ?>">
                    <i class="far fa-times-circle"></i>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?php echo base_url(""); ?>" class="brand-link">
            <img src="<?php echo base_url("img/polines.png"); ?>"
                 alt="PSS Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">PROGRAM STUDI</span>
        </a>

        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="<?php echo $base; ?>img/avatar.php?c=<?php echo $this->session->user; ?>" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="<?php echo base_url("../hub/profile"); ?>" class="d-block"><?php echo $this->session->name; ?></a>
                </div>
            </div>

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                        <a href="<?php echo base_url("dashboard"); ?>" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-database"></i>
                            <p>
                                Data Utama
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url("kelas"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Kelas</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("perkuliahan"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Perkuliahan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("mahasiswa"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Mahasiswa</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("dosen"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Dosen</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url("aktif"); ?>" class="nav-link">
                            <i class="nav-icon fas fa-database"></i>
                            <p>Perkuliahan Aktif</p>
                        </a>
                    </li>
                    <li class="nav-item has-treeview">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-chart-pie"></i>
                            <p>
                                Pelaporan
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url("pbm"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Laporan PBM</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("penilaian"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Penilaian</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url("survei"); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Survei</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </aside>

<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sistem Informasi Penjadwalan Fakultas Psikologi USM | Login</title>
        <link rel="icon" href="<?php echo CORE_FOLDER; ?>/img/usm-psikologi-pink.png">
        <link rel="stylesheet" href="<?php echo CORE_FOLDER; ?>/AdminLTE/themes/css/main.css">
        <link rel="stylesheet" href="<?php echo CORE_FOLDER; ?>/AdminLTE/themes/css/login.css">
        <script src="<?php echo CORE_FOLDER; ?>/AdminLTE/themes/js/main.js"></script>
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>libs/bootstrap.min.css">
        <script src="<?php echo base_url(); ?>libs/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>libs/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>libs/bootstrap.min.js"></script>
    </head>
    <body>
        <div id="container">
            <div class="small-page">
                <div class="login-head">
                    <img src="<?php echo CORE_FOLDER; ?>/img/usm-psikologi-pink.png" class="login-logo"><br/><br/>
                    <h3>Masukkan akun Anda</h3>
                </div>
                <div class="login-message"><?php echo $message; ?></div>
                <div class="login-form">
                    <form action="<?php echo base_url(); ?>Login/form" method="POST">
                        <input style="font-size: .9em;" type="text" name="username" class="form-control input-width center-text cstm-input-txt" placeholder="Username" />
                        <input style="font-size: .9em;" type="password" name="password" class="form-control input-width center-text cstm-input-txt" placeholder="Password" />
                        <input style="font-size: .8em;" type="submit" class="form-control input-width cstm-btn" value="Log IN">
                    </form>
                </div>
                <div class="login-footer center-text">
                    ©2023 Fakultas Psikologi USM
                    <!-- <br/><br/>
                    Apabila anda lupa password, silahkan hubungi operator utama untuk melakukan reset password! -->
                </div>
            </div>
        </div>
    </body>
</html>
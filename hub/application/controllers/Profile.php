<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Dosen', $heading2 = 'Dosen', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->initialize();
        $extra = "formcontrolprofile";
        $data = null;
        $editon = true;
        $title = "| Profile";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('profile') . " / Perbaharuan Data";
        $form = "" . base_url() . "profile/edit_submit";
        $h1 = "Profile";
        $h2 = "Formulir Perbaharuan Profile";

        $data[0][0] = "nip";
        $data[0][1] = "" . $_SESSION['user'];
        $data[1][0] = "nama";
        $data[1][1] = "" . $_SESSION['name'];

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit_submit() {
        $this->initialize();
        $this->load->model('mdl_pegawai');
        $p1 = $this->input->post('p1') . '';
        $p2 = $this->input->post('p2') . '';
        $p3 = $this->input->post('p3') . '';

        if (($p1 != "") && (strlen($p1 . "") > 2)) {
            if ((strlen($p2 . "") > 4)) {
                if ($p2 == $p3) {
                    $this->db->select('*');
                    $this->db->from('operator_login a');
                    $this->db->join('operator b', 'a.operator_username = b.operator_username');
                    $this->db->where('a.operator_username', $_SESSION['user']);
                    $this->db->where('a.operator_password', sha1($p1));
                    $query = $this->db->get();
                    $num = $query->num_rows();
                    if ($num == 1) {
                        $this->mdl_pegawai->edituser($_SESSION['user'], $p2);
                        redirect(base_url('profile/success'));
                    } else {
                        redirect(base_url('profile/failed'));
                    }
                } else {
                    redirect(base_url('profile/failedx'));
                }
            } else {
                redirect(base_url('profile/smol'));
            }
        } else {
            $_SESSION['name'] = $nama;
            redirect(base_url('profile'));
        }
    }

    public function failedx() {
        $this->initialize();
        echo "<script>alert('Kombinasi password baru tidak konsisten');";
        echo 'window.location = "' . base_url() . 'profile";</script>';
    }

    public function failed() {
        $this->initialize();
        echo "<script>alert('Kombinasi password lama tidak ditemukan');";
        echo 'window.location = "' . base_url() . 'profile";</script>';
    }

    public function success() {
        $this->initialize();
        echo "<script>alert('Password telah diubah, silahkan logout untuk menggunakan password baru');";
        echo 'window.location = "' . base_url() . 'profile";</script>';
    }

    public function smol() {
        $this->initialize();
        echo "<script>alert('Password baru kurang dari 5 karakter');";
        echo 'window.location = "' . base_url() . 'profile";</script>';
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private function initialized() {
        $this->load->helper('url');
        $this->load->model('template');
    }

    public function index() {
        $this->initialized();
        $log = $this->template->check_account_login_only();
        
        $data = array(
            'base' => base_url(),
            'log' => $log
        );
        $this->load->view('welcome', $data);
    }

    public function daftar_aktif() {
        
    }

    public function jadwal($kelas) {
        
    }

    public function dosen_ajar($nip) {
        
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private function loadview($message = '') {
        $title = "Login Area";
        $path = "";
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'message' => $message
        );
        $this->load->view('login', $data);
    }

    private function simpleclean($text) {
        $text = str_replace("'", "", $text);
        $text = str_replace('"', "", $text);
        $text = str_replace('\\', "", $text);
        return $text;
    }

    private function createsession($user, $name, $gender, $status) {
        $userdata = array(
            'user' => $user,
            'name' => $name,
            'gender' => $gender
        );

        if ($status == 0) {
            $userdata['adminer'] = 1;
            $userdata['status'] = "admin";
        } else {
            $userdata['status'] = "operator";
        }
        $this->load->model('basic');
        $datas = $this->basic->getOperatorData($user);
        $userdata['prodi'] = 0;
        $userdata['kode'] = "";
        $userdata['kodeaktif'] = "";
        $userdata['tahunajaran'] = "";
        $userdata['semester'] = "";
        foreach ($datas as $data) {
            $userdata['prodi'] = 1;
            $userdata['kodeaktif'] = $data->no_prodi;
            $userdata['kode'] = str_replace("[KODE]", ",", $userdata['kode']);
            $userdata['kode'] = $userdata['kode'] . $data->no_prodi . "[KOMA]";
        }
        $userdata['kode'] = str_replace("[KODE]", "", $userdata['kode']);

        $datas = $this->basic->getAcademicData();
        foreach ($datas as $data) {
            $userdata['tahunajaran'] = $data->tahun_aktif;
            $userdata['semester'] = $data->semester;
        }

        $this->session->set_userdata($userdata);
    }

    public function invalid() {
        $this->load->helper('url');
        $this->loadview('Unable to find user');
    }

    public function index() {
        $this->load->helper('url');
        $this->loadview();
    }

    public function form() {
        $this->load->helper('url');
        $username = $this->simpleclean($this->input->post('username'));
        $password = $this->simpleclean($this->input->post('password'));
        $this->db->select('*');
        $this->db->from('operator_login a');
        $this->db->join('operator b', 'a.operator_username = b.operator_username');
        $this->db->where('a.operator_username', $username);
        $this->db->where('a.operator_password', sha1($password));
        $query = $this->db->get();
        $num = $query->num_rows();
        if ($num == 1) {
            $row = $query->row();
            $this->createsession($row->operator_username, $row->operator_nama, $row->operator_gender, $row->operator_status);
            if($row->operator_status=='1')
            {
                redirect(base_url('../prodi/dashboard'));
            }else{
                redirect(base_url('../adminer/dashboard'));
            }
            // redirect(base_url());
        } else if ($num == 0) {
            redirect(base_url('login/invalid'));
        } else {
            redirect(base_url('error/breach'));
        }
    }

    public function close() {
        $this->load->helper('url');
        $override = array(
            'user',
            'name',
            'gender',
            'status',
            'adminer',
            'prodi',
            'kode'
        );
        $this->session->unset_userdata($override);
        $userdata = array(
            'user' => "",
            'name' => "",
            'gender' => "",
            'status' => "",
            'adminer' => 0,
            'prodi' => 0,
            'kode' => 0
        );
        $this->session->set_userdata($userdata);
        $this->session->unset_userdata($override);
        $this->session->sess_destroy();
        redirect(base_url());
    }

}

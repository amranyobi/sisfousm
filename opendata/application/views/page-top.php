<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ELEKTRO HUB</title>
    <link rel="icon" href="<?php echo $base; ?>img/polines.png">
    <link rel="stylesheet" href="<?php echo base_url("../AdminLTE"); ?>/themes/css/home.css">
    <link rel="stylesheet" href="<?php echo base_url("../AdminLTE"); ?>/plugins/fontawesome-free/css/all.min.css">
    <style>
        .main-icons {
            font-size: 30px;
        }
    </style>
</head>
<body>
    <div id="page">
        <header>
            <a class="logo" title="Electronic Archive System" href="./">EHUB</a>
            <div class="hero">
                <h1>Single Sign On</h1>
                <?php if ($log == 1) { ?>
                    <a class="btn" href="#">Welcome, <?php echo $this->session->name; ?></a>
                <?php } else { ?>
                    <a class="btn" href="<?php echo base_url("../hub"); ?>/login">SIGN IN</a>
                <?php } ?>
            </div>
        </header>
        <footer>
            <a title="Personal Website" href="#">&copy;2020 Teknik Elektro Polines</a>
            <div class="content">
                <a title="Privacy Policy" href="#">Privacy Policy</a>
                <a title="Term of Service" href="#">Term of Service</a>
            </div>
        </footer>
        <nav>
            <ul>
                <li><a title="Tentang" href="<?php echo base_url("../hub"); ?>/about">About</a></li>
                <?php if ($log == 1) { ?>
                    <li><a title="Login" href="<?php echo base_url("../hub"); ?>/login/close">Sign Out</a></li>
                <?php } else { ?>
                    <li><a title="Login" href="<?php echo base_url("../hub"); ?>/login">Sign In</a></li>
                <?php } ?>
            </ul>
        </nav>
        <section class="main">
<script>
    function selectprodi() {
        var prodi = document.getElementById("prodi").value;
        var target_url = "<?php echo base_url("jsondata"); ?>/kelas/" + prodi;
        $.ajax({
            type: 'POST',
            url: target_url,
            success: function (resultData) {
                var result = jQuery.parseJSON(resultData);
                var jml_x = parseInt(result.jumlah);
                var text = "<option value='-'>-- PILIH KELAS --</option>";

                for (var i = 0; i < jml_x; i++) {
                    var kode = result.data[i].kode;
                    var kls = result.data[i].kelas;

                    text = text + "<option value='" + kode + "'>" + kls + "</option>";
                }

                document.getElementById("kelas").innerHTML = text;
            },
            error: function (error, error_, text_message) {

            }
        });
    }

    function openpage() {
        var prodi = document.getElementById("prodi").value;
        var kelas = document.getElementById("kelas").value;
        window.location = "<?php echo base_url("schedule/terjadwal"); ?>/" + prodi + "/" + kelas;
    }
</script>
<style>
    .form-widget-4 {
        width: 20%;
        padding: 10px;
        text-align: center;
        -khtml-border-radius: 30px;
        -moz-border-radius: 30px;
        border-radius: 30px; 
    }
    .form-widget-3 {
        width: 33%;
        padding: 10px;

        text-align: center;

        -khtml-border-radius: 30px;
        -moz-border-radius: 30px;
        border-radius: 30px; 
    }


    .data {
        width: 100%;
        border: #ced4ca solid 1px;
        padding: 10px;
        margin: 0 20px;
    }

    .data h3 {
        font-size: 1.4em;
    }

    .datatable, .datatable tr, .datatable td{
        padding: 0;
        margin: 0;
    }

    .datatable {
        width: 100%;
        font-size: .8em;
    }

    .datatable a, .datatable a:hover, .datatable a:visited {
        color: #000000;
        font-size: .8em;
    }

    .datatable .datatable-heading {
        text-align: center;
        background-color: #0056b3;
    }

    .datatable .datatable-heading .datatable-heading-column{
        font-size: .8em;
        padding: 5px;
        border: #005cbf solid 1px;
        color: #ffffff;
        font-weight: 700;
    }

    .datatable .datatable-oddnumber {
        background-color: rgba(0, 0, 0, .1);
    }

    .datatable .datatable-datarow-column .subdata {
        font-size: .7em;
    }

    .datatable .datatable-datarow-column {
        padding-bottom: 10px;
        padding-top: 10px;
    }

</style>
<div>
    <select name="prodi" onchange="selectprodi();" id="prodi" class="form-widget-3">
        <option value="-">-- PROGRAM STUDI --</option>
        <?php echo $prodi; ?>
    </select>
    <select name="kelas" id="kelas" class="form-widget-3">
        <option value="-">-- KELAS --</option>
        <?php echo $kelas; ?>
    </select>
    <input type="submit" onclick="openpage();" value="Buka Jadwal" class="form-widget-3" />
</div>
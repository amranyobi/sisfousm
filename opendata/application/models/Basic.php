<?php

class Basic extends CI_Model {

    function getOperatorData($username) {
        $QRY = "SELECT * FROM operator_program_studi WHERE operator_username = '" . $username . "'";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    function getAcademicData() {
        $QRY = "SELECT * FROM konfigurasi";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    function getProdiData() {
        $QRY = "SELECT * FROM program_studi";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    function getKelasData($prodi) {
        $QRY = "SELECT * FROM kelas WHERE no_prodi = '" . $prodi . "'";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function getJadwalData($kelas, $semester) {
        $QRY = "SELECT * FROM jadwal a JOIN kelas_aktif b ON (a.aktif_id = b.aktif_id) JOIN dosen c ON (b.dosen_nip = c.dosen_nip) JOIN kelas d ON (b.kelas_id = d.kelas_id) JOIN matakuliah e ON (b.matakuliah_kode_utama = e.matakuliah_kode_utama) WHERE b.kelas_id = '" . $kelas . "' AND b.semester = '" . $semester . "'";
        $query = $this->db->query($QRY);
        return $query->result();
    }
}

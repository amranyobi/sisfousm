<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jsondata extends CI_Controller {

    private function initialized() {
        $this->load->helper('url');
        $this->load->model('basic');
        $this->load->model('template');
    }

    public function kelas($prodi) {
        $this->initialized();
        $prodi = $this->cleandata($prodi);
        $items = $this->basic->getKelasData($prodi);
        $content = '{"data":[';
        $i = 0;
        foreach ($items as $item) {
            $content = str_replace("[KOMA]", ", ", $content);
            $content = $content . '{';
            $content = $content . '"kode": "' . $item->kelas_id . '", ';
            $content = $content . '"kelas": "' . $item->kelas_nama . " (" . $item->kelas_angkatan . ')"';
            $content = $content . '}[KOMA]';
            $i++;
        }
        $content = str_replace("[KOMA]", "", $content);
        $content = $content . '], "jumlah": "' . $i . '"';
        $content = $content . '}';
        echo $content;
    }

    private function cleandata($text) {
        $text = str_replace("'", "", $text);
        $text = str_replace('"', "", $text);
        $text = str_replace("/", "", $text);
        $text = str_replace("\\", "", $text);
        $text = str_replace("  ", "", $text);
        $text = str_replace("&", "", $text);
        $text = str_replace(";", "", $text);
        return $text;
    }

}

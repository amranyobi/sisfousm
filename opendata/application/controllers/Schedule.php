<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

    private function initialized() {
        $this->load->helper('url');
        $this->load->model('basic');
        $this->load->model('template');
    }

    public function index() {
        $this->initialized();
        $log = $this->template->check_account_login_only();
        $content = "";
        $prodi = "";
        $items = $this->basic->getProdiData();

        foreach ($items as $item) {
            $prodi = $prodi . "<option value='" . $item->no_prodi . "'>" . $item->nama_prodi . " (" . $item->jenjang_prodi . ")</option>";
        }

        $data = array(
            'base' => base_url(),
            'log' => $log,
            'content' => $content,
            'prodi' => $prodi,
            'kelas' => ""
        );
        $this->load->view('page-top', $data);
        $this->load->view('widget-schedule-selector', $data);
        $this->load->view('page-content', $data);
        $this->load->view('page-bottom', $data);
    }

    public function terjadwal($prodi, $kelas) {
        $prodi = $this->cleandata($prodi);
        $kelas = $this->cleandata($kelas);
        $this->initialized();
        $log = $this->template->check_account_login_only();
        $content = "";
        $dprodi = "";
        $dkelas = "<option value='-'>-- PILIH KELAS --</option>";

        $SProdi = "";
        $SKelas = "";

        $THN = "";
        $SMS = "";
        $items = $this->basic->getAcademicData();
        foreach ($items as $item) {
            $THN = $item->tahun_aktif . "";
            $SMS = $item->semester . "";
        }
        $SEMESTER = $THN . "" . $SMS;

        $items = $this->basic->getProdiData();
        foreach ($items as $item) {
            if ($item->no_prodi == $prodi) {
                $SProdi = $item->nama_prodi . " (" . $item->jenjang_prodi . ")";
                $dprodi = $dprodi . "<option value='" . $item->no_prodi . "' selected>" . $item->nama_prodi . " (" . $item->jenjang_prodi . ")</option>";
            } else {
                $dprodi = $dprodi . "<option value='" . $item->no_prodi . "'>" . $item->nama_prodi . " (" . $item->jenjang_prodi . ")</option>";
            }
        }
        $items = $this->basic->getKelasData($prodi);
        foreach ($items as $item) {
            if ($item->kelas_id == $kelas) {
                $SKelas = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
                $dkelas = $dkelas . "<option value='" . $item->kelas_id . "' selected>" . $item->kelas_nama . " (" . $item->kelas_angkatan . ")</option>";
            } else {
                $dkelas = $dkelas . "<option value='" . $item->kelas_id . "'>" . $item->kelas_nama . " (" . $item->kelas_angkatan . ")</option>";
            }
        }

        $content = "<br/><br/><h3 style='color: #000000;'>Jadwal Perkuliahan</h3>";
        $content = $content . "<br/><table style='color: #000000;'>";
        $content = $content . "<tr>";
        $content = $content . "<td>Program Studi</td>";
        $content = $content . "<td> : </td>";
        $content = $content . "<td>" . $SProdi . "</td>";
        $content = $content . "</tr>";
        $content = $content . "<tr>";
        $content = $content . "<td>Kelas</td>";
        $content = $content . "<td> : </td>";
        $content = $content . "<td>" . $SKelas . "</td>";
        $content = $content . "</tr>";
        $content = $content . "</table><br/>";

        $content = $content . "<br/><div style='color: #000000;'>";
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "MATAKULIAH";
        $dataheading[2] = "PENGAMPU";
        $dataheading[3] = "KELAS";
        $dataheading[4] = "HARI";
        $dataheading[5] = "JAM";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        $items = $this->basic->getJadwalData($kelas, $SEMESTER);
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->jenis . "." . $item->matakuliah_nama;
            $datag[$i][2] = $item->dosen_nama;
            $datag[$i][3] = $item->kelas_nama . " (" . $item->kelas_angkatan . ")";
            $datag[$i][4] = $item->jadwal_hari . "";
            $datag[$i][5] = $item->jadwal_jam_mulai . "-" . $item->jadwal_jam_berakhir;

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = "";
            $datalink[$i][4] = "";
            $datalink[$i][5] = "";
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel . "</div><br/>";

        $data = array(
            'base' => base_url(),
            'log' => $log,
            'content' => $content,
            'prodi' => $dprodi,
            'kelas' => $dkelas
        );

        $this->load->view('page-top', $data);
        $this->load->view('widget-schedule-selector', $data);
        $this->load->view('page-content', $data);
        $this->load->view('page-bottom', $data);
    }

    private function cleandata($text) {
        $text = str_replace("'", "", $text);
        $text = str_replace('"', "", $text);
        $text = str_replace("/", "", $text);
        $text = str_replace("\\", "", $text);
        $text = str_replace("  ", "", $text);
        $text = str_replace("&", "", $text);
        $text = str_replace(";", "", $text);
        return $text;
    }

}

-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 28, 2021 at 05:12 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polineshub`
--

-- --------------------------------------------------------

--
-- Table structure for table `akreditasi_hasil_penilaian`
--

CREATE TABLE `akreditasi_hasil_penilaian` (
  `hasil_id` int(11) NOT NULL,
  `nilai_angka` double(15,5) DEFAULT NULL,
  `id_pelaksanaan` int(11) NOT NULL DEFAULT '0',
  `id_penilaian` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `akreditasi_pelaksanaan`
--

CREATE TABLE `akreditasi_pelaksanaan` (
  `id_pelaksanaan` int(11) NOT NULL,
  `pelaksanaan_tahun` int(11) DEFAULT NULL,
  `pelaksanaan_berlaku_dari` date DEFAULT NULL,
  `pelaksanaan_berlaku_hingga` date DEFAULT NULL,
  `nilai_akhir_nominal` double(15,5) DEFAULT NULL,
  `nilai_akhir_huruf` varchar(50) DEFAULT NULL,
  `no_prodi` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `akreditasi_poin_penilaian`
--

CREATE TABLE `akreditasi_poin_penilaian` (
  `id_penilaian` int(11) NOT NULL,
  `poin_id` int(11) NOT NULL DEFAULT '0',
  `penilaian_nama` mediumtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `akreditasi_standar`
--

CREATE TABLE `akreditasi_standar` (
  `standar_id` int(11) NOT NULL,
  `standar_nama` text,
  `standar_keterangan` longtext
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `akreditasi_standar_poin`
--

CREATE TABLE `akreditasi_standar_poin` (
  `poin_id` int(11) NOT NULL,
  `poin_nama` text,
  `poin_keterangan` longtext,
  `standar_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `algoritma_daftar_dijadwalkan`
--

CREATE TABLE `algoritma_daftar_dijadwalkan` (
  `algoritma_id` int(11) NOT NULL,
  `aktif_id` int(11) NOT NULL DEFAULT '0',
  `algoritma_jenis` varchar(50) DEFAULT NULL,
  `algoritma_jam` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `algoritma_daftar_dijadwalkan`
--

INSERT INTO `algoritma_daftar_dijadwalkan` (`algoritma_id`, `aktif_id`, `algoritma_jenis`, `algoritma_jam`) VALUES
(567, 8, 'P', 4),
(566, 8, 'T', 2),
(565, 7, 'P', 4),
(564, 7, 'T', 2),
(563, 15, 'T', 2),
(562, 12, 'P', 4),
(561, 12, 'T', 2),
(560, 14, 'T', 2),
(559, 11, 'P', 4),
(558, 11, 'T', 2),
(557, 9, 'P', 4),
(556, 9, 'T', 2),
(555, 5, 'P', 4),
(554, 5, 'T', 2),
(553, 4, 'T', 2),
(552, 17, 'P', 4),
(551, 17, 'T', 2),
(550, 16, 'P', 4),
(549, 16, 'T', 2),
(548, 13, 'T', 2),
(547, 10, 'P', 4),
(546, 10, 'T', 2),
(545, 6, 'P', 4),
(544, 6, 'T', 2);

-- --------------------------------------------------------

--
-- Table structure for table `algoritma_temp_jadwal`
--

CREATE TABLE `algoritma_temp_jadwal` (
  `algoritma_id` int(11) NOT NULL,
  `algoritma_hari` int(11) NOT NULL,
  `algoritma_jam_ke` int(11) DEFAULT NULL,
  `ruangan_id` int(11) NOT NULL DEFAULT '0',
  `aktif_id` int(11) NOT NULL DEFAULT '0',
  `daftar_id` int(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `algoritma_temp_jadwal`
--

INSERT INTO `algoritma_temp_jadwal` (`algoritma_id`, `algoritma_hari`, `algoritma_jam_ke`, `ruangan_id`, `aktif_id`, `daftar_id`) VALUES
(363, 3, 4, 1, 6, 545),
(362, 2, 4, 3, 10, 547),
(361, 0, 2, 1, 10, 546),
(360, 1, 2, 1, 6, 544),
(359, 4, 0, 2, 12, 562),
(358, 4, 0, 5, 5, 555),
(357, 4, 0, 1, 16, 550),
(356, 3, 0, 3, 7, 565),
(355, 3, 0, 2, 11, 559),
(354, 3, 0, 5, 9, 557),
(353, 2, 0, 3, 8, 567),
(352, 3, 0, 1, 17, 552),
(351, 2, 0, 2, 12, 561),
(350, 2, 0, 1, 13, 548),
(349, 2, 0, 5, 4, 553),
(348, 1, 0, 3, 8, 566),
(347, 1, 0, 2, 11, 558),
(346, 1, 0, 5, 5, 554),
(345, 0, 0, 3, 15, 563),
(344, 0, 0, 4, 7, 564),
(343, 1, 0, 1, 16, 549),
(342, 0, 0, 2, 14, 560),
(341, 0, 0, 5, 9, 556),
(340, 0, 0, 1, 17, 551);

-- --------------------------------------------------------

--
-- Table structure for table `daftar_hadir`
--

CREATE TABLE `daftar_hadir` (
  `daftar_id` int(15) NOT NULL,
  `id_kegiatan` int(11) NOT NULL DEFAULT '0',
  `mahasiswa_nim` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_jurnal`
--

CREATE TABLE `daftar_jurnal` (
  `id_jurnal` int(11) NOT NULL,
  `judul` text,
  `link` mediumtext,
  `tanggal` date DEFAULT NULL,
  `penulis_pertama` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_penelitian`
--

CREATE TABLE `daftar_penelitian` (
  `id_penelitian` int(11) NOT NULL,
  `judul` text,
  `tanggal_mulai` date DEFAULT NULL,
  `ketua_penelitian` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `daftar_pengabdian`
--

CREATE TABLE `daftar_pengabdian` (
  `id_pengabdian` int(11) NOT NULL,
  `judul` text,
  `tempat_pelaksanaan` varchar(250) DEFAULT NULL,
  `tanggal_pelaksanaan` date DEFAULT NULL,
  `ketua_pengabdian` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `dosen_nip` varchar(150) NOT NULL,
  `dosen_nidn` varchar(50) DEFAULT NULL,
  `dosen_nama` text,
  `dosen_status` varchar(25) DEFAULT NULL,
  `dosen_lang` varchar(50) DEFAULT NULL,
  `dosen_lat` varchar(50) DEFAULT NULL,
  `dosen_jarak` double(15,3) DEFAULT NULL,
  `homebase` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`dosen_nip`, `dosen_nidn`, `dosen_nama`, `dosen_status`, `dosen_lang`, `dosen_lat`, `dosen_jarak`, `homebase`) VALUES
('196506071990031001', '10101001', 'Abu Hasan, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196307181992031002', '10101002', 'Achmad Hardito, B.Tech., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196401221991031002', '10101003', 'Adi Wasono, B.Eng,, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195904141988031004', '10101004', 'Adi Wisaksono, S.T., M.M.', 'aktif', '0', '0', 0.000, 1),
('196009131987031001', '10101005', 'Drs. Agus Adiwismono, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196008241988031001', '10101006', 'Agus Rochadi, S.T., M.M.', 'aktif', '0', '0', 0.000, 1),
('196402141990031001', '10101007', 'Aji Hari Riyadi, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196203231985031004', '10101008', 'Akhmad Jamaah, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('197210271999031002', '10101009', 'Dr. Amin Suharjono, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('195901121987031001', '10101010', 'Drs. Amir Subagyo, M.M.', 'aktif', '0', '0', 0.000, 1),
('195903101986121002', '10101011', 'Drs. Ari Santoso, S.St., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('197409042005011001', '10101012', 'Ari Sriyanto Nugroho, S.T., M.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196107171986031001', '10101013', 'Drs. Arif Nursyahid, M.T.', 'aktif', '0', '0', 0.000, 1),
('195612091988031001', '10101014', 'Drs. Bambang Eko Sumarsono, M.Mt.', 'aktif', '0', '0', 0.000, 1),
('195507281984031001', '10101015', 'Drs. Bambang Sarjono, M.M.', 'aktif', '0', '0', 0.000, 1),
('196307071992031005', '10101016', 'Bambang Supriyo, Bsee., M.Eng.Sc., Ph.D.', 'aktif', '0', '0', 0.000, 1),
('195910111985031004', '10101017', 'Bangun Krishna, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196209111989031002', '10101018', 'Budi Basuki Subagio, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('197610032003121002', '10101019', 'Budi Suyanto, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196206101986031004', '10101020', 'Dadi, S.T, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195806091986031001', '10101021', 'Drs. Daeng Supriyadi Pasisarha, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('198208312005012001', '10101022', 'Dewi Anggraeni, S.Pd., M.Pd.', 'aktif', '0', '0', 0.000, 1),
('196312221991031005', '10101023', 'Djodi Antono, B.Tech, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195604261984031001', '10101024', 'Dr. Drs. Eddy Triyono, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('195511211984031002', '10101025', 'Ir. Edy Suwarto, M.T.', 'aktif', '0', '0', 0.000, 1),
('196902012000121001', '10101026', 'Eko Supriyanto, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('195910201987031003', '10101027', 'Eko Widiarto, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196008221989032001', '10101028', 'Dra. Endang Triyani, M.Pd.', 'aktif', '0', '0', 0.000, 1),
('196104241989031001', '10101029', 'Ir. Endro Wasito, M.Kom.', 'aktif', '0', '0', 0.000, 1),
('197409282000032001', '10101030', 'Dr. Eni Dwi Wardihani, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196005121986121001', '10101031', 'Haris Santosa, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('197908102006041001', '10101032', 'Helmy, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195705141986031012', '10101033', 'Drs. Hery Purnomo, M.Pd.', 'aktif', '0', '0', 0.000, 1),
('195905041988031001', '10101034', 'Hery Setijasa, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('197711192008012013', '10101035', 'Idhawati Hestiningsih, S.Kom., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196209051985031003', '10101036', 'Ilham Sayekti, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196106051987031003', '10101037', 'Drs. Iman Suroso, M.Pd.', 'aktif', '0', '0', 0.000, 1),
('197912272003122001', '10101038', 'Iswanti, S.Si., M.Sc.', 'aktif', '0', '0', 0.000, 1),
('195908071987031003', '10101039', 'Drs. Juwarta, M.M.', 'aktif', '0', '0', 0.000, 1),
('197206102000031001', '10101040', 'Khamami, S.Ag., M.M.', 'aktif', '0', '0', 0.000, 1),
('197904262003122002', '10101041', 'Dr. Kurnianingsih, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196003281986121001', '10101042', 'Kusno Utomo, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('198404202015041003', '10101043', 'Liliek Triyono, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196204061991031002', '10101044', 'Lilik Eko Nuryanto, B.Eng., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196005061986031001', '10101045', 'Drs. Makhfud, M.T.', 'aktif', '0', '0', 0.000, 1),
('197403112000121001', '10101046', 'Mardiyono, S.Kom., M.Sc.', 'aktif', '0', '0', 0.000, 1),
('196204201987031002', '10101047', 'Ir. Mochammad Muqorrobin, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196306101991031003', '10101048', 'Mohammad Khambali, B.Eng., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('197710092005011001', '10101049', 'Muhammad Anif, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('197911252006042001', '10101050', 'Muhlasah Novitasari Mara, S.Si., M.Si.', 'aktif', '0', '0', 0.000, 1),
('196107101988112001', '10101051', 'Dra. Netty Nurdiyani, M.Hum.', 'aktif', '0', '0', 0.000, 1),
('196008221988031001', '10101052', 'Drs. Parsumo Rahardjo, M.Kom.', 'aktif', '0', '0', 0.000, 1),
('198504102014041002', '10101053', 'Prayitno, S.St., M.T.', 'aktif', '0', '0', 0.000, 1),
('196404121996011001', '10101054', 'Dr. Samuel Beta Kuntardjo, Ing.Tech., M.T.', 'aktif', '0', '0', 0.000, 1),
('196403091991031003', '10101055', 'Sarono Widodo, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('195901191988031001', '10101056', 'Drs. Sasongko, M.Hum.', 'aktif', '0', '0', 0.000, 1),
('196007291988031001', '10101057', 'Ir. Setyoko, M.M.', 'aktif', '0', '0', 0.000, 1),
('197203112000031002', '10101058', 'Dr. Sidiq Syamsul Hidayat, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196012281986021001', '10101059', 'Sihono, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196301251991031001', '10101060', 'Sindung Hadwi Widi Sasono., Bsee., M.Eng.Sc.', 'aktif', '0', '0', 0.000, 1),
('197501302001121001', '10101061', 'Slamet Handoko, S.Kom., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196005101984031001', '10101062', 'Ir. Slamet Widodo, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196710171997022001', '10101063', 'Sri Anggraeni Kadiran, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196206251988032001', '10101064', 'Dra. Sri Astuti, M.M.', 'aktif', '0', '0', 0.000, 1),
('197102102005012001', '10101065', 'Sri Kusumastuti, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195508041984031001', '10101066', 'Sugijono, S.T., M.M.', 'aktif', '0', '0', 0.000, 1),
('195709051988031001', '10101067', 'Drs. Suhendro, M.M.', 'aktif', '0', '0', 0.000, 1),
('197101172003121001', '10101068', 'Sukamto, S.Kom., M.T.', 'aktif', '0', '0', 0.000, 1),
('195803061987031001', '10101069', 'Drs. Sulistyo Warjono, M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196108141988112001', '10101070', 'Supriyati, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196201291989031001', '10101071', 'Suryono, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('195508281986031003', '10101072', 'Drs. Suwinardi, M.M.', 'aktif', '0', '0', 0.000, 1),
('198010082005011001', '10101073', 'Syahid, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('197307082005011001', '10101074', 'Taufiq Yulianto, S.H., M.H.', 'aktif', '0', '0', 0.000, 1),
('197203292000031001', '10101075', 'Thomas Agung Setyawan, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('196810252000121001', '10101076', 'Tri Raharjo Yudantoro, S.Kom., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('196009291985031005', '10101077', 'Triyono, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('196306161992011001', '10101078', 'Tulus Pramuji. B.Eng.E.E., M.T.', 'aktif', '0', '0', 0.000, 1),
('197704012005011001', '10101079', 'Wahyu Sulistiyo, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('197503132006041001', '10101080', 'Yusnan Badruzzaman, S.T., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('198806262019031012', '10101081', 'Bagus Yunanto, St, Mt', 'aktif', '0', '0', 0.000, 1),
('198412112019031011', '10101082', 'Tahan Prahara, S.T., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('199107302019031010', '10101083', 'Nurseno Bayu Aji, S.Kom, M.Kom.', 'aktif', '0', '0', 0.000, 1),
('199204112019031014', '10101084', 'Aggie Brenda Vernandez, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('198407192019031008', '10101085', 'Kuwat Santoso', 'aktif', '0', '0', 0.000, 1),
('199004112019031014', '10101086', 'Afandi Nur Aziz Thohari, S.T., M.Cs', 'aktif', '0', '0', 0.000, 1),
('199001072019031020', '10101087', 'Muhammad Irwan Yanwari, S.Kom., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('198810142019031007', '10101088', 'Amran Yobioktabera', 'aktif', '0', '0', 0.000, 1),
('199202052019031009', '10101089', 'Angga Wahyu Wibowo, S.Kom., M.Eng.', 'aktif', '0', '0', 0.000, 1),
('198703272019032012', '10101090', 'Wiktasari S.T., M. Kom', 'aktif', '0', '0', 0.000, 1),
('199108112019032024', '10101091', 'Vinda Setya Kartika, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('199401272019032036', '10101092', 'Sirli Fahriah, S.Kom., M.Kom.', 'aktif', '0', '0', 0.000, 1),
('199108272019032026', '10101093', 'Prima Ayundyayasti, S.St., Msim', 'aktif', '0', '0', 0.000, 1),
('199407312019032020', '10101094', 'Rizkha Ajeng Rochmatika, S.T., M.T.', 'aktif', '0', '0', 0.000, 1),
('198605292019032009', '10101095', 'Aisyatul Karima, S.Kom., MCS', 'aktif', '0', '0', 0.000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gedung`
--

CREATE TABLE `gedung` (
  `gedung_id` int(11) NOT NULL,
  `gedung_nama` text,
  `gedung_alamat` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gedung`
--

INSERT INTO `gedung` (`gedung_id`, `gedung_nama`, `gedung_alamat`) VALUES
(1, 'SB', 'Tembalang'),
(2, 'SA', 'Tembalang'),
(3, 'MST', 'Tembalang');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `jadwal_id` int(11) NOT NULL,
  `jadwal_hari` tinytext,
  `jadwal_jam_mulai` time DEFAULT NULL,
  `jadwal_jam_berakhir` time DEFAULT NULL,
  `aktif_id` int(11) NOT NULL DEFAULT '0',
  `jenis` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`jadwal_id`, `jadwal_hari`, `jadwal_jam_mulai`, `jadwal_jam_berakhir`, `aktif_id`, `jenis`) VALUES
(363, 'Kamis', '10:20:00', '14:00:00', 6, 'P'),
(362, 'Rabu', '10:20:00', '14:00:00', 10, 'P'),
(361, 'Senin', '08:30:00', '10:20:00', 10, 'T'),
(360, 'Selasa', '08:30:00', '10:20:00', 6, 'T'),
(359, 'Jumat', '07:00:00', '10:20:00', 12, 'P'),
(358, 'Jumat', '07:00:00', '10:20:00', 5, 'P'),
(357, 'Jumat', '07:00:00', '10:20:00', 16, 'P'),
(356, 'Kamis', '07:00:00', '10:20:00', 7, 'P'),
(355, 'Kamis', '07:00:00', '10:20:00', 11, 'P'),
(354, 'Kamis', '07:00:00', '10:20:00', 9, 'P'),
(353, 'Rabu', '07:00:00', '10:20:00', 8, 'P'),
(352, 'Kamis', '07:00:00', '10:20:00', 17, 'P'),
(351, 'Rabu', '07:00:00', '08:30:00', 12, 'T'),
(350, 'Rabu', '07:00:00', '08:30:00', 13, 'T'),
(349, 'Rabu', '07:00:00', '08:30:00', 4, 'T'),
(348, 'Selasa', '07:00:00', '08:30:00', 8, 'T'),
(347, 'Selasa', '07:00:00', '08:30:00', 11, 'T'),
(346, 'Selasa', '07:00:00', '08:30:00', 5, 'T'),
(345, 'Senin', '07:00:00', '08:30:00', 15, 'T'),
(344, 'Senin', '07:00:00', '08:30:00', 7, 'T'),
(343, 'Selasa', '07:00:00', '08:30:00', 16, 'T'),
(342, 'Senin', '07:00:00', '08:30:00', 14, 'T'),
(341, 'Senin', '07:00:00', '08:30:00', 9, 'T'),
(340, 'Senin', '07:00:00', '08:30:00', 17, 'T');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_anggota`
--

CREATE TABLE `jurnal_anggota` (
  `id_keanggotaan` int(11) NOT NULL,
  `id_jurnal` int(11) NOT NULL DEFAULT '0',
  `dosen_nip` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kegiatan_pembelajaran`
--

CREATE TABLE `kegiatan_pembelajaran` (
  `id_kegiatan` int(11) NOT NULL,
  `topik_pembahasan` text,
  `minggu_ke` int(11) DEFAULT NULL,
  `tanggal_pelaksanaan` date DEFAULT NULL,
  `matakuliah_kode_utama` int(15) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL,
  `kelas_nama` text,
  `kelas_angkatan` int(11) DEFAULT NULL,
  `no_prodi` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kelas_id`, `kelas_nama`, `kelas_angkatan`, `no_prodi`) VALUES
(2, 'IK-A', 2019, 1),
(3, 'IK-B', 2018, 1),
(4, 'IK-A', 2018, 1),
(5, 'IK-C', 2018, 1),
(6, 'IK-D', 2018, 1),
(7, 'IK-E', 2018, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_aktif`
--

CREATE TABLE `kelas_aktif` (
  `aktif_id` int(11) NOT NULL,
  `no_prodi` int(11) NOT NULL DEFAULT '0',
  `dosen_nip` varchar(150) NOT NULL,
  `dosen_nip_p` varchar(150) DEFAULT NULL,
  `kelas_id` int(11) NOT NULL DEFAULT '0',
  `matakuliah_kode_utama` int(15) NOT NULL DEFAULT '0',
  `ruangan_khusus` int(11) DEFAULT NULL,
  `semester` varchar(50) NOT NULL,
  `proses` int(5) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_aktif`
--

INSERT INTO `kelas_aktif` (`aktif_id`, `no_prodi`, `dosen_nip`, `dosen_nip_p`, `kelas_id`, `matakuliah_kode_utama`, `ruangan_khusus`, `semester`, `proses`) VALUES
(4, 1, '199004112019031014', NULL, 4, 3, 0, '20201', 1),
(5, 1, '199107302019031010', NULL, 4, 15, 0, '20201', 1),
(6, 1, '199001072019031020', '199001072019031020', 3, 15, 0, '20201', 1),
(7, 1, '199107302019031010', NULL, 7, 15, 0, '20201', 1),
(8, 1, '199001072019031020', NULL, 7, 4, 0, '20201', 1),
(9, 1, '199001072019031020', NULL, 4, 4, 0, '20201', 1),
(10, 1, '199001072019031020', NULL, 3, 4, 0, '20201', 1),
(11, 1, '198407192019031008', NULL, 5, 4, 0, '20201', 1),
(12, 1, '198407192019031008', NULL, 6, 4, 0, '20201', 1),
(13, 1, '198703272019032012', NULL, 3, 3, 0, '20201', 1),
(14, 1, '198407192019031008', NULL, 5, 3, 0, '20201', 1),
(15, 1, '199004112019031014', NULL, 6, 3, 0, '20201', 1),
(16, 1, '197711192008012013', NULL, 3, 7, 0, '20201', 1),
(17, 1, '197610032003121002', '197610032003121002', 3, 7, 0, '20201', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_peserta`
--

CREATE TABLE `kelas_peserta` (
  `peserta_id` int(11) NOT NULL,
  `mahasiswa_nim` varchar(150) NOT NULL,
  `kelas_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_peserta`
--

INSERT INTO `kelas_peserta` (`peserta_id`, `mahasiswa_nim`, `kelas_id`) VALUES
(1, '3.34.19.0.01', 2),
(2, '3.34.19.0.02', 2),
(3, '3.34.19.0.03', 2),
(4, '3.34.19.0.04', 2),
(5, '3.34.19.0.05', 2),
(6, '3.34.19.0.06', 2),
(7, '3.34.19.0.07', 2);

-- --------------------------------------------------------

--
-- Table structure for table `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `kode` int(11) NOT NULL,
  `tahun_aktif` int(6) NOT NULL DEFAULT '2020',
  `semester` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konfigurasi`
--

INSERT INTO `konfigurasi` (`kode`, `tahun_aktif`, `semester`) VALUES
(1, 2020, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kurikulum`
--

CREATE TABLE `kurikulum` (
  `kurikulum_id` int(11) NOT NULL,
  `kurikulum_nama` text,
  `kurikulum_tahun` int(5) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurikulum`
--

INSERT INTO `kurikulum` (`kurikulum_id`, `kurikulum_nama`, `kurikulum_tahun`) VALUES
(1, 'Teknik Informatika - KKNI 2017', 2017);

-- --------------------------------------------------------

--
-- Table structure for table `log_jadwal`
--

CREATE TABLE `log_jadwal` (
  `jadwal_id` int(11) NOT NULL,
  `penjadwalan_id` int(11) NOT NULL DEFAULT '0',
  `jadwal_prodi` text,
  `jadwal_hari` text,
  `jadwal_jam_mulai` text,
  `jadwal_jam_berakhir` text,
  `jadwal_dosen` varchar(150) NOT NULL DEFAULT '0',
  `jadwal_matakuliah` text NOT NULL,
  `jadwal_ruangan` text,
  `jadwal_kelas` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_ujian_final`
--

CREATE TABLE `log_ujian_final` (
  `ujian_id` int(11) NOT NULL,
  `penjadwalan_id` int(11) NOT NULL DEFAULT '0',
  `matakuliah` int(11) NOT NULL DEFAULT '0',
  `dosen_pengampu` varchar(150) NOT NULL,
  `dosen_pengawas` varchar(150) DEFAULT NULL,
  `ruangan` text NOT NULL,
  `ujian_jam_mulai` time DEFAULT NULL,
  `ujian_jam_selesai` time DEFAULT NULL,
  `ujian_tanggal` date DEFAULT NULL,
  `ujian_hari` text,
  `kelas` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `mahasiswa_nim` varchar(150) NOT NULL,
  `mahasiswa_nama` text,
  `mahasiswa_tahun_masuk` int(11) DEFAULT NULL,
  `mahasiswa_tahun_lulus` int(5) NOT NULL DEFAULT '0',
  `mahasiswa_lulus` int(5) NOT NULL DEFAULT '0',
  `mahasiswa_status` varchar(100) NOT NULL DEFAULT 'aktif'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`mahasiswa_nim`, `mahasiswa_nama`, `mahasiswa_tahun_masuk`, `mahasiswa_tahun_lulus`, `mahasiswa_lulus`, `mahasiswa_status`) VALUES
('3.34.19.0.01', 'ABIA INENDASATRIA', 2019, 0, 0, 'aktif'),
('3.34.19.0.02', 'AFRIZA ZEAILHAM', 2019, 0, 0, 'aktif'),
('3.34.19.0.03', 'AKBAR ADI PRABOWO', 2019, 0, 0, 'aktif'),
('3.34.19.0.04', 'ANDINI TYAS ASIH', 2019, 0, 0, 'aktif'),
('3.34.19.0.05', 'ANJAR ANAS ABDILLAH', 2019, 0, 0, 'aktif'),
('3.34.19.0.06', 'ARIZKA HAMIDAN DAROJAD', 2019, 0, 0, 'aktif'),
('3.34.19.0.07', 'ARYANI WIDO WERDANI', 2019, 0, 0, 'aktif'),
('3.34.19.0.08', 'BAHTYAR BAHARUDIN AFTONY', 2019, 0, 0, 'aktif'),
('3.34.19.0.09', 'DENNY YANUAR NUGRAHA', 2019, 0, 0, 'aktif'),
('3.34.19.0.10', 'FARAHDIBA NABILAH ISLAMADDINA', 2019, 0, 0, 'aktif'),
('3.34.19.0.11', 'FARIS MIQDAD AL BARRO', 2019, 0, 0, 'aktif'),
('3.34.19.0.12', 'HERNANDA PRASETYA WIBOWO', 2019, 0, 0, 'aktif'),
('3.34.19.0.13', 'INDIKA AHSANU AMALA', 2019, 0, 0, 'aktif'),
('3.34.19.0.14', 'IRA AYU LESTARI', 2019, 0, 0, 'aktif'),
('3.34.19.0.15', 'LISSA RISQYA', 2019, 0, 0, 'aktif'),
('3.34.19.0.16', 'MUHAMMAD AGUNG DWI PRASETIYO', 2019, 0, 0, 'aktif'),
('3.34.19.0.17', 'MUHAMMAD FAFA ALFIAN', 2019, 0, 0, 'aktif'),
('3.34.19.0.18', 'MUHAMMAD FARID MAJDI', 2019, 0, 0, 'aktif'),
('3.34.19.0.19', 'MUHAMMAD HAFIZH ROIHAN', 2019, 0, 0, 'aktif'),
('3.34.19.0.20', 'NABILA ANNISAA FITHRIYANI', 2019, 0, 0, 'aktif'),
('3.34.19.0.21', 'NAFI HELMI ALDIANTO', 2019, 0, 0, 'aktif'),
('3.34.19.0.22', 'NUR INNAS TASSYA', 2019, 0, 0, 'aktif'),
('3.34.19.0.23', 'NURUL HIDAYAH', 2019, 0, 0, 'aktif'),
('3.34.19.0.24', 'QURROTU AINII', 2019, 0, 0, 'aktif'),
('3.34.19.0.25', 'RICKY PRAKOSO', 2019, 0, 0, 'aktif'),
('3.34.19.0.26', 'RIZAL WINA PRATAMA', 2019, 0, 0, 'aktif'),
('3.34.19.1.01', 'AGUNG DWI LEKSONO', 2019, 0, 0, 'aktif'),
('3.34.19.1.02', 'ALAN MAULANA AHSAN', 2019, 0, 0, 'aktif'),
('3.34.19.1.03', 'ASRAF APRIANTO', 2019, 0, 0, 'aktif'),
('3.34.19.1.04', 'AULIA NUR RAHMI', 2019, 0, 0, 'aktif'),
('3.34.19.1.05', 'CHARIEN WINA PRATAMA', 2019, 0, 0, 'aktif'),
('3.34.19.1.06', 'DHENI WINDO PRASETYO', 2019, 0, 0, 'aktif'),
('3.34.19.1.07', 'DHIMAS WAHYU SAPUTRA', 2019, 0, 0, 'aktif'),
('3.34.19.1.08', 'FREDY CATUR ANDI CAHYONO', 2019, 0, 0, 'aktif'),
('3.34.19.1.09', 'GALANG EKAYUDHA PERMANA', 2019, 0, 0, 'aktif'),
('3.34.19.1.10', 'IRFANI DWI ARIFIANTO', 2019, 0, 0, 'aktif'),
('3.34.19.1.11', 'IRMATUL AZIZAH', 2019, 0, 0, 'aktif'),
('3.34.19.1.12', 'LUTFIA ARIYATI', 2019, 0, 0, 'aktif'),
('3.34.19.1.13', 'MUHAMMAD DAFI HISBULLAH', 2019, 0, 0, 'aktif'),
('3.34.19.1.14', 'MUHAMMAD RIZKY NAUFAL', 2019, 0, 0, 'aktif'),
('3.34.19.1.15', 'MUHAMMAD SULTAN RAFI', 2019, 0, 0, 'aktif'),
('3.34.19.1.16', 'NADILA SASABILA RAMADONI', 2019, 0, 0, 'aktif'),
('3.34.19.1.17', 'NOVITA ALYA RAMADHANI', 2019, 0, 0, 'aktif'),
('3.34.19.1.18', 'RAFLI BAYU NURFAJRI', 2019, 0, 0, 'aktif'),
('3.34.19.1.19', 'RAKHA YUSAN AL HAFIZH', 2019, 0, 0, 'aktif'),
('3.34.19.1.20', 'RAKHMA AKSATA ARIELA', 2019, 0, 0, 'aktif'),
('3.34.19.1.21', 'RIZKI JUNIANTO', 2019, 0, 0, 'aktif'),
('3.34.19.1.22', 'ROSMA KHOIRUL ASIYYA', 2019, 0, 0, 'aktif'),
('3.34.19.1.23', 'TASYA MAHARANI', 2019, 0, 0, 'aktif'),
('3.34.19.1.24', 'TAUFIQ ALIM NURRIDHO', 2019, 0, 0, 'aktif'),
('3.34.19.1.25', 'WINDA RIZKY YUNNITA', 2019, 0, 0, 'aktif'),
('3.34.19.1.26', 'ZULFAN AHMADI', 2019, 0, 0, 'aktif');

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah`
--

CREATE TABLE `matakuliah` (
  `matakuliah_kode_utama` int(15) NOT NULL,
  `matakuliah_kode` varchar(150) DEFAULT NULL,
  `matakuliah_nama` tinytext,
  `matakuliah_sks_teori` int(11) DEFAULT NULL,
  `matakuliah_sks_praktek` int(11) DEFAULT NULL,
  `matakuliah_jam_teori` int(11) DEFAULT NULL,
  `matakuliah_jam_praktek` int(11) DEFAULT NULL,
  `kurikulum_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matakuliah`
--

INSERT INTO `matakuliah` (`matakuliah_kode_utama`, `matakuliah_kode`, `matakuliah_nama`, `matakuliah_sks_teori`, `matakuliah_sks_praktek`, `matakuliah_jam_teori`, `matakuliah_jam_praktek`, `kurikulum_id`) VALUES
(2, '337-181-101', 'Matematika Diskrit ( Discrete Mathematics )', 2, 0, 2, 0, 1),
(3, '337-181-102', 'Pengantar Teknologi Informasi ( Basic Information Technology )', 2, 0, 2, 0, 1),
(4, '337-181-103', 'Algoritma dan Pemrograman ( Algorithm and Programming )', 2, 1, 2, 4, 1),
(5, '337-181-104', 'Pemrograman Terstruktur ( Structured Programming )', 2, 1, 2, 4, 1),
(6, '337-181-105', 'Dasar-dasar Pemrograman ( Basic Programming )', 2, 1, 2, 4, 1),
(7, '337-181-106', 'Sistem Operasi ( Operating System )', 2, 1, 2, 4, 1),
(8, '337-181-107', 'Desain Grafis Dasar ( Basic Graphic Design )', 2, 1, 2, 4, 1),
(9, '337-181-108', 'Dasar- dasar Komputer ( Computer Fundamentals )', 0, 1, 0, 4, 1),
(10, '337-181-201', 'Pemrograman Visual (Visual Programming)', 2, 1, 2, 4, 1),
(11, '337-181-202', 'Pendidikan Agama (Education of Religion)', 2, 0, 2, 0, 1),
(12, '337-181-203', 'Pancasila (Pancasila)', 2, 0, 2, 0, 1),
(13, '337-181-204', 'Metode Numerik (Numeric Method)', 2, 0, 2, 0, 1),
(14, '337-181-205', 'Statistika (Statistics)', 2, 0, 2, 0, 1),
(15, '337-181-206', 'Desain Grafis Lanjut (Advance Graphic Design)', 2, 1, 2, 4, 1),
(16, '337-181-207', 'Pemrograman Berbasis Objek (Data Structure)', 2, 1, 2, 4, 1),
(17, '337-181-208', 'Sistem Basisdata Dasar (T/P) (Basic Database system )', 2, 1, 2, 4, 1),
(18, '337-181-209', 'Komunikasi Data (T/P) (Data Communications)', 2, 1, 2, 4, 1),
(19, '337-181-301', 'Kewarganegaraan (Citizenship)', 2, 0, 2, 0, 1),
(20, '337-181-302', 'Bahasa Indonesia (Indonesian)', 2, 0, 2, 0, 1),
(21, '337-181-303', 'Bahasa Inggris I (English I)', 2, 0, 2, 0, 1),
(22, '337-181-304', 'Arsitektur dan Organisasi Komputer (Computer Architecture and Organization)', 2, 0, 2, 0, 1),
(23, '337-181-305', 'Keamanan Sistem Informasi (Information System Security)', 2, 1, 2, 4, 1),
(24, '337-181-306', 'Sistem Basisdata Lanjut (T/P) (Advanced Database System)', 2, 1, 2, 4, 1),
(25, '337-181-307', 'Jaringan Komputer I (Network Computer I)', 2, 1, 2, 4, 1),
(26, '337-181-308', 'Pemrograman Web Dinamis (Dynamic Web Programming)', 2, 1, 2, 4, 1),
(27, '337-181-309', 'Pemrograman Perangkat Bergerak (Mobile Programming)', 2, 1, 2, 4, 1),
(28, '337-181-401', 'Bahasa Inggris II (English II)', 2, 0, 2, 0, 1),
(29, '337-181-402', 'Pemrograman Basisdata (Database Programming)', 2, 1, 2, 4, 1),
(30, '337-181-403', 'Mikroprosesor & Antarmuka (Microprocessor and Interface)', 2, 1, 2, 4, 1),
(31, '337-181-404', 'Jaringan Komputer II (Computer Networks II)', 2, 1, 2, 4, 1),
(32, '337-181-405', 'Pemrograman Web Berbasis Framework (Framework Web Programming)', 2, 1, 2, 4, 1),
(33, '337-181-406', 'Sistem Multimedia (Multimedia System)', 2, 1, 2, 4, 1),
(34, '337-181-407', 'Perancangan Sistem Informasi (Information System Design)', 2, 1, 2, 4, 1),
(35, '337-181-501', 'Sistem Terbenam & IOT (Embedded System & IOT)', 0, 1, 0, 4, 1),
(36, '337-181-502', 'Pemrograman Game (Game Programming)', 2, 1, 2, 4, 1),
(37, '337-181-503', 'PKL (Industry Practice)', 2, 0, 2, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `matakuliah_kelengkapan`
--

CREATE TABLE `matakuliah_kelengkapan` (
  `id_kelengkapan` int(11) NOT NULL,
  `kelengkapan_rps` int(11) DEFAULT NULL,
  `kelengkapan_kontrak` int(11) DEFAULT NULL,
  `kelengkapan_mid` int(11) DEFAULT NULL,
  `kelengkapan_final` int(11) DEFAULT NULL,
  `dosen_penanggungjawab` varchar(150) NOT NULL,
  `operator` varchar(150) NOT NULL,
  `matakuliah_kode_utama` int(15) NOT NULL DEFAULT '0',
  `tanggal_input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tanggal_update` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE `operator` (
  `operator_username` varchar(150) NOT NULL,
  `operator_nama` text,
  `operator_gender` varchar(1) DEFAULT NULL,
  `operator_email` text,
  `operator_hp` varchar(50) DEFAULT NULL,
  `operator_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`operator_username`, `operator_nama`, `operator_gender`, `operator_email`, `operator_hp`, `operator_status`) VALUES
('admin', 'administrator', 'L', 'admin@hub.com', '0000000000', 0),
('13245687', 'Admin Tester', 'L', 'a@', '4234', 1);

-- --------------------------------------------------------

--
-- Table structure for table `operator_login`
--

CREATE TABLE `operator_login` (
  `operator_username` varchar(150) NOT NULL,
  `operator_password` text,
  `operator_status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator_login`
--

INSERT INTO `operator_login` (`operator_username`, `operator_password`, `operator_status`) VALUES
('admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 0),
('13245687', '5a87517e6469ddc432e96d4faaa4d293c1333df0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `operator_program_studi`
--

CREATE TABLE `operator_program_studi` (
  `id_relasi` int(11) NOT NULL,
  `operator_username` varchar(150) NOT NULL,
  `no_prodi` int(11) NOT NULL DEFAULT '0',
  `kode_unik` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `operator_program_studi`
--

INSERT INTO `operator_program_studi` (`id_relasi`, `operator_username`, `no_prodi`, `kode_unik`) VALUES
(3, '13245687', 2, '13245687-2'),
(6, '13245687', 1, '13245687-1');

-- --------------------------------------------------------

--
-- Table structure for table `penelitian_anggota`
--

CREATE TABLE `penelitian_anggota` (
  `id_keanggotaan` int(11) NOT NULL,
  `id_penelitian` int(11) NOT NULL DEFAULT '0',
  `dosen_nip` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengabdian_anggota`
--

CREATE TABLE `pengabdian_anggota` (
  `id_keanggotaan` int(11) NOT NULL,
  `id_pengabdian` int(11) NOT NULL DEFAULT '0',
  `dosen_nip` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjadwalan`
--

CREATE TABLE `penjadwalan` (
  `penjadwalan_id` int(11) NOT NULL,
  `penjadwalan_nama` text,
  `penjadwalan_tahun` int(11) DEFAULT NULL,
  `penjadwalan_semester` int(11) DEFAULT NULL,
  `penjadwalan_status` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program_studi`
--

CREATE TABLE `program_studi` (
  `no_prodi` int(11) NOT NULL,
  `kode_prodi` text,
  `nama_prodi` text,
  `jenjang_prodi` varchar(20) DEFAULT NULL,
  `kurikulum_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program_studi`
--

INSERT INTO `program_studi` (`no_prodi`, `kode_prodi`, `nama_prodi`, `jenjang_prodi`, `kurikulum_id`) VALUES
(1, '029379', 'Teknik Informatika', 'DIII', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rencana_kegiatan`
--

CREATE TABLE `rencana_kegiatan` (
  `id_rencana` int(11) NOT NULL,
  `topik_pembahasan` text,
  `minggu_ke` int(11) DEFAULT NULL,
  `matakuliah_kode_utama` int(15) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ruangan`
--

CREATE TABLE `ruangan` (
  `ruangan_id` int(11) NOT NULL,
  `ruangan_nama` text,
  `ruangan_kapasitas` int(11) DEFAULT NULL,
  `ruangan_lantai` int(11) DEFAULT NULL,
  `gedung_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangan`
--

INSERT INTO `ruangan` (`ruangan_id`, `ruangan_nama`, `ruangan_kapasitas`, `ruangan_lantai`, `gedung_id`) VALUES
(1, 'SB-1/4', 26, 1, 1),
(2, 'Lab. Jaringan', 26, 2, 1),
(3, 'Lab. Multimedia', 26, 2, 1),
(4, 'Lab. Pemrograman', 26, 2, 1),
(5, 'SA 2-3', 26, 2, 2),
(6, 'SA 2-4', 26, 2, 2),
(7, 'MST 05', 26, 3, 3),
(8, 'SA 2-7', 26, 2, 2),
(9, 'SA 2-8', 26, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ruangan_prodi`
--

CREATE TABLE `ruangan_prodi` (
  `no_relasi` int(11) NOT NULL,
  `ruangan_id` int(11) NOT NULL DEFAULT '0',
  `no_prodi` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruangan_prodi`
--

INSERT INTO `ruangan_prodi` (`no_relasi`, `ruangan_id`, `no_prodi`) VALUES
(11, 1, 1),
(2, 5, 1),
(3, 2, 1),
(4, 3, 1),
(5, 4, 1),
(6, 6, 1),
(7, 7, 1),
(8, 8, 1),
(9, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ujian_final`
--

CREATE TABLE `ujian_final` (
  `ujian_id` int(11) NOT NULL,
  `aktif_id` int(11) NOT NULL DEFAULT '0',
  `penjadwalan_id` int(11) NOT NULL DEFAULT '0',
  `dosen_pengawas` varchar(150) NOT NULL,
  `ruangan_id` int(11) NOT NULL DEFAULT '0',
  `ujian_jam_mulai` time DEFAULT NULL,
  `ujian_jam_selesai` time DEFAULT NULL,
  `ujian_tanggal` date DEFAULT NULL,
  `ujian_hari` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akreditasi_hasil_penilaian`
--
ALTER TABLE `akreditasi_hasil_penilaian`
  ADD PRIMARY KEY (`hasil_id`);

--
-- Indexes for table `akreditasi_pelaksanaan`
--
ALTER TABLE `akreditasi_pelaksanaan`
  ADD PRIMARY KEY (`id_pelaksanaan`);

--
-- Indexes for table `akreditasi_poin_penilaian`
--
ALTER TABLE `akreditasi_poin_penilaian`
  ADD PRIMARY KEY (`id_penilaian`);

--
-- Indexes for table `akreditasi_standar`
--
ALTER TABLE `akreditasi_standar`
  ADD PRIMARY KEY (`standar_id`);

--
-- Indexes for table `akreditasi_standar_poin`
--
ALTER TABLE `akreditasi_standar_poin`
  ADD PRIMARY KEY (`poin_id`);

--
-- Indexes for table `algoritma_daftar_dijadwalkan`
--
ALTER TABLE `algoritma_daftar_dijadwalkan`
  ADD PRIMARY KEY (`algoritma_id`);

--
-- Indexes for table `algoritma_temp_jadwal`
--
ALTER TABLE `algoritma_temp_jadwal`
  ADD PRIMARY KEY (`algoritma_id`);

--
-- Indexes for table `daftar_hadir`
--
ALTER TABLE `daftar_hadir`
  ADD PRIMARY KEY (`daftar_id`);

--
-- Indexes for table `daftar_jurnal`
--
ALTER TABLE `daftar_jurnal`
  ADD PRIMARY KEY (`id_jurnal`);

--
-- Indexes for table `daftar_penelitian`
--
ALTER TABLE `daftar_penelitian`
  ADD PRIMARY KEY (`id_penelitian`);

--
-- Indexes for table `daftar_pengabdian`
--
ALTER TABLE `daftar_pengabdian`
  ADD PRIMARY KEY (`id_pengabdian`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`dosen_nip`);

--
-- Indexes for table `gedung`
--
ALTER TABLE `gedung`
  ADD PRIMARY KEY (`gedung_id`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `jurnal_anggota`
--
ALTER TABLE `jurnal_anggota`
  ADD PRIMARY KEY (`id_keanggotaan`);

--
-- Indexes for table `kegiatan_pembelajaran`
--
ALTER TABLE `kegiatan_pembelajaran`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kelas_id`);

--
-- Indexes for table `kelas_aktif`
--
ALTER TABLE `kelas_aktif`
  ADD PRIMARY KEY (`aktif_id`);

--
-- Indexes for table `kelas_peserta`
--
ALTER TABLE `kelas_peserta`
  ADD PRIMARY KEY (`peserta_id`);

--
-- Indexes for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `kurikulum`
--
ALTER TABLE `kurikulum`
  ADD PRIMARY KEY (`kurikulum_id`);

--
-- Indexes for table `log_jadwal`
--
ALTER TABLE `log_jadwal`
  ADD PRIMARY KEY (`jadwal_id`);

--
-- Indexes for table `log_ujian_final`
--
ALTER TABLE `log_ujian_final`
  ADD PRIMARY KEY (`ujian_id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`mahasiswa_nim`);

--
-- Indexes for table `matakuliah`
--
ALTER TABLE `matakuliah`
  ADD PRIMARY KEY (`matakuliah_kode_utama`),
  ADD UNIQUE KEY `matakuliah_kode` (`matakuliah_kode`),
  ADD UNIQUE KEY `matakuliah_kode_2` (`matakuliah_kode`);

--
-- Indexes for table `matakuliah_kelengkapan`
--
ALTER TABLE `matakuliah_kelengkapan`
  ADD PRIMARY KEY (`id_kelengkapan`);

--
-- Indexes for table `operator`
--
ALTER TABLE `operator`
  ADD PRIMARY KEY (`operator_username`);

--
-- Indexes for table `operator_login`
--
ALTER TABLE `operator_login`
  ADD PRIMARY KEY (`operator_username`);

--
-- Indexes for table `operator_program_studi`
--
ALTER TABLE `operator_program_studi`
  ADD PRIMARY KEY (`id_relasi`),
  ADD UNIQUE KEY `kode_unik` (`kode_unik`);

--
-- Indexes for table `penelitian_anggota`
--
ALTER TABLE `penelitian_anggota`
  ADD PRIMARY KEY (`id_keanggotaan`);

--
-- Indexes for table `pengabdian_anggota`
--
ALTER TABLE `pengabdian_anggota`
  ADD PRIMARY KEY (`id_keanggotaan`);

--
-- Indexes for table `penjadwalan`
--
ALTER TABLE `penjadwalan`
  ADD PRIMARY KEY (`penjadwalan_id`);

--
-- Indexes for table `program_studi`
--
ALTER TABLE `program_studi`
  ADD PRIMARY KEY (`no_prodi`);

--
-- Indexes for table `rencana_kegiatan`
--
ALTER TABLE `rencana_kegiatan`
  ADD PRIMARY KEY (`id_rencana`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`ruangan_id`);

--
-- Indexes for table `ruangan_prodi`
--
ALTER TABLE `ruangan_prodi`
  ADD PRIMARY KEY (`no_relasi`);

--
-- Indexes for table `ujian_final`
--
ALTER TABLE `ujian_final`
  ADD PRIMARY KEY (`ujian_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akreditasi_hasil_penilaian`
--
ALTER TABLE `akreditasi_hasil_penilaian`
  MODIFY `hasil_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `akreditasi_pelaksanaan`
--
ALTER TABLE `akreditasi_pelaksanaan`
  MODIFY `id_pelaksanaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `akreditasi_poin_penilaian`
--
ALTER TABLE `akreditasi_poin_penilaian`
  MODIFY `id_penilaian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `akreditasi_standar`
--
ALTER TABLE `akreditasi_standar`
  MODIFY `standar_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `akreditasi_standar_poin`
--
ALTER TABLE `akreditasi_standar_poin`
  MODIFY `poin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `algoritma_daftar_dijadwalkan`
--
ALTER TABLE `algoritma_daftar_dijadwalkan`
  MODIFY `algoritma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=568;

--
-- AUTO_INCREMENT for table `algoritma_temp_jadwal`
--
ALTER TABLE `algoritma_temp_jadwal`
  MODIFY `algoritma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;

--
-- AUTO_INCREMENT for table `daftar_hadir`
--
ALTER TABLE `daftar_hadir`
  MODIFY `daftar_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftar_jurnal`
--
ALTER TABLE `daftar_jurnal`
  MODIFY `id_jurnal` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftar_penelitian`
--
ALTER TABLE `daftar_penelitian`
  MODIFY `id_penelitian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `daftar_pengabdian`
--
ALTER TABLE `daftar_pengabdian`
  MODIFY `id_pengabdian` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gedung`
--
ALTER TABLE `gedung`
  MODIFY `gedung_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `jadwal_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=364;

--
-- AUTO_INCREMENT for table `jurnal_anggota`
--
ALTER TABLE `jurnal_anggota`
  MODIFY `id_keanggotaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kegiatan_pembelajaran`
--
ALTER TABLE `kegiatan_pembelajaran`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `kelas_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kelas_aktif`
--
ALTER TABLE `kelas_aktif`
  MODIFY `aktif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `kelas_peserta`
--
ALTER TABLE `kelas_peserta`
  MODIFY `peserta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kurikulum`
--
ALTER TABLE `kurikulum`
  MODIFY `kurikulum_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `log_jadwal`
--
ALTER TABLE `log_jadwal`
  MODIFY `jadwal_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_ujian_final`
--
ALTER TABLE `log_ujian_final`
  MODIFY `ujian_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `matakuliah`
--
ALTER TABLE `matakuliah`
  MODIFY `matakuliah_kode_utama` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=149;

--
-- AUTO_INCREMENT for table `matakuliah_kelengkapan`
--
ALTER TABLE `matakuliah_kelengkapan`
  MODIFY `id_kelengkapan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `operator_program_studi`
--
ALTER TABLE `operator_program_studi`
  MODIFY `id_relasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `penelitian_anggota`
--
ALTER TABLE `penelitian_anggota`
  MODIFY `id_keanggotaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pengabdian_anggota`
--
ALTER TABLE `pengabdian_anggota`
  MODIFY `id_keanggotaan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjadwalan`
--
ALTER TABLE `penjadwalan`
  MODIFY `penjadwalan_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program_studi`
--
ALTER TABLE `program_studi`
  MODIFY `no_prodi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rencana_kegiatan`
--
ALTER TABLE `rencana_kegiatan`
  MODIFY `id_rencana` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ruangan`
--
ALTER TABLE `ruangan`
  MODIFY `ruangan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ruangan_prodi`
--
ALTER TABLE `ruangan_prodi`
  MODIFY `no_relasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `ujian_final`
--
ALTER TABLE `ujian_final`
  MODIFY `ujian_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

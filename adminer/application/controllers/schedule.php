<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kelas Aktif', $heading2 = 'Kelas Aktif', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialized() {
        $this->load->helper('url');
        $this->load->model('template');
    }

    public function index() {
        $this->pages(1);
    }

    public function pages($page) {
        $this->initialized();
        $this->load->model('prodi_mdl');
        $page = (int) $page;
        $title = ' | Penjadwalan Program Studi';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule');
        $type = 'page';
        $heading1 = 'Penjadwalan Program Studi';
        $heading2 = 'Daftar Program Studi';
        $content = '';

        $ex = $this->prodi_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $datas = $this->prodi_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KODE";
        $dataheading[2] = "PROGRAM STUDI";
        $dataheading[3] = "JADWAL SMESTER";
        $dataheading[4] = "JADWAL FINAL";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "width: 10%;";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 15%;";
        $headingstyles[4] = "width: 15%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "text-align: center;";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($datas as $data) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $data->kode_prodi . "";
            $datag[$i][2] = $data->nama_prodi . "";
            $datag[$i][3] = "<span class='fa fa-calendar'></span>";
            $datag[$i][4] = "<span class='fa fa-calendar'></span>";

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = base_url() . "schedule/process/1/" . $data->no_prodi;
            $datalink[$i][4] = base_url() . "schedule/process/2/" . $data->no_prodi;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . "<br/>" . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "schedule/pages/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function process($jenis, $prodi) {
        if ($jenis == 1) {
            $this->semester($prodi);
        } else {
            $this->finals($prodi);
        }
    }

    private function semester($prodi) {
        $this->initialized();
        $title = ' | Penjadwalan Semester';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule') . " / Semester / Proses";
        $type = 'page';
        $heading2 = 'Proses Penjadwalan Semester';
        $content = '';
        
        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);
        $heading1 = '' . $prodi_detail->nama_prodi;

        $content = $content . "<br/><br/>";
        $content = $content . "" . $this->template->schedulertablemaker();
        $content = $content . "<br/><br/>";
        $content = $content . "<div style='text-align: right;'>";
        $content = $content . "<input type='button' class='btn btn-success add-btn' value='Mulai Penjadwalan' onclick='' />";
        $content = $content . "</div>";
        $content = $content . "<br/><br/>";

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    private function finals($prodi) {
        $this->initialized();
        $title = ' | Penjadwalan Ujian Final';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule') . " / Semester / Proses";
        $type = 'page';
        $heading2 = 'Proses Penjadwalan Ujian Final';
        $content = '';
        
        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);
        $heading1 = '' . $prodi_detail->nama_prodi;

        $content = $content . "<br/><br/>";
        $content = $content . "" . $this->template->schedulertablemaker();
        $content = $content . "<br/><br/>";
        $content = $content . "<div style='text-align: right;'>";
        $content = $content . "<input type='button' class='btn btn-success add-btn' value='Mulai Penjadwalan' onclick='' />";
        $content = $content . "</div>";
        $content = $content . "<br/><br/>";

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    // -------------------------------------------------------------------------

    public function active() {
        $this->pactive(1);
    }

    public function pactive($page) {
        $this->initialized();
        $this->load->model('prodi_mdl');
        $page = (int) $page;
        $title = ' | Hasil Penjadwalan Program Studi';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule') . " / Hasil";
        $type = 'page';
        $heading1 = 'Hasil Penjadwalan Program Studi';
        $heading2 = 'Daftar Program Studi';
        $content = '';

        $ex = $this->prodi_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $datas = $this->prodi_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KODE";
        $dataheading[2] = "PROGRAM STUDI";
        $dataheading[3] = "JADWAL SMESTER";
        $dataheading[4] = "JADWAL FINAL";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "width: 10%;";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 15%;";
        $headingstyles[4] = "width: 15%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "text-align: center;";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($datas as $data) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $data->kode_prodi . "";
            $datag[$i][2] = $data->nama_prodi . "";
            $datag[$i][3] = "<span class='fa fa-calendar'></span>";
            $datag[$i][4] = "<span class='fa fa-calendar'></span>";

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = base_url() . "schedule/viewschedule/1/" . $data->no_prodi;
            $datalink[$i][4] = base_url() . "schedule/viewschedule/2/" . $data->no_prodi;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . "<br/>" . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "schedule/pages/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function viewschedule($jenis, $prodi) {
        if ($jenis == 1) {
            $this->viewsemester($prodi);
        } else {
            $this->viewfinals($prodi);
        }
    }

    private function viewsemester($prodi) {
        $this->initialized();
        $title = ' | Hasil Penjadwalan Semester';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule') . " / Semester / Proses";
        $type = 'page';
        $heading2 = 'Hasil Penjadwalan Semester';
        $content = '';
        
        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);
        $heading1 = '' . $prodi_detail->nama_prodi;
        
        $content = $content . "<br/><br/>";

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    private function viewfinals($prodi) {
        $this->initialized();
        $title = ' | Hasil Penjadwalan Ujian Final';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('schedule') . " / Semester / Proses";
        $type = 'page';
        $heading2 = 'Hasil Penjadwalan Ujian Final';
        $content = '';
        
        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);
        $heading1 = '' . $prodi_detail->nama_prodi;
        
        $content = $content . "<br/><br/>";

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
}

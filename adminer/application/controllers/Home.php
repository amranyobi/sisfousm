<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    private function initialized() {
        $this->load->helper('url');
    }

    public function index() {
        $this->initialized();
        redirect(base_url('dashboard'));
    }
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Matakuliah extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Matakuliah', $heading2 = 'Matakuliah', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->pages(1, "");
    }

    public function pages($page, $kurikulum) {
        $this->initialize();
        $this->load->model('matakuliah_mdl');
        $page = (int) $page;
        $title = ' | Matakuliah';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kurikulum') . " / " . $this->template->pathmaker('matakuliah');
        $type = 'page';
        $heading1 = 'Matakuliah';
        $heading2 = 'Daftar Matakuliah';
        $content = '';
        $ex = $this->matakuliah_mdl->get_status($kurikulum);

        $num = 0;
        $maxdata = 30;
        $matakuliahs = $this->matakuliah_mdl->get_list($kurikulum, $maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KURIKULUM";
        $dataheading[2] = "KODE MK";
        $dataheading[3] = "MATAKULIAH";
        $dataheading[4] = "SKS TEORI";
        $dataheading[5] = "SKS PRAKTEK";
        $dataheading[6] = "EDIT";
        $dataheading[7] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";
        $datag[$i][6] = "";
        $datag[$i][7] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";
        $datalink[$i][7] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";
        $headingstyles[6] = "width: 5%;";
        $headingstyles[7] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";
        $datastyles[7] = "text-align: center;";

        $in = 0;
        foreach ($matakuliahs as $matakuliah) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $matakuliah->kurikulum_nama . "";
            $datag[$i][2] = $matakuliah->matakuliah_kode . "";
            $datag[$i][3] = $matakuliah->matakuliah_nama . "";
            $datag[$i][4] = $matakuliah->matakuliah_sks_teori . "";
            $datag[$i][5] = $matakuliah->matakuliah_sks_praktek . "";
            $datag[$i][6] = "<span class='fa fa-database'></span>";
            $datag[$i][7] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][1] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][2] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][3] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][4] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][5] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][6] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $short = str_replace(" ", "_", $matakuliah->matakuliah_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][7] = base_url() . "matakuliah/delete/" . $matakuliah->kurikulum_id . "/" . $matakuliah->matakuliah_kode . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("matakuliah", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "matakuliah/pages/[PGN]/" . $kurikulum, "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('matakuliah_mdl');
        $title = ' | Search - Matakuliah';
        $type = 'page';
        $heading1 = 'Matakuliah';
        $heading2 = 'Pencarian Matakuliah';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('matakuliah');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $matakuliahs = $this->matakuliah_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KURIKULUM";
        $dataheading[2] = "KODE MK";
        $dataheading[3] = "MATAKULIAH";
        $dataheading[4] = "SKS TEORI";
        $dataheading[5] = "SKS PRAKTEK";
        $dataheading[6] = "EDIT";
        $dataheading[7] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";
        $datag[$i][6] = "";
        $datag[$i][7] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";
        $datalink[$i][7] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";
        $headingstyles[6] = "width: 5%;";
        $headingstyles[7] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";
        $datastyles[7] = "text-align: center;";

        $in = 0;
        foreach ($matakuliahs as $matakuliah) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $matakuliah->kurikulum_nama . "";
            $datag[$i][2] = $matakuliah->matakuliah_kode . "";
            $datag[$i][3] = $matakuliah->matakuliah_nama . "";
            $datag[$i][4] = $matakuliah->matakuliah_sks_teori . "";
            $datag[$i][5] = $matakuliah->matakuliah_sks_praktek . "";
            $datag[$i][6] = "<span class='fa fa-database'></span>";
            $datag[$i][7] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][1] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][2] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][3] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][4] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][5] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $datalink[$i][6] = base_url() . "matakuliah/edit/" . $matakuliah->matakuliah_kode;
            $short = str_replace(" ", "_", $matakuliah->matakuliah_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][7] = base_url() . "matakuliah/delete/" . $matakuliah->kurikulum_id . "/" . $matakuliah->matakuliah_kode . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kurikulum, $kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data ruangan ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'matakuliah/delete_konfirmasi/' . $kurikulum . '/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'matakuliah/pages/1/' . $kurikulum . '";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kurikulum, $kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('matakuliah_mdl');
        $this->matakuliah_mdl->delete($kode);
        redirect(base_url('matakuliah/pages/1/' . $kurikulum));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolmatakuliah";
        $editon = false;
        $title = "| Matakuliah";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('matakuliah') . " / Tambah Data";
        $form = "" . base_url() . "matakuliah/add_submit";
        $h1 = "Formulir Matakuliah";
        $h2 = "Formulir Penambahan Matakuliah";

        $data[0][0] = "matakuliah";
        $data[0][1] = "";
        $data[1][0] = "kurikulum";
        $data[1][1] = "";
        $data[2][0] = "nama";
        $data[2][1] = "";
        $data[3][0] = "teori";
        $data[3][1] = "";
        $data[4][0] = "praktek";
        $data[4][1] = "";
        $data[5][0] = "jteori";
        $data[5][1] = "";
        $data[6][0] = "jpraktek";
        $data[6][1] = "";

        $this->load->model('kurikulum_mdl');
        $categories = $this->kurikulum_mdl->get_list(2000, 1);
        foreach ($categories as $category) {
            $data[1][1] = $data[1][1] . "<option value='" . $category->kurikulum_id . "'>" . $category->kurikulum_nama . " (" . $category->kurikulum_tahun . ")</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolmatakuliah";
        $data = null;
        $editon = true;
        $title = "| Matakuliah";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('ruangan') . " / Perbaharuan Data";
        $form = "" . base_url() . "matakuliah/edit_submit";
        $h1 = "Formulir Matakuliah";
        $h2 = "Formulir Perbaharuan Matakuliah";

        $this->load->model('matakuliah_mdl');
        $matakuliah = $this->matakuliah_mdl->get_item($id);

        $kat = $matakuliah->kurikulum_id;
        $data[0][0] = "matakuliah";
        $data[0][1] = "" . $matakuliah->matakuliah_kode;
        $data[1][0] = "kurikulum";
        $data[1][1] = "";
        $data[2][0] = "nama";
        $data[2][1] = "". $matakuliah->matakuliah_nama;
        $data[3][0] = "teori";
        $data[3][1] = "". $matakuliah->matakuliah_sks_teori;
        $data[4][0] = "praktek";
        $data[4][1] = "". $matakuliah->matakuliah_sks_praktek;
        $data[5][0] = "jteori";
        $data[5][1] = "". $matakuliah->matakuliah_jam_teori;
        $data[6][0] = "jpraktek";
        $data[6][1] = "". $matakuliah->matakuliah_jam_praktek;

        $this->load->model('kurikulum_mdl');
        $categories = $this->kurikulum_mdl->get_list(2000, 1);
        foreach ($categories as $category) {
            if ($kat == $category->kurikulum_id) {
                $data[1][1] = $data[1][1] . "<option value='" . $category->kurikulum_id . "' selected>" . $category->kurikulum_nama . " (" . $category->kurikulum_tahun . ")</option>";
            } else {
                $data[1][1] = $data[1][1] . "<option value='" . $category->kurikulum_id . "'>" . $category->kurikulum_nama . " (" . $category->kurikulum_tahun . ")</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $kode = $this->input->post('matakuliah') . '';
        $nama = $this->input->post('nama') . '';
        $kurikulum = $this->input->post('kurikulum') . '';
        $teori = $this->input->post('teori') . '';
        $praktek = $this->input->post('praktek') . '';
        $jteori = $this->input->post('jteori') . '';
        $jpraktek = $this->input->post('jpraktek') . '';

        $this->load->model('matakuliah_mdl');
        $this->matakuliah_mdl->add_item($kode, $kurikulum, $nama, $teori, $praktek, $jteori, $jpraktek);

        redirect(base_url('matakuliah/pages/1/' . $kurikulum));
    }

    public function edit_submit() {
        $this->initialize();
        $kode = $this->input->post('matakuliah') . '';
        $nama = $this->input->post('nama') . '';
        $kurikulum = $this->input->post('kurikulum') . '';
        $teori = $this->input->post('teori') . '';
        $praktek = $this->input->post('praktek') . '';
        $jteori = $this->input->post('jteori') . '';
        $jpraktek = $this->input->post('jpraktek') . '';

        $this->load->model('matakuliah_mdl');
        $this->matakuliah_mdl->edit_item($kode, $nama, $teori, $praktek, $jteori, $jpraktek);
        
        redirect(base_url('matakuliah/pages/1/' . $kurikulum));
    }

}

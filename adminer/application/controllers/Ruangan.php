<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ruangan extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Ruangan', $heading2 = 'Ruangan', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->pages(1, "");
    }

    public function pages($page, $gedung) {
        $this->initialize();
        $this->load->model('ruangan_mdl');
        $page = (int) $page;
        $title = ' | Ruangan';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('gedung') . " / " . $this->template->pathmaker('ruangan');
        $type = 'page';
        $heading1 = 'Ruangan';
        $heading2 = 'Daftar Ruangan';
        $content = '';

        $ex = $this->ruangan_mdl->get_status($gedung);

        $num = 0;
        $maxdata = 30;
        $ruangans = $this->ruangan_mdl->get_list($gedung, $maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "RUANGAN";
        $dataheading[2] = "GEDUNG";
        $dataheading[3] = "LANTAI";
        $dataheading[4] = "KAPASITAS";
        $dataheading[5] = "PENGGUNA";
        $dataheading[6] = "EDIT";
        $dataheading[7] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";
        $datag[$i][6] = "";
        $datag[$i][7] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";
        $datalink[$i][7] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";
        $headingstyles[6] = "width: 5%;";
        $headingstyles[7] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";
        $datastyles[7] = "text-align: center;";

        $in = 0;
        foreach ($ruangans as $ruangan) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $ruangan->ruangan_nama . "";
            $datag[$i][2] = $ruangan->gedung_nama . "";
            $datag[$i][3] = $ruangan->ruangan_lantai . "";
            $datag[$i][4] = $ruangan->ruangan_kapasitas . "";
            $datag[$i][5] = "<span class='fa fa-user'></span>";
            $datag[$i][6] = "<span class='fa fa-database'></span>";
            $datag[$i][7] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][1] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][2] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][3] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][4] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][5] = base_url() . "ruangan/pengguna/" . $ruangan->ruangan_id;
            $datalink[$i][6] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $short = str_replace(" ", "_", $ruangan->ruangan_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][7] = base_url() . "ruangan/delete/" . $ruangan->gedung_id . "/" . $ruangan->ruangan_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("ruangan", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "ruangan/pages/[PGN]/" . $gedung, "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('ruangan_mdl');
        $title = ' | Search - Ruangan';
        $type = 'page';
        $heading1 = 'Ruangan';
        $heading2 = 'Pencarian Ruangan';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('ruangan');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $ruangans = $this->ruangan_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "RUANGAN";
        $dataheading[2] = "GEDUNG";
        $dataheading[3] = "LANTAI";
        $dataheading[4] = "KAPASITAS";
        $dataheading[5] = "PENGGUNA";
        $dataheading[6] = "EDIT";
        $dataheading[7] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";
        $datag[$i][6] = "";
        $datag[$i][7] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";
        $datalink[$i][7] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "";
        $headingstyles[5] = "";
        $headingstyles[6] = "width: 5%;";
        $headingstyles[7] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";
        $datastyles[7] = "text-align: center;";

        $in = 0;
        foreach ($ruangans as $ruangan) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $ruangan->ruangan_nama . "";
            $datag[$i][2] = $ruangan->gedung_nama . "";
            $datag[$i][3] = $ruangan->ruangan_lantai . "";
            $datag[$i][4] = $ruangan->ruangan_kapasitas . "";
            $datag[$i][5] = "<span class='fa fa-user'></span>";
            $datag[$i][6] = "<span class='fa fa-database'></span>";
            $datag[$i][7] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][1] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][2] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][3] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][4] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $datalink[$i][5] = base_url() . "ruangan/pengguna/" . $ruangan->ruangan_id;
            $datalink[$i][6] = base_url() . "ruangan/edit/" . $ruangan->ruangan_id;
            $short = str_replace(" ", "_", $ruangan->ruangan_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][7] = base_url() . "ruangan/delete/" . $ruangan->gedung_id . "/" . $ruangan->ruangan_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($gedung, $kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data ruangan ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'ruangan/delete_konfirmasi/' . $gedung . '/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'ruangan/pages/1/' . $gedung . '";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($gedung, $kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('ruangan_mdl');
        $this->ruangan_mdl->delete($kode);
        redirect(base_url('ruangan/pages/1/' . $gedung));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolruangan";
        $editon = false;
        $title = "| Ruangan";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('ruangan') . " / Tambah Data";
        $form = "" . base_url() . "ruangan/add_submit";
        $h1 = "Formulir Ruangan";
        $h2 = "Formulir Penambahan Ruangan";

        $data[0][0] = "ruangan";
        $data[0][1] = "";
        $data[1][0] = "gedung";
        $data[1][1] = "";
        $data[2][0] = "lantai";
        $data[2][1] = "";
        $data[3][0] = "kapasitas";
        $data[3][1] = "";

        $this->load->model('gedung_mdl');
        $gedungs = $this->gedung_mdl->get_list(2000, 1);
        foreach ($gedungs as $gedung) {
            $data[1][1] = $data[1][1] . "<option value='" . $gedung->gedung_id . "'>" . $gedung->gedung_nama . "</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolruangan";
        $data = null;
        $editon = true;
        $title = "| Ruangan";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('ruangan') . " / Perbaharuan Data";
        $form = "" . base_url() . "ruangan/edit_submit";
        $h1 = "Formulir Ruangan";
        $h2 = "Formulir Perbaharuan Ruangan";

        $this->load->model('ruangan_mdl');
        $ruangan = $this->ruangan_mdl->get_item($id);

        $gdg = $ruangan->gedung_id;
        $data[0][0] = "kode";
        $data[0][1] = "" . $ruangan->ruangan_id;
        $data[1][0] = "ruangan";
        $data[1][1] = "" . $ruangan->ruangan_nama;
        $data[2][0] = "gedung";
        $data[2][1] = "";
        $data[3][0] = "lantai";
        $data[3][1] = "" . $ruangan->ruangan_lantai;
        $data[4][0] = "kapasitas";
        $data[4][1] = "" . $ruangan->ruangan_kapasitas;

        $this->load->model('gedung_mdl');
        $gedungs = $this->gedung_mdl->get_list(2000, 1);
        foreach ($gedungs as $gedung) {
            if ($gdg == $gedung->gedung_id) {
                $data[2][1] = $data[2][1] . "<option value='" . $gedung->gedung_id . "' selected>" . $gedung->gedung_nama . "</option>";
            } else {
                $data[2][1] = $data[2][1] . "<option value='" . $gedung->gedung_id . "'>" . $gedung->gedung_nama . "</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('ruangan') . '';
        $gedung = $this->input->post('gedung') . '';
        $lantai = $this->input->post('lantai') . '';
        $kapasitas = $this->input->post('kapasitas') . '';

        $this->load->model('ruangan_mdl');
        $this->ruangan_mdl->add($nama, $gedung, $lantai, $kapasitas);

        redirect(base_url('ruangan/pages/1/' . $gedung));
    }

    public function edit_submit() {
        $this->initialize();
        $kode = $this->input->post('kode') . '';
        $nama = $this->input->post('ruangan') . '';
        $gedung = $this->input->post('gedung') . '';
        $lantai = $this->input->post('lantai') . '';
        $kapasitas = $this->input->post('kapasitas') . '';

        $this->load->model('ruangan_mdl');
        $this->ruangan_mdl->edit($kode, $nama, $gedung, $lantai, $kapasitas);

        redirect(base_url('ruangan/pages/1/' . $gedung));
    }

    public function pengguna($ruangan) {
        $this->initialize();
        $extra = "formcontrolpenggunaruangan";
        $editon = false;
        $title = "| Pengguna Ruangan";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('ruangan') . " / Pengaturan Ruangan";
        $form = "" . base_url() . "ruangan/addpenggunaconfirm";
        $h1 = "Daftar Pengguna Ruangan";
        $h2 = "";

        $data[0][0] = "pengguna";
        $data[0][1] = "";
        $data[1][0] = "daftar";
        $data[1][1] = "";

        $this->load->model('ruangan_mdl');

        $prodis = $this->ruangan_mdl->pengguna($ruangan);
        foreach ($prodis as $prodi) {
            $data[0][1] = $data[0][1] . "<div class='peserta-list'>";
            $data[0][1] = $data[0][1] . "<div class='peserta-list-button'>";
            $data[0][1] = $data[0][1] . "<a href='" . base_url() . "ruangan/rmvpenggunaconfirm/" . $ruangan . "/" . $prodi->no_prodi . "' title='Keluarkan dari daftar pengguna ruangan'><span class='fa fa-trash'></span></a>";
            $data[0][1] = $data[0][1] . "</div>";
            $data[0][1] = $data[0][1] . "" . $prodi->kode_prodi . " : " . $prodi->nama_prodi . " (" . $prodi->jenjang_prodi . ")";
            $data[0][1] = $data[0][1] . "</div>";
        }

        $prodis = $this->ruangan_mdl->nonpengguna($ruangan);
        foreach ($prodis as $prodi) {
            $data[1][1] = $data[1][1] . "<div class='peserta-list'>";
            $data[1][1] = $data[1][1] . "<div class='peserta-list-button'>";
            $data[1][1] = $data[1][1] . "<a href='" . base_url() . "ruangan/addpenggunaconfirm/" . $ruangan . "/" . $prodi->no_prodi . "' title='Masukkan kedalam daftar pengguna ruangan'><span class='fa fa-user'></span></a>";
            $data[1][1] = $data[1][1] . "</div>";
            $data[1][1] = $data[1][1] . "" . $prodi->kode_prodi . " : " . $prodi->nama_prodi . " (" . $prodi->jenjang_prodi . ")";
            $data[1][1] = $data[1][1] . "</div>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function addpenggunaconfirm($ruangan, $prodi) {
        $this->initialize();
        $this->load->model('ruangan_mdl');
        $this->ruangan_mdl->tambahpengguna($ruangan, $prodi);
        redirect(base_url('ruangan/pengguna/' . $ruangan));
    }

    public function rmvpenggunaconfirm($ruangan, $prodi) {
        $this->initialize();
        $this->load->model('ruangan_mdl');
        $this->ruangan_mdl->hapuspengguna($ruangan, $prodi);
        redirect(base_url('ruangan/pengguna/' . $ruangan));
    }

}

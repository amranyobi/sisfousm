<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    private function loadview($title = ' | Dashboard', $path = '<a href="" title="Beranda"><span class="fa fa-home"></span></a>', $type = 'dashboard', $heading1 = 'Dashboard', $heading2 = 'Dashboard', $content = '', $extra1 = '0', $extra2 = '0', $extra3 = '0', $extra4 = '0') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'extra1' => $extra1,
            'extra2' => $extra2,
            'extra3' => $extra3,
            'extra4' => $extra4
        );
        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);
        $this->load->view('adminlte-content', $data);
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->initialize();
        $title = ' | Dashboard';
        $path = $this->template->pathmaker('dashboard');
        $type = 'dashboard';
        $heading1 = 'Dashboard';
        $heading2 = 'Status Sistem';
        $content = '';

        $this->load->model('basic');
        $ex1 = $this->basic->get_status_dosen();
        $ex2 = $this->basic->get_status_kelas();
        $ex3 = $this->basic->get_status_ruangan();
        $ex4 = $this->basic->get_status_aktif();
        $sms = $this->basic->get_status();
        $prodi = $this->basic->get_status_prodi();
        $thn = $sms->tahun_aktif + 1;
        
        $content = $content . "<form method='POST' action=''>";
        $content = $content . "<h3>Semester Aktif</h3>";
        $content = $content . "<select class='form-control' name='tahunaktif'>";
        $content = $content . "<option value='" . $sms->tahun_aktif . "'>" . $sms->tahun_aktif . "/" . $thn . "</option>";
        $today = (int) date("Y");
        $today = $today - 5;
        for ($i = 0; $i < 10; $i++) {
            $start = $today + $i;
            $end = $start + 1;
            $content = $content . "<option value='" . $start . "'>" . $start . "/" . $end . "</option>";
        }
        $content = $content . "</select><br/>";
        $content = $content . "<select class='form-control' name='semester'>";
        if ($sms->semester == 1) {
            $content = $content . "<option value='1' selected>Gasal</option>";
            $content = $content . "<option value='2'>Genap</option>";
        } else {
            $content = $content . "<option value='1'>Gasal</option>";
            $content = $content . "<option value='2' selected>Genap</option>";
        }
        $content = $content . "</select><br/>";
        $content = $content . '<input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN PERUBAHAN" placeholder="" /><br/><br/>';

        $content = $content . "</form>";
        
        $i = 0;
        $datalst;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "PROGRAM STUDI";
        $dataheading[2] = "JENJANG";
        $dataheading[3] = "JUMLAH KELAS AKTIF";

        $datalst[$i][0] = "";
        $datalst[$i][1] = "";
        $datalst[$i][2] = "";
        $datalst[$i][3] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";

        $headingstyles[0] = "width: 50px;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 25%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        foreach ($prodi as $item) {
            $x = $i + 1;
            $datalst[$i][0] = $x . "";
            $datalst[$i][1] = $item->nama_prodi . "";
            $datalst[$i][2] = $item->jenjang_prodi . "";
            $datalst[$i][3] = $item->jumlah . "";
            
            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = "";
            $i++;
        }
        
        $datatabel = $this->template->tablemaker($dataheading, $datalst, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;
                
        $this->loadview($title, $path, $type, $heading1, $heading2, $content, $ex1, $ex2, $ex3, $ex4);
    }

}

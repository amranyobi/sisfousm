<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kurikulum extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kurikulum', $heading2 = 'Kurikulum', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    private function page($page) {
        $this->initialize();
        $this->load->model('kurikulum_mdl');
        $page = (int) $page;
        $title = ' | Kurikulum';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kurikulum');
        $type = 'page';
        $heading1 = 'Kurikulum';
        $heading2 = 'Daftar Kurikulum';
        $content = '';
        
        $ex = $this->kurikulum_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $kurikulums = $this->kurikulum_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KURIKULUM";
        $dataheading[2] = "TAHUN";
        $dataheading[3] = "MATAKULIAH";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($kurikulums as $kurikulum) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $kurikulum->kurikulum_nama . "";
            $datag[$i][2] = $kurikulum->kurikulum_tahun . "";
            $datag[$i][3] = "<span class='fa fa-pencil-alt'></span>";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][1] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][2] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][3] = base_url() . "matakuliah/pages/1/" . $kurikulum->kurikulum_id;
            $datalink[$i][4] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $short = str_replace(" ", "_", $kurikulum->kurikulum_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][5] = base_url() . "kurikulum/delete/" . $kurikulum->kurikulum_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("kurikulum", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "kurikulum/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('kurikulum_mdl');
        $title = ' | Search - Kurikulum';
        $type = 'page';
        $heading1 = 'Kurikulum';
        $heading2 = 'Pencarian Kurikulum';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('kurikulum');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';
        
        $kurikulums = $this->kurikulum_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KURIKULUM";
        $dataheading[2] = "TAHUN";
        $dataheading[3] = "MATAKULIAH";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($kurikulums as $kurikulum) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $kurikulum->kurikulum_nama . "";
            $datag[$i][2] = $kurikulum->kurikulum_tahun . "";
            $datag[$i][3] = "<span class='fa fa-pencil-alt'></span>";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][1] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][2] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $datalink[$i][3] = base_url() . "matakuliah/pages/1/" . $kurikulum->kurikulum_id;
            $datalink[$i][4] = base_url() . "kurikulum/edit/" . $kurikulum->kurikulum_id;
            $short = str_replace(" ", "_", $kurikulum->kurikulum_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][5] = base_url() . "kurikulum/delete/" . $kurikulum->kurikulum_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;
        
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function delete($kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data kurikulum ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'kurikulum/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'kurikulum";
                }
            </script>
            ';
        
        echo $text;
    }
    
    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('kurikulum_mdl');
        $this->kurikulum_mdl->delete($kode);
        redirect(base_url('kurikulum'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolkurikulum";
        $editon = false;
        $title = "| Kurikulum";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kurikulum') . " / Tambah Data";
        $form = "" . base_url() . "kurikulum/add_submit";
        $h1 = "Formulir Kurikulum";
        $h2 = "Formulir Penambahan Kurikulum";

        $data[0][0] = "ids";
        $data[0][1] = "";
        $data[1][0] = "nama";
        $data[1][1] = "";
        $data[2][0] = "tahun";
        $data[2][1] = "";

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolkurikulum";
        $data = null;
        $editon = true;
        $title = "| Kurikulum";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('kurikulum') . " / Perbaharuan Data";
        $form = "" . base_url() . "kurikulum/edit_submit";
        $h1 = "Formulir Kurikulum";
        $h2 = "Formulir Perbaharuan Kurikulum";

        $this->load->model('kurikulum_mdl');
        $kurikulum = $this->kurikulum_mdl->get_item($id);

        $data[0][0] = "ids";
        $data[0][1] = "" . $kurikulum->kurikulum_id;
        $data[1][0] = "nama";
        $data[1][1] = "" . $kurikulum->kurikulum_nama;
        $data[2][0] = "tahun";
        $data[2][1] = "" . $kurikulum->kurikulum_tahun;

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('nama') . '';
        $tahun = $this->input->post('tahun') . '';

        $this->load->model('kurikulum_mdl');
        $this->kurikulum_mdl->add($nama, $tahun);
        
        redirect(base_url('kurikulum'));
    }

    public function edit_submit() {
        $this->initialize();
        $id = $this->input->post('kode') . '';
        $nama = $this->input->post('nama') . '';
        $tahun = $this->input->post('tahun') . '';

        $this->load->model('kurikulum_mdl');
        $this->kurikulum_mdl->edit($id, $nama, $tahun);
        
        redirect(base_url('kurikulum'));
    }
}

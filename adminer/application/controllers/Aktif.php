<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aktif extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Kelas Aktif', $heading2 = 'Kelas Aktif', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialized() {
        $this->load->helper('url');
        $this->load->model('template');
    }

    public function index() {
        $this->pages(1);
    }

    public function pages($page) {
        $this->initialized();
        $this->load->model('prodi_mdl');
        $page = (int) $page;
        $title = ' | Daftar Program Studi';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('prodi');
        $type = 'page';
        $heading1 = 'Program Studi';
        $heading2 = 'Daftar Program Studi';
        $content = '';

        $ex = $this->prodi_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $datas = $this->prodi_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KODE";
        $dataheading[2] = "PROGRAM STUDI";
        $dataheading[3] = "KELAS AKTIF";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "width: 10%;";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 15%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "text-align: center;";
        $datastyles[2] = "";
        $datastyles[3] = "text-align: center;";

        $in = 0;
        foreach ($datas as $data) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $data->kode_prodi . "";
            $datag[$i][2] = $data->nama_prodi . "";
            $datag[$i][3] = "<span class='fa fa-calendar'></span>";

            $datalink[$i][0] = "";
            $datalink[$i][1] = "";
            $datalink[$i][2] = "";
            $datalink[$i][3] = base_url() . "aktif/listedclass/" . $data->no_prodi;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . "<br/>" . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "aktif/pages/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function listedclass($prodi) {
        $this->activepage($prodi, "", 1);
    }

    public function activepage($prodi, $param = "", $page = 1) {
        $this->initialized();
        $this->load->model('aktif_mdl');
        $page = (int) $page;
        $title = ' | Kelas Aktif';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif') . " / " . $this->template->pathmaker('prodi') . " / List";
        $type = 'page';
        $heading1 = 'Kelas Aktif';
        $heading2 = 'Daftar Kelas Yang Akan Dijadwalkan';
        $content = '';

        $ex = $this->aktif_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->aktif_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KELAS";
        $dataheading[2] = "MATAKULIAH";
        $dataheading[3] = "DOSEN PENGAMPU";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";
        $datag[$i][5] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "";
        $datastyles[3] = "";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";

        $in = 0;
        foreach ($items as $item) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $item->kelas_nama . "";
            $datag[$i][2] = $item->matakuliah_nama . "";
            $datag[$i][3] = $item->dosen_nama . "";
            $datag[$i][4] = "<span class='fa fa-database'></span>";
            $datag[$i][5] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "aktif/edit/" . $item->aktif_id . "/" . $prodi;
            $datalink[$i][1] = base_url() . "aktif/edit/" . $item->aktif_id . "/" . $prodi;
            $datalink[$i][2] = base_url() . "aktif/edit/" . $item->aktif_id . "/" . $prodi;
            $datalink[$i][3] = base_url() . "aktif/edit/" . $item->aktif_id . "/" . $prodi;
            $datalink[$i][4] = base_url() . "aktif/edit/" . $item->aktif_id . "/" . $prodi;
            $short = str_replace(" ", "_", $item->kelas_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][5] = base_url() . "aktif/delete/" . $item->aktif_id . "/" . $prodi . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->addbutton("aktif", "/" . $prodi) . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "aktif/activepage/" . $prodi . "/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kode, $prodi, $nama) {
        $this->initialized();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data kelas aktif ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'aktif/delete_konfirmasi/' . $kode . '/' . $prodi . '";
                } else {
                    window.location = "' . base_url() . 'aktif/activepage/' . $prodi . '";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kode, $prodi) {
        $this->initialized();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('aktif_mdl');
        $this->aktif_mdl->delete($kode);
        redirect(base_url('aktif/activepage/' . $prodi));
    }

    public function add($prodi) {
        $this->initialized();
        $extra = "formcontrolkelasaktif";
        $editon = false;
        $title = "| Kelas Aktif";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif') . " / Tambah Data";
        $form = "" . base_url() . "aktif/add_submit";
        $h1 = "Formulir Kelas Aktif";
        $h2 = "Formulir Penambahan Kelas Aktif";

        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);

        $data[0][0] = "ids";
        $data[0][1] = "";
        $data[1][0] = "prodi";
        $data[1][1] = "" . $prodi;
        $data[2][0] = "mk";
        $data[2][1] = "";
        $data[3][0] = "kelas";
        $data[3][1] = "";
        $data[4][0] = "dosen";
        $data[4][1] = "";

        $this->load->model('dosen_mdl');
        $dosens = $this->dosen_mdl->get_list(2000, 1);
        foreach ($dosens as $dosen) {
            $data[4][1] = $data[4][1] . "<option value='" . $dosen->dosen_nip . "'>(" . $dosen->dosen_nip . ") " . $dosen->dosen_nama . "</option>";
        }

        $this->load->model('kelas_mdl');
        $kelass = $this->kelas_mdl->get_list_prodi($prodi, 2000, 1);
        foreach ($kelass as $kelas) {
            $data[3][1] = $data[3][1] . "<option value='" . $kelas->kelas_id . "'>(" . $kelas->kelas_angkatan . ") " . $kelas->kelas_nama . "</option>";
        }

        $this->load->model('matakuliah_mdl');
        $mks = $this->matakuliah_mdl->get_list_kurkulum($prodi_detail->kurikulum_id, 2000, 1);
        foreach ($mks as $mk) {
            $data[2][1] = $data[2][1] . "<option value='" . $mk->matakuliah_kode . "'>(" . $mk->matakuliah_kode . ") " . $mk->matakuliah_nama . "</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id, $prodi) {
        $this->initialized();
        $extra = "formcontrolkelasaktif";
        $data = null;
        $editon = true;
        $title = "| Kelas Aktif";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('aktif') . " / Perbaharuan Data";
        $form = "" . base_url() . "aktif/edit_submit";
        $h1 = "Formulir Kelas Aktif";
        $h2 = "Formulir Perbaharuan Kelas Aktif";

        $this->load->model('prodi_mdl');
        $prodi_detail = $this->prodi_mdl->get_item($prodi);

        $this->load->model('aktif_mdl');
        $aktif_detail = $this->aktif_mdl->get_item($id);

        $data[0][0] = "ids";
        $data[0][1] = "" . $id;
        $data[1][0] = "prodi";
        $data[1][1] = "" . $prodi;
        $data[2][0] = "mk";
        $data[2][1] = "";
        $data[3][0] = "kelas";
        $data[3][1] = "";
        $data[4][0] = "dosen";
        $data[4][1] = "";

        $this->load->model('dosen_mdl');
        $dosens = $this->dosen_mdl->get_list(2000, 1);
        foreach ($dosens as $dosen) {
            if ($aktif_detail->dosen_nip == $dosen->dosen_nip) {
                $data[4][1] = $data[4][1] . "<option value='" . $dosen->dosen_nip . "' selected>(" . $dosen->dosen_nip . ") " . $dosen->dosen_nama . "</option>";
            } else {
                $data[4][1] = $data[4][1] . "<option value='" . $dosen->dosen_nip . "'>(" . $dosen->dosen_nip . ") " . $dosen->dosen_nama . "</option>";
            }
        }

        $this->load->model('kelas_mdl');
        $kelass = $this->kelas_mdl->get_list_prodi($prodi, 2000, 1);
        foreach ($kelass as $kelas) {
            if ($aktif_detail->kelas_id == $kelas->kelas_id) {
                $data[3][1] = $data[3][1] . "<option value='" . $kelas->kelas_id . "' selected>(" . $kelas->kelas_angkatan . ") " . $kelas->kelas_nama . "</option>";
            } else {
                $data[3][1] = $data[3][1] . "<option value='" . $kelas->kelas_id . "'>(" . $kelas->kelas_angkatan . ") " . $kelas->kelas_nama . "</option>";
            }
        }

        $this->load->model('matakuliah_mdl');
        $mks = $this->matakuliah_mdl->get_list_kurkulum($prodi_detail->kurikulum_id, 2000, 1);
        foreach ($mks as $mk) {
            if ($aktif_detail->matakuliah_kode == $mk->matakuliah_kode) {
                $data[2][1] = $data[2][1] . "<option value='" . $mk->matakuliah_kode . "' selected>(" . $mk->matakuliah_kode . ") " . $mk->matakuliah_nama . "</option>";
            } else {
                $data[2][1] = $data[2][1] . "<option value='" . $mk->matakuliah_kode . "'>(" . $mk->matakuliah_kode . ") " . $mk->matakuliah_nama . "</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialized();
        $prodi = $this->input->post('prodi') . '';
        $mk = $this->input->post('mk') . '';
        $kelas = $this->input->post('kelas') . '';
        $dosen = $this->input->post('dosen') . '';

        $this->load->model('aktif_mdl');
        $this->aktif_mdl->add($prodi, $mk, $kelas, $dosen);

        redirect(base_url('aktif/activepage/' . $prodi));
    }

    public function edit_submit() {
        $this->initialized();
        $id = $this->input->post('ids') . '';
        $prodi = $this->input->post('prodi') . '';
        $mk = $this->input->post('mk') . '';
        $kelas = $this->input->post('kelas') . '';
        $dosen = $this->input->post('dosen') . '';

        $this->load->model('aktif_mdl');
        $this->aktif_mdl->edit($id, $prodi, $mk, $kelas, $dosen);

        redirect(base_url('aktif/activepage/' . $prodi));
    }

}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Operator extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Mahasiswa', $heading2 = 'Mahasiswa', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function page($page) {
        $this->initialize();
        $this->load->model('operator_mdl');
        $page = (int) $page;
        $title = ' | Operator';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('operator');
        $type = 'page';
        $heading1 = 'Operator';
        $heading2 = 'Daftar Operator';
        $content = '';

        $ex = $this->operator_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->operator_mdl->get_list($maxdata, $page);
        $i = 0;
        $datalst;
        $dataheading;
        $datalink;

        $dataheading[0] = "USERNAME";
        $dataheading[1] = "NAMA";
        $dataheading[2] = "EMAIL";
        $dataheading[3] = "AKSES";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "RESET";
        $dataheading[6] = "HAPUS";

        $datalst[$i][0] = "";
        $datalst[$i][1] = "";
        $datalst[$i][2] = "";
        $datalst[$i][3] = "";
        $datalst[$i][4] = "";
        $datalst[$i][5] = "";
        $datalst[$i][6] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";
        $headingstyles[6] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";

        foreach ($items as $item) {
            $datalst[$i][0] = $item->operator_username . "";
            $datalst[$i][1] = $item->operator_nama . "";
            $datalst[$i][2] = $item->operator_email . "";
            $datalst[$i][3] = "<span class='fa fa-key'></span>";
            $datalst[$i][4] = "<span class='fa fa-database'></span>";
            $datalst[$i][5] = "<span class='fa fa-key'></span>";
            $datalst[$i][6] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][1] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][2] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][3] = base_url() . "operator/akses/" . $item->operator_username;
            $datalink[$i][4] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][5] = base_url() . "operator/reset/" . $item->operator_username;
            $datalink[$i][6] = base_url() . "operator/delete/" . $item->operator_username;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datalst, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("operator", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "operator/page/[PGN]", "[PGN]");
        $content = $content . '<div class="input-block">
                <span style="font-size: .7em;">
                    *Password default adalah "PASSWORD" + USERNAME contoh username : 0192739 maka password : PASSWORD0192739
                </span>
            </div>';
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function index() {
        $this->page(1);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('operator_mdl');
        $title = ' | Search - Operator';
        $type = 'page';
        $heading1 = 'Operator';
        $heading2 = 'Pencarian Operator';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('operator');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $items = $this->operator_mdl->search($keyword, $maxdata);
        $i = 0;
        $datalst;
        $dataheading;
        $datalink;

        $dataheading[0] = "USERNAME";
        $dataheading[1] = "NAMA";
        $dataheading[2] = "EMAIL";
        $dataheading[3] = "AKSES";
        $dataheading[4] = "EDIT";
        $dataheading[5] = "RESET";
        $dataheading[6] = "HAPUS";

        $datalst[$i][0] = "";
        $datalst[$i][1] = "";
        $datalst[$i][2] = "";
        $datalst[$i][3] = "";
        $datalst[$i][4] = "";
        $datalst[$i][5] = "";
        $datalst[$i][6] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";
        $datalink[$i][5] = "";
        $datalink[$i][6] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";
        $headingstyles[5] = "width: 5%;";
        $headingstyles[6] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";
        $datastyles[5] = "text-align: center;";
        $datastyles[6] = "text-align: center;";

        foreach ($items as $item) {
            $datalst[$i][0] = $item->operator_username . "";
            $datalst[$i][1] = $item->operator_nama . "";
            $datalst[$i][2] = $item->operator_email . "";
            $datalst[$i][3] = "<span class='fa fa-key'></span>";
            $datalst[$i][4] = "<span class='fa fa-database'></span>";
            $datalst[$i][5] = "<span class='fa fa-key'></span>";
            $datalst[$i][6] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][1] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][2] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][3] = base_url() . "operator/akses/" . $item->operator_username;
            $datalink[$i][4] = base_url() . "operator/edit/" . $item->operator_username;
            $datalink[$i][5] = base_url() . "operator/reset/" . $item->operator_username;
            $datalink[$i][6] = base_url() . "operator/delete/" . $item->operator_username;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datalst, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;
        $content = $content . '<div class="input-block">
                <span style="font-size: .7em;">
                    *Password default adalah "PASSWORD" + USERNAME contoh username : 0192739 maka password : PASSWORD0192739
                </span>
            </div>';

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kode) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data operator dengan username ' . $kode . ', lanjutkan?")){
                    window.location = "' . base_url() . 'operator/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'operator";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kode) {
        $this->initialize();
        $nip = str_replace(" ", "", $kode);
        $nip = str_replace("-", "", $kode);
        $this->load->model('operator_mdl');
        $this->operator_mdl->delete($kode);
        redirect(base_url('operator'));
    }
    
    public function akses($username){
        $this->initialize();
        $extra = "formcontroloperatorakses";
        $editon = false;
        $title = "| Operator";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('operator') . " / akses";
        $form = "" . base_url() . "operator/akses_submit";
        $h1 = "Formulir Hak Akses Operator";
        $h2 = "Formulir Pengaturan Hak Akses Operator";

        $data[0][0] = "listakses";
        $data[0][1] = "";
        $data[1][0] = "daftar";
        $data[1][1] = "";
        
        $this->load->model('operator_mdl');
        $akses_ps = $this->operator_mdl->get_access_list($username);
        foreach ($akses_ps as $prodi) {
            $data[0][1] = $data[0][1] . "<div class='peserta-list'>";
            $data[0][1] = $data[0][1] . "<div class='peserta-list-button'>";
            $data[0][1] = $data[0][1] . "<a href='" . base_url() . "operator/rmvaksesconfirm/" . $username . "/" . $prodi->no_prodi . "' title='Hapus hak akses'><span class='fa fa-trash'></span></a>";
            $data[0][1] = $data[0][1] . "</div>";
            $data[0][1] = $data[0][1] . "" . $prodi->kode_prodi . " : " . $prodi->nama_prodi . " (" . $prodi->jenjang_prodi . ")";
            $data[0][1] = $data[0][1] . "</div>";
        }
        
        $akses_ps = $this->operator_mdl->get_nonaccess_list($username);
        foreach ($akses_ps as $prodi) {
            $data[1][1] = $data[1][1] . "<div class='peserta-list'>";
            $data[1][1] = $data[1][1] . "<div class='peserta-list-button'>";
            $data[1][1] = $data[1][1] . "<a href='" . base_url() . "operator/addaksesconfirm/" . $username . "/" . $prodi->no_prodi . "' title='Tambahkan hak akses'><span class='fa fa-user'></span></a>";
            $data[1][1] = $data[1][1] . "</div>";
            $data[1][1] = $data[1][1] . "" . $prodi->kode_prodi . " : " . $prodi->nama_prodi . " (" . $prodi->jenjang_prodi . ")";
            $data[1][1] = $data[1][1] . "</div>";
        }
        
        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add() {
        $this->initialize();
        $extra = "formcontroloperator";
        $editon = false;
        $title = "| Operator";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('operator') . " / Tambah Data";
        $form = "" . base_url() . "operator/add_submit";
        $h1 = "Formulir Operator";
        $h2 = "Formulir Penambahan Operator";

        $data[0][0] = "username";
        $data[0][1] = "";
        $data[1][0] = "nama";
        $data[1][1] = "";
        $data[2][0] = "email";
        $data[2][1] = "";
        $data[3][0] = "hp";
        $data[3][1] = "";
        $data[4][0] = "gender";
        $data[4][1] = "<option value='L'>Laki-Laki</option><option value='P'>Perempuan</option>";

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($kode) {
        $this->initialize();
        $extra = "formcontroloperator";
        $data = null;
        $editon = true;
        $title = "| Operator";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('operator') . " / Perbaharuan Data";
        $form = "" . base_url() . "operator/edit_submit";
        $h1 = "Formulir Operator";
        $h2 = "Formulir Perbaharuan Operator";

        $this->load->model('operator_mdl');
        $item = $this->operator_mdl->get_item($kode);

        $data[0][0] = "username";
        $data[0][1] = "" . $item->operator_username;
        $data[1][0] = "nama";
        $data[1][1] = "" . $item->operator_nama;
        $data[2][0] = "email";
        $data[2][1] = "" . $item->operator_email;
        $data[3][0] = "hp";
        $data[3][1] = "" . $item->operator_hp;
        $data[4][0] = "gender";
        $data[4][1] = "<option value='L'>Laki-Laki</option><option value='P'>Perempuan</option>";

        if ($item->operator_gender == "L") {
            $data[4][1] = "<option value='L' selected>Laki-Laki</option><option value='P'>Perempuan</option>";
        } else {
            $data[4][1] = "<option value='L'>Laki-Laki</option><option value='P' selected>Perempuan</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $username = $this->input->post('username') . '';
        $nama = $this->input->post('nama') . '';
        $email = $this->input->post('email') . '';
        $hp = $this->input->post('hp') . '';
        $gender = $this->input->post('gender') . '';

        $username = str_replace(" ", "", $username);

        $this->load->model('operator_mdl');
        $this->operator_mdl->add($username, $nama, $email, $gender, $hp);
        $this->operator_mdl->add_login($username, "PASSWORD" . $username);

        redirect(base_url('operator/edit/' . $username));
    }

    public function edit_submit() {
        $this->initialize();
        $nim = $this->input->post('nim') . '';
        $nama = $this->input->post('nama') . '';
        $thnmasuk = $this->input->post('thnmasuk') . '';
        $thnlulus = $this->input->post('thnlulus') . '';
        $lulus = $this->input->post('lulus') . '';
        $status = $this->input->post('status') . '';

        $this->load->model('operator_mdl');
        $this->operator_mdl->edit($nim, $nama, $thnmasuk, $thnlulus, $lulus, $status);

        redirect(base_url('operator'));
    }
    
    public function addaksesconfirm($user, $prodi) {
        $this->initialize();
        $this->load->model('operator_mdl');
        $this->operator_mdl->add_access($user, $prodi);
        redirect(base_url('operator/akses/' . $user));
    }
    
    public function rmvaksesconfirm($user, $prodi) {
        $this->initialize();
        $this->load->model('operator_mdl');
        $this->operator_mdl->remove_access($user, $prodi);
        redirect(base_url('operator/akses/' . $user));
    }
    
    public function reset($kode) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan melakukan reset password operator dengan username ' . $kode . ', lanjutkan?")){
                    window.location = "' . base_url() . 'operator/reset_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'operator";
                }
            </script>
            ';

        echo $text;
    }

    public function reset_konfirmasi($kode) {
        $this->initialize();
        $nip = str_replace(" ", "", $kode);
        $nip = str_replace("-", "", $kode);
        $this->load->model('operator_mdl');
        $this->operator_mdl->reset($kode);
        redirect(base_url('operator'));
    }
    
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Gedung extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Gedung', $heading2 = 'Gedung', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }
        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    private function page($page) {
        $this->initialize();
        $this->load->model('gedung_mdl');
        $page = (int) $page;
        $title = ' | Gedung';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('gedung');
        $type = 'page';
        $heading1 = 'Gedung';
        $heading2 = 'Daftar Gedung';
        $content = '';

        $ex = $this->gedung_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $gedungs = $this->gedung_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "GEDUNG";
        $dataheading[2] = "RUANGAN";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "width: 5%;";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($gedungs as $gedung) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $gedung->gedung_nama . "";
            $datag[$i][2] = "<span class='fa fa-building'></span>";
            $datag[$i][3] = "<span class='fa fa-database'></span>";
            $datag[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $datalink[$i][1] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $datalink[$i][2] = base_url() . "ruangan/pages/1/" . $gedung->gedung_id;
            $datalink[$i][3] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $short = str_replace(" ", "_", $gedung->gedung_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][4] = base_url() . "gedung/delete/" . $gedung->gedung_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("gedung", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "gedung/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('gedung_mdl');
        $title = ' | Search - Gedung';
        $type = 'page';
        $heading1 = 'Gedung';
        $heading2 = 'Pencarian Gedung';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('gedung');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';
        
        $gedungs = $this->gedung_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "GEDUNG";
        $dataheading[2] = "RUANGAN";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "width: 5%;";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($gedungs as $gedung) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $gedung->gedung_nama . "";
            $datag[$i][2] = "<span class='fa fa-building'></span>";
            $datag[$i][3] = "<span class='fa fa-database'></span>";
            $datag[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $datalink[$i][1] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $datalink[$i][2] = base_url() . "ruangan/pages/1/" . $gedung->gedung_id;
            $datalink[$i][3] = base_url() . "gedung/edit/" . $gedung->gedung_id;
            $short = str_replace(" ", "_", $gedung->gedung_nama);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][4] = base_url() . "gedung/delete/" . $gedung->gedung_id . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;
        
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }
    
    public function delete($kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data gedung ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'gedung/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'gedung";
                }
            </script>
            ';
        
        echo $text;
    }
    
    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('gedung_mdl');
        $this->gedung_mdl->delete($kode);
        redirect(base_url('gedung'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolgedung";
        $editon = false;
        $title = "| Gedung";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('gedung') . " / Tambah Data";
        $form = "" . base_url() . "gedung/add_submit";
        $h1 = "Formulir Gedung";
        $h2 = "Formulir Penambahan Gedung";

        $data[0][0] = "ids";
        $data[0][1] = "";
        $data[1][0] = "nama";
        $data[1][1] = "";
        $data[2][0] = "alamat";
        $data[2][1] = "";

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolgedung";
        $data = null;
        $editon = true;
        $title = "| Gedung";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('gedung') . " / Perbaharuan Data";
        $form = "" . base_url() . "gedung/edit_submit";
        $h1 = "Formulir Gedung";
        $h2 = "Formulir Perbaharuan Gedung";

        $this->load->model('gedung_mdl');
        $gedung = $this->gedung_mdl->get_item($id);

        $data[0][0] = "ids";
        $data[0][1] = "" . $gedung->gedung_id;
        $data[1][0] = "nama";
        $data[1][1] = "" . $gedung->gedung_nama;
        $data[2][0] = "alamat";
        $data[2][1] = "" . $gedung->gedung_alamat;

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('nama') . '';
        $alamat = $this->input->post('alamat') . '';

        $this->load->model('gedung_mdl');
        $gedung = $this->gedung_mdl->add($nama, $alamat);
        
        redirect(base_url('gedung'));
    }

    public function edit_submit() {
        $this->initialize();
        $id = $this->input->post('kode') . '';
        $nama = $this->input->post('nama') . '';
        $alamat = $this->input->post('alamat') . '';

        $this->load->model('gedung_mdl');
        $gedung = $this->gedung_mdl->edit($id, $nama, $alamat);
        
        redirect(base_url('gedung'));
    }
}

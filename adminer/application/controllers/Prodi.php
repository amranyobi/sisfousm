<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Program Studi', $heading2 = 'Program Studi', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function index() {
        $this->page(1);
    }

    private function page($page) {
        $this->initialize();
        $this->load->model('prodi_mdl');
        $page = (int) $page;
        $title = ' | Program Studi';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('prodi');
        $type = 'page';
        $heading1 = 'Program Studi';
        $heading2 = 'Daftar Program Studi';
        $content = '';

        $ex = $this->prodi_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $datas = $this->prodi_mdl->get_list($maxdata, $page);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KODE";
        $dataheading[2] = "PROGRAM STUDI";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "width: 10%;";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($datas as $data) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $data->kode_prodi . "";
            $datag[$i][2] = $data->nama_prodi . "";
            $datag[$i][3] = "<span class='fa fa-database'></span>";
            $datag[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][1] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][2] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][3] = base_url() . "prodi/edit/" . $data->no_prodi;
            $short = str_replace(" ", "_", $data->nama_prodi);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][4] = base_url() . "prodi/delete/" . $data->no_prodi . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("prodi", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "prodi/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('prodi_mdl');
        $title = ' | Search - Program Studi';
        $type = 'page';
        $heading1 = 'Program Studi';
        $heading2 = 'Pencarian Program Studi';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('prodi');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $datas = $this->prodi_mdl->search($keyword, $maxdata);
        $i = 0;
        $datag;
        $dataheading;
        $datalink;

        $dataheading[0] = "NO";
        $dataheading[1] = "KODE";
        $dataheading[2] = "PROGRAM STUDI";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datag[$i][0] = "";
        $datag[$i][1] = "";
        $datag[$i][2] = "";
        $datag[$i][3] = "";
        $datag[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 2%;";
        $headingstyles[1] = "width: 10%;";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        $in = 0;
        foreach ($datas as $data) {
            $in++;
            $datag[$i][0] = $in;
            $datag[$i][1] = $data->kode_prodi . "";
            $datag[$i][2] = $data->nama_prodi . "";
            $datag[$i][3] = "<span class='fa fa-database'></span>";
            $datag[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][1] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][2] = base_url() . "prodi/edit/" . $data->no_prodi;
            $datalink[$i][3] = base_url() . "prodi/edit/" . $data->no_prodi;
            $short = str_replace(" ", "_", $data->nama_prodi);
            $short = str_replace("\\", "_", $short);
            $short = str_replace("/", "_", $short);
            $short = str_replace("%", "_", $short);
            $datalink[$i][4] = base_url() . "prodi/delete/" . $data->no_prodi . "/" . $short;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datag, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kode, $nama) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data program studi ' . $nama . ', lanjutkan?")){
                    window.location = "' . base_url() . 'prodi/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'prodi";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kode) {
        $this->initialize();
        $kode = str_replace(" ", "", $kode);
        $kode = str_replace("-", "", $kode);
        $this->load->model('prodi_mdl');
        $this->prodi_mdl->delete($kode);
        redirect(base_url('prodi'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolprodi";
        $editon = false;
        $title = "| Program Studi";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('prodi') . " / Tambah Data";
        $form = "" . base_url() . "prodi/add_submit";
        $h1 = "Formulir Program Studi";
        $h2 = "Formulir Penambahan Program Studi";

        $data[0][0] = "ids";
        $data[0][1] = "";
        $data[1][0] = "kode";
        $data[1][1] = "";
        $data[2][0] = "nama";
        $data[2][1] = "";
        $data[3][0] = "jenjang";
        $data[3][1] = "";
        $data[4][0] = "kurikulum";
        $data[4][1] = "";

        $data[3][1] = "<option value='DIII'>DIII</option>";
        $data[3][1] = $data[3][1] . "<option value='DIV / S.Tr'>DIV / S.Tr</option>";
        $data[3][1] = $data[3][1] . "<option value='MST'>MST</option>";

        $this->load->model('kurikulum_mdl');
        $kurikulums = $this->kurikulum_mdl->get_list(2000, 1);
        foreach ($kurikulums as $kurikulum) {
            $data[4][1] = $data[4][1] . "<option value='" . $kurikulum->kurikulum_id . "'>" . $kurikulum->kurikulum_nama . " (" . $kurikulum->kurikulum_tahun . ")</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($id) {
        $this->initialize();
        $extra = "formcontrolprodi";
        $data = null;
        $editon = true;
        $title = "| Program Studi";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('prodi') . " / Perbaharuan Data";
        $form = "" . base_url() . "prodi/edit_submit";
        $h1 = "Formulir Program Studi";
        $h2 = "Formulir Perbaharuan Program Studi";

        $this->load->model('prodi_mdl');
        $prodi = $this->prodi_mdl->get_item($id);

        $data[0][0] = "ids";
        $data[0][1] = "" . $prodi->no_prodi;
        $data[1][0] = "kode";
        $data[1][1] = "" . $prodi->kode_prodi;
        $data[2][0] = "nama";
        $data[2][1] = "" . $prodi->nama_prodi;
        $data[3][0] = "jenjang";
        $data[3][1] = "";
        $data[4][0] = "kurikulum";
        $data[4][1] = "";

        if ($prodi->jenjang_prodi == "DIII") {
            $data[3][1] = "<option value='DIII' selected>DIII</option>";
            $data[3][1] = $data[3][1] . "<option value='DIV / S.Tr'>DIV / S.Tr</option>";
            $data[3][1] = $data[3][1] . "<option value='MST'>MST</option>";
        } else if ($prodi->jenjang_prodi == "DIV / S.Tr") {
            $data[3][1] = "<option value='DIII'>DIII</option>";
            $data[3][1] = $data[3][1] . "<option value='DIV / S.Tr' selected>DIV / S.Tr</option>";
            $data[3][1] = $data[3][1] . "<option value='MST'>MST</option>";
        } else if ($prodi->jenjang_prodi == "MST") {
            $data[3][1] = "<option value='DIII'>DIII</option>";
            $data[3][1] = $data[3][1] . "<option value='DIV / S.Tr'>DIV / S.Tr</option>";
            $data[3][1] = $data[3][1] . "<option value='MST' selected>MST</option>";
        } else {
            $data[3][1] = "<option value='DIII'>DIII</option>";
            $data[3][1] = $data[3][1] . "<option value='DIV / S.Tr'>DIV / S.Tr</option>";
            $data[3][1] = $data[3][1] . "<option value='MST'>MST</option>";
        }

        $this->load->model('kurikulum_mdl');
        $kurikulums = $this->kurikulum_mdl->get_list(2000, 1);
        foreach ($kurikulums as $kurikulum) {
            if ($prodi->kurikulum_id == $kurikulum->kurikulum_id) {
                $data[4][1] = $data[4][1] . "<option value='" . $kurikulum->kurikulum_id . "' selected>" . $kurikulum->kurikulum_nama . " (" . $kurikulum->kurikulum_tahun . ")</option>";
            } else {
                $data[4][1] = $data[4][1] . "<option value='" . $kurikulum->kurikulum_id . "'>" . $kurikulum->kurikulum_nama . " (" . $kurikulum->kurikulum_tahun . ")</option>";
            }
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nama = $this->input->post('nama') . '';
        $kode = $this->input->post('kode') . '';
        $jenjang = $this->input->post('jenjang') . '';
        $kurikulum = $this->input->post('kurikulum') . '';

        $this->load->model('prodi_mdl');
        $this->prodi_mdl->add($nama, $kode, $jenjang, $kurikulum);

        redirect(base_url('prodi'));
    }

    public function edit_submit() {
        $this->initialize();
        $id = $this->input->post('ids') . '';
        $nama = $this->input->post('nama') . '';
        $kode = $this->input->post('kode') . '';
        $jenjang = $this->input->post('jenjang') . '';
        $kurikulum = $this->input->post('kurikulum') . '';

        $this->load->model('prodi_mdl');
        $this->prodi_mdl->edit($id, $nama, $kode, $jenjang, $kurikulum);

        redirect(base_url('prodi'));
    }

}

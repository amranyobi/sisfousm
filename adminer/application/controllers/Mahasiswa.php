<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    private function loadview($editon = false, $extra = '', $datas = null, $form = '', $title = '', $path = '', $heading1 = 'Mahasiswa', $heading2 = 'Mahasiswa', $content = '') {
        $data = array(
            'base' => base_url(),
            'path' => $path,
            'title' => $title,
            'heading1' => $heading1,
            'heading2' => $heading2,
            'content' => $content,
            'form' => $form,
            'editon' => $editon
        );
        if (isset($datas)) {
            if ($datas != null) {
                foreach ($datas as $value) {
                    $data[$value[0]] = $value[1];
                }
            }
        }

        $this->load->view('adminlte-head', $data);
        $this->load->view('adminlte-navbar', $data);

        if ($extra == '') {
            $this->load->view('adminlte-content', $data);
        } else {
            $this->load->view('adminlte-content-top', $data);
            $this->load->view($extra, $data);
            $this->load->view('adminlte-content-bottom', $data);
        }
        $this->load->view('adminlte-footer', $data);
    }

    private function initialize() {
        $this->load->helper('url');
        $this->load->model('template');
        $this->template->check_account_login();
    }

    public function page($page) {
        $this->initialize();
        $this->load->model('mahasiswa_mdl');
        $page = (int) $page;
        $title = ' | Mahasiswa';
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('mahasiswa');
        $type = 'page';
        $heading1 = 'Mahasiswa';
        $heading2 = 'Daftar Mahasiswa';
        $content = '';

        $ex = $this->mahasiswa_mdl->get_status();

        $num = 0;
        $maxdata = 30;
        $items = $this->mahasiswa_mdl->get_list($maxdata, $page);
        $i = 0;
        $datalst;
        $dataheading;
        $datalink;

        $dataheading[0] = "NIM";
        $dataheading[1] = "NAMA";
        $dataheading[2] = "STATUS";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datalst[$i][0] = "";
        $datalst[$i][1] = "";
        $datalst[$i][2] = "";
        $datalst[$i][3] = "";
        $datalst[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        foreach ($items as $item) {
            $datalst[$i][0] = $item->mahasiswa_nim . "";
            $datalst[$i][1] = $item->mahasiswa_nama . "";
            $datalst[$i][2] = $item->mahasiswa_status . "";
            $datalst[$i][3] = "<span class='fa fa-database'></span>";
            $datalst[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][1] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][2] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][3] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][4] = base_url() . "mahasiswa/delete/" . $item->mahasiswa_nim;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datalst, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $this->template->searchbar("mahasiswa", true);
        $content = $content . $datatabel;
        $paging = $this->template->paging($page, $ex, $maxdata, base_url() . "mahasiswa/page/[PGN]", "[PGN]");
        $content = $content . $paging . "<br/><br/>";
        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function index() {
        $this->page(1);
    }

    public function search($keyword = "", $maxdata = 100) {
        $this->initialize();
        $this->load->model('mahasiswa_mdl');
        $title = ' | Search - Mahasiswa';
        $type = 'page';
        $heading1 = 'Mahasiswa';
        $heading2 = 'Pencarian Mahasiswa';
        $path = $this->template->pathmaker('dashboard') . " / Pencarian / " . $this->template->pathmaker('mahasiswa');
        $q1 = str_replace("_", " ", $keyword);
        $content = '<div class="search-keyword">Keyword Pencarian : ' . $q1 . '<div>';

        $items = $this->mahasiswa_mdl->search($keyword, $maxdata);
        $i = 0;
        $datalst;
        $dataheading;
        $datalink;

        $dataheading[0] = "NIM";
        $dataheading[1] = "NAMA";
        $dataheading[2] = "STATUS";
        $dataheading[3] = "EDIT";
        $dataheading[4] = "HAPUS";

        $datalst[$i][0] = "";
        $datalst[$i][1] = "";
        $datalst[$i][2] = "";
        $datalst[$i][3] = "";
        $datalst[$i][4] = "";

        $datalink[$i][0] = "";
        $datalink[$i][1] = "";
        $datalink[$i][2] = "";
        $datalink[$i][3] = "";
        $datalink[$i][4] = "";

        $headingstyles[0] = "width: 20%;";
        $headingstyles[1] = "";
        $headingstyles[2] = "";
        $headingstyles[3] = "width: 5%;";
        $headingstyles[4] = "width: 5%;";

        $datastyles[0] = "text-align: center;";
        $datastyles[1] = "";
        $datastyles[2] = "text-align: center;";
        $datastyles[3] = "text-align: center;";
        $datastyles[4] = "text-align: center;";

        foreach ($items as $item) {
            $datalst[$i][0] = $item->mahasiswa_nim . "";
            $datalst[$i][1] = $item->mahasiswa_nama . "";
            $datalst[$i][2] = $item->mahasiswa_status . "";
            $datalst[$i][3] = "<span class='fa fa-database'></span>";
            $datalst[$i][4] = "<span class='fa fa-trash'></span>";

            $datalink[$i][0] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][1] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][2] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][3] = base_url() . "mahasiswa/edit/" . $item->mahasiswa_nim;
            $datalink[$i][4] = base_url() . "mahasiswa/delete/" . $item->mahasiswa_nim;
            $i++;
        }
        $datatabel = $this->template->tablemaker($dataheading, $datalst, $datalink, $i, $headingstyles, $datastyles);
        $content = $content . $datatabel;

        $this->loadview(false, "", null, "", $title, $path, $heading1, $heading2, $content);
    }

    public function delete($kode) {
        $this->initialize();
        $text = '
            <script>
                if(confirm("Anda akan menghapus data mahasiswa dengan NIM ' . $kode . ', lanjutkan?")){
                    window.location = "' . base_url() . 'mahasiswa/delete_konfirmasi/' . $kode . '";
                } else {
                    window.location = "' . base_url() . 'mahasiswa";
                }
            </script>
            ';

        echo $text;
    }

    public function delete_konfirmasi($kode) {
        $this->initialize();
        $nip = str_replace(" ", "", $kode);
        $nip = str_replace("-", "", $kode);
        $this->load->model('mahasiswa_mdl');
        $this->mahasiswa_mdl->delete($kode);
        redirect(base_url('mahasiswa'));
    }

    public function add() {
        $this->initialize();
        $extra = "formcontrolmahasiswa";
        $editon = false;
        $title = "| Mahasiswa";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('mahasiswa') . " / Tambah Data";
        $form = "" . base_url() . "mahasiswa/add_submit";
        $h1 = "Formulir Mahasiswa";
        $h2 = "Formulir Penambahan Mahasiswa";

        $data[0][0] = "nim";
        $data[0][1] = "";
        $data[1][0] = "nama";
        $data[1][1] = "";
        $data[2][0] = "thnmasuk";
        $data[2][1] = "" . date("Y");
        $data[3][0] = "thnlulus";
        $data[3][1] = "";
        $data[4][0] = "lulus";
        $data[4][1] = "<option value='0'>Belum Lulus</option><option value='1'>Lulus</option>";
        $data[5][0] = "status";
        $data[5][1] = "<option value='tidak aktif'>Tidak Aktif</option><option selected value='aktif'>Aktif</option>";

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function edit($kode) {
        $this->initialize();
        $extra = "formcontrolmahasiswa";
        $data = null;
        $editon = true;
        $title = "| Mahasiswa";
        $path = $this->template->pathmaker('dashboard') . " / " . $this->template->pathmaker('mahasiswa') . " / Perbaharuan Data";
        $form = "" . base_url() . "mahasiswa/edit_submit";
        $h1 = "Formulir Mahasiswa";
        $h2 = "Formulir Perbaharuan Mahasiswa";

        $this->load->model('mahasiswa_mdl');
        $item = $this->mahasiswa_mdl->get_item($kode);

        $lulus = $item->mahasiswa_lulus;
        $status = $item->mahasiswa_status;
        $data[0][0] = "nim";
        $data[0][1] = "" . $item->mahasiswa_nim;
        $data[1][0] = "nama";
        $data[1][1] = "" . $item->mahasiswa_nama;
        $data[2][0] = "thnmasuk";
        $data[2][1] = "" . $item->mahasiswa_tahun_masuk;
        $data[3][0] = "thnlulus";
        $data[3][1] = "" . $item->mahasiswa_tahun_lulus;
        $data[4][0] = "lulus";
        $data[4][1] = "";
        $data[5][0] = "status";
        $data[5][1] = "";

        if ($lulus == 1) {
            $data[4][1] = "<option value='0'>Belum Lulus</option><option value='1' selected>Lulus</option>";
        } else {
            $data[4][1] = "<option value='0' selected>Belum Lulus</option><option value='1'>Lulus</option>";
        }

        if ($status == 'aktif') {
            $data[5][1] = "<option value='tidak aktif'>Tidak Aktif</option><option value='aktif' selected>Aktif</option>";
        } else {
            $data[5][1] = "<option value='tidak aktif' selected>Tidak Aktif</option><option value='aktif'>Aktif</option>";
        }

        $this->loadview($editon, $extra, $data, $form, $title, $path, $h1, $h2);
    }

    public function add_submit() {
        $this->initialize();
        $nim = $this->input->post('nim') . '';
        $nama = $this->input->post('nama') . '';
        $thnmasuk = $this->input->post('thnmasuk') . '';
        $thnlulus = $this->input->post('thnlulus') . '';
        $lulus = $this->input->post('lulus') . '';
        $status = $this->input->post('status') . '';

        $nim = str_replace(" ", "", $nim);

        $this->load->model('mahasiswa_mdl');
        $this->mahasiswa_mdl->add($nim, $nama, $thnmasuk, $thnlulus, $lulus, $status);

        redirect(base_url('mahasiswa/edit/' . $nim));
    }

    public function edit_submit() {
        $this->initialize();
        $nim = $this->input->post('nim') . '';
        $nama = $this->input->post('nama') . '';
        $thnmasuk = $this->input->post('thnmasuk') . '';
        $thnlulus = $this->input->post('thnlulus') . '';
        $lulus = $this->input->post('lulus') . '';
        $status = $this->input->post('status') . '';

        $this->load->model('mahasiswa_mdl');
        $this->mahasiswa_mdl->edit($nim, $nama, $thnmasuk, $thnlulus, $lulus, $status);

        redirect(base_url('mahasiswa'));
    }

}

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Adminer<?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo CORE_FOLDER; ?>/img/polines.png">
        <link rel="stylesheet" href="<?php echo CORE_FOLDER; ?>/AdminLTE/themes/css/dashboard.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>../AdminLTE/plugins/fontawesome-free/css/all.min.css">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo CORE_FOLDER; ?>/AdminLTE/dist/css/adminlte.min.css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <style>
            #search-key {
                width: 200px;
                padding: 6px;
                border-top-left-radius: 5px;
                border-bottom-left-radius: 5px;
                border: #005cbf solid 1px;
            }

            .search-btn {
                margin-left: -5px;
                margin-top: -6px;
            }
        </style>

    </head>
    <body class="hold-transition sidebar-mini layout-navbar-fixed">
<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Dashboard<?php echo $title; ?></title>
        <link rel="icon" href="<?php echo CORE_FOLDER; ?>/img/polines.png">
        <link rel="stylesheet" href="<?php echo $base; ?>themes/css/main.css">
        <link rel="stylesheet" href="<?php echo $base; ?>themes/css/dashboard.css">
        <link rel="stylesheet" href="<?php echo $base; ?>themes/css/menu.css">
        <script src="<?php echo $base; ?>themes/js/main.js"></script>

        <link rel="stylesheet" href="<?php echo $base; ?>libs/bootstrap.min.css">
        <script src="<?php echo $base; ?>libs/jquery.min.js"></script>
        <script src="<?php echo $base; ?>libs/popper.min.js"></script>
        <script src="<?php echo $base; ?>libs/bootstrap.min.js"></script>

        <link rel="stylesheet" href="<?php echo $base; ?>libs/fontawesome/css/all.min.css">
    </head>
    <body>
        <header class="">
            <div class="right-menu dropdown">
                <span class="user-menu" data-toggle="dropdown">
                    <div class="user-menu-img float-left">
                        <img src="<?php echo $base; ?>img/default/man.png" class="main-user" />
                    </div>
                    <button type="button" class="user-btn">Nama User</button>
                </span>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#"><span class="fa fa-user icon-end"></span> Profile</a>
                    <a class="dropdown-item" href="#"><span class="fa fa-power-off icon-end"></span> Logout</a>
                </div>
            </div>
            <div class="dashboard-logo">
                <img src="<?php echo $base; ?>img/pss.png" class="main-logo" />
            </div>
        </header>
        <nav class="left-menu full-height">
            <div class="user-info">
                <div class="user-avatar">
                    <img src="<?php echo $base; ?>img/default/man.png" class="avatar" />
                </div>
                <p class="user-name">Nama User</p>
                <p class="user-status">Operator</p>
            </div>
            <div class="main-navigation">
                <ul>
                    <li>
                        <label for="group-1"><a title="Beranda" href="<?php echo base_url(); ?>dashboard"><span class="fa fa-home"></span> Beranda</a></label>
                    </li>
                    <li>
                        <input id="group-2" type="checkbox" hidden />
                        <label for="group-2"><span class="fa fa-angle-right"></span> Data Utama</label>
                        <ul class="group-list">
                            <li><a title="Data dosen" href="<?php echo base_url(); ?>dashboard/dosen"><span class="fa fa-user"></span> Data Dosen</a></li>
                            <li><a title="Data gedung dan ruangan" href="<?php echo base_url(); ?>dashboard/gedung"><span class="fa fa-building"></span> Data Gedung</a></li>
                            <li><a title="Data kurikulum dan matakuliah" href="<?php echo base_url(); ?>dashboard/kurikulum"><span class="fa fa-calendar"></span> Data Kurikulum</a></li>
                            <li><a title="Data kelas dan mahasiswa" href="<?php echo base_url(); ?>dashboard/mahasiswa"><span class="fa fa-user"></span> Data Mahasiswa</a></li>
                        </ul>
                    </li>
                    <li>
                        <label for="group-1"><a title="Kelas aktif dan matakuliah" href="<?php echo base_url(); ?>dashboard/kelas"><span class="fa fa-bell"></span> Kelas Aktif</a></label>
                    </li>
                    <li>
                        <label for="group-1"><a title="Sejarah penjadwalan" href="<?php echo base_url(); ?>schedule"><span class="fa fa-calendar"></span> Penjadwalan</a></label>
                    </li>
                    <li>
                        <label for="group-1"><a title="Jadwal aktif semester ini" href="<?php echo base_url(); ?>schedule/active"><span class="fa fa-calendar"></span> Jadwal Aktif</a></label>
                    </li>
                </ul>
            </div>
        </nav>
        <section class="page">
            <div class="container">
                <div class="content-head">
                    <h3>Beranda</h3>
                    <span class="location">
                        <a href="" title="Beranda"><span class="fa fa-home"></span></a> /
                    </span>
                </div>
                <div class="content-body">
                    <div class="status row">
                        <div class="col-lg-3 stat-a">
                            <div class="aside">
                                <h3><a title="Daftar dosen" href="<?php echo base_url(); ?>dashboard/dosen">Jumlah Dosen</a></h3>
                                <span>
                                    <span class="fa fa-user"></span> 
                                    <label>20</label>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3 stat-b">
                            <div class="aside">
                                <h3><a title="Daftar kelas" href="<?php echo base_url(); ?>dashboard/kelas">Jumlah Kelas</a></h3>
                                <span>
                                    <span class="fa fa-user"></span> 
                                    <label>20</label>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3 stat-c">
                            <div class="aside">
                                <h3><a title="Daftar ruangan" href="<?php echo base_url(); ?>dashboard/ruangan">Jumlah Ruangan</a></h3>
                                <span>
                                    <span class="fa fa-building"></span> 
                                    <label>20</label>
                                </span>
                            </div>
                        </div>
                        <div class="col-lg-3 stat-d">
                            <div class="aside">
                                <h3><a title="Daftar matakuliah aktif" href="<?php echo base_url(); ?>dashboard/aktif">Jumlah Matakuliah Aktif</a></h3>
                                <span>
                                    <span class="fa fa-calendar"></span> 
                                    <label>20</label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="data">
                            <h3>Jadwalku</h3>
                            <div>
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="separator"></div>
        <footer>
            <span>&COPY;2019 D3 Teknik Informatika</span>
        </footer>
    </body>
</html>
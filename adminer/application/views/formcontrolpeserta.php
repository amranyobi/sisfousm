<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data"> 
        <div>
            <?php echo $content; ?>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 50%;">PESERTA KELAS</td>
                    <td style="width: 50%;">DAFTAR MAHASISWA AKTIF</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td><?php echo $peserta; ?></td>
                    <td><?php echo $mahasiswa; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="ids" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Kode Program Studi</label>
                    <input class="form-control" type="text" name="kode" value="<?php echo $kode; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Nama Program Studi</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Jenjang Pendidikan</label>
                    <select name="jenjang" class="form-control">
                        <option value="0"> -- Jenjang -- </option>
                        <option value="D3">D3</option>
                        <option value="S1">S1</option>
                        <option value="S2">S2</option>
                    </select>
                </div>
                <div class="input-block">
                    <label>Kurikulum</label>
                    <select name="kurikulum" class="form-control">
                        <option value="0"> -- Kurikulum -- </option>
                        <?php echo $kurikulum; ?>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
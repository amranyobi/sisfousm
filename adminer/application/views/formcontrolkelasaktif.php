<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon) { ?>
                    <input class="form-control" type="text" name="ids" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <input class="form-control" type="text" name="prodi" value="<?php echo $prodi; ?>" style="display: none;" />
                <div class="input-block">
                    <label>Matakuliah</label>
                    <select name="mk" class="form-control">
                        <option value="0"> -- Matakuliah -- </option>
                        <?php echo $mk; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control">
                        <option value="0"> -- Kelas -- </option>
                        <?php echo $kelas; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Dosen</label>
                    <select name="dosen" class="form-control">
                        <option value="0"> -- Dosen -- </option>
                        <?php echo $dosen; ?>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
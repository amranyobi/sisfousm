<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data"> 
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="kode" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nama Kurikulum</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Tahun Pengajuan</label>
                    <input class="form-control" type="number" name="tahun" value="<?php echo $tahun; ?>" placeholder="" />
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
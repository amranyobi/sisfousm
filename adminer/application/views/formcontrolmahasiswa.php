<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data"> 
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="nim" value="<?php echo $nim; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nomor Induk Mahasiswa (NIM)</label>
                    <?php if (!$editon){ ?>
                    <input class="form-control" type="text" name="nim" value="<?php echo $nim; ?>" placeholder="" />
                    <?php } else { echo "<br/>" . $nim; } ?>
                </div>
                <div class="input-block">
                    <label>Nama</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Tahun Masuk</label>
                    <input class="form-control" type="number" name="thnmasuk" value="<?php echo $thnmasuk; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Tahun Lulus</label>
                    <input class="form-control" type="number" name="thnlulus" value="<?php echo $thnlulus; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Status Kelulusan</label>
                    <select class="form-control" name="lulus">
                        <?php echo $lulus; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Status Kemahasiswaan</label>
                    <select class="form-control" name="status">
                        <?php echo $status; ?>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
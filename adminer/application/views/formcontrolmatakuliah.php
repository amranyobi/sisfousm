<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data"> 
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                <div class="input-block">
                    <label>Kode Matakuliah</label>
                    <input class="form-control" type="text" name="matakuliah" value="<?php echo $matakuliah; ?>" style="display: none;" />
                    <?php echo $matakuliah; ?>
                </div>
                <?php } else { ?>
                <div class="input-block">
                    <label>Kode Matakuliah</label>
                    <input class="form-control" type="text" name="matakuliah" value="<?php echo $matakuliah; ?>" placeholder="" />
                </div>
                <?php } ?>
                <div class="input-block">
                    <label>Nama Matakuliah</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Kurikulum</label>
                    <select name="kurikulum" class="form-control">
                        <option value="0"> -- Kurikulum -- </option>
                        <?php echo $kurikulum; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Jumlah SKS Teori</label>
                    <input class="form-control" type="number" name="teori" value="<?php echo $teori; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Jumlah SKS Praktek</label>
                    <input class="form-control" type="number" name="praktek" value="<?php echo $praktek; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Jumlah Jam Teori</label>
                    <input class="form-control" type="number" name="jteori" value="<?php echo $jteori; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Jumlah Jam Praktek</label>
                    <input class="form-control" type="number" name="jpraktek" value="<?php echo $jpraktek; ?>" placeholder="" />
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
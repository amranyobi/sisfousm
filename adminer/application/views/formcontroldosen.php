<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <input type="text" name="jarak" value="<?php echo $jarak; ?>" style="display: none;">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="kode" value="<?php echo $nip; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nomor Induk Pegawai (NIP)</label>
                    <?php if (!$editon){ ?>
                    <input class="form-control" type="text" name="nip" value="<?php echo $nip; ?>" placeholder="" />
                    <?php } else { echo "<br/>" . $nip; } ?>
                </div>
                <div class="input-block">
                    <label>Nomor Induk Dosen Negeri (NIDN)</label>
                    <?php if (!$editon){ ?>
                    <input class="form-control" type="text" name="nidn" value="<?php echo $nidn; ?>" placeholder="" />
                    <?php } else { echo "<br/>" . $nidn; } ?>
                </div>
                <div class="input-block">
                    <label>Nama</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Status Homebase</label>
                    <select class="form-control" name="homebase">
                        <option value="0">-- Belum Ada --</option>
                        <?php echo $homebase; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Status Kepegawaian</label>
                    <select class="form-control" name="status">
                    <?php if ($status == "nonaktif"){ ?>
                        <option value="nonaktif" selected>Non-Aktif</option>
                        <option value="aktif">Aktif</option>
                    <?php } else { ?>
                        <option value="nonaktif">Non-Aktif</option>
                        <option value="aktif" selected>Aktif</option>
                    <?php } ?>
                    </select>
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
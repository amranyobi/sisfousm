<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="kode" value="<?php echo $kode; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nama Ruangan</label>
                    <input class="form-control" type="text" name="ruangan" value="<?php echo $ruangan; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Gedung</label>
                    <select name="gedung" class="form-control">
                        <option value="0"> -- Gedung -- </option>
                        <?php echo $gedung; ?>
                    </select>
                </div>
                <div class="input-block">
                    <label>Posisi Lantai</label>
                    <input class="form-control" type="number" name="lantai" value="<?php echo $lantai; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Kapasitas</label>
                    <input class="form-control" type="number" name="kapasitas" value="<?php echo $kapasitas; ?>" placeholder="" />
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
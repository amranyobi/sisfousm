<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <?php echo $content; ?>
        <form method="POST" action="<?php echo $form; ?>">
            <?php if ($editon) { ?>
                <input class="form-control" type="text" name="username" value="<?php echo $username; ?>" style="display: none;" />
            <?php } ?>
            <div class="input-block">
                <label>Username</label>
                <?php if (!$editon) { ?>
                    <input class="form-control" type="text" name="username" value="<?php echo $username; ?>" placeholder="" />
                <?php } else {
                    echo "<br/>" . $username;
                } ?>
            </div>
            <div class="input-block">
                <label>Nama</label>
                <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
            </div>
            <div class="input-block">
                <label>eMail</label>
                <input class="form-control" type="text" name="email" value="<?php echo $email; ?>" placeholder="" />
            </div>
            <div class="input-block">
                <label>HP</label>
                <input class="form-control" type="text" name="hp" value="<?php echo $hp; ?>" placeholder="" />
            </div>
            <div class="input-block">
                <label>Gender</label>
                <select class="form-control" name="gender">
                <?php echo $gender; ?>
                </select>
            </div>
            <div class="input-block">
                <span style="font-size: .7em;">
                    *Password default adalah "PASSWORD" + USERNAME contoh username : 0192739 maka password : PASSWORD0192739
                </span>
            </div>
            <div class="input-block-closing">
                <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
            </div>
        </form>
    </div>
</div>
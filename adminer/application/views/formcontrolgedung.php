<?php
defined("BASEPATH") OR exit("No direct script access allowed");
?>
<div class="row">
    <div class="data">
        <div>
            <?php echo $content; ?>
            <form method="POST" action="<?php echo $form; ?>">
                <?php if ($editon){ ?>
                    <input class="form-control" type="text" name="kode" value="<?php echo $ids; ?>" style="display: none;" />
                <?php } ?>
                <div class="input-block">
                    <label>Nama Gedung</label>
                    <input class="form-control" type="text" name="nama" value="<?php echo $nama; ?>" placeholder="" />
                </div>
                <div class="input-block">
                    <label>Alamat Gedung</label>
                    <input class="form-control" type="text" name="alamat" value="<?php echo $alamat; ?>" placeholder="" />
                </div>
                <div class="input-block-closing">
                    <input style="font-weight: 700;" class="form-control btn btn-success" type="submit" value="SIMPAN" placeholder="" />
                </div>
            </form>
        </div>
    </div>
</div>
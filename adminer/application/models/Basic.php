<?php

class Basic extends CI_Model {

    public function get_status() {
        $QRY = "SELECT * FROM konfigurasi LIMIT 0, 1";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row;
    }
    
    public function get_status_prodi() {
        $QRY = "SELECT b.nama_prodi, b.jenjang_prodi, COUNT(a.aktif_id) AS jumlah FROM kelas_aktif a JOIN program_studi b ON (a.no_prodi = b.no_prodi) GROUP BY nama_prodi, jenjang_prodi";
        $query = $this->db->query($QRY);
        return $query->result();
    }

    public function get_status_dosen() {
        $QRY = "SELECT COUNT(dosen_nip) AS jumlah FROM dosen";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_kelas() {
        $QRY = "SELECT COUNT(kelas_id) AS jumlah FROM kelas";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_gedung() {
        $QRY = "SELECT COUNT(gedung_id) AS jumlah FROM gedung";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_ruangan($gedung = "") {
        if (($gedung == "")||($gedung == null)) {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan";
        } else {
            $QRY = "SELECT COUNT(ruangan_id) AS jumlah FROM ruangan WHERE gedung_id='" . $gedung . "';";
        }
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

    public function get_status_aktif() {
        $QRY = "SELECT COUNT(aktif_id) AS jumlah FROM kelas_aktif";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }

}

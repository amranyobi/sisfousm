<?php

class Operator_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(operator_username) AS jumlah FROM operator WHERE operator_status > 0";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM operator WHERE operator_status > 0 LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_access_list($keyword) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM operator_program_studi a JOIN program_studi b ON (a.no_prodi = b.no_prodi) WHERE operator_username = '" . $keyword . "';";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_nonaccess_list($keyword) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM program_studi WHERE no_prodi NOT IN (SELECT no_prodi FROM operator_program_studi WHERE operator_username = '" . $keyword . "');";
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($key) {
        $QRY = "SELECT * FROM operator WHERE operator_username = '" . $key . "' AND operator_status > 0";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM operator WHERE (operator_username LIKE '%" . $keyword . "%' OR operator_nama LIKE '%" . $keyword . "%') AND operator_status > 0 LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($username, $nama, $mail, $gender, $hp) {
        $username = $this->doclean($username);
        $nama = $this->doclean($nama);
        $mail = $this->doclean($mail);
        $gender = $this->doclean($gender);
        $hp = $this->doclean($hp);
        
        $QRY = "INSERT INTO operator(operator_username, operator_nama, operator_gender, operator_email, operator_hp) VALUES (";
        $QRY = $QRY . "'" . $username . "', ";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $gender . "', ";
        $QRY = $QRY . "'" . $mail . "', ";
        $QRY = $QRY . "'" . $hp . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function add_login($username, $password) {
        $username = $this->doclean($username);
        $password = $this->doclean($password);
        
        $QRY = "INSERT INTO operator_login(operator_username, operator_password, operator_status) VALUES (";
        $QRY = $QRY . "'" . $username . "', ";
        $QRY = $QRY . "SHA1('" . $password . "'), ";
        $QRY = $QRY . "'1'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($username, $nama, $mail, $gender, $hp) {
        $username = $this->doclean($username);
        $nama = $this->doclean($nama);
        $mail = $this->doclean($mail);
        $gender = $this->doclean($gender);
        $hp = $this->doclean($hp);
        
        $QRY = "UPDATE operator SET ";
        $QRY = $QRY . "operator_nama='" . $nama . "', ";
        $QRY = $QRY . "operator_gender='" . $gender . "', ";
        $QRY = $QRY . "operator_email='" . $mail . "', ";
        $QRY = $QRY . "operator_hp='" . $hp . "' ";
        $QRY = $QRY . "WHERE operator_username='" . $username . "' AND operator_status > 0;";
        $this->db->query($QRY);
        return;
    }
    
    public function delete($kode) {
        $kode = $this->doclean($kode);
        $QRY = "DELETE FROM operator WHERE operator_username = '" . $kode . "' AND operator_status > 0;";
        $this->db->query($QRY);
        $QRY = "DELETE FROM operator_login WHERE operator_username = '" . $kode . "' AND operator_status > 0;";
        $this->db->query($QRY);
        $QRY = "DELETE FROM operator_program_studi WHERE operator_username = '" . $kode . "';";
        $this->db->query($QRY);
        return;
    }
    
    public function delete_login($kode) {
        $kode = $this->doclean($kode);
        $QRY = "DELETE FROM operator_login WHERE operator_username = '" . $kode . "' AND operator_status > 0;";
        $this->db->query($QRY);
        $QRY = "DELETE FROM operator_program_studi WHERE operator_username = '" . $kode . "';";
        $this->db->query($QRY);
        return;
    }
    
    public function reset($kode) {
        $QRY = "UPDATE operator_login SET ";
        $QRY = $QRY . "operator_password=SHA1('PASSWORD" . $kode . "') ";
        $QRY = $QRY . "WHERE operator_username='" . $kode . "' AND operator_status > 0;";
        $this->db->query($QRY);
        return;
    }
    
    public function add_access($username, $prodi) {
        $username = $this->doclean($username);
        $prodi = $this->doclean($prodi);
        $kode = $username . "-" . $prodi;
        $QRY = "INSERT INTO operator_program_studi(operator_username, no_prodi, kode_unik) VALUES (";
        $QRY = $QRY . "'" . $username . "', ";
        $QRY = $QRY . "'" . $prodi . "', ";
        $QRY = $QRY . "'" . $kode . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    } 
    
    public function remove_access($username, $prodi) {
        $username = $this->doclean($username);
        $prodi = $this->doclean($prodi);
        $kode = $username . "-" . $prodi;
        $QRY = "DELETE FROM operator_program_studi WHERE operator_username = '" . $username . "' AND no_prodi = '" . $prodi . "';";
        $this->db->query($QRY);
        return;
    }
}

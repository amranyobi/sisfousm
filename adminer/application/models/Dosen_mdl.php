<?php

class Dosen_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(dosen_nip) AS jumlah FROM dosen";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM dosen LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($dosen_nip) {
        $QRY = "SELECT * FROM dosen WHERE dosen_nip = '" . $dosen_nip . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM dosen WHERE (dosen_nip LIKE '%" . $keyword . "%' OR dosen_nidn LIKE '%" . $keyword . "%' OR dosen_nama LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($nip, $nidn, $nama, $status, $homebase) {
        $nip = $this->doclean($nip);
        $nidn = $this->doclean($nidn);
        $nama = $this->doclean($nama);
        $homebase = $this->doclean($homebase);
        
        $QRY = "INSERT INTO dosen(dosen_nip, dosen_nidn, dosen_nama, dosen_status, homebase) VALUES (";
        $QRY = $QRY . "'" . $nip . "', ";
        $QRY = $QRY . "'" . $nidn . "', ";
        $QRY = $QRY . "'" . $nama . "', ";
        $QRY = $QRY . "'" . $status . "', ";
        $QRY = $QRY . "'" . $homebase . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($nip, $nidn, $nama, $status, $homebase) {
        $nip = $this->doclean($nip);
        $nama = $this->doclean($nama);
        $homebase = $this->doclean($homebase);
        $status = $this->doclean($status);
        
        $QRY = "UPDATE dosen SET ";
        $QRY = $QRY . "dosen_nama='" . $nama . "', ";
        $QRY = $QRY . "dosen_status='" . $status . "', ";
        $QRY = $QRY . "homebase='" . $homebase . "' ";
        $QRY = $QRY . "WHERE dosen_nip='" . $nip . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }
    
    public function delete($nip) {
        $nip = $this->doclean($nip);
        $QRY = "DELETE FROM kelas_aktif WHERE dosen_nip = '" . $nip . "';";
        $this->db->query($QRY);
        $QRY = "DELETE FROM dosen WHERE dosen_nip = '" . $nip . "';";
        $this->db->query($QRY);
        return;
    }
    
}

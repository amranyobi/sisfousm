<?php

class Template extends CI_Model {

    public function check_account_login() {
        $user = $this->session->userdata('user');
        $name = $this->session->userdata('name');
        $gender = $this->session->userdata('gender');
        $status = $this->session->userdata('status');

        if (($user == null) || ($user == "") || ($user == " ")) {
            redirect(base_url('../hub/login'));
        }
        if (($name == null) || ($name == "") || ($name == " ")) {
            redirect(base_url('../hub/login'));
        }
        if (($status == null) || ($status == "") || ($status == " ")) {
            redirect(base_url('../hub/login'));
        }
    }

    public function pathmaker($location) {
        $path = '';
        switch ($location) {
            case 'dosen':
                $path = '<a href="' . base_url() . 'dosen/' . '" title="Dosen"><span class="">Dosen</span></a>';
                break;
            case 'gedung':
                $path = '<a href="' . base_url() . 'gedung/' . '" title="Gedung"><span class="">Gedung</span></a>';
                break;
            case 'ruangan':
                $path = '<a href="' . base_url() . 'ruangan/' . '" title="Ruangan"><span class="">Ruangan</span></a>';
                break;
            case 'kurikulum':
                $path = '<a href="' . base_url() . 'kurikulum/' . '" title="Kurikulum"><span class="">Kurikulum</span></a>';
                break;
            case 'matakuliah':
                $path = '<a href="' . base_url() . 'matakuliah/' . '" title="Matakuliah"><span class="">Matakuliah</span></a>';
                break;
            case 'mahasiswa':
                $path = '<a href="' . base_url() . 'mahasiswa/' . '" title="Mahasiswa"><span class="">Mahasiswa</span></a>';
                break;
            case 'kelas':
                $path = '<a href="' . base_url() . 'kelas/' . '" title="Kelas"><span class="">Kelas</span></a>';
                break;
            case 'aktif':
                $path = '<a href="' . base_url() . 'aktif/' . '" title="Plotting Perkuliahan Pagi"><span class="">Plotting Perkuliahan Pagi</span></a>';
                break;
            case 'aktif_sore':
                $path = '<a href="' . base_url() . 'aktif_sore/' . '" title="Plotting Perkuliahan Sore"><span class="">Plotting Perkuliahan Sore</span></a>';
                break;
            case 'prodi':
                $path = '<a href="' . base_url() . 'prodi/' . '" title="Program Studi"><span class="">Program Studi</span></a>';
                break;
            case 'schedule':
                $path = '<a href="' . base_url() . 'schedule/' . '" title="Program Studi"><span class="">Penjadwalan</span></a>';
                break;
            case 'dashboard':
                $path = '<a href="' . base_url() . 'dashboard' . '" title="Beranda"><span class="fa fa-home"></span></a>';
                break;
            case 'home':
                $path = '<a href="' . base_url() . 'dashboard' . '" title="Beranda"><span class="fa fa-home"></span></a>';
                break;
            default:
                $path = '<a href="' . base_url() . $location . '" title="Beranda"><span class="">' . $location . '</span></a>';
                break;
        }
        return $path;
    }

    public function tablemaker($heading, $data, $linkers, $num, $hstyle = null, $dstyle = null) {
        $table = "<table class='datatable'>";
        $hdstyle = "";
        $dtstyle = "";
        $i = 0;
        $table = $table . "<tr class='datatable-heading'>";
        foreach ($heading as $h) {
            if (isset($hstyle[$i])) {
                $hdstyle = "" . $hstyle[$i];
            }
            $table = $table . "<td style='" . $hdstyle . "' class='datatable-heading-column'>";
            $table = $table . $h;
            $table = $table . "</td>";
            $i++;
        }
        $table = $table . "</tr>";
        for ($y = 0; $y < $num; $y++) {
            if (($y % 2) == 0) {
                $table = $table . "<tr class='datatable-datarow'>";
            } else {
                $table = $table . "<tr class='datatable-datarow datatable-oddnumber'>";
            }
            for ($x = 0; $x < $i; $x++) {
                if (isset($dstyle[$x])) {
                    $dtstyle = "" . $dstyle[$x];
                }
                $table = $table . "<td style='" . $dtstyle . "' class='datatable-datarow-column'>";
                if (isset($linkers[$y][$x])) {
                    if ($linkers[$y][$x] != "") {
                        $table = $table . "<a class='datatable-link' href='" . $linkers[$y][$x] . "'>" . $data[$y][$x] . "</a>";
                    } else {
                        $table = $table . $data[$y][$x];
                    }
                } else {
                    $table = $table . $data[$y][$x];
                }
                $table = $table . "</td>";
            }
            $table = $table . "</tr>";
        }
        $table = $table . "</table>";
        return $table;
    }

    public function paging($page, $num, $max, $location, $marker) {
        $pgnm = (int) ($num / $max);
        $sisa = (int) ($num % $max);
        if ($sisa > 0) {
            $pgnm++;
        }
        $add = 0;
        $s1 = $page - 5;
        if ($s1 < 1) {
            $add = $s1 * (-1);
            $s1 = 1;
        }
        $s2 = $page + 5 + $add;
        $min = 0;
        if ($s2 > $pgnm) {
            $min = $s2 - $pgnm;
            $s2 = $pgnm;
        }
        $s1 = $s1 - $min;
        if ($s1 < 1) {
            $s1 = 1;
        }

        $result = '<div class="paging-section">';
        $link = str_replace($marker, "1", $location);
        $result = $result . '<div class="paging-nonaktif" style="margin-right: 25px;"><a href="' . $link . '"><span class="fa fa-angle-double-left"></span></a></div>';
        for ($i = $s1; $i <= $s2; $i++) {
            $link = str_replace($marker, $i, $location);
            if ($i != $page) {
                $result = $result . '<div class="paging-nonaktif"><a href="' . $link . '">' . $i . '</a></div>';
            } else {
                $result = $result . '<div class="paging-aktif">' . $i . '</div>';
            }
        }

        $link = str_replace($marker, $pgnm, $location);
        $result = $result . '<div class="paging-nonaktif" style="margin-left: 20px;"><a href="' . $link . '"><span class="fa fa-angle-double-right"></span></a></div>';
        $result = $result . '<div class="paging-section-close"></div>';
        $result = $result . '</div>';
        return $result;
    }

    public function searchbar($location, $addbtn = false) {
        $content = "";
        $content = $content . "<div style='text-align: right; width: 100%;padding-bottom: 5px;'>";
        $content = $content . "<script>";
        $content = $content . 'function searchclick(){
        var key = document.getElementById("search-key").value;
        key = key.split(" ").join("_");
        key = key.split(",").join("_");
        key = key.split("/").join("_");
        key = key.split("\\\\").join("_");
        ' . "key = key.split('" . '"' . "').join('_');" . '    
            key = key.split("' . "'" . '").join("_");
        window.location = "' . base_url() . $location . '/search/" + key;
            }';
        $content = $content . "</script>";
        $content = $content . "<input type='text' id='search-key'><a onclick='searchclick();' href='#search' style='color: #ffffff;' class='btn btn-primary search-btn'><span class='fa fa-search'></span></a>";
        if ($addbtn) {
            $content = $content . "&nbsp;&nbsp;<a href='" . base_url() . $location . "/add' style='color: #ffffff;' class='btn btn-success add-btn'><span class='fa fa-database'></span> Tambah Data</a>";
        }
        $content = $content . "</div>";
        return $content;
    }

    public function addbutton($location, $extra = "") {
        $content = "";
        $content = $content . "<div style='text-align: right; width: 100%;padding-bottom: 5px;'>";
        $content = $content . "&nbsp;&nbsp;<a href='" . base_url() . $location . "/add" . $extra . "' style='color: #ffffff;' class='btn btn-success add-btn'><span class='fa fa-database'></span> Tambah Data</a>";
        $content = $content . "</div>";
        return $content;
    }

    public function schedulertablemaker() {
        $table = "<table class='datatable'>";
        $i = 0;
        $table = $table . "<tr class='datatable-heading'>";
        $table = $table . "<td style='' class='datatable-heading-column'>No</td>";
        $table = $table . "<td style='' class='datatable-heading-column'>Prosess</td>";
        $table = $table . "<td class='datatable-heading-column'>Status</td>";
        $table = $table . "</tr>";
        $table = $table . "<tr class='datatable-datarow'>";
        $table = $table . "<td style='text-align: center;' class='datatable-datarow-column'>1</td>";
        $table = $table . "<td class='datatable-datarow-column'>Inisialisasi</td>";
        $table = $table . "<td id='stat-1' style='text-align: center; padding-bottom: 10px; padding-top: 10px;' class='datatable-datarow-column'>Pending</td>";
        $table = $table . "</tr>";
        $table = $table . "<tr class='datatable-datarow datatable-oddnumber'>";
        $table = $table . "<td style='text-align: center;' class='datatable-datarow-column'>2</td>";
        $table = $table . "<td class='datatable-datarow-column'>Perhitungan Prioritas Dosen</td>";
        $table = $table . "<td id='stat-2' style='text-align: center; padding-bottom: 10px; padding-top: 10px;' class='datatable-datarow-column'>Pending</td>";
        $table = $table . "</tr>";
        $table = $table . "<tr class='datatable-datarow'>";
        $table = $table . "<td style='text-align: center;' class='datatable-datarow-column'>3</td>";
        $table = $table . "<td class='datatable-datarow-column'>Penyusunan Jadwal</td>";
        $table = $table . "<td id='stat-3' style='text-align: center; padding-bottom: 10px; padding-top: 10px;' class='datatable-datarow-column'>Pending</td>";
        $table = $table . "</tr>";
        $table = $table . "<tr class='datatable-datarow datatable-oddnumber'>";
        $table = $table . "<td style='text-align: center;' class='datatable-datarow-column'>4</td>";
        $table = $table . "<td class='datatable-datarow-column'>Pengecekan Jadwal Bentrok</td>";
        $table = $table . "<td id='stat-4' style='text-align: center; padding-bottom: 10px; padding-top: 10px;' class='datatable-datarow-column'>Pending</td>";
        $table = $table . "</tr>";
        $table = $table . "<tr class='datatable-datarow'>";
        $table = $table . "<td style='text-align: center;' class='datatable-datarow-column'>5</td>";
        $table = $table . "<td class='datatable-datarow-column'>Finalisasi</td>";
        $table = $table . "<td id='stat-5' style='text-align: center; padding-bottom: 10px; padding-top: 10px;' class='datatable-datarow-column'>Pending</td>";
        $table = $table . "</tr>";
        $table = $table . "</table>";
        return $table;
    }

}

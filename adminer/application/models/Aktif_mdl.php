<?php

class Aktif_mdl extends CI_Model {

    public function get_status() {
        $QRY = "SELECT COUNT(aktif_id) AS jumlah FROM kelas_aktif";
        $query = $this->db->query($QRY);
        $row = $query->row();
        return $row->jumlah;
    }
    
    public function get_list($num = 10, $page = 1) {
        $start = ($page - 1) * $num;
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode = b.matakuliah_kode) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) LIMIT " . $start . ", " . $num;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    public function get_item($ids) {
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode = b.matakuliah_kode) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE a.aktif_id = '" . $ids . "'";
        $query = $this->db->query($QRY);
        return $query->row();
    }
    
    public function search($keyword, $maxdata) {
        $keyword = str_replace("_", "%", $keyword);
        $keyword = str_replace(" ", "%", $keyword);
        $keyword = str_replace("'", "%", $keyword);
        $QRY = "SELECT * FROM kelas_aktif a JOIN matakuliah b ON (a.matakuliah_kode = b.matakuliah_kode) JOIN dosen c ON (a.dosen_nip = c.dosen_nip) JOIN kelas d ON (a.kelas_id = d.kelas_id) WHERE (a.dosen_nip LIKE '%" . $keyword . "%' OR c.dosen_nama LIKE '%" . $keyword . "%' OR b.matakuliah_nama LIKE '%" . $keyword . "%' OR d.kelas_nama LIKE '%" . $keyword . "%') LIMIT 0, " . $maxdata;
        $query = $this->db->query($QRY);
        return $query->result();
    }
    
    private function doclean($text) {
        $text = str_replace("'", "\\'", $text);
        $text = str_replace('"', '\\"', $text);
        return $text;
    }
    
    public function add($prodi, $matakuliah_kode, $kelas_id, $dosen_nip) {
        $matakuliah_kode = $this->doclean($matakuliah_kode);
        $kelas_id = $this->doclean($kelas_id);
        $dosen_nip = $this->doclean($dosen_nip);
        $prodi = $this->doclean($prodi);
        
        $QRY = "INSERT INTO kelas_aktif(no_prodi, matakuliah_kode, kelas_id, dosen_nip) VALUES (";
        $QRY = $QRY . "'" . $prodi . "', ";
        $QRY = $QRY . "'" . $matakuliah_kode . "', ";
        $QRY = $QRY . "'" . $kelas_id . "', ";
        $QRY = $QRY . "'" . $dosen_nip . "'";
        $QRY = $QRY . ");";
        $this->db->query($QRY);
        return;
    }
    
    public function edit($ids, $prodi, $matakuliah_kode, $kelas_id, $dosen_nip) {
        $ids = $this->doclean($ids);
        $matakuliah_kode = $this->doclean($matakuliah_kode);
        $kelas_id = $this->doclean($kelas_id);
        $dosen_nip = $this->doclean($dosen_nip);
        $prodi = $this->doclean($prodi);
        
        $QRY = "UPDATE kelas_aktif SET ";
        $QRY = $QRY . "matakuliah_kode='" . $matakuliah_kode . "', ";
        $QRY = $QRY . "kelas_id='" . $kelas_id . "', ";
        $QRY = $QRY . "dosen_nip='" . $dosen_nip . "' ";
        $QRY = $QRY . "WHERE aktif_id ='" . $ids . "';";
        echo $QRY;
        $this->db->query($QRY);
        return;
    }
    
    public function delete($ids) {
        $ids = $this->doclean($ids);
        $QRY = "DELETE FROM kelas_aktif WHERE aktif_id = '" . $ids . "';";
        $this->db->query($QRY);
        return;
    }
    
}
